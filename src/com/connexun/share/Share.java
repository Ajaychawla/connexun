package com.connexun.share;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;

import com.connexun.main.NewsWebview;

public class Share {
	
	Context context;
	
	public Share(Intent intent,Context context,ImageView shareImAGE) {
		// TODO Auto-generated constructor stub
		//Intent intent = getIntent();
		this.context=context;
		String action = intent.getAction();
		String type = intent.getType();
		//String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		//Toast.makeText(HomeActivity.this, type+"--"+sharedText+"--"+action+"--"+intent, Toast.LENGTH_LONG).show();
		if (Intent.ACTION_SEND.equals(action) && type != null) {
			if ("text/plain".equals(type)) {
				handleSendText(intent); // Handle text being sent
			//	Toast.makeText(HomeActivity.this, type, Toast.LENGTH_LONG).show();
			} else if (type.startsWith("image/")) {
				 handleSendImage(intent,shareImAGE); // Handle single image being sent
			}
		} else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
			if (type.startsWith("image/")) {
				// handleSendMultipleImages(intent); // Handle multiple images
				// being sent
			}
		} else {
			// Handle other intents, such as being started from the home screen
		}
	}

	

	public void handleSendText(Intent intent) {
		String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		//Toast.makeText(HomeActivity.this, sharedText, Toast.LENGTH_LONG).show();
		if (sharedText != null) {
			// Update UI to reflect text being shared
			Intent intent_b = new Intent(context, NewsWebview.class);
			intent_b.putExtra("newsurl", sharedText);
			context.startActivity(intent_b);
		}
	}
	void handleSendImage(Intent intent,ImageView shareImAGE) {
	    Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
	    if (imageUri != null) {
	        // Update UI to reflect image being shared
	    	//shareImAGE.setImageURI(imageUri);
	    }
	}
}
