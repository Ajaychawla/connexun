package com.connexun.main;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.adapters.PoiAdapter;
import com.connexun.adapters.ViewPagerAdapterNew;
import com.connexun.adapters.ViewPagerControllers;
import com.connexun.adapters.ViewpageAddbanner;
import com.connexun.adapters.WeatherAdapter;
import com.connexun.interfaces.DataListners;
import com.connexun.recivers.MainGpsReciever;
import com.connexun.share.Share;
import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.AsyncImageLoader;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.GPSTracker;
import com.connexun.utils.GpsService;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;
import com.connexun.utils.ScreenSizeResolution;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.connexun.widgets.CreatViewCustom;
import com.connexun.widgets.EditLangCustom;
import com.connexun.widgets.NewsCustomList;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

@SuppressLint("NewApi")
public class HomeActivity extends Activity implements DataListners {

	SessionManager session;
	SessionManager_email session_email;

	LinearLayout linearNews, linearWeater_One, linearWeater_two,
			linearPoiLayout, linearWeater_inner_one, linearWeater_inner_two,
			linearPeopleArround;

	ArrayList<HashMap<String, String>> NEWSLIST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> WEATHERLIST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> PEOPLE_AROUND = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_POI = new ArrayList<HashMap<String, String>>();
	public ArrayList<HashMap<String, String>> GET_USERDETAILS = new ArrayList<HashMap<String, String>>();

	// TextView textPeopleAround, textPeopleaoundtext;
	// public ProgressBar newsProgress;

	public static HomeActivity sharedInstances;
	public ImageView account, imgCountry, imgInterestCountry;
	ImageLoader imgloader;
	ImageButton imgBtnlogorefresh, imgbtnMarker, imgbtnNews,
			imgbtnPeopleArround;
	TextView textMrakerCount, textNewsCount, textPeopleArroundCount;
	public static boolean IsEditProfileClickable = false;
	ScrollView scrollViewMain;
	public LinearLayout linearEditlanguagePage, linearEditUserdetail;
	String latitude_ = "", longitude_ = "";
	String CompleteURL = "";
	GPSTracker gps;
	public static String base64 = "";
	ImageView imgUserimage;
	ProgressBar newsProgress, PeoplearoundProgress, POIProgress;
	AlertDialogManager alert = new AlertDialogManager();

	// Services

	Intent myGpsService;
	MainGpsReciever gpsReciever;
	String GPS_FILTER = "MyGPSLocation";

	ImageView shareImAGE;

	ViewPager viewPager, viewPager1, viewPager2;
	PagerAdapter adapter, adapter1, adapter2;

	float i = 0, x = 0, j = 40, k = 40;

	ArrayList<HashMap<String, String>> GET_NEWS = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_NEWS1 = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_NEWS2 = new ArrayList<HashMap<String, String>>();
	LinearLayout buttons_viewPager, buttons_viewPager1, buttons_viewPager2;
	ViewPagerControllers vpController;
	RelativeLayout pagerRelativeOne, pagerRelativeTwo, pagerRelativeThree,
			LoaderRelativeFour;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		// initGpsListeners();
		shareImAGE = (ImageView) findViewById(R.id.shareImAGE);
		new Share(getIntent(), this, shareImAGE);

		session = new SessionManager(HomeActivity.this);
		session_email = new SessionManager_email(HomeActivity.this);
		// session.checkLogin();
		Log.e("session.isLoggedIn()", session.isLoggedIn() + "--");
		if (!session.isLoggedIn()) {
			session.checkLogin();
			finish();
			return;
		}
		new ConnectivityServer(HomeActivity.this).filesLoad();
		new JsonLocalFileFetch(HomeActivity.this).saveFile();
		new JsonLocalFileFetch(HomeActivity.this).getFile();
		imgloader = new ImageLoader(HomeActivity.this);

		linearNews = (LinearLayout) findViewById(R.id.linearNews);
		linearWeater_One = (LinearLayout) findViewById(R.id.linearWeater_One);
		linearWeater_two = (LinearLayout) findViewById(R.id.linearWeater_two);
		linearWeater_inner_one = (LinearLayout) findViewById(R.id.linearWeater_inner_one);
		linearWeater_inner_two = (LinearLayout) findViewById(R.id.linearWeater_inner_two);
		linearPoiLayout = (LinearLayout) findViewById(R.id.linearPoiLayout);
		linearPeopleArround = (LinearLayout) findViewById(R.id.linearPeopleArround);
		// textPeopleAround = (TextView) findViewById(R.id.textPeopleAround);
		// textPeopleaoundtext = (TextView)
		// findViewById(R.id.textPeopleaoundtext);
		// textPeopleaoundtext.setTypeface(getNormalFont());
		// newsProgress = (ProgressBar) findViewById(R.id.newsProgress);

		account = (ImageView) findViewById(R.id.account);
		imgCountry = (ImageView) findViewById(R.id.imgCountry);
		imgInterestCountry = (ImageView) findViewById(R.id.imgInterestCountry);

		imgBtnlogorefresh = (ImageButton) findViewById(R.id.imgBtnlogorefresh);
		imgbtnNews = (ImageButton) findViewById(R.id.imgbtnNews);
		imgbtnMarker = (ImageButton) findViewById(R.id.imgbtnMarker);
		imgbtnPeopleArround = (ImageButton) findViewById(R.id.imgbtnPeopleArround);

		textMrakerCount = (TextView) findViewById(R.id.textMrakerCount);
		textNewsCount = (TextView) findViewById(R.id.textNewsCount);
		textPeopleArroundCount = (TextView) findViewById(R.id.textPeopleArroundCount);

		scrollViewMain = (ScrollView) findViewById(R.id.scrollViewMain);
		linearEditlanguagePage = (LinearLayout) findViewById(R.id.linearEditlanguagePage);
		linearEditUserdetail = (LinearLayout) findViewById(R.id.linearEditUserdetail);

		newsProgress = (ProgressBar) findViewById(R.id.newsProgress);
		PeoplearoundProgress = (ProgressBar) findViewById(R.id.PeoplearoundProgress);
		POIProgress = (ProgressBar) findViewById(R.id.POIProgress);

		sharedInstances = HomeActivity.this;

		// radarLayout.addView(new CreatViewCustom(HomeActivity.this,
		// PEOPLE_AROUND));

		account.setEnabled(false);
		account.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Intent i = new Intent(HomeActivity.this,
				// EditIntLangActivity.class);
				// i.putExtra("GET_USERDETAILS", GET_USERDETAILS);
				// HomeActivity.this.startActivity(i);
				// if (newsProgress.getVisibility() == View.GONE) {
				linearEditUserdetail.removeAllViews();
				linearEditlanguagePage.removeAllViews();
				linearEditlanguagePage.setVisibility(View.VISIBLE);
				linearEditlanguagePage.addView(new EditLangCustom(
						HomeActivity.this, GET_USERDETAILS, HomeActivity.this)
						.languageChangeView());
				// }

			}
		});

		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}
		imgBtnlogorefresh.setOnClickListener(onClickNevICON);
		imgbtnMarker.setOnClickListener(onClickNevICON);
		imgbtnNews.setOnClickListener(onClickNevICON);
		imgbtnPeopleArround.setOnClickListener(onClickNevICON);
		// createSpinnerContent("");
		// gps = new GPSTracker(HomeActivity.this);
		GetLocations();
		// check if GPS enabled

		Log.e("Latlong", "?latitude=" + latitude_ + "&longitude=" + longitude_);
		// ?latitude=123123&longitude=123123
		// new NewsParse().execute(Constants.NEWS);
		// new ConnectivityServer(HomeActivity.this, "Userallinfo",
		// Constants.USERALLINFO + "" + CompleteURL, this);
		// new ConnectivityServer(HomeActivity.this, "Weather",
		// Constants.WEATHER_URL + "" + CompleteURL, this);
		// new ConnectivityServer(HomeActivity.this, "News", Constants.NEWS + ""
		// + CompleteURL + "?latitude=" + latitude_ + "&longitude="
		// + longitude_, this);
		// new ConnectivityServer(HomeActivity.this, "Pointofinterst",
		// Constants.POINTOFINTEREST + "" + CompleteURL + "?latitude="
		// + latitude_ + "&longitude=" + longitude_, this);
		// new ConnectivityServer(HomeActivity.this, "Peoplearound",
		// Constants.PEOPLEAROUND + "" + CompleteURL + "?latitude="
		// + latitude_ + "&longitude=" + longitude_, this);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String filter = extras.getString("filter");
			// Log.e("filter", filter + "--"+filter.subSequence(10,
			// filter.length()));
			if (filter != null)
				if (filter.contains("P")) {
					Intent intent = new Intent(HomeActivity.this,
							NewsWebview.class);
					intent.putExtra("newsurl",
							"http://connexun.com/en/Point_of_Interest/"
									+ filter.subSequence(1, filter.length()));
					startActivity(intent);
				}
		}

		// / New Swipe Add News
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager1 = (ViewPager) findViewById(R.id.pager1);
		viewPager2 = (ViewPager) findViewById(R.id.pager2);
		buttons_viewPager = (LinearLayout) findViewById(R.id.pagerBullets);
		buttons_viewPager1 = (LinearLayout) findViewById(R.id.pagerBullets1);
		buttons_viewPager2 = (LinearLayout) findViewById(R.id.pagerBullets2);
		pagerRelativeOne = (RelativeLayout) findViewById(R.id.pagerRelativeOne);
		pagerRelativeTwo = (RelativeLayout) findViewById(R.id.pagerRelativeTwo);
		pagerRelativeThree = (RelativeLayout) findViewById(R.id.pagerRelativeThree);
		LoaderRelativeFour = (RelativeLayout) findViewById(R.id.LoaderRelativeFour);

		vpController = new ViewPagerControllers();
		viewPager.setOnTouchListener(ontouchViewpagere);
		viewPager1.setOnTouchListener(ontouchViewpagere);
		viewPager2.setOnTouchListener(ontouchViewpagere);

		imgBtnlogorefresh.performClick();
		// viewPager.setOffscreenPageLimit(0);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				try {

					Log.d("arg0", arg0 + "--");
					vpController.getViewsByTag(buttons_viewPager, arg0 + "");
					// adapter.notifyDataSetChanged();
					RefreshSinglePager(viewPager, GET_NEWS);
					RefreshSinglePager(viewPager1, GET_NEWS1);
					RefreshSinglePager(viewPager2, GET_NEWS2);
					// buttons_viewPager.setVisibility(View.VISIBLE);
					visibleBulletes("");
				} catch (Exception e) {
					// TODO: handle exception
				}
				// adapter1.notifyDataSetChanged();
				// adapter2.notifyDataSetChanged();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				// buttons_viewPager

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		viewPager1
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

					@Override
					public void onPageSelected(int arg0) {
						// TODO Auto-generated method stub
						try {

							Log.d("arg0", arg0 + "--");
							vpController.getViewsByTag(buttons_viewPager1, arg0
									+ "");
							// adapter1.notifyDataSetChanged();
							RefreshSinglePager(viewPager, GET_NEWS);
							RefreshSinglePager(viewPager1, GET_NEWS1);
							RefreshSinglePager(viewPager2, GET_NEWS2);
							// buttons_viewPager1.setVisibility(View.VISIBLE);
							visibleBulletes("");
						} catch (Exception e) {
							// TODO: handle exception
						}
						// adapter.notifyDataSetChanged();
						// adapter2.notifyDataSetChanged();
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub
						// buttons_viewPager

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}
				});
		viewPager2
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

					@Override
					public void onPageSelected(int arg0) {
						// TODO Auto-generated method stub
						try {

							Log.d("arg0", arg0 + "--");
							vpController.getViewsByTag(buttons_viewPager2, arg0
									+ "");
							// adapter2.notifyDataSetChanged();
							RefreshSinglePager(viewPager, GET_NEWS);
							RefreshSinglePager(viewPager1, GET_NEWS1);
							RefreshSinglePager(viewPager2, GET_NEWS2);
							// buttons_viewPager2.setVisibility(View.VISIBLE);
							visibleBulletes("");
						} catch (Exception e) {
							// TODO: handle exception
						}
						// adapter.notifyDataSetChanged();
						// adapter1.notifyDataSetChanged();
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub
						// buttons_viewPager

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}
				});

	}

	public void refreshAllPagers(String pager) {

		// adapter1.notifyDataSetChanged();
		if (pager.equals("0")) {
			if (GET_NEWS2.size() > 0) {
				// adapter2.notifyDataSetChanged();
				RefreshSinglePager(viewPager2, GET_NEWS2);
				buttons_viewPager2.setVisibility(View.VISIBLE);
			}
			if (GET_NEWS1.size() > 0) {
				// adapter1.notifyDataSetChanged();
				RefreshSinglePager(viewPager1, GET_NEWS1);
				buttons_viewPager1.setVisibility(View.VISIBLE);
			}
		} else if (pager.equals("1")) {
			if (GET_NEWS2.size() > 0) {
				// adapter2.notifyDataSetChanged();
				RefreshSinglePager(viewPager2, GET_NEWS2);
				buttons_viewPager2.setVisibility(View.VISIBLE);
			}
			if (GET_NEWS.size() > 0) {
				// adapter.notifyDataSetChanged();
				RefreshSinglePager(viewPager, GET_NEWS);
				buttons_viewPager.setVisibility(View.VISIBLE);
			}
		} else if (pager.equals("2")) {
			if (GET_NEWS.size() > 0) {
				// adapter.notifyDataSetChanged();
				RefreshSinglePager(viewPager, GET_NEWS);
				buttons_viewPager.setVisibility(View.VISIBLE);
			}
			if (GET_NEWS1.size() > 0) {
				// adapter1.notifyDataSetChanged();
				RefreshSinglePager(viewPager1, GET_NEWS1);
				buttons_viewPager1.setVisibility(View.VISIBLE);
			}
		}

		if (pager.equals("0") && GET_NEWS.size() > 0)
			buttons_viewPager.setVisibility(View.INVISIBLE);
		else if (pager.equals("1") && GET_NEWS1.size() > 0)
			buttons_viewPager1.setVisibility(View.INVISIBLE);
		else if (pager.equals("2") && GET_NEWS2.size() > 0)
			buttons_viewPager2.setVisibility(View.INVISIBLE);

		// else
		// buttons_viewPager.setVisibility(View.VISIBLE);
	}

	public void visibleBulletes(String pager) {
		// if (pager.equals("0check") && GET_NEWS.size() > 0)
		buttons_viewPager.setVisibility(View.VISIBLE);
		// else if (pager.equals("1check") && GET_NEWS1.size() > 0)
		buttons_viewPager1.setVisibility(View.VISIBLE);
		// else if (pager.equals("2check") && GET_NEWS2.size() > 0)
		buttons_viewPager2.setVisibility(View.VISIBLE);
	}

	public void RefreshSinglePager(ViewPager viewpager,
			ArrayList<HashMap<String, String>> GET_NEWS) {
		for (int i = 0; i < GET_NEWS.size(); i++) {

			View view = (View) viewpager.findViewWithTag(i + "check");

			if (view != null) {
				// if (i == position)
				// view.setVisibility(View.VISIBLE);
				// else
				view.setVisibility(View.GONE);
			}
		}
		// Log.e("viewpager.getChildCount()", viewpager.getChildCount()+"--");
		// Toast.makeText(HomeActivity.this, viewpager.getChildCount()+"--"+
		// GET_NEWS.size(), Toast.LENGTH_LONG).show();
		// for (int i = 0; i < viewpager.getChildCount(); i++) {
		// if (position == i)
		// ;
		// else {
		// // View view = viewpager.getChildAt(i);
		// // RelativeLayout relativeNewsUppder = (RelativeLayout) view
		// // .findViewById(R.id.relativeNewsUppder);
		// // relativeNewsUppder.setVisibility(View.GONE);
		// }
		//
		// }
	}

	@Override
	public void GetNewsListners(String NEWS, View view) {
		// TODO Auto-generated method stub
		// Toast.makeText(HomeActivity.this, NEWS, Toast.LENGTH_LONG).show();
		// Log.e("News", NEWS + "--");
		try {

			newsProgress.setVisibility(View.GONE);
			GET_NEWS = new ArrayList<HashMap<String, String>>();
			GET_NEWS1 = new ArrayList<HashMap<String, String>>();
			GET_NEWS2 = new ArrayList<HashMap<String, String>>();
			createSpinnerNews(NEWS);

			if (GET_NEWS.size() > 0 || GET_NEWS.size() > 0
					|| GET_NEWS.size() > 0) {

			} else {
				NEWS = new JsonLocalFileFetch(HomeActivity.this)
						.ReadFile(Constants.FolderPath + Constants.NewsFileName);
				createSpinnerNews(NEWS);
			}

			if (GET_NEWS.size() > 0 || GET_NEWS.size() > 0
					|| GET_NEWS.size() > 0) {
				LoaderRelativeFour.removeAllViews();
				// if (view != null) {
				// //Get the real LinearLayout which is holding your View
				// ViewGroup parent = (ViewGroup) view.getParent();
				// //check if it is null
				// if (parent != null) {
				// //remove your view
				// parent.removeView(view);
				// }
				// }
				pagerRelativeOne.setVisibility(View.VISIBLE);
				pagerRelativeTwo.setVisibility(View.VISIBLE);
				pagerRelativeThree.setVisibility(View.VISIBLE);

				viewPager.removeAllViewsInLayout();
				viewPager1.removeAllViewsInLayout();
				viewPager2.removeAllViewsInLayout();
				buttons_viewPager.removeAllViews();
				buttons_viewPager1.removeAllViews();
				buttons_viewPager2.removeAllViews();
				// Binds the Adapter to the ViewPager
				if (GET_NEWS.size() > 0) {
					adapter = new ViewPagerAdapterNew(HomeActivity.this,
							GET_NEWS, true, "0");
					viewPager.setAdapter(adapter);
				} else {
					pagerRelativeOne.setVisibility(View.GONE);
				}
				if (GET_NEWS1.size() > 0) {
					adapter1 = new ViewPagerAdapterNew(HomeActivity.this,
							GET_NEWS1, false, "1");
					viewPager1.setAdapter(adapter1);

					// adapter1 = new ViewpageAddbanner(HomeActivity.this,
					// GET_NEWS1,
					// false);
					// viewPager1.setAdapter(adapter1);
				} else {
					pagerRelativeTwo.setVisibility(View.GONE);
				}
				if (GET_NEWS2.size() > 0) {
					adapter2 = new ViewPagerAdapterNew(HomeActivity.this,
							GET_NEWS2, false, "2");
					viewPager2.setAdapter(adapter2);
				} else {
					pagerRelativeThree.setVisibility(View.GONE);
				}
				vpController.CreateCustomButtons(buttons_viewPager,
						GET_NEWS.size(), viewPager, HomeActivity.this);
				vpController.CreateCustomButtons(buttons_viewPager1,
						GET_NEWS1.size(), viewPager1, HomeActivity.this);
				vpController.CreateCustomButtons(buttons_viewPager2,
						GET_NEWS2.size(), viewPager2, HomeActivity.this);
				new JsonLocalFileFetch(HomeActivity.this).WriteFile(NEWS,
						Constants.FolderPath + Constants.NewsFileName);
			} else {
				((ProgressBar) view.findViewById(R.id.loaderProgress))
						.setVisibility(View.GONE);
				TextView loaderText = (TextView) view
						.findViewById(R.id.loaderText);
				loaderText.setVisibility(View.VISIBLE);
				loaderText.setText(HomeActivity.this.getResources().getString(
						R.string.Alert_NoNews_text));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		// textNewsCount.setVisibility(View.VISIBLE);
		// NEWSLIST = new ConnectivityServer().createSpinnerNews(NEWS);
		// if (NEWSLIST.size() > 0) {
		// view.setVisibility(View.GONE);
		// AddNewsInLinear(-1);
		// textNewsCount.setText(NEWSLIST.size() + "");
		// new JsonLocalFileFetch(HomeActivity.this).WriteFile(NEWS,
		// Constants.FolderPath + Constants.NewsFileName);
		// } else {
		// NEWSLIST = new ConnectivityServer()
		// .createSpinnerNews(new JsonLocalFileFetch(HomeActivity.this)
		// .ReadFile(Constants.FolderPath
		// + Constants.NewsFileName));
		// if (NEWSLIST.size() > 0) {
		// view.setVisibility(View.GONE);
		// AddNewsInLinear(-1);
		// textNewsCount.setText(NEWSLIST.size() + "");
		// // new JsonLocalFileFetch(HomeActivity.this).writeToFile(NEWS,
		// // "json/news_test.json");
		// } else {
		// ((ProgressBar) view.findViewById(R.id.loaderProgress))
		// .setVisibility(View.GONE);
		// TextView loaderText = (TextView) view
		// .findViewById(R.id.loaderText);
		// loaderText.setVisibility(View.VISIBLE);
		// loaderText.setText(HomeActivity.this.getResources().getString(
		// R.string.Alert_NoNews_text));
		// }
		// textNewsCount.setText(NEWSLIST.size() + "");
		// }
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String filter = extras.getString("filter");
			Log.e("filter", filter + "--");
			if (filter != null)
				if (filter.equalsIgnoreCase("1"))
					imgbtnNews.performClick();
		}
	}

	OnTouchListener ontouchViewpagere = new View.OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				i = event.getY();
				x = event.getX();
				// Here u can write code which is executed after the user touch
				// on the screen
				// v.getParent().requestDisallowInterceptTouchEvent(true);
				break;
			}
			case MotionEvent.ACTION_UP: {
				// Here u can write code which is executed after the user
				// release the touch on the screen
				v.getParent().requestDisallowInterceptTouchEvent(false);
				break;
			}
			case MotionEvent.ACTION_MOVE: {
				Log.d("event.getY()", event.getY() + "--" + i);

				if ((event.getX() > x + k || event.getX() < x - k)) {
					v.getParent().requestDisallowInterceptTouchEvent(true);
				} else if ((event.getY() > i + j || event.getY() < i - j)) {
					v.getParent().requestDisallowInterceptTouchEvent(false);
				}

				// Here u can write code which is executed when user move the
				// finger on the screen
				break;
			}
			}

			return false;
		}
	};

	@Override
	public void GetPeopleAroundListners(String PeopleAround, View view) {
		// TODO Auto-generated method stub
		try {

			// Log.e("PeopleAround", PeopleAround + "--");
			PeoplearoundProgress.setVisibility(View.GONE);
			textPeopleArroundCount.setVisibility(View.VISIBLE);

			PEOPLE_AROUND = new ConnectivityServer()
					.createSpinnerPeopleAround(PeopleAround);
			if (PEOPLE_AROUND.size() > 0) {
				view.setVisibility(View.GONE);
				linearPeopleArround.setVisibility(View.VISIBLE);
				linearPeopleArround.removeAllViews();
				CreatViewCustom custem = new CreatViewCustom(HomeActivity.this,
						PEOPLE_AROUND);
				linearPeopleArround.addView(custem);
				// textPeopleAround.setText(custem.getPeopleCount());
				textPeopleArroundCount.setText(PEOPLE_AROUND.size() + "");
				new JsonLocalFileFetch(HomeActivity.this).WriteFile(
						PeopleAround, Constants.FolderPath
								+ Constants.PeopleAroundFileName);
			} else {
				PEOPLE_AROUND = new ConnectivityServer()
						.createSpinnerPeopleAround(new JsonLocalFileFetch(
								HomeActivity.this)
								.ReadFile(Constants.FolderPath
										+ Constants.PeopleAroundFileName));
				if (PEOPLE_AROUND.size() > 0) {
					view.setVisibility(View.GONE);
					linearPeopleArround.setVisibility(View.VISIBLE);
					linearPeopleArround.removeAllViews();
					CreatViewCustom custem = new CreatViewCustom(
							HomeActivity.this, PEOPLE_AROUND);
					linearPeopleArround.addView(custem);
					// textPeopleAround.setText(custem.getPeopleCount());
					textPeopleArroundCount.setText(PEOPLE_AROUND.size() + "");
				} else {
					((ProgressBar) view.findViewById(R.id.loaderProgress))
							.setVisibility(View.GONE);
					TextView loaderText = (TextView) view
							.findViewById(R.id.loaderText);
					loaderText.setVisibility(View.VISIBLE);
					loaderText.setText(HomeActivity.this.getResources()
							.getString(R.string.Alert_NoPeople_text));

					// radarLayout.removeAllViews();
					// CreatViewCustom custem = new
					// CreatViewCustom(HomeActivity.this,
					// PEOPLE_AROUND);
					// radarLayout.addView(custem);
					// textPeopleAround.setText("0 "
					// + getResources().getString(R.string.PA_poeple)
					// + " < 30km\n0 "
					// + getResources().getString(R.string.PA_poeple)
					// + " < 50km");
					textPeopleArroundCount.setText(PEOPLE_AROUND.size() + "");
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String filter = extras.getString("filter");
			Log.e("filter", filter + "--");
			if (filter != null)
				if (filter.equalsIgnoreCase("3"))
					imgbtnPeopleArround.performClick();

		}
	}

	@Override
	public void GetPointOfIntrestListners(String PointOfInterest, View view) {
		// TODO Auto-generated method stub
		// Log.e("PointOfInterest", new
		// ConnectivityServer().createSpinnerPOI(PointOfInterest) + "--");
		// Log.e("PointOfInterest",
		// new ConnectivityServer()
		// .createSpinnerPOI(new JsonLocalFileFetch(
		// HomeActivity.this)
		// .loadJSONFromAsset("json/poi.json"))
		// + "--");
		// GET_POI = new ConnectivityServer()
		// .createSpinnerPOI(new JsonLocalFileFetch(HomeActivity.this)

		// Toast.makeText(HomeActivity.this, PointOfInterest + "-----",
		// Toast.LENGTH_LONG).show();
		// .loadJSONFromAsset("json/poi.json"));
		try {

			POIProgress.setVisibility(View.GONE);
			textMrakerCount.setVisibility(View.VISIBLE);

			GET_POI = new ConnectivityServer()
					.createSpinnerPOI(PointOfInterest);
			if (GET_POI.size() > 0) {
				// Log.e("GETPOI_SIZe", GET_POI.size() + "----");
				view.setVisibility(View.GONE);
				linearPoiLayout.removeAllViews();
				// new ConnectivityServer().createSpinnerPOI(new
				// JsonLocalFileFetch(HomeActivity.this).loadJSONFromAsset("json/poi.json"))
				linearPoiLayout.addView(new PoiAdapter(HomeActivity.this,
						GET_POI).getView(0));
				textMrakerCount.setText(GET_POI.size() + "");
				new JsonLocalFileFetch(HomeActivity.this).WriteFile(
						PointOfInterest, Constants.FolderPath
								+ Constants.POIFileName);
			} else {
				GET_POI = new ConnectivityServer()
						.createSpinnerPOI(new JsonLocalFileFetch(
								HomeActivity.this)
								.ReadFile(Constants.FolderPath
										+ Constants.POIFileName));
				if (GET_POI.size() > 0) {
					view.setVisibility(View.GONE);
					// Log.e("GETPOI_SIZe", GET_POI.size() + "----");
					linearPoiLayout.removeAllViews();
					// new ConnectivityServer().createSpinnerPOI(new
					// JsonLocalFileFetch(HomeActivity.this).loadJSONFromAsset("json/poi.json"))
					linearPoiLayout.addView(new PoiAdapter(HomeActivity.this,
							GET_POI).getView(0));
					textMrakerCount.setText(GET_POI.size() + "");
				} else {
					((ProgressBar) view.findViewById(R.id.loaderProgress))
							.setVisibility(View.GONE);
					TextView loaderText = (TextView) view
							.findViewById(R.id.loaderText);
					loaderText.setVisibility(View.VISIBLE);
					loaderText.setText(HomeActivity.this.getResources()
							.getString(R.string.Alert_NoPoi_text));
					// linearPoiLayout.addView(new PoiAdapter(HomeActivity.this,
					// GET_POI)
					// .getView(0));
					textMrakerCount.setText(GET_POI.size() + "");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String filter = extras.getString("filter");
			Log.e("filter", filter + "--");
			if (filter != null)
				if (filter.equalsIgnoreCase("2"))
					imgbtnMarker.performClick();

		}

	}

	// @Override
	// public void WeathListner(Document doc) {
	// // TODO Auto-generated method stub
	// if (doc != null)
	// if (new ConnectivityWeather().createSpinnerWeather(doc) != null)
	// WEATHERLIST.add(new ConnectivityWeather()
	// .createSpinnerWeather(doc));
	// }

	@Override
	public void GetUserAllInfo(String GetAllInfo, View view) {
		// TODO Auto-generated method stub
		try {

			if (new ConnectivityServer().CheckExpirationTocken(
					HomeActivity.this, GetAllInfo)) {
				return;
			}

			GET_USERDETAILS = new ConnectivityServer()
					.createArryListforUserAllInfo(GetAllInfo);
			if (GET_USERDETAILS.size() > 0) {
				IsEditProfileClickable = true;
				account.setEnabled(true);
				// if(!GET_USERDETAILS.get(0).get(Constants.USER_AVATAR)
				// .toString().equals("")){
				imgloader.DisplayImage(
						GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
								.toString(), R.drawable.default_avatar_single,
						account, 100);
				// account.setImageBitmap(imgloader
				// .ConvertBase64ToBitmap(GET_USERDETAILS.get(0)
				// .get(Constants.USER_AVATAR).toString()));
				session.createLoginSession(
						session.getUserDetails().get(SessionManager.KEY_USERID),
						session.getUserDetails().get(
								SessionManager.KEY_USERTOKEN), GET_USERDETAILS
								.get(0).get(Constants.USER_EXPERT).toString());
//				if (!GET_USERDETAILS
//						.get(0)
//						.get(Constants.USER_LANGUAGE)
//						.toString()
//						.equals(session_email.getUserDetails().get(
//								SessionManager_email.KEY_LANGUAGE))) {
//					session_email.createLoginSession(
//							session_email.getUserDetails().get(
//									SessionManager_email.KEY_EMAIL),
//							GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE)
//									.toString());
//
//					new ChangeLanguage(HomeActivity.this, session_email
//							.getUserDetails()
//							.get(SessionManager_email.KEY_LANGUAGE).toString());
//
//					Intent intent = new Intent(HomeActivity.this,
//							HomeActivity.class);
//					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					startActivity(intent);
//					return;
//				}

				// }else{
				// imgloader.DisplayImage(
				// GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
				// .toString(), R.drawable.user_avatar_default,
				// account, 100);
				// account.setImageResource(R.drawable.user_avatar_default);
				// }
				Uri otherPath_1 = Uri
						.parse("android.resource://com.connexun.main/drawable/"
								+ GET_USERDETAILS.get(0).get(
										Constants.USER_HOMECOUNT));

				imgCountry.setImageURI(otherPath_1);
				Uri otherPath_2 = Uri
						.parse("android.resource://com.connexun.main/drawable/"
								+ GET_USERDETAILS.get(0).get(
										Constants.USER_INTERESTCOUNT));

				imgInterestCountry.setImageURI(otherPath_2);
				new JsonLocalFileFetch(HomeActivity.this).WriteFile(GetAllInfo,
						Constants.FolderPath + Constants.UserInfoFileName);
			} else {

				if (!new ConnectionDetector(HomeActivity.this)
						.isConnectingToInternet()) {
					try {

						alert.showAlertDialog(
								HomeActivity.this,
								HomeActivity.this.getResources().getString(
										R.string.Alert_Internet_connection)
										+ "", HomeActivity.this.getResources()
										.getString(R.string.Alert_Internet)
										+ "", false);
					} catch (Exception e) {
						// TODO: handle exception
					}
				} else {
					Toast.makeText(
							HomeActivity.this,
							HomeActivity.this.getResources().getString(
									R.string.Alert_ServerError)
									+ "", Toast.LENGTH_LONG).show();
				}

				GET_USERDETAILS = new ConnectivityServer()
						.createArryListforUserAllInfo(new JsonLocalFileFetch(
								HomeActivity.this)
								.ReadFile(Constants.FolderPath
										+ Constants.UserInfoFileName));
				if (GET_USERDETAILS.size() > 0) {
					IsEditProfileClickable = true;
					account.setEnabled(true);
					// if(!GET_USERDETAILS.get(0).get(Constants.USER_AVATAR)
					// .toString().equals("")){
					imgloader.DisplayImage(
							GET_USERDETAILS.get(0)
									.get(Constants.USER_IMAGENAME).toString(),
							R.drawable.default_avatar_single, account, 100);
					// account.setImageBitmap(imgloader
					// .ConvertBase64ToBitmap(GET_USERDETAILS.get(0)
					// .get(Constants.USER_AVATAR).toString()));
					session.createLoginSession(
							session.getUserDetails().get(
									SessionManager.KEY_USERID),
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN),
							GET_USERDETAILS.get(0).get(Constants.USER_EXPERT)
									.toString());

					session_email.createLoginSession(
							session_email.getUserDetails().get(
									SessionManager_email.KEY_EMAIL),
							GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE)
									.toString());
					// }else{
					// imgloader.DisplayImage(
					// GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
					// .toString(), R.drawable.user_avatar_default,
					// account, 100);
					// account.setImageResource(R.drawable.user_avatar_default);
					// }
					Uri otherPath_1 = Uri
							.parse("android.resource://com.connexun.main/drawable/"
									+ GET_USERDETAILS.get(0).get(
											Constants.USER_HOMECOUNT));

					imgCountry.setImageURI(otherPath_1);
					Uri otherPath_2 = Uri
							.parse("android.resource://com.connexun.main/drawable/"
									+ GET_USERDETAILS.get(0).get(
											Constants.USER_INTERESTCOUNT));

					imgInterestCountry.setImageURI(otherPath_2);
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String filter = extras.getString("filter");
			Log.e("filter", filter + "--");
			if (filter != null) {
				if (filter.equalsIgnoreCase("EL"))
					account.performClick();
				else if (filter.equalsIgnoreCase("ED")) {
					StartEditProfileActivit(GET_USERDETAILS);
				}
			}

		}

	}

	OnClickListener onClickNevICON = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.imgbtnMarker:
				scrollViewMain.smoothScrollTo(0, (int) linearPoiLayout.getY());
				linearEditlanguagePage.removeAllViews();
				linearEditUserdetail.removeAllViews();

				break;
			case R.id.imgbtnNews:
				scrollViewMain.smoothScrollTo(0, (int) linearNews.getY());
				linearEditlanguagePage.removeAllViews();
				linearEditUserdetail.removeAllViews();
				break;
			case R.id.imgbtnPeopleArround:
				// scrollViewMain.smoothScrollTo(0, (int)
				// linearPeopleArround.getY());
				scrollViewMain.fullScroll(View.FOCUS_DOWN);
				linearEditlanguagePage.removeAllViews();
				linearEditUserdetail.removeAllViews();
				break;
			case R.id.imgBtnlogorefresh:
				// scrollViewMain.smoothScrollTo(0, (int) radarLayout.getY());
				// Intent in = new Intent(HomeActivity.this,
				// HomeActivity.class);
				// in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// startActivity(in);
				// if (linearPeopleArround.getVisibility() == View.VISIBLE) {
				// if (newsProgress.getVisibility() == View.GONE) {

				GetLocations();
				try {
					// imgloader.clearSingleCache(GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
					// .toString());
					CompleteURL = session.getUserDetails().get(
							SessionManager.KEY_USERID)
							+ "/"
							+ URLEncoder.encode(
									session.getUserDetails().get(
											SessionManager.KEY_USERTOKEN),
									"UTF-8");
				} catch (Exception e) {
					// TODO: handle exception
				}

				newsProgress.setVisibility(View.VISIBLE);
				PeoplearoundProgress.setVisibility(View.VISIBLE);
				POIProgress.setVisibility(View.VISIBLE);

				textNewsCount.setVisibility(View.GONE);
				textPeopleArroundCount.setVisibility(View.GONE);
				textMrakerCount.setVisibility(View.GONE);

				textMrakerCount.setText("0");
				textNewsCount.setText("0");
				textPeopleArroundCount.setText("0");
				linearPeopleArround.removeAllViews();
				linearWeater_two.removeAllViews();
				// linearNews.removeAllViews();
				linearPoiLayout.removeAllViews();
				linearWeater_inner_one.removeAllViews();
				linearWeater_inner_two.removeAllViews();
				linearPeopleArround.setVisibility(View.VISIBLE);
				linearEditlanguagePage.setVisibility(View.GONE);
				linearEditUserdetail.setVisibility(View.GONE);

				new ConnectivityServer(HomeActivity.this, "Userallinfo",
						Constants.USERALLINFO + "" + CompleteURL,
						HomeActivity.this, null);

				// Weather
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View row = inflater.inflate(R.layout.loader_view, null, false);
				TextView textTitle = (TextView) row
						.findViewById(R.id.textTitle);
				textTitle.setText(HomeActivity.this.getResources().getString(
						R.string.Weather_text));
				linearWeater_two.addView(row);
				new ConnectivityServer(HomeActivity.this, "Weather",
						Constants.WEATHER_URL + "" + CompleteURL + "?lat="
								+ latitude_ + "&lon=" + longitude_,
						HomeActivity.this, row);

				// NEWS
				row = inflater.inflate(R.layout.loader_view, null, false);
				textTitle = (TextView) row.findViewById(R.id.textTitle);
				textTitle.setText(HomeActivity.this.getResources().getString(
						R.string.News_news)
						+ "");

				pagerRelativeOne.setVisibility(View.GONE);
				pagerRelativeTwo.setVisibility(View.GONE);
				pagerRelativeThree.setVisibility(View.GONE);
				viewPager.removeAllViewsInLayout();
				viewPager1.removeAllViewsInLayout();
				viewPager2.removeAllViewsInLayout();
				buttons_viewPager.removeAllViews();
				buttons_viewPager1.removeAllViews();
				buttons_viewPager2.removeAllViews();
				LoaderRelativeFour.removeAllViews();
				LoaderRelativeFour.addView(row);
				buttons_viewPager.setVisibility(View.VISIBLE);
				buttons_viewPager1.setVisibility(View.VISIBLE);
				buttons_viewPager2.setVisibility(View.VISIBLE);
				// new ConnectivityServer(HomeActivity.this, "News",
				// Constants.NEWS + "" + CompleteURL + "?latitude="
				// + latitude_ + "&longitude=" + longitude_,
				// HomeActivity.this, row);
				new ConnectivityServer(HomeActivity.this, "News",
						Constants.NEW_NEWSURL + "" + CompleteURL,
						HomeActivity.this, row);
				// POI
				row = inflater.inflate(R.layout.loader_view, null, false);
				textTitle = (TextView) row.findViewById(R.id.textTitle);
				textTitle.setText(HomeActivity.this.getResources().getString(
						R.string.POI_nearbypoint)
						+ "");
				linearPoiLayout.addView(row);
				new ConnectivityServer(HomeActivity.this, "Pointofinterst",
						Constants.POINTOFINTEREST + "" + CompleteURL
								+ "?latitude=" + latitude_ + "&longitude="
								+ longitude_ + "&dev="
								+ Constants.GCM_REGISTRATION_ID,
						HomeActivity.this, row);

				// People Around
				row = inflater.inflate(R.layout.loader_view, null, false);
				textTitle = (TextView) row.findViewById(R.id.textTitle);
				textTitle.setText(HomeActivity.this.getResources().getString(
						R.string.PA_peoplearound)
						+ "");
				linearPeopleArround.addView(row);
				new ConnectivityServer(HomeActivity.this, "Peoplearound",
						Constants.PEOPLEAROUND + "" + CompleteURL
								+ "?latitude=" + latitude_ + "&longitude="
								+ longitude_, HomeActivity.this, row);
				// }
				// }
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void GetWeatherDetail(String GetWeatherDetail, View view) {
		// TODO Auto-generated method stub
		try {

			WEATHERLIST = new ConnectivityServer()
					.createWeatherSpinner(GetWeatherDetail);
			if (WEATHERLIST.size() > 0) {
				// linearWeater_One.removeViewAt(3);
				// linearWeater_One.removeView(view);
				view.setVisibility(View.GONE);

				linearWeater_inner_one.removeAllViews();
				linearWeater_inner_two.removeAllViews();
				linearWeater_inner_one.addView(new WeatherAdapter(
						HomeActivity.this, WEATHERLIST, "home").getView(0));
				linearWeater_inner_two.addView(new WeatherAdapter(
						HomeActivity.this, WEATHERLIST, "interesthome")
						.getView(0));
				new JsonLocalFileFetch(HomeActivity.this).WriteFile(
						GetWeatherDetail, Constants.FolderPath
								+ Constants.WeatherFileName);
			} else {
				WEATHERLIST = new ConnectivityServer()
						.createWeatherSpinner(new JsonLocalFileFetch(
								HomeActivity.this)
								.ReadFile(Constants.FolderPath
										+ Constants.WeatherFileName));
				if (WEATHERLIST.size() > 0) {
					// linearWeater_One.removeView(view);
					view.setVisibility(View.GONE);
					linearWeater_inner_one.removeAllViews();
					linearWeater_inner_two.removeAllViews();
					linearWeater_inner_one.addView(new WeatherAdapter(
							HomeActivity.this, WEATHERLIST, "home").getView(0));
					linearWeater_inner_two.addView(new WeatherAdapter(
							HomeActivity.this, WEATHERLIST, "interesthome")
							.getView(0));
				} else {
					((ProgressBar) view.findViewById(R.id.loaderProgress))
							.setVisibility(View.GONE);
					TextView loaderText = (TextView) view
							.findViewById(R.id.loaderText);
					loaderText.setVisibility(View.VISIBLE);
					loaderText.setText(HomeActivity.this.getResources()
							.getString(R.string.Alert_NoWeather_text));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if (linearEditlanguagePage.getVisibility() == View.VISIBLE) {
			linearEditlanguagePage.removeAllViews();
			linearEditlanguagePage.setVisibility(View.GONE);
		} else if (linearEditUserdetail.getVisibility() == View.VISIBLE) {
			linearEditUserdetail.removeAllViews();
			linearEditUserdetail.setVisibility(View.GONE);
		} else
			super.onBackPressed();
	}

	public void AddNewsInLinear(int point) {
		linearNews.removeAllViews();
		for (int i = 0; i < 3; i++) {
			// if (i == point) {
			// linearNews.addView(new NewsAdapter(HomeActivity.this, NEWSLIST,
			// point).getView(i));
			// }
			// // View a;
			// else
			// linearNews.addView(new NewsAdapter(HomeActivity.this, NEWSLIST,
			// -1).getView(i));
			// boolean tabletSize =
			// getResources().getBoolean(R.bool.screen_size);
			// if (tabletSize) {
			// linearNews.addView(new
			// NewsCustomList(HomeActivity.this).NewsDoubleView(NEWSLIST, i,
			// point,2));
			// // do something
			// } else {
			// linearNews.addView(new
			// NewsCustomList(HomeActivity.this).NewsDoubleView(NEWSLIST, i,
			// point,1));
			// // do something else
			// }

			int getScreenSize = new ScreenSizeResolution()
					.getScreenSizeResolution(HomeActivity.this);
			if (getScreenSize == 720) {
				// Device is a 10" tablet
				linearNews.addView(new NewsCustomList(HomeActivity.this)
						.NewsDoubleView(NEWSLIST, i, point, 3, linearNews));
			} else if (getScreenSize == 600) {
				// Device is a 7" tablet
				linearNews.addView(new NewsCustomList(HomeActivity.this)
						.NewsDoubleView(NEWSLIST, i, point, 2, linearNews));
			} else {
				linearNews.addView(new NewsCustomList(HomeActivity.this)
						.NewsDoubleView(NEWSLIST, i, point, 1, linearNews));
			}
		}
	}

	private Typeface getNormalFont() {
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		return normalFont;

		// textView.setTypeface(getNormalFont());
	}

	public void StartEditProfileActivit(
			ArrayList<HashMap<String, String>> GET_USERDETAILS_) {
		Intent i = new Intent(HomeActivity.this, EditProfileActivity.class);
		i.putExtra("GET_USERDETAILS", GET_USERDETAILS_);
		startActivityForResult(i, 1);
	}

	public void selectImage(ImageView imgUserimage_) {
		imgUserimage = imgUserimage_;
		final CharSequence[] options = getResources().getStringArray(
				R.array.EP_addphoto_itmes);

		AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
		builder.setTitle(getResources().getString(R.string.EP_addphoto));
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File f = new File(android.os.Environment
							.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, 2);
				} else if (item == 1) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(intent, 3);

				} else if (item == 2) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1) {
				imgBtnlogorefresh.performClick();
			} else if (requestCode == 2) {
				File f = new File(Environment.getExternalStorageDirectory()
						.toString());
				for (File temp : f.listFiles()) {
					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {
					Bitmap bitmap = new AsyncImageLoader(
							getApplicationContext())
							.decodeSampledBitmapFromPath(f.getAbsolutePath(),
									Constants.ImageSize, Constants.ImageSize);
					imgUserimage.setImageBitmap(imgloader.getCroppedBitmap(
							bitmap, bitmap.getWidth()));
					// bitmap_ = bitmap;
					ByteArrayOutputStream bao = new ByteArrayOutputStream();

					bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

					byte[] ba = bao.toByteArray();

					String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
					base64 = ba1;

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (requestCode == 3) {

				Uri selectedImage = data.getData();
				String[] filePath = { MediaStore.Images.Media.DATA };
				Cursor c = getContentResolver().query(selectedImage, filePath,
						null, null, null);
				c.moveToFirst();
				int columnIndex = c.getColumnIndex(filePath[0]);
				String picturePath = c.getString(columnIndex);
				c.close();
				Bitmap bitmap = new AsyncImageLoader(getApplicationContext())
						.decodeSampledBitmapFromPath(picturePath,
								Constants.ImageSize, Constants.ImageSize);

				imgUserimage.setImageBitmap(imgloader.getCroppedBitmap(bitmap,
						bitmap.getWidth()));

				// bitmap_ = bitmap;
				ByteArrayOutputStream bao = new ByteArrayOutputStream();

				bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

				byte[] ba = bao.toByteArray();

				String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
				base64 = ba1;

				Log.w("path of image from gallery......******************.........",
						picturePath + "");

			}
		}
	}

	public void GetLocations() {
		gps = new GPSTracker(HomeActivity.this);
		if (gps.canGetLocation()) {

			// double latitude = gps.getLatitude();
			// double longitude = gps.getLongitude();
			latitude_ = gps.getLatitude() + "";
			longitude_ = gps.getLongitude() + "";
			// latitude_ = 28.6100 + "";
			// longitude_ = 77.2300 + "";
			// latitude_ = 45.2826 + "";
			// longitude_ = 9.1821 + "";
			// \n is for new line

		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}
	}

//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//
//		new ChangeLanguage(HomeActivity.this, session_email.getUserDetails()
//				.get(SessionManager_email.KEY_LANGUAGE).toString());
//
//	}

	private void initGpsListeners() {

		myGpsService = new Intent(this, GpsService.class);
		startService(myGpsService);
		// String GPS_FILTER = this.getResources().getString(GPS_FILTER);
		IntentFilter mainFilter = new IntentFilter(GPS_FILTER);
		gpsReciever = new MainGpsReciever();
		registerReceiver(gpsReciever, mainFilter);

	}

	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "Home");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}

	@Override
	public void onStop() {
		super.onStop();
		// The rest of your onStop() code.
		// EasyTracker.getInstance(this).set(Fields.SCREEN_NAME, "Home Screen");
		// // Add this method.
	}

	public ArrayList<HashMap<String, String>> createSpinnerNews(String content) {
		ArrayList<HashMap<String, String>> NEWSLIST = new ArrayList<HashMap<String, String>>();

		try {

			JSONObject obj = new JSONObject(content);
			JSONArray m_jArry = obj.getJSONArray("response");
			// JSONArray m_jArry = new JSONArray(content);
			// JSONArray json = new JSONArray(result);
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			Log.e(" m_jArry.length()", m_jArry.length() + "--");
			for (int j = 0; j < m_jArry.length(); j++) {
				Log.e("j count", j + "--");
				JSONArray json = m_jArry.getJSONArray(j);
				Log.e("jo_inside", json + "--");
				// JSONArray json = new JSONArray(jo_inside.toString());
				for (int i = 0; i < json.length(); i++) {
					JSONObject jo_inside = json.getJSONObject(i);
					// Log.d("Details-->", jo_inside
					// .getString(Constants.NEWS_ACQIRED_DATETIME));
					String NEWS_ID = (jo_inside.has(Constants.NEWS_ID) == false) ? ""
							: jo_inside.getString(Constants.NEWS_ID);
					String acq_time = (jo_inside
							.has(Constants.NEWS_ACQIRED_DATETIME) == false) ? ""
							: jo_inside
									.getString(Constants.NEWS_ACQIRED_DATETIME);
					String type = (jo_inside.has(Constants.NEWS_TYPE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_TYPE);
					String category = (jo_inside.has(Constants.NEWS_CATEGORY) == false) ? ""
							: jo_inside.getString(Constants.NEWS_CATEGORY);
					String pitcher = (jo_inside.has(Constants.NEWS_PICTURE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_PICTURE);
					String title = (jo_inside.has(Constants.NEWS_TITLE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_TITLE);
					String description = (jo_inside
							.has(Constants.NEWS_DESCRIPTION) == false) ? ""
							: jo_inside.getString(Constants.NEWS_DESCRIPTION);
					String url = (jo_inside.has(Constants.NEWS_URL) == false) ? ""
							: jo_inside.getString(Constants.NEWS_URL);
					String NEWS_SOURCE = (jo_inside.has(Constants.NEWS_SOURCE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_SOURCE);
					String NEWS_BOXTYPE = (jo_inside
							.has(Constants.NEWS_BOXTYPE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_BOXTYPE);
					String NEWS_EVENTDATE = (jo_inside
							.has(Constants.NEWS_EVENTDATE) == false) ? ""
							: jo_inside.getString(Constants.NEWS_EVENTDATE);
					String NEWS_TAGS = (jo_inside.has(Constants.NEWS_TAGS) == false) ? ""
							: jo_inside.getString(Constants.NEWS_TAGS);
					String NEWS_POIS = (jo_inside.has(Constants.NEWS_POIS) == false) ? ""
							: jo_inside.getString(Constants.NEWS_POIS);
					String NEWS_EXPERT_USERID = (jo_inside
							.has(Constants.NEWS_EXPERT_USERID) == false) ? ""
							: jo_inside.getString(Constants.NEWS_EXPERT_USERID);
					// Add your values in your `ArrayList` as below:

					HashMap<String, String> m_li;
					m_li = new HashMap<String, String>();
					m_li.put(Constants.NEWS_ID, NEWS_ID.replaceAll("null", "")
							.replaceAll("NULL", ""));
					m_li.put(Constants.NEWS_ACQIRED_DATETIME, acq_time
							.replaceAll("null", "").replaceAll("NULL", ""));
					m_li.put(Constants.NEWS_TYPE, type.replaceAll("null", "")
							.replaceAll("NULL", ""));
					m_li.put(
							Constants.NEWS_CATEGORY,
							category.replaceAll("null", "").replaceAll("NULL",
									""));
					m_li.put(
							Constants.NEWS_PICTURE,
							pitcher.replaceAll("null", "").replaceAll("NULL",
									""));
					m_li.put(Constants.NEWS_TITLE, title.replaceAll("null", "")
							.replaceAll("NULL", ""));
					m_li.put(Constants.NEWS_DESCRIPTION, description
							.replaceAll("null", "").replaceAll("NULL", ""));
					m_li.put(Constants.NEWS_URL, url.replaceAll("null", "")
							.replaceAll("NULL", ""));
					m_li.put(
							Constants.NEWS_SOURCE,
							NEWS_SOURCE.replaceAll("null", "").replaceAll(
									"NULL", ""));
					m_li.put(
							Constants.NEWS_BOXTYPE,
							NEWS_BOXTYPE.replaceAll("null", "").replaceAll(
									"NULL", ""));
					m_li.put(Constants.NEWS_EVENTDATE, NEWS_EVENTDATE
							.replaceAll("null", "").replaceAll("NULL", ""));
					m_li.put(
							Constants.NEWS_TAGS,
							NEWS_TAGS.replaceAll("null", "").replaceAll("NULL",
									""));
					m_li.put(
							Constants.NEWS_POIS,
							NEWS_POIS.replaceAll("null", "").replaceAll("NULL",
									""));
					m_li.put(Constants.NEWS_EXPERT_USERID, NEWS_EXPERT_USERID
							.replaceAll("null", "").replaceAll("NULL", ""));

					if (j == 0)
						GET_NEWS.add(m_li);
					else if (j == 1)
						GET_NEWS1.add(m_li);
					else if (j == 2)
						GET_NEWS2.add(m_li);
				}

			}
			return NEWSLIST;
		} catch (Exception e) {
			// TODO: handle exception

			Log.e("exceptionsss", e.toString() + "--");
			return NEWSLIST;
		}
	}
}
