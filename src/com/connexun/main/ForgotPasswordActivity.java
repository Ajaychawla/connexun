package com.connexun.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.connexun.main.R;

import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager_email;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class ForgotPasswordActivity extends Activity {

	EditText editEmailaddress;
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_password);

		ImageButton resetPasswordButton = (ImageButton) findViewById(R.id.resetPasswordButton);
		ImageButton close = (ImageButton) findViewById(R.id.closeButton);

		editEmailaddress = (EditText) findViewById(R.id.editEmailaddress);

		resetPasswordButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (isValidated()) {
					// Use AsyncTask execute Method To Prevent ANR Problem

					if (new ConnectionDetector(ForgotPasswordActivity.this)
							.isConnectingToInternet()) {
						new ForgotPassOpration()
								.execute(Constants.ForgotPassword);
					} else {
						try {
							alert.showAlertDialog(
									ForgotPasswordActivity.this,
									ForgotPasswordActivity.this
											.getResources()
											.getString(
													R.string.Alert_Internet_connection)
											+ "",
									ForgotPasswordActivity.this.getResources()
											.getString(R.string.Alert_Internet)
											+ "", false);
						} catch (Exception e) {
							// TODO: handle exception
						}
					}

				}

			}
		});

		close.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}

	private boolean isValidated() {
		editEmailaddress.setError(null);

		if (editEmailaddress.getText().toString().equalsIgnoreCase("")) {
			// Toast.makeText(LoginActivity.this, "Please enter Id",
			// Toast.LENGTH_LONG).show();
			editEmailaddress.setError(Html.fromHtml("<font color='red'>"
					+ ForgotPasswordActivity.this.getResources().getString(
							R.string.Alert_login_usernampass) + "</font>"));

			return false;
		} else if (!editEmailaddress.getText().toString().contains("@")) {
			editEmailaddress.setError(Html.fromHtml("<font color='red'>"
					+ ForgotPasswordActivity.this.getResources().getString(
							R.string.Alert_login_invalidusername) + "</font>"));
			return false;
		}
		return true;
	}

	private class ForgotPassOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				ForgotPasswordActivity.this);

		String username = "";

		String apitoken = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

			Dialog.setMessage(ForgotPasswordActivity.this.getResources()
					.getString(R.string.Alert_Pleasewait) + "");
			Dialog.setCancelable(false);
			Dialog.show();

			try {
				// Set Request parameter
				username += "&" + URLEncoder.encode("username", "UTF-8") + "="
						+ editEmailaddress.getText();

				apitoken += "&" + URLEncoder.encode("apitoken", "UTF-8")
						+ "=4ssiw3B29F4BD5I0iPkSE4BD41jq29F4";

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(Constants.RequestTimeOutLimit);
				conn.setReadTimeout(Constants.RequestTimeOutLimit);
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(username);
				wr.write(apitoken);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);
				Toast.makeText(
						ForgotPasswordActivity.this,
						ForgotPasswordActivity.this.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();
			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);

					Log.wtf("error1", Content);

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {
						Toast.makeText(ForgotPasswordActivity.this,
								"Token is not valid!", Toast.LENGTH_LONG)
								.show();
					}
					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("email is not registred")) {
						Toast.makeText(ForgotPasswordActivity.this,
								"Token is not valid!", Toast.LENGTH_LONG)
								.show();
					}
					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("ok")) {
						Toast.makeText(
								ForgotPasswordActivity.this,
								ForgotPasswordActivity.this
										.getResources()
										.getString(
												R.string.Alert_forgetpass_response)
										+ "", Toast.LENGTH_LONG).show();
						finish();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(ForgotPasswordActivity.this)
//
//		.getUserDetails().get(SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(ForgotPasswordActivity.this,
//				new SessionManager_email(ForgotPasswordActivity.this)
//
//				.getUserDetails().get(SessionManager_email.KEY_LANGUAGE)
//						.toString());
//
//	}
}
