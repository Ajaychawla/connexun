package com.connexun.main;

import static com.connexun.gcm.CommonUtilities.SENDER_ID;



import com.connexun.main.R;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gcm.GCMRegistrar;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

public class SplashActivity extends Activity {

	SessionManager session;
	SessionManager_email session_email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		// Calendar calendar = Calendar.getInstance();
		// calendar.set(2015, 2, 9,
		// 12, 15, 0);
		// long startTime = calendar.getTimeInMillis();
		//
		// Log.e("startTime", startTime+"----");
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(this);

		// Get GCM registration id
		final String regId = GCMRegistrar.getRegistrationId(this);
		Constants.GCM_REGISTRATION_ID=regId;
		Log.e("regId", regId + "--");
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, SENDER_ID);
			final String regId1 = GCMRegistrar.getRegistrationId(this);
			Log.e("regId", regId1 + "--");
			Constants.GCM_REGISTRATION_ID=regId;
			//return;
		} else {
			// Device is already registered on GCM
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				Toast.makeText(getApplicationContext(),
						"Already registered with GCM", Toast.LENGTH_LONG)
						.show();
			}
		}

		session = new SessionManager(SplashActivity.this);
		session_email = new SessionManager_email(SplashActivity.this);

		if (session_email.isLoggedIn()) {
			if (session_email.getUserDetails().get(
					SessionManager_email.KEY_LANGUAGE) != null) {
//				new ChangeLanguage(SplashActivity.this, session_email
//						.getUserDetails()
//						.get(SessionManager_email.KEY_LANGUAGE).toString());
			}

		}
		if (session.isLoggedIn()) {

			// new ChangeLanguage(SplashActivity.this, "hi");
			Intent in = new Intent(SplashActivity.this, HomeActivity.class);
			startActivity(in);
			SplashActivity.this.finish();
			return;
		}
		new CountDownTimer(5000, 10000) {

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				Intent in = new Intent(SplashActivity.this,
						RegisterLoginActivity.class);
				startActivity(in);
				SplashActivity.this.finish();
			}
		}.start();
	}

//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(SplashActivity.this).getUserDetails().get(
//				SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(SplashActivity.this, new SessionManager_email(
//				SplashActivity.this).getUserDetails()
//				.get(SessionManager_email.KEY_LANGUAGE).toString());
//
//	}
	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "Launch");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}
}
