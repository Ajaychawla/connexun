package com.connexun.main;

import static com.connexun.gcm.CommonUtilities.SENDER_ID;
import static com.connexun.gcm.CommonUtilities.displayMessage;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;

import com.connexun.gcm.MainActivity;
import com.connexun.gcm.ServerUtilities;
import com.connexun.utils.Constants;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    public void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        displayMessage(context, "Your device registred with GCM");
        //Log.d("NAME", MainActivity.name);
        Constants.GCM_REGISTRATION_ID=registrationId;
       /// ServerUtilities.register(context, MainActivity.name, MainActivity.email, registrationId);
    }

    /**
     * Method called on device un registred
     * */
    @Override
    public void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    public void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("intent","Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                Log.e("intent","[" + key + "=" + bundle.get(key)+"]");
            }
            Log.e("intent","Dumping Intent end"+getTopActivityStackName(context));
        }
        String message = intent.getExtras().getString("price");
        String filter = intent.getExtras().getString("filter");
        String id = intent.getExtras().getString("id");
        
        displayMessage(context, message);
        // notifies user
       // if()
        //if(!MainActivity.running)
        if(!getTopActivityStackName(context).equals("com.connexun.main"))
        generateNotification(context, message,filter,id);
    }
    public String getTopActivityStackName(Context context)
    {
        ActivityManager mActivityManager = (ActivityManager)
        		context.getSystemService(Activity.ACTIVITY_SERVICE);
        PackageManager mPackageManager = context.getPackageManager();
        String packageName = mActivityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
        ApplicationInfo mApplicationInfo;
        try 
        {
            mApplicationInfo = mPackageManager.getApplicationInfo( packageName, 0);
        } catch (NameNotFoundException e) {
            mApplicationInfo = null;
        }
       String appName = (String) (mApplicationInfo != null ? 
               mPackageManager.getApplicationLabel(mApplicationInfo) : "(unknown)");

        return packageName;
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    public void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        //generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    public boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message,String filter,String id) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        
        String title = context.getString(R.string.app_name);
        
        Intent notificationIntent = new Intent(context, HomeActivity.class);
        // notificationIntent.putExtra("message", message);
        notificationIntent.putExtra("filter", filter);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        
        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
        
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        long time = new Date().getTime();
        String tmpStr = String.valueOf(time);
        String last4Str = tmpStr.substring(tmpStr.length() - 5);
        int notificationId = Integer.valueOf(last4Str);
        notificationManager.notify(Integer.parseInt(id), notification);      

    }

}
