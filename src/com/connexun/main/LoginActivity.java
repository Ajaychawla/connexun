package com.connexun.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.main.R;

import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class LoginActivity extends Activity {

	EditText editUsername, editPassword;

	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;
	SessionManager_email session_email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		ImageButton loginButton = (ImageButton) findViewById(R.id.loginButton);

		TextView register_text = (TextView) findViewById(R.id.registerText);
		TextView forgot_password_text = (TextView) findViewById(R.id.forgetPasswordText);
		editUsername = (EditText) findViewById(R.id.editUsername);
		editPassword = (EditText) findViewById(R.id.editPassword);
		session = new SessionManager(LoginActivity.this);
		session_email = new SessionManager_email(LoginActivity.this);
		if (session_email.isLoggedIn()) {
			editUsername.setText(session_email.getUserDetails()
					.get(SessionManager_email.KEY_EMAIL).toString());
		}

		loginButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (isValidated()) {
					// Use AsyncTask execute Method To Prevent ANR Problem
					if (new ConnectionDetector(LoginActivity.this)
							.isConnectingToInternet()) {
						new LongInOperation().execute(Constants.LoginUrl);
					} else {
						try{
						alert.showAlertDialog(
								LoginActivity.this,
								LoginActivity.this.getResources().getString(
										R.string.Alert_Internet_connection)
										+ "", LoginActivity.this.getResources()
										.getString(R.string.Alert_Internet)
										+ "", false);
						} catch (Exception e) {
							// TODO: handle exception
						}
					}

				}

			}
		});

		register_text.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(LoginActivity.this,
						RegisterActivity.class);
				startActivityForResult(i, 1);
			}
		});

		forgot_password_text.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(LoginActivity.this,
						ForgotPasswordActivity.class);
				LoginActivity.this.startActivity(i);
			}
		});
	}

	private boolean isValidated() {
		editUsername.setError(null);
		editPassword.setError(null);

		if (editUsername.getText().toString().equalsIgnoreCase("")) {
			// Toast.makeText(LoginActivity.this, "Please enter Id",
			// Toast.LENGTH_LONG).show();
			editUsername.setError(Html.fromHtml("<font color='red'>"
					+ getResources()
							.getString(R.string.Alert_login_usernampass)
					+ "</font>"));

			return false;
		} else if (!editUsername.getText().toString().contains("@")) {
			editUsername.setError(Html.fromHtml("<font color='red'>"
					+ getResources().getString(
							R.string.Alert_login_invalidusername) + "</font>"));
			return false;
		} else if (editPassword.getText().toString().equalsIgnoreCase("")) {
			// Toast.makeText(LoginActivity.this, "Please enter password",
			// Toast.LENGTH_LONG).show();
			editPassword.setError(Html.fromHtml("<font color='red'>"
					+ getResources().getString(R.string.Alert_login_password)
					+ "</font>"));
			return false;
		}
		return true;
	}

	private class LongInOperation extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(LoginActivity.this);

		String username = "";
		String password = "";
		String apitoken = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.

			// Start Progress Dialog (Message)

			Dialog.setMessage(LoginActivity.this.getResources().getString(
					R.string.Alert_Pleasewait)
					+ "");
			Dialog.setCancelable(false);
			Dialog.show();

			try {
				// Set Request parameter
				username += "&" + URLEncoder.encode("username", "UTF-8") + "="
						+ editUsername.getText();
				password += "&" + URLEncoder.encode("password", "UTF-8") + "="
						+ editPassword.getText();
				apitoken += "&" + URLEncoder.encode("apitoken", "UTF-8")
						+ "=57893bDk7Bh78vbf57ui73bDbDk78k78";

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(Constants.RequestTimeOutLimit);
				conn.setReadTimeout(Constants.RequestTimeOutLimit);
				conn.setDoOutput(true);

				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(username);
				wr.write(password);
				wr.write(apitoken);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						LoginActivity.this,
						LoginActivity.this.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);

					Log.wtf("error1", Content);

					if (jsonObj.get("status").toString().equals("OK")) {
						String usertoken = jsonObj.get("usertoken").toString();
						String userid = jsonObj.get("userid").toString();
						String language = jsonObj.get("language").toString();
						// alert.showAlertDialog(LoginActivity.this, "Error",
						// "TEST", false);

						Log.wtf("USERTOKEN:", usertoken + "USERID:" + userid);

						session.createLoginSession(userid, usertoken,"");
						session_email.createLoginSession(editUsername.getText()
								.toString(), language);
						Toast.makeText(
								LoginActivity.this,
								LoginActivity.this.getResources().getString(
										R.string.LG_loginsuccess)
										+ "", Toast.LENGTH_LONG).show();
						// Staring MainActivity
						//new ChangeLanguage(LoginActivity.this, language);
						Intent i = new Intent(getApplicationContext(),
								HomeActivity.class);
						startActivity(i);
						Intent in = getIntent();
						setResult(RESULT_OK, in);
						finish();
					} else {
						String response = jsonObj.get("response").toString();
						alert.showAlertDialog(LoginActivity.this, "Error",
								response, false);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			Intent in = getIntent();
			setResult(RESULT_OK, in);
			LoginActivity.this.finish();
		}
	}
	// @Override
	// public void onBackPressed() {
	// // TODO Auto-generated method stub
	// super.onBackPressed();
	// Intent i = new Intent(LoginActivity.this, RegisterLoginActivity.class);
	// LoginActivity.this.startActivity(i);
	// finish();
	// }
	
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(LoginActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(LoginActivity.this,
//				new SessionManager_email(LoginActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE)
//						.toString());
//
//	}
	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "Login");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}
}
