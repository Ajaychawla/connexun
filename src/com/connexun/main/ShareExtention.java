package com.connexun.main;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ShareExtention extends Activity {

	EditText sharetextarea;
	Button postbtn, cancelbtn;
	String sharelink = "";
	TextView alerttext;
	LinearLayout linearofshare;
	String URL = "";
	String shareURL = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shareextention);

		sharetextarea = (EditText) findViewById(R.id.sharetextarea);
		postbtn = (Button) findViewById(R.id.postbtn);
		cancelbtn = (Button) findViewById(R.id.cancelbtn);
		alerttext = (TextView) findViewById(R.id.alerttext);
		linearofshare = (LinearLayout) findViewById(R.id.linearofshare);
		// Toast.makeText(ShareExtention.this,
		// getIntent().getStringExtra(Intent.EXTRA_TEXT),
		// Toast.LENGTH_LONG).show();
		shareURL = getIntent().getStringExtra(Intent.EXTRA_TEXT).toString();
		// if (new SessionManager(ShareExtention.this).getUserDetails()
		// .get(SessionManager.KEY_EXPERT).toString().equals("0")) {
		// linearofshare.setVisibility(View.GONE);
		// alerttext.setVisibility(View.VISIBLE);
		// }
		sharelink = getIntent().getStringExtra(Intent.EXTRA_TEXT);
		postbtn.setOnClickListener(shareclick);
		cancelbtn.setOnClickListener(shareclick);

	}

	OnClickListener shareclick = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.postbtn) {
				URL = "http://192.168.0.104/ios/index.php?"
						+ "user_id="
						+ new SessionManager(ShareExtention.this)
								.getUserDetails()
								.get(SessionManager.KEY_USERID).toString()
						+ "&token="
						+ new SessionManager(ShareExtention.this)
								.getUserDetails()
								.get(SessionManager.KEY_USERTOKEN).toString()
						+ "&text=" + sharetextarea.getText().toString()
						+ "&url=" + shareURL;

				new LongOperation().execute();

			} else if (v.getId() == R.id.cancelbtn) {
				// Intent i = new Intent(ShareExtention.this,
				// HomeActivity.class);
				// i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// startActivity(i);
				ShareExtention.this.finish();
			}
		}
	};

	private class LongOperation extends AsyncTask<String, Void, String> {

		String Content = "";
		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(ShareExtention.this, "Loading...",
					"", true);
		}

		@Override
		protected String doInBackground(String... params) {
			try {

				// Defined URL where to send data

				HttpClient Client = new DefaultHttpClient();

				// Create URL string

				// String URL =
				// "http://androidexample.com/media/webservice/httpget.php?user="+loginValue+"&name="+fnameValue+"&email="+emailValue+"&pass="+passValue;

				// Log.i("httpget", URL);

				String SetServerString = "";

				// Create Request to server and get response

				HttpGet httpget = new HttpGet(URL);

				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is
				// established.
				// The default value is zero, that means the timeout is not
				// used.
				int timeoutConnection = Constants.RequestTimeOutLimit;
				HttpConnectionParams.setConnectionTimeout(httpParameters,
						timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = Constants.RequestTimeOutLimit;
				HttpConnectionParams
						.setSoTimeout(httpParameters, timeoutSocket);

				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				// SetServerString = Client.execute(httpget, responseHandler);

				DefaultHttpClient httpClient = new DefaultHttpClient(
						httpParameters);
				SetServerString = httpClient.execute(httpget, responseHandler);

				// Show response on activity
				Content = SetServerString;
				// content.setText(SetServerString);

			} catch (Exception ex) {

			}
			return Content;
		}

		@Override
		protected void onPostExecute(String result) {

			Toast.makeText(ShareExtention.this, "Success fully post",
					Toast.LENGTH_LONG).show();
			progress.dismiss();
			ShareExtention.this.finish();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}
	}

}
