package com.connexun.main;

import java.io.BufferedReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.adapters.PeoplearoundListAdapter;
import com.connexun.interfaces.DataListners;
import com.connexun.main.R;

import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.GPSTracker;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class PeopleAroundList extends Activity {

	SessionManager session;

	ListView listview;

	ArrayList<HashMap<String, String>> PEOPLEAROUND_ARRAY = new ArrayList<HashMap<String, String>>();

	String xmls = "";
	TextView backButton, textTobbar;
	String CompleteURL = "";
	PeoplearoundListAdapter listadapt;
	boolean loadingMore = true;
	ProgressBar progressBar1;
	int i = 1;
	String latitude_ = "", longitude_ = "";
	String filter = "";
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.peoplearound_list);
		session = new SessionManager(PeopleAroundList.this);

		listview = (ListView) findViewById(R.id.list);
		backButton = (TextView) findViewById(R.id.backButton);
		textTobbar = (TextView) findViewById(R.id.textTobbar);
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

		// PEOPLEAROUND_ARRAY = (ArrayList<HashMap<String, String>>) getIntent()
		// .getSerializableExtra("PEOPLEAROUND_ARRAY");
		filter = getIntent().getExtras().getString("filter");
		listadapt = new PeoplearoundListAdapter(PeopleAroundList.this,
				PEOPLEAROUND_ARRAY);

		listview.setAdapter(listadapt);

		GPSTracker gps = new GPSTracker(PeopleAroundList.this);

		// check if GPS enabled
		if (gps.canGetLocation()) {

			// double latitude = gps.getLatitude();
			// double longitude = gps.getLongitude();
			latitude_ = gps.getLatitude() + "";
			longitude_ = gps.getLongitude() + "";
			// latitude_ = 28.6100 + "";
			// longitude_ = 77.2300 + "";
			// \n is for new line

		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}
		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				HashMap<String, String> hashMap = PEOPLEAROUND_ARRAY
						.get(position);
				Intent intent = new Intent(PeopleAroundList.this,
						UsersDetails.class);
				intent.putExtra("hashMap", hashMap);
				startActivity(intent);
			}
		});
		listview.setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				int lastInScreen = firstVisibleItem + visibleItemCount;
				if ((lastInScreen == totalItemCount) && !(loadingMore)) {
					loadingMore = true;
					if (filter.equalsIgnoreCase("in"))
						new PeopleAroundListserver()
								.execute(Constants.PEOPLEARROUND_IN_URL
										+ CompleteURL + "?startrow=" + (++i)
										+ "&latitude=" + latitude_
										+ "&longitude=" + longitude_);
					else
						new PeopleAroundListserver()
								.execute(Constants.PEOPLEARROUND_OUT_URL
										+ CompleteURL + "?startrow=" + (++i)
										+ "&latitude=" + latitude_
										+ "&longitude=" + longitude_);
				}
			}
		});
		if (new ConnectionDetector(PeopleAroundList.this)
				.isConnectingToInternet()) {
			if (filter.equalsIgnoreCase("in"))

				new PeopleAroundListserver()
						.execute(Constants.PEOPLEARROUND_IN_URL + CompleteURL
								+ "?startrow=1&latitude=" + latitude_
								+ "&longitude=" + longitude_);
			else
				new PeopleAroundListserver()
						.execute(Constants.PEOPLEARROUND_OUT_URL + CompleteURL
								+ "?startrow=1&latitude=" + latitude_
								+ "&longitude=" + longitude_);
		} else {
			try {
				alert.showAlertDialog(
						PeopleAroundList.this,
						PeopleAroundList.this.getResources().getString(
								R.string.Alert_Internet_connection)
								+ "", PeopleAroundList.this.getResources()
								.getString(R.string.Alert_Internet) + "", false);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public class PeopleAroundListserver extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				PeopleAroundList.this);

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			try {
				if (i != 1)
					progressBar1.setVisibility(View.VISIBLE);
				else {
					Dialog.setMessage(PeopleAroundList.this.getResources()
							.getString(R.string.Alert_Pleasewait) + "");
					Dialog.setCancelable(false);
					Dialog.show();
				}
				// Set Request parameter
				// user_id += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERID);
				// apitoken += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERTOKEN);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data

				HttpClient Client = new DefaultHttpClient();

				// Create URL string

				// String URL =
				// "http://androidexample.com/media/webservice/httpget.php?user="+loginValue+"&name="+fnameValue+"&email="+emailValue+"&pass="+passValue;

				// Log.i("httpget", URL);

				String SetServerString = "";

				// Create Request to server and get response

				HttpGet httpget = new HttpGet(urls[0]);
				Log.e("urls[0]", urls[0]);

				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is
				// established.
				// The default value is zero, that means the timeout is not
				// used.
				int timeoutConnection = Constants.RequestTimeOutLimit;
				HttpConnectionParams.setConnectionTimeout(httpParameters,
						timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = Constants.RequestTimeOutLimit;
				HttpConnectionParams
						.setSoTimeout(httpParameters, timeoutSocket);

				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				// SetServerString = Client.execute(httpget, responseHandler);

				DefaultHttpClient httpClient = new DefaultHttpClient(
						httpParameters);
				SetServerString = httpClient.execute(httpget, responseHandler);

				// Show response on activity
				Content = SetServerString;
				// content.setText(SetServerString);

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog

			if (i != 1)
				progressBar1.setVisibility(View.GONE);
			else {

				Dialog.dismiss();
			}

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				// dataListner.GetNewsListners(Error);

				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						PeopleAroundList.this,
						PeopleAroundList.this.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				// Log.wtf("NO ERROR : ", Content);

				try {
					if (new ConnectivityServer().CheckExpirationTocken(
							PeopleAroundList.this, Content)) {
						return;
					}
					ArrayList<HashMap<String, String>> PEOPLEAROUND_ARRAY_New = new ArrayList<HashMap<String, String>>();

					PEOPLEAROUND_ARRAY_New = new ConnectivityServer()
							.createPeopleAround_ListSpinner(Content, filter);

					PEOPLEAROUND_ARRAY.addAll(PEOPLEAROUND_ARRAY_New);

					Log.e("PEOPLEAROUND_ARRAY.size()",
							PEOPLEAROUND_ARRAY.size() + "---");
					runOnUiThread(new Runnable() {
						public void run() {
							listadapt.notifyDataSetChanged();
						}
					});
					// listadapt = new PeoplearoundListAdapter(
					// PeopleAroundList.this, PEOPLEAROUND_ARRAY);
					// listview.setAdapter(listadapt);
					if (PEOPLEAROUND_ARRAY_New.size() >= 10)
						loadingMore = false;
					else
						loadingMore = true;
					// listadapt.notifyDataSetChanged();
					// listview.invalidateViews();
					// JSONObject jsonObj = new JSONObject(Content);
					// createSpinnerContent(Content);
					Log.wtf("error1", Content);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//	    super.onConfigurationChanged(newConfig);
//	   
//	    new ChangeLanguage(PeopleAroundList.this, new SessionManager_email(PeopleAroundList.this)
//				.getUserDetails()
//				.get(SessionManager_email.KEY_LANGUAGE).toString());
//	    
//	}
	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "People Around");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}
}
