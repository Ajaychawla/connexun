package com.connexun.main;

import com.connexun.main.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if("wee" == "wee"){
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(MainActivity.this, RegisterLoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			MainActivity.this.startActivity(i);
			this.finish();
		} else {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(MainActivity.this, HomeActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			MainActivity.this.startActivity(i);
			this.finish();
		}
	}
}
