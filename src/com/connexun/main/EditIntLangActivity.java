package com.connexun.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.adapters.SpinnerAdapters;
import com.connexun.adapters.spinnerInterest;
import com.connexun.main.R;
import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;
import com.connexun.utils.SessionManager;

public class EditIntLangActivity extends Activity {

	SessionManager session;
	ArrayList<HashMap<String, String>> GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
	ImageView imgUserimage;
	TextView textUsername, textUserCountry, textUserBussiness,
			textUserInterestedCountry, textUserLanguage;
	ImageLoader imgloader;
	ImageButton imgbtnEditCompanyinterst, imgbtnEditlanguage;
	Spinner spinnerInterestCountry, spinnerLanguage;
	ImageView imgCountryIcon;

	ArrayList<HashMap<String, String>> GET_COUNTRYLIST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_ORIGIN = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
	public static String COUNTRYCODE = "countrycode";
	public static String ID = "id";
	public static String RELATED = "related";
	public static String COUNTRYNAME = "countryname";
	public static String CURRENCYCODE = "currencycode";
	public static String CURRENCYNAME = "currencyname";
	public static String LATITUDE = "latitude";
	public static String LONGITUDE = "longitude";
	public static String WOEID = "woeid";
	Integer[] interestCountry_image = { R.drawable.interest_icon,
			R.drawable.br, R.drawable.ca, R.drawable.cn, R.drawable.it,
			R.drawable.in };
	String[] Language_list = { "Select Language", "English", "Italy" };
	String[] Language_list_NICNAME = { "Select Language", "en", "it" };
	AlertDialogManager alert = new AlertDialogManager();
	String CompleteURL = "";
	ImageButton imgBtnLangSave;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_int_lang);
		session = new SessionManager(EditIntLangActivity.this);
		session.checkLogin();
		GET_USERDETAILS = (ArrayList<HashMap<String, String>>) getIntent()
				.getSerializableExtra("GET_USERDETAILS");
		imgloader = new ImageLoader(EditIntLangActivity.this);
		imgUserimage = (ImageView) findViewById(R.id.imgUserimage);

		textUsername = (TextView) findViewById(R.id.textUsername);
		textUserCountry = (TextView) findViewById(R.id.textUserCountry);
		textUserBussiness = (TextView) findViewById(R.id.textUserBussiness);
		textUserInterestedCountry = (TextView) findViewById(R.id.textUserInterestedCountry);
		textUserLanguage = (TextView) findViewById(R.id.textUserLanguage);
		imgbtnEditCompanyinterst = (ImageButton) findViewById(R.id.imgbtnEditCompanyinterst);
		imgbtnEditlanguage = (ImageButton) findViewById(R.id.imgbtnEditlanguage);
		spinnerInterestCountry = (Spinner) findViewById(R.id.spinnerInterestCountry);
		spinnerLanguage = (Spinner) findViewById(R.id.spinnerLanguage);
		imgCountryIcon = (ImageView) findViewById(R.id.imgCountryIcon);
		imgBtnLangSave = (ImageButton) findViewById(R.id.imgbtnLangSave);

		getPosition();
		// spinnerLanguage.setAdapter(adapter)
		spinnerLanguage.setAdapter(new spinnerInterest(
				EditIntLangActivity.this, R.layout.spinner_country,
				interestCountry_image, Language_list));
		spinnerInterestCountry.setAdapter(new SpinnerAdapters(
				EditIntLangActivity.this, GET_COUNTRYLIST_INTEREST));

		imgloader
				.DisplayImage(
						GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
								.toString(), R.drawable.user_avatar_default,
						imgUserimage, 50);
		textUsername.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_FIRSTNAME).toString()
				+ " "
				+ GET_USERDETAILS.get(0).get(Constants.USER_LASTNAME)
						.toString());
		textUserCountry
				.setText(Constants.ARRAY_COUNTRY_COMPLETENAME[new JsonLocalFileFetch(
						EditIntLangActivity.this).getPositionCountry(
						Constants.ARRAY_COUNTRY_NICNAME, GET_USERDETAILS.get(0)
								.get(Constants.USER_HOMECOUNT).toString())]);
		if (GET_USERDETAILS.get(0).get(Constants.USER_JOBPOSITION).toString()
				.equals("")
				&& GET_USERDETAILS.get(0).get(Constants.USER_COMPANY)
						.toString().equals(""))
			textUserBussiness.setVisibility(View.GONE);
		else
			textUserBussiness.setText(GET_USERDETAILS.get(0)
					.get(Constants.USER_JOBPOSITION).toString()
					+ " "
					+ GET_USERDETAILS.get(0).get(Constants.USER_COMPANY)
							.toString());
		textUserInterestedCountry
				.setText(""
						+ Constants.ARRAY_COUNTRY_COMPLETENAME[new JsonLocalFileFetch(
								EditIntLangActivity.this).getPositionCountry(
								Constants.ARRAY_COUNTRY_NICNAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_INTERESTCOUNT)
										.toString())]);
		Uri otherPath = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ GET_USERDETAILS.get(0)
								.get(Constants.USER_INTERESTCOUNT).toString());
		imgCountryIcon.setImageURI(otherPath);
		// textUserLanguage.setText("Language: "+GET_USERDETAILS.get(0)
		// .get(Constants.USER_LANGUAGE).toString());
		String lang = "";
		if (GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE).toString()
				.equalsIgnoreCase("en"))
			lang = Language_list[1];
		if (GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE).toString()
				.equalsIgnoreCase("it"))
			lang = Language_list[2];
		textUserLanguage.setText("" + lang);
		textUserLanguage.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_LANGUAGE).toString());
		textUserInterestedCountry.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_INTERESTCOUNT).toString());
		// TextView logout = (TextView) findViewById(R.id.logout);
		//
		//
		// logout.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// Intent i = new Intent(HomeActivity.this,
		// RegisterLoginActivity.class);
		// HomeActivity.this.startActivity(i);
		// }
		// });

		ImageButton editProfileButton = (ImageButton) findViewById(R.id.editProfileButton);

		editProfileButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(EditIntLangActivity.this,
						EditProfileActivity.class);
				i.putExtra("GET_USERDETAILS", GET_USERDETAILS);
				EditIntLangActivity.this.startActivity(i);
				// session.logoutUser();
				finish();

			}
		});

		ImageButton closeButton = (ImageButton) findViewById(R.id.closeButton);

		closeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				finish();
			}
		});
		imgbtnEditCompanyinterst.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerInterestCountry.performClick();
			}
		});
		imgbtnEditlanguage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerLanguage.performClick();
			}
		});
		spinnerInterestCountry
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						// Toast.makeText(EditIntLangActivity.this,
						// "Positions" + position, Toast.LENGTH_LONG)
						// .show();
						textUserInterestedCountry
								.setText(GET_COUNTRYLIST_INTEREST.get(position)
										.get(COUNTRYNAME).toString());
						Uri otherPath = Uri
								.parse("android.resource://com.connexun.main/drawable/"
										+ GET_COUNTRYLIST_INTEREST
												.get(position).get(COUNTRYCODE));
						textUserInterestedCountry
								.setTag(GET_COUNTRYLIST_INTEREST.get(position)
										.get(COUNTRYCODE));
						imgCountryIcon.setImageURI(otherPath);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		spinnerLanguage
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						// Toast.makeText(EditIntLangActivity.this,
						// "Positions" + position, Toast.LENGTH_LONG)
						// .show();
						if (position != 0) {
							textUserLanguage.setText(""
									+ Language_list[position]);
							textUserLanguage
									.setTag(Language_list_NICNAME[position]);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}

		imgBtnLangSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new EditLangOpration()
						.execute(Constants.LANGUAGE_INTERESTCHANGE
								+ CompleteURL);
			}
		});
		// createSpinnerContent("");

	}

	public void createSpinnerContent() {
		try {

			JSONObject obj = new JSONObject(loadJSONFromAsset());
			JSONArray m_jArry = obj.getJSONArray("countries");
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				Log.d("Details-->", jo_inside.getString("countrycode"));
				String countrycode = jo_inside.getString(COUNTRYCODE);
				String id = jo_inside.getString(ID);
				String related = jo_inside.getString(RELATED);
				String countryname = jo_inside.getString(COUNTRYNAME);
				String currencycode = jo_inside.getString(CURRENCYCODE);
				String currencyname = jo_inside.getString(CURRENCYNAME);
				String latitude = jo_inside.getString(LATITUDE);
				String longitude = jo_inside.getString(LONGITUDE);
				String woeid = jo_inside.getString(WOEID);

				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li = new HashMap<String, String>();
				m_li.put(COUNTRYCODE, countrycode);
				m_li.put(ID, id);
				m_li.put(RELATED, related);
				m_li.put(COUNTRYNAME, countryname);
				m_li.put(CURRENCYCODE, currencycode);
				m_li.put(CURRENCYNAME, currencyname);
				m_li.put(LATITUDE, latitude);
				m_li.put(LONGITUDE, longitude);
				m_li.put(WOEID, woeid);

				GET_COUNTRYLIST.add(m_li);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String loadJSONFromAsset() {
		String json = null;
		try {

			InputStream is = getAssets().open("json/CountryList.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	public void createInterestSpinnerContent(int position) {

		GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_COUNTRYLIST.get(position);

		// label.setText(map.get(RegisterActivity.COUNTRYNAME));
		String itemsCount = map.get(RegisterActivity.RELATED);
		if (itemsCount.contains(",")) {
			String items[] = itemsCount.split(",");
			for (int i = 0; i < items.length; i++) {
				GET_COUNTRYLIST_INTEREST.add(GET_COUNTRYLIST.get(Integer
						.parseInt(items[i])));
			}
		} else {
			GET_COUNTRYLIST_INTEREST.add(GET_COUNTRYLIST.get(Integer
					.parseInt(itemsCount)));
		}

	}

	public void getPosition() {
		createSpinnerContent();
		int position = 0;
		for (int i = 0; i < GET_COUNTRYLIST.size(); i++) {
			if (GET_COUNTRYLIST
					.get(i)
					.get(COUNTRYCODE)
					.equalsIgnoreCase(
							GET_USERDETAILS.get(0)
									.get(Constants.USER_HOMECOUNT).toString())) {
				position = i;
			}
		}
		createInterestSpinnerContent(position);
	}

	public class EditLangOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				EditIntLangActivity.this);

		String country_interest = "";

		String language = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

			Dialog.setMessage("Please wait..");
			Dialog.show();

			try {
				// Set Request parameter
				country_interest += "&"
						+ URLEncoder.encode("interestcount", "UTF-8") + "="
						+ textUserInterestedCountry.getTag();

				language += "&" + URLEncoder.encode("language", "UTF-8") + "="
						+ textUserLanguage.getTag();

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(country_interest);
				wr.write(language);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

				alert.showAlertDialog(EditIntLangActivity.this,
						"Connection Error",
						"Please verify your internet connection", false);

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);

					Log.wtf("error1", Content);

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {
						Toast.makeText(EditIntLangActivity.this,
								"Token is not valid!", Toast.LENGTH_LONG)
								.show();
					}

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("ok")) {
						Toast.makeText(EditIntLangActivity.this, "Saved",
								Toast.LENGTH_LONG).show();
						HashMap<String, String> m_li;
						m_li = new HashMap<String, String>();
						m_li.put(Constants.USER_ID,
								GET_USERDETAILS.get(0).get(Constants.USER_ID)
										.toString());
						m_li.put(
								Constants.USER_FIRSTNAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_FIRSTNAME)
										.toString());
						m_li.put(Constants.USER_LASTNAME, GET_USERDETAILS
								.get(0).get(Constants.USER_LASTNAME).toString());
						m_li.put(Constants.USER_GENDER, GET_USERDETAILS.get(0)
								.get(Constants.USER_GENDER).toString());
						m_li.put(Constants.USER_DATEOFBIRTH, GET_USERDETAILS
								.get(0).get(Constants.USER_DATEOFBIRTH)
								.toString());
						m_li.put(Constants.USER_EMAIL, GET_USERDETAILS.get(0)
								.get(Constants.USER_EMAIL).toString());
						m_li.put(Constants.USER_PHONENUMBER, GET_USERDETAILS
								.get(0).get(Constants.USER_PHONENUMBER)
								.toString());
						m_li.put(
								Constants.USER_IMAGENAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_IMAGENAME)
										.toString());
						m_li.put(
								Constants.USER_HOMECOUNT,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_HOMECOUNT)
										.toString());
						m_li.put(Constants.USER_INTERESTCOUNT,
								textUserInterestedCountry.getTag().toString());
						m_li.put(Constants.USER_LANGUAGE, textUserLanguage
								.getTag().toString());
						m_li.put(Constants.USER_DESCRIPTION, GET_USERDETAILS
								.get(0).get(Constants.USER_DESCRIPTION)
								.toString());
						m_li.put(Constants.USER_WEBSITE, GET_USERDETAILS.get(0)
								.get(Constants.USER_WEBSITE).toString());
						m_li.put(Constants.USER_STUDY, GET_USERDETAILS.get(0)
								.get(Constants.USER_STUDY).toString());
						m_li.put(Constants.USER_BUS, GET_USERDETAILS.get(0)
								.get(Constants.USER_BUS).toString());
						m_li.put(Constants.USER_TRAVAL, GET_USERDETAILS.get(0)
								.get(Constants.USER_TRAVAL).toString());
						m_li.put(Constants.USER_SOCCULT, GET_USERDETAILS.get(0)
								.get(Constants.USER_SOCCULT).toString());
						m_li.put(Constants.USER_COMPANY, GET_USERDETAILS.get(0)
								.get(Constants.USER_COMPANY).toString());
						m_li.put(Constants.USER_JOBPOSITION, GET_USERDETAILS
								.get(0).get(Constants.USER_JOBPOSITION)
								.toString());
						m_li.put(Constants.USER_COMPANYWEBSITE, GET_USERDETAILS
								.get(0).get(Constants.USER_COMPANYWEBSITE)
								.toString());
						m_li.put(Constants.USER_SCHOOL, GET_USERDETAILS.get(0)
								.get(Constants.USER_SCHOOL).toString());
						m_li.put(Constants.USER_DEGREE, GET_USERDETAILS.get(0)
								.get(Constants.USER_DEGREE).toString());
						m_li.put(
								Constants.USER_SCHOOLURL,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_SCHOOLURL)
										.toString());
						m_li.put(Constants.USER_LATITUDE, GET_USERDETAILS
								.get(0).get(Constants.USER_LATITUDE).toString());
						m_li.put(
								Constants.USER_LONGITUDE,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_LONGITUDE)
										.toString());
						m_li.put(Constants.USER_DATEOFBIRTH_YESNO, GET_USERDETAILS.get(0).get(Constants.USER_DATEOFBIRTH_YESNO)
								.toString());
						m_li.put(Constants.USER_PHONENUMBER_YESNO, GET_USERDETAILS.get(0).get(Constants.USER_PHONENUMBER_YESNO)
								.toString());
						m_li.put(Constants.USER_EMAIL_YESNO, GET_USERDETAILS.get(0).get(Constants.USER_EMAIL_YESNO)
								.toString());
						GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						//GET_USERDETAILS.set(0, m_li);
						GET_USERDETAILS.add(m_li);
						HomeActivity.sharedInstances.GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						HomeActivity.sharedInstances.GET_USERDETAILS = GET_USERDETAILS;

						// finish();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

}
