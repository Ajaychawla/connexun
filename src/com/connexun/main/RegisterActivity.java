package com.connexun.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.adapters.SpinnerAdapters;
import com.connexun.main.R;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class RegisterActivity extends Activity {

	String[] originCountry_list = { "Country of Origin", "Brazil", "Canada",
			"China", "Italy", "India" };

	String[] interestCountry_list = { "Country of Interest", "Brazil",
			"Canada", "China", "Italy", "India" };

	Integer[] originCountry_image = { R.drawable.home_icon, R.drawable.br,
			R.drawable.ca, R.drawable.cn, R.drawable.it, R.drawable.in };

	Integer[] interestCountry_image = { R.drawable.interest_icon,
			R.drawable.br, R.drawable.ca, R.drawable.cn, R.drawable.it,
			R.drawable.in };

	EditText editFirstname, editLastname, editEmail, editPassword,
			editReconfirmPassword;
	Spinner spinnerOriginCountry, spinnerInterestCountry;

	ArrayList<HashMap<String, String>> GET_COUNTRYLIST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_ORIGIN = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
	public static String COUNTRYCODE = "countrycode";
	public static String ID = "id";
	public static String RELATED = "related";
	public static String COUNTRYNAME = "countryname";
	public static String CURRENCYCODE = "currencycode";
	public static String CURRENCYNAME = "currencyname";
	public static String LATITUDE = "latitude";
	public static String LONGITUDE = "longitude";
	public static String WOEID = "woeid";
	public static String LANGUAGE = "Laguages";

	public static String firstname = "firstname", lastname = "lastname",
			email = "email", password = "password",
			countryorigin = "countryorigin",
			countryinterest = "countryinterest", language = "language";
	TextView registerButton;
	TextView backButton, clickTerms;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		createSpinnerContent();
		registerButton = (TextView) findViewById(R.id.registerButton);
		backButton = (TextView) findViewById(R.id.backButton);

		editFirstname = (EditText) findViewById(R.id.editFirstname);
		editLastname = (EditText) findViewById(R.id.editLastname);
		editEmail = (EditText) findViewById(R.id.editEmail);
		editPassword = (EditText) findViewById(R.id.editPassword);
		editReconfirmPassword = (EditText) findViewById(R.id.editReconfirmPassword);

		spinnerOriginCountry = (Spinner) findViewById(R.id.spinnerOriginCountry);
		spinnerInterestCountry = (Spinner) findViewById(R.id.spinnerInterestCountry);
		clickTerms = (TextView) findViewById(R.id.clickTerms);
		// spinnerOriginCountry.setAdapter(new spinnerInterest(
		// RegisterActivity.this, R.layout.spinner_country,
		// originCountry_image, originCountry_list));
		spinnerOriginCountry.setAdapter(new SpinnerAdapters(
				RegisterActivity.this, GET_COUNTRYLIST_ORIGIN));

		HashMap<String, String> m_li;
		m_li = new HashMap<String, String>();
		m_li.put(COUNTRYCODE, "interest_icon");
		m_li.put(ID, "0");
		m_li.put(RELATED, "1");
		m_li.put(
				COUNTRYNAME,
				RegisterActivity.this.getResources().getString(
						R.string.Alert_registercontry_of_interest)
						+ "");
		m_li.put(CURRENCYCODE, "EUR");
		m_li.put(CURRENCYNAME, "");
		m_li.put(LATITUDE, "");
		m_li.put(LONGITUDE, "");
		m_li.put(WOEID, "");

		GET_COUNTRYLIST_INTEREST.add(m_li);

		spinnerInterestCountry.setAdapter(new SpinnerAdapters(
				RegisterActivity.this, GET_COUNTRYLIST_INTEREST));

		// spinnerInterestCountry.setAdapter(new spinnerInterest(
		// RegisterActivity.this, R.layout.spinner_country,
		// interestCountry_image, interestCountry_list));

		registerButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (isValidated()) {
					Intent i = new Intent(RegisterActivity.this,
							Register2Activity.class);
					i.putExtra(firstname, editFirstname.getText().toString());
					i.putExtra(lastname, editLastname.getText().toString());
					i.putExtra(email, editEmail.getText().toString());
					i.putExtra(password, editPassword.getText().toString());
					i.putExtra(
							countryorigin,
							GET_COUNTRYLIST_ORIGIN.get(
									spinnerOriginCountry
											.getSelectedItemPosition()).get(
									COUNTRYCODE));
					i.putExtra(
							countryinterest,
							GET_COUNTRYLIST_INTEREST.get(
									spinnerInterestCountry
											.getSelectedItemPosition()).get(
									COUNTRYCODE));
					String lang[] = GET_COUNTRYLIST_ORIGIN
							.get(spinnerOriginCountry.getSelectedItemPosition())
							.get(LANGUAGE).split(",");
					String language_=setLanguage();
					Log.e("language", language_+"-----");
					i.putExtra(language, language_);
					// RegisterActivity.this.startActivity(i);
					startActivityForResult(i, 1);

					// finish();
				}
			}
		});
		spinnerOriginCountry
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						if (position > 0) {
							createInterestSpinnerContent(position - 1);

							Log.e("GET_COUNTRYLIST_INTEREST",
									GET_COUNTRYLIST_INTEREST + "-----");
							spinnerInterestCountry
									.setAdapter(new SpinnerAdapters(
											RegisterActivity.this,
											GET_COUNTRYLIST_INTEREST));
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		spinnerInterestCountry
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						if (position > 0) {

						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
			}
		});
		clickTerms.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RegisterActivity.this,
						TermsandCondition.class);
				// Intent intent = new Intent(Intent.ACTION_VIEW,
				// Uri.parse(Constants.TERMSANDCONDITION_URL));
				startActivity(intent);
				// TermsNconditionsPOPUP();
			}
		});
		
	}

	private boolean isValidated() {
		// editFirstname.setError(null);
		// editLastname.setError(null);
		// editEmail.setError(null);
		// editPassword.setError(null);
		// editReconfirmPassword.setError(null);

		if (spinnerOriginCountry.getSelectedItemPosition() == 0) {
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValiditycont_origin)
							+ "", Toast.LENGTH_LONG).show();

			return false;
		} else if (spinnerInterestCountry.getSelectedItemPosition() == 0) {

			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValiditycont_interest)
							+ "", Toast.LENGTH_LONG).show();

			return false;
		} else if (editFirstname.getText().toString().equalsIgnoreCase("")) {

			// editFirstname
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter firstname</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_firstname)
							+ "", Toast.LENGTH_LONG).show();

			return false;
		} else if (editLastname.getText().toString().equalsIgnoreCase("")) {

			// editLastname
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter lastname</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_lastname)
							+ "", Toast.LENGTH_LONG).show();

			return false;
		} else if (editEmail.getText().toString().equalsIgnoreCase("")) {
			// editEmail.setError(Html
			// .fromHtml("<font color='red'>Please enter email</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_email)
							+ "", Toast.LENGTH_LONG).show();
			return false;
		} else if (!editEmail.getText().toString().contains("@")) {
			// editEmail
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter valid email</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_validemail)
							+ "", Toast.LENGTH_LONG).show();
			return false;
		} else if (editPassword.getText().toString().length() < 4) {

			// editPassword
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter password</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_parssfourchar)
							+ "", Toast.LENGTH_LONG).show();
			return false;
		} else if (editPassword.getText().toString().equalsIgnoreCase("")) {

			// editPassword
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter password</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this.getResources().getString(
							R.string.Alert_registerValidityenter_password)
							+ "", Toast.LENGTH_LONG).show();
			return false;
		} else if (editReconfirmPassword.getText().toString()
				.equalsIgnoreCase("")) {

			// editReconfirmPassword
			// .setError(Html
			// .fromHtml("<font color='red'>Please enter password</font>"));
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this
							.getResources()
							.getString(
									R.string.Alert_registerValidityenter_reenterpassword)
							+ "", Toast.LENGTH_LONG).show();
			return false;
		} else if (!editReconfirmPassword.getText().toString()
				.equalsIgnoreCase(editPassword.getText().toString())) {
			Toast.makeText(
					RegisterActivity.this,
					RegisterActivity.this
							.getResources()
							.getString(
									R.string.Alert_registerValidityenter_reenterpassword_notmatch)
							+ "", Toast.LENGTH_LONG).show();

			return false;
		}
		return true;
	}

	public void createSpinnerContent() {
		try {

			JSONObject obj = new JSONObject(loadJSONFromAsset());
			JSONArray m_jArry = obj.getJSONArray("countries");
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			HashMap<String, String> m_li;
			m_li = new HashMap<String, String>();
			m_li.put(COUNTRYCODE, "home_icon");
			m_li.put(ID, "0");
			m_li.put(RELATED, "1");
			m_li.put(COUNTRYNAME, RegisterActivity.this.getResources()
					.getString(R.string.Alert_registercontry_of_origin) + "");
			m_li.put(CURRENCYCODE, "EUR");
			m_li.put(CURRENCYNAME, "");
			m_li.put(LATITUDE, "");
			m_li.put(LONGITUDE, "");
			m_li.put(WOEID, "");

			GET_COUNTRYLIST_ORIGIN.add(m_li);

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				Log.d("Details-->", jo_inside.getString("countrycode"));
				String countrycode = jo_inside.getString(COUNTRYCODE);
				String id = jo_inside.getString(ID);
				String related = jo_inside.getString(RELATED);
				String countryname = jo_inside.getString(COUNTRYNAME);
				String currencycode = jo_inside.getString(CURRENCYCODE);
				String currencyname = jo_inside.getString(CURRENCYNAME);
				String latitude = jo_inside.getString(LATITUDE);
				String longitude = jo_inside.getString(LONGITUDE);
				String woeid = jo_inside.getString(WOEID);
				String language = jo_inside.getString(LANGUAGE);

				// Add your values in your `ArrayList` as below:

				m_li = new HashMap<String, String>();
				m_li.put(COUNTRYCODE, countrycode);
				m_li.put(ID, id);
				m_li.put(RELATED, related);
				m_li.put(COUNTRYNAME, countryname);
				m_li.put(CURRENCYCODE, currencycode);
				m_li.put(CURRENCYNAME, currencyname);
				m_li.put(LATITUDE, latitude);
				m_li.put(LONGITUDE, longitude);
				m_li.put(WOEID, woeid);
				m_li.put(LANGUAGE, language);
				//GET_COUNTRYLIST_ORIGIN.add(m_li);
				GET_COUNTRYLIST.add(m_li);
			}
			
			Collections.sort(GET_COUNTRYLIST, new Comparator<HashMap< String,String >>() {

			    @Override
			    public int compare(HashMap<String, String> lhs,
			            HashMap<String, String> rhs) {
			        // Do your comparison logic here and retrn accordingly.
			    	 String firstValue = lhs.get(COUNTRYNAME);
			         String secondValue = rhs.get(COUNTRYNAME);
			         return firstValue.compareTo(secondValue);
			    }
			});
			GET_COUNTRYLIST_ORIGIN.addAll(GET_COUNTRYLIST);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String loadJSONFromAsset() {
		String json = null;
		try {

			InputStream is = getAssets().open("json/CountryList.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	public void createInterestSpinnerContent(int position) {

		GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_COUNTRYLIST.get(position);
		HashMap<String, String> m_li;
		m_li = new HashMap<String, String>();
		m_li.put(COUNTRYCODE, "interest_icon");
		m_li.put(ID, "0");
		m_li.put(RELATED, "1");
		m_li.put(
				COUNTRYNAME,
				RegisterActivity.this.getResources().getString(
						R.string.Alert_registercontry_of_interest)
						+ "");
		m_li.put(CURRENCYCODE, "EUR");
		m_li.put(CURRENCYNAME, "");
		m_li.put(LATITUDE, "");
		m_li.put(LONGITUDE, "");
		m_li.put(WOEID, "");

		GET_COUNTRYLIST_INTEREST.add(m_li);
		// label.setText(map.get(RegisterActivity.COUNTRYNAME));
		ArrayList<HashMap<String, String>> arrylist_ = new ArrayList<HashMap<String, String>>();
		String itemsCount = map.get(RegisterActivity.RELATED);
		if (itemsCount.contains(",")) {
			String items[] = itemsCount.split(",");
			for (int i = 0; i < items.length; i++) {
				arrylist_.add(getIndexOFValue(items[i], GET_COUNTRYLIST));
			}
		} else {
			arrylist_.add(getIndexOFValue(itemsCount, GET_COUNTRYLIST));
		}
		Collections.sort(arrylist_, new Comparator<HashMap< String,String >>() {

		    @Override
		    public int compare(HashMap<String, String> lhs,
		            HashMap<String, String> rhs) {
		        // Do your comparison logic here and retrn accordingly.
		    	 String firstValue = lhs.get(COUNTRYNAME);
		         String secondValue = rhs.get(COUNTRYNAME);
		         return firstValue.compareTo(secondValue);
		    }
		});
		GET_COUNTRYLIST_INTEREST.addAll(arrylist_);

	}

	public void TermsNconditionsPOPUP() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Terms and Condition");
		alert.setCancelable(false);
		WebView wv = new WebView(this);
		wv.loadUrl(Constants.TERMSANDCONDITION_URL);
		wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);

				return true;
			}
		});

		alert.setView(wv);
		alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});

		alert.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			Intent in = getIntent();
			setResult(RESULT_OK, in);
			RegisterActivity.this.finish();
		}
	}
	public HashMap<String, String> getIndexOFValue(String value, ArrayList<HashMap<String, String>> listMap) {

	 
	    for (int i = 0;i<listMap.size();i++) {
	        if (listMap.get(i).get(ID).equalsIgnoreCase(value)) {
	            return listMap.get(i);
	        } 
	        
	    }
	    return null;
	}
	
	public String setLanguage(){
		
		String language=Locale.getDefault().getLanguage();
		Log.e("local", language+"--");
		String orginlang[]=GET_COUNTRYLIST_ORIGIN
		.get(spinnerOriginCountry.getSelectedItemPosition())
		.get(LANGUAGE).split(",");
		
		String interustlang[]=GET_COUNTRYLIST_INTEREST.get(
				spinnerInterestCountry
						.getSelectedItemPosition()).get(LANGUAGE).split(",");
		for(int i=0;i<interustlang.length;i++){
			if(language.equalsIgnoreCase(interustlang[i])){
				return language;
			}
		}
		
		for(int i=0;i<orginlang.length;i++){
			if(language.equalsIgnoreCase(orginlang[i])){
				return language;
			}
		}
		
		
		return language=orginlang[0];
	}
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(RegisterActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(RegisterActivity.this,
//				new SessionManager_email(RegisterActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE)
//						.toString());
//
//	}
	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "Registration");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}
}
