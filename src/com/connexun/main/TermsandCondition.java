package com.connexun.main;

import com.connexun.main.R;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.Constants;
import com.connexun.utils.SessionManager_email;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

public class TermsandCondition extends Activity {

	WebView NewsWebview;
	private static final String TAG = "Main";
	private ProgressDialog progressBar;

	ImageButton imgbtnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_termscandcondition);
		NewsWebview = (WebView) findViewById(R.id.NewsWebview);
		imgbtnBack = (ImageButton) findViewById(R.id.imgbtnBack);

		WebSettings settings = NewsWebview.getSettings();
		settings.setJavaScriptEnabled(true);
		NewsWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		progressBar = ProgressDialog.show(TermsandCondition.this, "",
				TermsandCondition.this.getResources().getString(
						R.string.Alert_Pleasewait)
						+ "");
		imgbtnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		NewsWebview.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.i(TAG, "Processing webview url click...");
				view.loadUrl(url);
				return true;
			}

			public void onPageFinished(WebView view, String url) {
				Log.i(TAG, "Finished loading URL: " + url);
				if (progressBar.isShowing()) {
					progressBar.dismiss();
				}
			}

			@SuppressWarnings("deprecation")
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Log.e(TAG, "Error: " + description);
				Toast.makeText(TermsandCondition.this, "Oh no! " + description,
						Toast.LENGTH_SHORT).show();
				alertDialog.setTitle("Error");
				alertDialog.setMessage(description);
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								return;
							}
						});
				alertDialog.show();
			}
		});
		NewsWebview.loadUrl(Constants.TERMSANDCONDITION_URL);

	}
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(TermsandCondition.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(TermsandCondition.this,
//				new SessionManager_email(TermsandCondition.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE)
//						.toString());
//
//	}
}
