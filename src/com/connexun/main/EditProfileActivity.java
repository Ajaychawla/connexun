package com.connexun.main;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.adapters.ContrySipnnerAdapter;
import com.connexun.main.R;

import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.AsyncImageLoader;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.connexun.widgets.EditLangCustom;
import com.connexun.widgets.MyCustomAnimation;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class EditProfileActivity extends Activity {

	// Head Title of Views
	TextView textHeadPerInfo, textHeadAboutMe, textHeadInterest,
			textHeadWorkEducation, textHeadSecurity, textLogout;

	//iiii
	// Child Container of Views
	LinearLayout linearChieldPerInfo, linearChieldAboutMe,
			linearChieldInterest, linearChieldWorkEducation,
			linearChieldSecurity;
	SessionManager session;
	SessionManager_email session_email;
	// Personal Information Section View
	ImageView PI_imgUserImage, PI_imgEdituser;
	TextView PI_textFirstname, PI_textLastname, PI_textSexMale,
			PI_textSexFemale, PI_textDOB, PI_textEmail, PI_textPhoneNo;
	LinearLayout PI_maleLinear, PI_femaleLinear;
	ImageButton imgbtnEditFirstName, imgbtnEditLastName, imgbtnEditDOB,
			imgbtnEditEmail, imgbtnEditPhoneNumber;

	// About me Section View
	TextView textDescDesc, textDescWebsite;
	ImageButton imgbtnEditDescription, imgbtnEditWebsiteAbout;

	// Interest Section View
	CheckBox checkBusiness, checkEducation, checkTravels, checkCultural;

	// Work/Education Section View
	TextView textDescWDDesc, textDescWDjoboDesc, textDescWDWebsiteDesc,
			textDescWDSchoolDesc, textDescWDegreeDesc, textDescWWeb2Desc;
	ImageButton imgbtnEditCompanyName, imgbtnEditJobPossition,
			imgbtnEditCompanyWesite, imgbtnEditSchool, imgbtnEditDegree,
			imgbtnEditSchoolWebsite;

	// Security Section View
	TextView textSecurityChangePass;
	ImageButton imgbtnEditPassword;

	TextView backButton, textSaveuserinfo;

	ArrayList<HashMap<String, String>> GET_USERDETAILS = new ArrayList<HashMap<String, String>>();

	ImageLoader imgLoader;
	AlertDialog alertDialog;

	private int year;
	private int month;
	private int day;

	static final int DATE_DIALOG_ID = 999;
	AlertDialogManager alert = new AlertDialogManager();
	String Gender_ = "";
	String CompleteURL = "";
	String OldPassword = "";
	String NewPassword = "";
	String IsDate0fBirthShow = "";
	String IsEmailShow = "";
	String IsPhoneNumShow = "";
	Bitmap bitmap_ = null;
	String base64 = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_profile);
		session = new SessionManager(EditProfileActivity.this);
		session_email = new SessionManager_email(EditProfileActivity.this);

		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}
		GET_USERDETAILS = (ArrayList<HashMap<String, String>>) getIntent()
				.getSerializableExtra("GET_USERDETAILS");
		imgLoader = new ImageLoader(EditProfileActivity.this);

		// Head Title of Views
		textHeadPerInfo = (TextView) findViewById(R.id.textHeadPerInfo);
		textHeadAboutMe = (TextView) findViewById(R.id.textHeadAboutMe);
		textHeadInterest = (TextView) findViewById(R.id.textHeadInterest);
		textHeadWorkEducation = (TextView) findViewById(R.id.textHeadWorkEducation);
		textHeadSecurity = (TextView) findViewById(R.id.textHeadSecurity);
		textLogout = (TextView) findViewById(R.id.textLogout);

		// Child Container of Views
		linearChieldPerInfo = (LinearLayout) findViewById(R.id.linearChieldPerInfo);
		linearChieldAboutMe = (LinearLayout) findViewById(R.id.linearChieldAboutMe);
		linearChieldInterest = (LinearLayout) findViewById(R.id.linearChieldInterest);
		linearChieldWorkEducation = (LinearLayout) findViewById(R.id.linearChieldWorkEducation);
		linearChieldSecurity = (LinearLayout) findViewById(R.id.linearChieldSecurity);

		// Personal Information Section View
		PI_imgUserImage = (ImageView) findViewById(R.id.PI_imgUserImage);
		PI_imgEdituser = (ImageView) findViewById(R.id.PI_imgEdituser);
		PI_textFirstname = (TextView) findViewById(R.id.PI_textFirstname);
		PI_textLastname = (TextView) findViewById(R.id.PI_textLastname);
		PI_textSexMale = (TextView) findViewById(R.id.PI_textSexMale);
		PI_textSexFemale = (TextView) findViewById(R.id.PI_textSexFemale);
		PI_textDOB = (TextView) findViewById(R.id.PI_textDOB);
		PI_textEmail = (TextView) findViewById(R.id.PI_textEmail);
		PI_textPhoneNo = (TextView) findViewById(R.id.PI_textPhoneNo);
		imgbtnEditFirstName = (ImageButton) findViewById(R.id.imgbtnEditFirstName);
		imgbtnEditLastName = (ImageButton) findViewById(R.id.imgbtnEditLastName);
		imgbtnEditDOB = (ImageButton) findViewById(R.id.imgbtnEditDOB);
		imgbtnEditEmail = (ImageButton) findViewById(R.id.imgbtnEditEmail);
		imgbtnEditPhoneNumber = (ImageButton) findViewById(R.id.imgbtnEditPhoneNumber);
		PI_maleLinear = (LinearLayout) findViewById(R.id.PI_maleLinear);
		PI_femaleLinear = (LinearLayout) findViewById(R.id.PI_femaleLinear);

		// About me Section View
		textDescDesc = (TextView) findViewById(R.id.textDescDesc);
		textDescWebsite = (TextView) findViewById(R.id.textDescWebsite);
		imgbtnEditDescription = (ImageButton) findViewById(R.id.imgbtnEditDescription);
		imgbtnEditWebsiteAbout = (ImageButton) findViewById(R.id.imgbtnEditWebsiteAbout);

		// Interest section View
		checkBusiness = (CheckBox) findViewById(R.id.checkBusiness);
		checkEducation = (CheckBox) findViewById(R.id.checkEducation);
		checkTravels = (CheckBox) findViewById(R.id.checkTravels);
		checkCultural = (CheckBox) findViewById(R.id.checkCultural);

		// Work/Education Section View
		textDescWDDesc = (TextView) findViewById(R.id.textDescWDDesc);
		textDescWDjoboDesc = (TextView) findViewById(R.id.textDescWDjoboDesc);
		textDescWDWebsiteDesc = (TextView) findViewById(R.id.textDescWDWebsiteDesc);
		textDescWDSchoolDesc = (TextView) findViewById(R.id.textDescWDSchoolDesc);
		textDescWDegreeDesc = (TextView) findViewById(R.id.textDescWDegreeDesc);
		textDescWWeb2Desc = (TextView) findViewById(R.id.textDescWWeb2Desc);
		imgbtnEditCompanyName = (ImageButton) findViewById(R.id.imgbtnEditCompanyName);
		imgbtnEditJobPossition = (ImageButton) findViewById(R.id.imgbtnEditJobPossition);
		imgbtnEditCompanyWesite = (ImageButton) findViewById(R.id.imgbtnEditCompanyWesite);
		imgbtnEditSchool = (ImageButton) findViewById(R.id.imgbtnEditSchool);
		imgbtnEditDegree = (ImageButton) findViewById(R.id.imgbtnEditDegree);
		imgbtnEditSchoolWebsite = (ImageButton) findViewById(R.id.imgbtnEditSchoolWebsite);

		// Security Section View
		textSecurityChangePass = (TextView) findViewById(R.id.textSecurityChangePass);
		imgbtnEditPassword = (ImageButton) findViewById(R.id.imgbtnEditPassword);

		backButton = (TextView) findViewById(R.id.backButton);
		textSaveuserinfo = (TextView) findViewById(R.id.textSaveuserinfo);

		textHeadPerInfo.setOnClickListener(textClickListner);
		textHeadAboutMe.setOnClickListener(textClickListner);
		textHeadInterest.setOnClickListener(textClickListner);
		textHeadWorkEducation.setOnClickListener(textClickListner);
		textHeadSecurity.setOnClickListener(textClickListner);
		textLogout.setOnClickListener(textClickListner);

		PI_maleLinear.setOnClickListener(SexClick);
		PI_femaleLinear.setOnClickListener(SexClick);

		PI_imgEdituser.setOnClickListener(onClickEditBtn);
		imgbtnEditFirstName.setOnClickListener(onClickEditBtn);
		imgbtnEditLastName.setOnClickListener(onClickEditBtn);
		imgbtnEditDOB.setOnClickListener(onClickEditBtn);
		imgbtnEditEmail.setOnClickListener(onClickEditBtn);
		imgbtnEditPhoneNumber.setOnClickListener(onClickEditBtn);
		imgbtnEditDescription.setOnClickListener(onClickEditBtn);
		imgbtnEditWebsiteAbout.setOnClickListener(onClickEditBtn);
		imgbtnEditCompanyName.setOnClickListener(onClickEditBtn);
		imgbtnEditJobPossition.setOnClickListener(onClickEditBtn);
		imgbtnEditCompanyWesite.setOnClickListener(onClickEditBtn);
		imgbtnEditSchool.setOnClickListener(onClickEditBtn);
		imgbtnEditDegree.setOnClickListener(onClickEditBtn);
		imgbtnEditSchoolWebsite.setOnClickListener(onClickEditBtn);
		imgbtnEditPassword.setOnClickListener(onClickEditBtn);
		PI_textDOB.setOnClickListener(onClickEditBtn);
		PI_textEmail.setOnClickListener(onClickEditBtn);
		PI_textPhoneNo.setOnClickListener(onClickEditBtn);

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		// PI_imgUserImage.setImageBitmap(imgLoader
		// .ConvertBase64ToBitmap(GET_USERDETAILS.get(0)
		// .get(Constants.USER_AVATAR).toString()));

		imgLoader
				.DisplayImage(
						GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
								.toString(), R.drawable.default_avatar_single,
						PI_imgUserImage, 100);
		PI_textFirstname.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_FIRSTNAME).toString());
		PI_textFirstname.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_FIRSTNAME).toString());

		PI_textLastname.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_LASTNAME).toString());
		PI_textLastname.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_LASTNAME).toString());

		String dob_ = GET_USERDETAILS.get(0).get(Constants.USER_DATEOFBIRTH)
				.toString();
		if (dob_.contains("T") && dob_.contains("Z"))
			dob_ = dob_.substring(0, dob_.indexOf("T"));

		PI_textDOB.setText(dob_);
		PI_textDOB.setTag(dob_);
		String email_ = GET_USERDETAILS.get(0).get(Constants.USER_EMAIL)
				.toString();
//		if (email_.length() > 20)
//			email_ = email_.substring(0, 17) + "...";
		PI_textEmail.setText(email_);
		PI_textEmail.setTag(GET_USERDETAILS.get(0).get(Constants.USER_EMAIL)
				.toString());
		String phoneval = GET_USERDETAILS.get(0)
				.get(Constants.USER_PHONENUMBER).toString();
		if (!phoneval.equals("")) {
			if (phoneval.subSequence(0, 1).equals(" ")) {
				phoneval = phoneval.replaceAll(" ", "");
				phoneval = "+" + phoneval;
			}
		}
		PI_textPhoneNo.setText(phoneval);
		PI_textPhoneNo.setTag(phoneval);

		textDescDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_DESCRIPTION).toString());
		textDescDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_DESCRIPTION).toString());

		textDescWebsite.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_WEBSITE).toString());
		textDescWebsite.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_WEBSITE).toString());

		textDescWDDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_COMPANY).toString());
		textDescWDDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_COMPANY).toString());

		textDescWDjoboDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_JOBPOSITION).toString());
		textDescWDjoboDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_JOBPOSITION).toString());

		textDescWDWebsiteDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_COMPANYWEBSITE).toString());
		textDescWDWebsiteDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_COMPANYWEBSITE).toString());

		textDescWDSchoolDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_SCHOOL).toString());
		textDescWDSchoolDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_SCHOOL).toString());

		textDescWDegreeDesc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_DEGREE).toString());
		textDescWDegreeDesc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_DEGREE).toString());

		textDescWWeb2Desc.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_SCHOOLURL).toString());
		textDescWWeb2Desc.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_SCHOOLURL).toString());

		if (GET_USERDETAILS.get(0).get(Constants.USER_BUS).toString()
				.equals("1"))
			checkBusiness.setChecked(true);
		if (GET_USERDETAILS.get(0).get(Constants.USER_STUDY).toString()
				.equals("1"))
			checkEducation.setChecked(true);
		if (GET_USERDETAILS.get(0).get(Constants.USER_TRAVAL).toString()
				.equals("1"))
			checkTravels.setChecked(true);
		if (GET_USERDETAILS.get(0).get(Constants.USER_SOCCULT).toString()
				.equals("1"))
			checkCultural.setChecked(true);

		if (GET_USERDETAILS.get(0).get(Constants.USER_GENDER).toString()
				.equalsIgnoreCase("m"))
			PI_maleLinear.performClick();
		else if (GET_USERDETAILS.get(0).get(Constants.USER_GENDER).toString()
				.equalsIgnoreCase("f"))
			PI_femaleLinear.performClick();

		IsDate0fBirthShow = GET_USERDETAILS.get(0)
				.get(Constants.USER_DATEOFBIRTH_YESNO).toString();
		if (IsDate0fBirthShow.equalsIgnoreCase("N")) {
			IsDate0fBirthShow = "N";
			imgbtnEditDOB.setBackgroundResource(R.drawable.lock_closed_icon);
		} else if (IsDate0fBirthShow.equalsIgnoreCase("Y")) {
			IsDate0fBirthShow = "Y";
			imgbtnEditDOB.setBackgroundResource(R.drawable.check_icon);
		} else {
			IsDate0fBirthShow = "N";
			imgbtnEditDOB.setBackgroundResource(R.drawable.lock_closed_icon);
		}

		IsPhoneNumShow = GET_USERDETAILS.get(0)
				.get(Constants.USER_PHONENUMBER_YESNO).toString();
		if (IsPhoneNumShow.equals("N")) {
			IsPhoneNumShow = "N";
			imgbtnEditPhoneNumber
					.setBackgroundResource(R.drawable.lock_closed_icon);
		} else if (IsPhoneNumShow.equals("Y")) {
			IsPhoneNumShow = "Y";
			imgbtnEditPhoneNumber.setBackgroundResource(R.drawable.check_icon);
		} else {
			IsPhoneNumShow = "N";
			imgbtnEditPhoneNumber
					.setBackgroundResource(R.drawable.lock_closed_icon);
		}

		IsEmailShow = GET_USERDETAILS.get(0).get(Constants.USER_EMAIL_YESNO)
				.toString();
		if (IsEmailShow.equals("N")) {
			IsEmailShow = "N";
			imgbtnEditEmail.setBackgroundResource(R.drawable.lock_closed_icon);
		} else if (IsEmailShow.equals("Y")) {
			IsEmailShow = "Y";
			imgbtnEditEmail.setBackgroundResource(R.drawable.check_icon);
		} else {
			IsEmailShow = "N";
			imgbtnEditEmail.setBackgroundResource(R.drawable.lock_closed_icon);
		}

		setCurrentDateOnView();
		textSaveuserinfo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!base64.equals("")) {
					imgLoader.clearSingleCache(GET_USERDETAILS.get(0)
							.get(Constants.USER_IMAGENAME).toString());
				}
				if (!checkBusiness.isChecked() && !checkEducation.isChecked()
						&& !checkTravels.isChecked()
						&& !checkCultural.isChecked()) {
					Toast.makeText(
							EditProfileActivity.this,
							EditProfileActivity.this
									.getResources()
									.getString(
											R.string.Alert_register2Validity_selectanyone)
									+ "", Toast.LENGTH_LONG).show();
					return;
				}
				if (new ConnectionDetector(EditProfileActivity.this)
						.isConnectingToInternet()) {
					new EditProfileOpration()
							.execute(Constants.USER_EDIT_PROFILE_SAVE + ""
									+ CompleteURL);
				} else {
					try{
					alert.showAlertDialog(
							EditProfileActivity.this,
							EditProfileActivity.this.getResources().getString(
									R.string.Alert_Internet_connection)
									+ "",
							EditProfileActivity.this.getResources().getString(
									R.string.Alert_Internet)
									+ "", false);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

				// if (base64 != "") {
				// new ImageuploadOnServer().execute(Constants.IMAGEUPLOAD_URL
				// + CompleteURL);
				// }
			}
		});

	}

	OnClickListener textClickListner = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			// linearChieldPerInfo.setVisibility(View.GONE);
			// linearChieldAboutMe.setVisibility(View.GONE);
			// linearChieldInterest.setVisibility(View.GONE);
			// linearChieldWorkEducation.setVisibility(View.GONE);
			// linearChieldSecurity.setVisibility(View.GONE);

			switch (v.getId()) {
			case R.id.textHeadPerInfo:
				// linearChieldPerInfo.setVisibility(View.GONE);
				linearChieldAboutMe.setVisibility(View.GONE);
				linearChieldInterest.setVisibility(View.GONE);
				linearChieldWorkEducation.setVisibility(View.GONE);
				linearChieldSecurity.setVisibility(View.GONE);
				textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadWorkEducation.setCompoundDrawablesWithIntrinsicBounds(
						0, 0, R.drawable.ico_accordion_close, 0);
				textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);

				if (linearChieldPerInfo.getVisibility() == View.VISIBLE) {
					// linearChieldPerInfo.setVisibility(View.GONE);
					textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_close, 0);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldPerInfo, MyCustomAnimation.duration_,
					// MyCustomAnimation.COLLAPSE);
					// a.setHeight(250);
					// linearChieldPerInfo.startAnimation(a);
					new MyCustomAnimation().collapse(linearChieldPerInfo);
				} else {
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldPerInfo, MyCustomAnimation.duration_,
					// MyCustomAnimation.EXPAND);
					// // linearChieldPerInfo.setVisibility(View.VISIBLE);
					// a.setHeight(250);
					// linearChieldPerInfo.startAnimation(a);
					new MyCustomAnimation().expand(linearChieldPerInfo);
					textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_open, 0);
				}
				break;
			case R.id.textHeadAboutMe:
				linearChieldPerInfo.setVisibility(View.GONE);
				// linearChieldAboutMe.setVisibility(View.GONE);
				linearChieldInterest.setVisibility(View.GONE);
				linearChieldWorkEducation.setVisibility(View.GONE);
				linearChieldSecurity.setVisibility(View.GONE);

				textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadWorkEducation.setCompoundDrawablesWithIntrinsicBounds(
						0, 0, R.drawable.ico_accordion_close, 0);
				textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);

				if (linearChieldAboutMe.getVisibility() == View.VISIBLE) {
					// linearChieldAboutMe.setVisibility(View.GONE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldAboutMe, MyCustomAnimation.duration_,
					// MyCustomAnimation.COLLAPSE);
					// a.setHeight(80);
					// linearChieldAboutMe.startAnimation(a);
					new MyCustomAnimation().collapse(linearChieldAboutMe);
					textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_close, 0);
				} else {
					// linearChieldAboutMe.setVisibility(View.VISIBLE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldAboutMe, MyCustomAnimation.duration_,
					// MyCustomAnimation.EXPAND);
					// a.setHeight(80);
					// linearChieldAboutMe.startAnimation(a);
					new MyCustomAnimation().expand(linearChieldAboutMe);
					textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_open, 0);
				}
				break;
			case R.id.textHeadInterest:
				linearChieldPerInfo.setVisibility(View.GONE);
				linearChieldAboutMe.setVisibility(View.GONE);
				// linearChieldInterest.setVisibility(View.GONE);
				linearChieldWorkEducation.setVisibility(View.GONE);
				linearChieldSecurity.setVisibility(View.GONE);

				textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadWorkEducation.setCompoundDrawablesWithIntrinsicBounds(
						0, 0, R.drawable.ico_accordion_close, 0);
				textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);

				if (linearChieldInterest.getVisibility() == View.VISIBLE) {
					// linearChieldInterest.setVisibility(View.GONE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldInterest, MyCustomAnimation.duration_,
					// MyCustomAnimation.COLLAPSE);
					// a.setHeight(200);
					// linearChieldInterest.startAnimation(a);
					new MyCustomAnimation().collapse(linearChieldInterest);
					textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_close, 0);
				} else {
					// linearChieldInterest.setVisibility(View.VISIBLE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldInterest, MyCustomAnimation.duration_,
					// MyCustomAnimation.EXPAND);
					// a.setHeight(200);
					// linearChieldInterest.startAnimation(a);
					new MyCustomAnimation().expand(linearChieldInterest);
					textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_open, 0);
				}
				break;
			case R.id.textHeadWorkEducation:
				linearChieldPerInfo.setVisibility(View.GONE);
				linearChieldAboutMe.setVisibility(View.GONE);
				linearChieldInterest.setVisibility(View.GONE);
				// linearChieldWorkEducation.setVisibility(View.GONE);
				linearChieldSecurity.setVisibility(View.GONE);

				textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);

				if (linearChieldWorkEducation.getVisibility() == View.VISIBLE) {
					// linearChieldWorkEducation.setVisibility(View.GONE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldWorkEducation,
					// MyCustomAnimation.duration_,
					// MyCustomAnimation.COLLAPSE);
					// a.setHeight(200);
					// linearChieldWorkEducation.startAnimation(a);
					new MyCustomAnimation().collapse(linearChieldWorkEducation);
					textHeadWorkEducation
							.setCompoundDrawablesWithIntrinsicBounds(0, 0,
									R.drawable.ico_accordion_close, 0);
				} else {
					// linearChieldWorkEducation.setVisibility(View.VISIBLE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldWorkEducation,
					// MyCustomAnimation.duration_,
					// MyCustomAnimation.EXPAND);
					// a.setHeight(200);
					// linearChieldWorkEducation.startAnimation(a);
					new MyCustomAnimation().expand(linearChieldWorkEducation);
					textHeadWorkEducation
							.setCompoundDrawablesWithIntrinsicBounds(0, 0,
									R.drawable.ico_accordion_open, 0);
				}
				break;
			case R.id.textHeadSecurity:
				linearChieldPerInfo.setVisibility(View.GONE);
				linearChieldAboutMe.setVisibility(View.GONE);
				linearChieldInterest.setVisibility(View.GONE);
				linearChieldWorkEducation.setVisibility(View.GONE);
				// linearChieldSecurity.setVisibility(View.GONE);

				textHeadPerInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadAboutMe.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadInterest.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.ico_accordion_close, 0);
				textHeadWorkEducation.setCompoundDrawablesWithIntrinsicBounds(
						0, 0, R.drawable.ico_accordion_close, 0);

				if (linearChieldSecurity.getVisibility() == View.VISIBLE) {
					// linearChieldSecurity.setVisibility(View.GONE);
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldSecurity, MyCustomAnimation.duration_,
					// MyCustomAnimation.COLLAPSE);
					// a.setHeight(60);
					// linearChieldSecurity.startAnimation(a);
					new MyCustomAnimation().collapse(linearChieldSecurity);
					textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_close, 0);
				} else {
					// MyCustomAnimation a = new MyCustomAnimation(
					// linearChieldSecurity, MyCustomAnimation.duration_,
					// MyCustomAnimation.EXPAND);
					// a.setHeight(60);
					// linearChieldSecurity.startAnimation(a);
					new MyCustomAnimation().expand(linearChieldSecurity);
					// linearChieldSecurity.setVisibility(View.VISIBLE);
					textHeadSecurity.setCompoundDrawablesWithIntrinsicBounds(0,
							0, R.drawable.ico_accordion_open, 0);
				}
				break;
			case R.id.textLogout:
				
				EasyTracker easyTracker = EasyTracker.getInstance(EditProfileActivity.this);

				  // MapBuilder.createEvent().build() returns a Map of event fields and values
				  // that are set and sent with the hit.
				  easyTracker.send(MapBuilder
				      .createEvent("Log Out",     // Event category (required)
				                   "Log Out",  // Event action (required)
				                   "Log Out",   // Event label
				                   null)            // Event value
				      .build()
				  );
				imgLoader.clearCache();
				session.logoutUser();

				finish();
				break;

			default:
				break;
			}

		}
	};

	OnClickListener SexClick = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.PI_maleLinear:
				Gender_ = "M";
				// PI_textSexFemale.setTextColor(Color.parseColor("#E7E9EB"));
				// PI_textSexFemale
				// .setBackgroundColor(Color.parseColor("#ffffff"));
				PI_textSexMale.setTextColor(Color.parseColor("#ffffff"));
				PI_textSexFemale.setTextColor(Color.parseColor("#d5d6d9"));

				PI_textSexMale.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.male_button_on, 0, 0, 0);
				PI_textSexFemale.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.female_button_off, 0, 0, 0);

				PI_maleLinear.setBackgroundColor(Color.parseColor("#4fc0ea"));
				PI_femaleLinear.setBackgroundColor(Color.parseColor("#ffffff"));

				// PI_textSexFemale
				// .setBackgroundResource(R.drawable.female_button_off);
				// PI_textSexMale.setBackgroundResource(R.drawable.male_button_on);
				// PI_textSexMale.setTextColor(Color.parseColor("#ffffff"));
				// PI_textSexMale.setBackgroundColor(Color.parseColor("#4FC0EA"));
				break;
			case R.id.PI_femaleLinear:
				Gender_ = "F";

				PI_textSexMale.setTextColor(Color.parseColor("#d5d6d9"));
				PI_textSexFemale.setTextColor(Color.parseColor("#ffffff"));

				PI_textSexMale.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.male_button_off, 0, 0, 0);
				PI_textSexFemale.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.female_button_on, 0, 0, 0);

				PI_maleLinear.setBackgroundColor(Color.parseColor("#ffffff"));
				PI_femaleLinear.setBackgroundColor(Color.parseColor("#e1a0e3"));
				// PI_textSexFemale.setTextColor(Color.parseColor("#ffffff"));
				// PI_textSexFemale
				// .setBackgroundColor(Color.parseColor("#E1A0E3"));
				//
				// PI_textSexMale.setTextColor(Color.parseColor("#E7E9EB"));
				// PI_textSexMale.setBackgroundColor(Color.parseColor("#ffffff"));
				// PI_textSexFemale
				// .setBackgroundResource(R.drawable.female_button_on);
				// PI_textSexMale
				// .setBackgroundResource(R.drawable.male_button_on_copy);
				break;

			default:
				break;
			}

		}
	};

	OnClickListener onClickEditBtn = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.PI_imgEdituser:
				selectImage();
				break;
			case R.id.imgbtnEditFirstName:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_firstname) + "",
						PI_textFirstname,
						PI_textFirstname.getText().toString(), 20);
				break;
			case R.id.imgbtnEditLastName:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_lastname) + "", PI_textLastname,
						PI_textLastname.getText().toString(), 20);
				break;
			case R.id.imgbtnEditDOB:
				if (IsDate0fBirthShow.equals("Y")) {
					IsDate0fBirthShow = "N";
					imgbtnEditDOB
							.setBackgroundResource(R.drawable.lock_closed_icon);
				} else {
					IsDate0fBirthShow = "Y";
					imgbtnEditDOB.setBackgroundResource(R.drawable.check_icon);
				}

				// showDialog(DATE_DIALOG_ID);
				// EditTextDialog("Date Of Birth", PI_textDOB);
				break;
			case R.id.PI_textDOB:
				showDialog(DATE_DIALOG_ID);
				// EditTextDialog("Date Of Birth", PI_textDOB);
				break;
			case R.id.imgbtnEditEmail:
				// EditTextDialog("Lastname", PI_textEmail);
				if (IsEmailShow.equals("Y")) {
					IsEmailShow = "N";
					imgbtnEditEmail
							.setBackgroundResource(R.drawable.lock_closed_icon);
				} else {
					IsEmailShow = "Y";
					imgbtnEditEmail
							.setBackgroundResource(R.drawable.check_icon);
				}
				break;
			case R.id.PI_textEmail:
				// EditTextDialog("Lastname", PI_textEmail);
				break;
			case R.id.imgbtnEditPhoneNumber:
				if (IsPhoneNumShow.equals("Y")) {
					IsPhoneNumShow = "N";
					imgbtnEditPhoneNumber
							.setBackgroundResource(R.drawable.lock_closed_icon);
				} else {
					IsPhoneNumShow = "Y";
					imgbtnEditPhoneNumber
							.setBackgroundResource(R.drawable.check_icon);
				}
				// EditTextDialog("Phone Number", PI_textPhoneNo, PI_textPhoneNo
				// .getText().toString());
				break;
			case R.id.PI_textPhoneNo:
				PhoneDialog(
						EditProfileActivity.this.getResources().getString(
								R.string.EP_phonenumberhint)
								+ "", PI_textPhoneNo, PI_textPhoneNo.getText()
								.toString(), 15);
				break;
			case R.id.imgbtnEditDescription:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_aboutdesc) + "", textDescDesc,
						textDescDesc.getText().toString(), 300);
				break;
			case R.id.imgbtnEditWebsiteAbout:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_aboutweb) + "", textDescWebsite,
						textDescWebsite.getText().toString(), 20);
				break;

			case R.id.imgbtnEditCompanyName:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEcompany) + "", textDescWDDesc,
						textDescWDDesc.getText().toString(), 20);
				break;
			case R.id.imgbtnEditJobPossition:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEjobposition) + "",
						textDescWDjoboDesc, textDescWDjoboDesc.getText()
								.toString(), 20);
				break;
			case R.id.imgbtnEditCompanyWesite:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEwebsite) + "",
						textDescWDWebsiteDesc, textDescWDWebsiteDesc.getText()
								.toString(), 20);
				break;
			case R.id.imgbtnEditSchool:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEschoolandunicersity) + "",
						textDescWDSchoolDesc, textDescWDSchoolDesc.getText()
								.toString(), 20);
				break;
			case R.id.imgbtnEditDegree:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEdegree) + "",
						textDescWDegreeDesc, textDescWDegreeDesc.getText()
								.toString(), 20);
				break;
			case R.id.imgbtnEditSchoolWebsite:
				EditTextDialog(EditProfileActivity.this.getResources()
						.getString(R.string.EP_WEschoolurl) + "",
						textDescWWeb2Desc, textDescWWeb2Desc.getText()
								.toString(), 20);
				break;
			case R.id.imgbtnEditPassword:
				ChangePasswordsDialog();
				break;
			default:
				break;
			}

		}
	};

	public void EditTextDialog(String Message, final TextView SetTextViewId,
			String editTextValue, int maxLength) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// alertDialogBuilder.setMessage(Message);
		// alertDialogBuilder.setView(edittext);
		LayoutInflater inflater = LayoutInflater.from(this);
		View dialogview = inflater.inflate(R.layout.dialog_custom, null);
		final EditText edittextTextArea = (EditText) dialogview
				.findViewById(R.id.edittextTextArea);

		TextView textBackDialog = (TextView) dialogview
				.findViewById(R.id.textBackDialog);
		TextView textTitleDialog = (TextView) dialogview
				.findViewById(R.id.textTitleDialog);
		TextView textSaveDialog = (TextView) dialogview
				.findViewById(R.id.textSaveDialog);

		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(maxLength);
		edittextTextArea.setFilters(fArray);

		// InputMethodManager imm =
		// (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		// imm.showSoftInput(edittextTextArea,
		// InputMethodManager.SHOW_IMPLICIT);

		if (SetTextViewId.getId() == R.id.PI_textPhoneNo)
			edittextTextArea.setInputType(InputType.TYPE_CLASS_NUMBER);

		alertDialogBuilder.setCancelable(false);
		edittextTextArea.setText(editTextValue);
		textTitleDialog.setText(Message);
		textSaveDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SetTextViewId.setText(edittextTextArea.getText().toString());//
				SetTextViewId.setTag(edittextTextArea.getText().toString());
				// SetTagId.setTag(edittextTextArea.getText().toString());
				alertDialog.cancel();
			}
		});
		textBackDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});
		alertDialogBuilder.setView(dialogview);
		// alertDialogBuilder.setPositiveButton("Save",
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface arg0, int arg1) {
		// SetTextViewId.setText(edittext.getText().toString());//
		// edittext.gette
		// SetTagId.setTag(edittext.getText().toString());
		// arg0.dismiss();
		//
		// }
		// });

		alertDialog = alertDialogBuilder.create();

		alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(edittextTextArea,
						InputMethodManager.SHOW_IMPLICIT);
			}
		});
		alertDialog.show();

		// InputMethodManager imm =
		// (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		// imm.showSoftInput(edittextTextArea,
		// InputMethodManager.SHOW_IMPLICIT);
	}

	public void PhoneDialog(String Message, final TextView SetTextViewId,
			String editTextValue, int maxLength) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// alertDialogBuilder.setMessage(Message);
		// alertDialogBuilder.setView(edittext);
		LayoutInflater inflater = LayoutInflater.from(this);
		View dialogview = inflater.inflate(R.layout.dialog_custom_phonenumber,
				null);
		final EditText edittextTextArea = (EditText) dialogview
				.findViewById(R.id.edittextTextArea);

		TextView textBackDialog = (TextView) dialogview
				.findViewById(R.id.textBackDialog);
		TextView textTitleDialog = (TextView) dialogview
				.findViewById(R.id.textTitleDialog);
		TextView textSaveDialog = (TextView) dialogview
				.findViewById(R.id.textSaveDialog);
		LinearLayout contlinear = (LinearLayout) dialogview
				.findViewById(R.id.contlinear);
		final Spinner spinnercontry = (Spinner) dialogview
				.findViewById(R.id.spinnercontry);
		final ImageView imgContChange = (ImageView) dialogview
				.findViewById(R.id.imgContChange);
		final EditText edittextContPhonecode = (EditText) dialogview
				.findViewById(R.id.edittextContPhonecode);
		String josnResult = new JsonLocalFileFetch(EditProfileActivity.this)
				.loadJSONFromAsset("json/country.json");
		final ArrayList<HashMap<String, String>> CONTLIST = new ConnectivityServer()
				.CreateSpinnerCountires(josnResult);
		ContrySipnnerAdapter contspinadapter = new ContrySipnnerAdapter(
				EditProfileActivity.this, R.layout.item_cont_list, CONTLIST);
		spinnercontry.setAdapter(contspinadapter);

		if (!editTextValue.equals("")
				&& (editTextValue.contains("+") || editTextValue.subSequence(0,
						1).equals(" "))) {
			if (editTextValue.subSequence(0, 1).equals(" ")) {
				editTextValue = editTextValue.replaceAll(" ", "");
				editTextValue = "+" + editTextValue;
			}
			String getP = getCountPostionForNumber(editTextValue, CONTLIST);
			String arrayCont[] = getP.split("-");
			Log.e("Cont___position",
					"--"
							+ CONTLIST.get(Integer.parseInt(arrayCont[0]))
							+ "----"
							+ editTextValue.substring(
									Integer.parseInt(arrayCont[1]),
									editTextValue.length()) + "----"
							+ arrayCont[0] + "---" + arrayCont[1]);
			spinnercontry.setSelection(Integer.parseInt(arrayCont[0]));
			edittextTextArea.setText(editTextValue.substring(
					Integer.parseInt(arrayCont[1]), editTextValue.length()));
		} else {
			edittextTextArea.setText(editTextValue);
		}

		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(maxLength);
		edittextTextArea.setFilters(fArray);

		if (SetTextViewId.getId() == R.id.PI_textPhoneNo)
			edittextTextArea.setInputType(InputType.TYPE_CLASS_NUMBER);

		alertDialogBuilder.setCancelable(false);

		textTitleDialog.setText(Message);
		textSaveDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SetTextViewId.setText(edittextContPhonecode.getText()
						.toString() + edittextTextArea.getText().toString());//
				SetTextViewId.setTag(edittextContPhonecode.getText().toString()
						+ edittextTextArea.getText().toString());
				// SetTagId.setTag(edittextTextArea.getText().toString());
				alertDialog.cancel();
			}
		});
		textBackDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});

		contlinear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnercontry.performClick();
			}
		});
		spinnercontry
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						HashMap<String, String> map = new HashMap<String, String>();
						map = CONTLIST.get(position);

						edittextContPhonecode.setText(map.get(
								Constants.COUNTRYLSIT_DIALCODE).toString());
						try {
							// get input stream
							InputStream ims = getAssets().open(
									"flags/"
											+ map.get(
													Constants.COUNTRYLSIT_CODE)
													.toString() + ".png");
							// load image as Drawable
							Drawable d = Drawable.createFromStream(ims, null);
							// set image to ImageView
							imgContChange.setImageDrawable(d);
						} catch (IOException ex) {

						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		alertDialogBuilder.setView(dialogview);

		alertDialog = alertDialogBuilder.create();
		alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(edittextTextArea,
						InputMethodManager.SHOW_IMPLICIT);
			}
		});
		alertDialog.show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, selectedYear);
			cal.set(Calendar.DAY_OF_MONTH, selectedDay);
			cal.set(Calendar.MONTH, selectedMonth);
			// String format = new
			// SimpleDateFormat("E,d MMM, yyyy").format(cal.getTime());
			// String format = new SimpleDateFormat("yyyy-MM-dd").format(cal
			// .getTime());
			String format = selectedYear + "-" + (selectedMonth + 1) + "-"
					+ selectedDay;
			// set selected date into textview
			PI_textDOB.setText(format);
			PI_textDOB.setTag(format);
			// PI_textDOB.setText(new
			// StringBuilder().append(day).append(" ").append(month + 1)
			// .append(", ").append(year)
			// .append(" "));

			// set selected date into datepicker also

		}
	};

	public void setCurrentDateOnView() {

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// set current date into textview

	}

	public class EditProfileOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				EditProfileActivity.this);

		String username = "";
		String firstname = "";
		String lastname = "";
		String homecount = "";
		String interestcount = "";
		String bus = "";
		String study = "";
		String travel = "";
		String soccult = "";
		String gender = "";
		String phnumber = "";
		String dateofbirth = "";
		String language = "";
		String latitude = "";
		String longitude = "";
		String description = "";
		String website = "";
		String companyname = "";
		String jobtitle = "";
		String compwebsite = "";
		String school = "";
		String degree = "";
		String schoolurl = "";
		String emailyesno = "";
		String dateofbirthyesno = "";
		String phnumberyesno = "";
		String base64_ = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

			Dialog.setMessage(EditProfileActivity.this.getResources()
					.getString(R.string.Alert_Pleasewait) + "");
			Dialog.setCancelable(false);
			Dialog.show();

			try {
				// Set Request parameter
				username += "&" + URLEncoder.encode("username", "UTF-8") + "="
						+ PI_textEmail.getTag().toString();
				firstname += "&" + URLEncoder.encode("firstname", "UTF-8")
						+ "=" + PI_textFirstname.getTag().toString();
				lastname += "&" + URLEncoder.encode("lastname", "UTF-8") + "="
						+ PI_textLastname.getTag().toString();
				homecount += "&"
						+ URLEncoder.encode("homecount", "UTF-8")
						+ "="
						+ GET_USERDETAILS.get(0).get(Constants.USER_HOMECOUNT)
								.toString();
				interestcount += "&"
						+ URLEncoder.encode("interestcount", "UTF-8")
						+ "="
						+ GET_USERDETAILS.get(0)
								.get(Constants.USER_INTERESTCOUNT).toString();
				bus += "&" + URLEncoder.encode("bus", "UTF-8") + "="
						+ ((checkBusiness.isChecked()) ? "1" : "0");
				study += "&" + URLEncoder.encode("study", "UTF-8") + "="
						+ ((checkEducation.isChecked()) ? "1" : "0");
				travel += "&" + URLEncoder.encode("travel", "UTF-8") + "="
						+ ((checkTravels.isChecked()) ? "1" : "0");
				soccult += "&" + URLEncoder.encode("soccult", "UTF-8") + "="
						+ ((checkCultural.isChecked()) ? "1" : "0");
				gender += "&" + URLEncoder.encode("gender", "UTF-8") + "="
						+ Gender_;
				phnumber += "&"
						+ URLEncoder.encode("phnumber", "UTF-8")
						+ "="
						+ URLEncoder.encode(PI_textPhoneNo.getTag().toString(),
								"UTF-8");
				dateofbirth += "&" + URLEncoder.encode("dateofbirth", "UTF-8")
						+ "=" + PI_textDOB.getTag().toString();
				language += "&"
						+ URLEncoder.encode("language", "UTF-8")
						+ "="
						+ GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE)
								.toString();
				latitude += "&"
						+ URLEncoder.encode("latitude", "UTF-8")
						+ "="
						+ GET_USERDETAILS.get(0).get(Constants.USER_LATITUDE)
								.toString();
				longitude += "&"
						+ URLEncoder.encode("longitude", "UTF-8")
						+ "="
						+ GET_USERDETAILS.get(0).get(Constants.USER_LONGITUDE)
								.toString();
				description += "&" + URLEncoder.encode("description", "UTF-8")
						+ "=" + textDescDesc.getTag().toString();
				website += "&" + URLEncoder.encode("website", "UTF-8") + "="
						+ textDescWebsite.getTag().toString();
				companyname += "&" + URLEncoder.encode("companyname", "UTF-8")
						+ "=" + textDescWDDesc.getTag().toString();
				jobtitle += "&" + URLEncoder.encode("jobtitle", "UTF-8") + "="
						+ textDescWDjoboDesc.getTag().toString();
				compwebsite += "&" + URLEncoder.encode("compwebsite", "UTF-8")
						+ "=" + textDescWDWebsiteDesc.getTag().toString();
				school += "&" + URLEncoder.encode("school", "UTF-8") + "="
						+ textDescWDSchoolDesc.getTag().toString();
				degree += "&" + URLEncoder.encode("degree", "UTF-8") + "="
						+ textDescWDegreeDesc.getTag().toString();
				schoolurl += "&" + URLEncoder.encode("schoolurl", "UTF-8")
						+ "=" + textDescWWeb2Desc.getTag().toString();
				emailyesno += "&" + URLEncoder.encode("emailyesno", "UTF-8")
						+ "=" + IsEmailShow;
				dateofbirthyesno += "&"
						+ URLEncoder.encode("dateofbirthyesno", "UTF-8") + "="
						+ IsDate0fBirthShow;
				phnumberyesno += "&"
						+ URLEncoder.encode("phnumberyesno", "UTF-8") + "="
						+ IsPhoneNumShow;
				base64_ += "&" + URLEncoder.encode("avatar", "UTF-8") + "="
						+ URLEncoder.encode(base64, "UTF-8");

				Log.e("interest and dob", bus + study + travel + soccult
						+ dateofbirth + homecount + interestcount);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(Constants.RequestTimeOutLimit);
				conn.setReadTimeout(Constants.RequestTimeOutLimit);
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(username);
				wr.write(firstname);
				wr.write(lastname);
				wr.write(homecount);
				wr.write(interestcount);
				wr.write(bus);
				wr.write(study);
				wr.write(travel);
				wr.write(soccult);
				wr.write(gender);
				wr.write(phnumber);
				wr.write(dateofbirth);
				wr.write(language);
				wr.write(latitude);
				wr.write(longitude);
				wr.write(description);
				wr.write(website);
				wr.write(companyname);
				wr.write(jobtitle);
				wr.write(compwebsite);
				wr.write(school);
				wr.write(degree);
				wr.write(schoolurl);
				wr.write(emailyesno);
				wr.write(dateofbirthyesno);
				wr.write(phnumberyesno);
				wr.write(base64_);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						EditProfileActivity.this,
						EditProfileActivity.this.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();
			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);
					if (new ConnectivityServer().CheckExpirationTocken(
							EditProfileActivity.this, Content)) {
						return;
					}
					Log.wtf("error1", Content);

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {
						Toast.makeText(EditProfileActivity.this,
								"Token is not valid!", Toast.LENGTH_LONG)
								.show();
					}

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("ok")) {
						Toast.makeText(
								EditProfileActivity.this,
								EditProfileActivity.this
										.getResources()
										.getString(R.string.Alert_editlang_save)
										+ "", Toast.LENGTH_LONG).show();
						HashMap<String, String> m_li;
						m_li = new HashMap<String, String>();
						m_li.put(Constants.USER_ID,
								GET_USERDETAILS.get(0).get(Constants.USER_ID)
										.toString());
						m_li.put(Constants.USER_FIRSTNAME, PI_textFirstname
								.getTag().toString());
						m_li.put(Constants.USER_LASTNAME, PI_textLastname
								.getTag().toString().toString());
						m_li.put(Constants.USER_GENDER, Gender_);
						m_li.put(Constants.USER_DATEOFBIRTH, PI_textDOB
								.getTag().toString());
						m_li.put(Constants.USER_EMAIL, PI_textEmail.getTag()
								.toString());
						m_li.put(Constants.USER_PHONENUMBER, PI_textPhoneNo
								.getTag().toString());
						m_li.put(
								Constants.USER_IMAGENAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_IMAGENAME)
										.toString());
						m_li.put(
								Constants.USER_HOMECOUNT,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_HOMECOUNT)
										.toString());
						m_li.put(Constants.USER_INTERESTCOUNT, GET_USERDETAILS
								.get(0).get(Constants.USER_INTERESTCOUNT)
								.toString());
						m_li.put(Constants.USER_LANGUAGE, GET_USERDETAILS
								.get(0).get(Constants.USER_LANGUAGE).toString());
						m_li.put(Constants.USER_DESCRIPTION, textDescDesc
								.getTag().toString());
						m_li.put(Constants.USER_WEBSITE, textDescWebsite
								.getTag().toString());
						m_li.put(Constants.USER_STUDY,
								((checkEducation.isChecked()) ? 1 : 0) + "");
						m_li.put(Constants.USER_BUS,
								((checkBusiness.isChecked()) ? 1 : 0) + "");
						m_li.put(Constants.USER_TRAVAL,
								((checkTravels.isChecked()) ? 1 : 0) + "");
						m_li.put(Constants.USER_SOCCULT,
								((checkCultural.isChecked()) ? 1 : 0) + "");
						m_li.put(Constants.USER_COMPANY, textDescWDDesc
								.getTag().toString());
						m_li.put(Constants.USER_JOBPOSITION, textDescWDjoboDesc
								.getTag().toString());
						m_li.put(Constants.USER_COMPANYWEBSITE,
								textDescWDWebsiteDesc.getTag().toString());
						m_li.put(Constants.USER_SCHOOL, textDescWDSchoolDesc
								.getTag().toString());
						m_li.put(Constants.USER_DEGREE, textDescWDegreeDesc
								.getTag().toString());
						m_li.put(Constants.USER_SCHOOLURL, textDescWWeb2Desc
								.getTag().toString());
						m_li.put(Constants.USER_LATITUDE, GET_USERDETAILS
								.get(0).get(Constants.USER_LATITUDE).toString());
						m_li.put(
								Constants.USER_LONGITUDE,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_LONGITUDE)
										.toString());
						m_li.put(Constants.USER_DATEOFBIRTH_YESNO,
								IsDate0fBirthShow);
						m_li.put(Constants.USER_PHONENUMBER_YESNO,
								IsPhoneNumShow);
						m_li.put(Constants.USER_EMAIL_YESNO, IsEmailShow);
						GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						// GET_USERDETAILS.set(0, m_li);
						GET_USERDETAILS.add(m_li);
						HomeActivity.sharedInstances.GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						HomeActivity.sharedInstances.GET_USERDETAILS = GET_USERDETAILS;
						EditLangCustom.GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						EditLangCustom.GET_USERDETAILS = GET_USERDETAILS;

						Intent in = getIntent();
						setResult(RESULT_OK, in);
						finish();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	public void ChangePasswordsDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// alertDialogBuilder.setMessage(Message);
		// alertDialogBuilder.setView(edittext);
		LayoutInflater inflater = LayoutInflater.from(this);
		View dialogview = inflater.inflate(R.layout.change_pass_dialog_custom,
				null);
		final EditText edittextOldPass = (EditText) dialogview
				.findViewById(R.id.edittextOldPass);
		final EditText edittextNewPass = (EditText) dialogview
				.findViewById(R.id.edittextNewPass);
		final EditText edittextConfirmPass = (EditText) dialogview
				.findViewById(R.id.edittextConfirmPass);
		TextView textBackDialog = (TextView) dialogview
				.findViewById(R.id.textBackDialog);
		// TextView textTitleDialog = (TextView) dialogview
		// .findViewById(R.id.textTitleDialog);
		TextView textSaveDialog = (TextView) dialogview
				.findViewById(R.id.textSaveDialog);

		alertDialogBuilder.setCancelable(false);

		textSaveDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// SetTextViewId.setText(edittextTextArea.getText().toString());//

				// SetTagId.setTag(edittextTextArea.getText().toString());
				if (edittextOldPass.getText().equals("")
						|| edittextNewPass.getText().equals("")
						|| edittextConfirmPass.getText().equals("")) {
					Toast.makeText(EditProfileActivity.this, "Empty Field",
							Toast.LENGTH_LONG).show();
					return;
				} else if (!edittextNewPass.getText().toString()
						.equals(edittextConfirmPass.getText().toString())) {
					Toast.makeText(EditProfileActivity.this,
							"New password and confirm pssword doesn't match",
							Toast.LENGTH_LONG).show();
					return;
				} else {
					OldPassword = edittextOldPass.getText().toString();
					NewPassword = edittextNewPass.getText().toString();

					if (new ConnectionDetector(EditProfileActivity.this)
							.isConnectingToInternet()) {
						new PassChangeOpration()
								.execute(Constants.CHANGE_PASSWORDURL + ""
										+ CompleteURL);
					} else {
						try{
						alert.showAlertDialog(
								EditProfileActivity.this,
								EditProfileActivity.this
										.getResources()
										.getString(
												R.string.Alert_Internet_connection)
										+ "",
								EditProfileActivity.this.getResources()
										.getString(R.string.Alert_Internet)
										+ "", false);
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}

				alertDialog.cancel();
			}
		});
		textBackDialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});
		alertDialogBuilder.setView(dialogview);
		// alertDialogBuilder.setPositiveButton("Save",
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface arg0, int arg1) {
		// SetTextViewId.setText(edittext.getText().toString());//
		// edittext.gette
		// SetTagId.setTag(edittext.getText().toString());
		// arg0.dismiss();
		//
		// }
		// });

		alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public class PassChangeOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				EditProfileActivity.this);

		String oldpassword = "";

		String newpassword = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

			Dialog.setMessage(EditProfileActivity.this.getResources()
					.getString(R.string.Alert_Pleasewait) + "");
			Dialog.setCancelable(false);
			Dialog.show();

			try {
				// Set Request parameter
				oldpassword += "&" + URLEncoder.encode("oldpassword", "UTF-8")
						+ "=" + OldPassword;

				newpassword += "&" + URLEncoder.encode("newpassword", "UTF-8")
						+ "=" + NewPassword;

				// Log.e("oldpass and newpass", oldpassword+"---"+newpassword);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;
			// Log.e("urls[0]", urls[0]);
			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(Constants.RequestTimeOutLimit);
				conn.setReadTimeout(Constants.RequestTimeOutLimit);
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(oldpassword);
				wr.write(newpassword);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						EditProfileActivity.this,
						EditProfileActivity.this.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);
					if (new ConnectivityServer().CheckExpirationTocken(
							EditProfileActivity.this, Content)) {
						return;
					}
					Log.wtf("error1", Content);

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {
						Toast.makeText(EditProfileActivity.this,
								"Token is not valid!", Toast.LENGTH_LONG)
								.show();
					} else if (jsonObj.get("response").toString()
							.contains("mandatory")) {
						Toast.makeText(EditProfileActivity.this,
								jsonObj.get("response").toString(),
								Toast.LENGTH_LONG).show();
						// return;
					} else {
						Toast.makeText(EditProfileActivity.this,
								"Password has been changed.", Toast.LENGTH_LONG)
								.show();
						session.createLoginSession(session.getUserDetails()
								.get(SessionManager.KEY_USERID),
								jsonObj.get("response").toString(),session.getUserDetails()
								.get(SessionManager.KEY_EXPERT));

						// finish();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	private void selectImage() {

		final CharSequence[] options = getResources().getStringArray(
				R.array.EP_addphoto_itmes);

		AlertDialog.Builder builder = new AlertDialog.Builder(
				EditProfileActivity.this);
		builder.setTitle(getResources().getString(R.string.EP_addphoto));
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File f = new File(android.os.Environment
							.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, 1);
				} else if (item == 1) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(intent, 2);

				} else if (item == 2) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == 1) {
				File f = new File(Environment.getExternalStorageDirectory()
						.toString());
				for (File temp : f.listFiles()) {
					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {
					Bitmap bitmap = new AsyncImageLoader(
							getApplicationContext())
							.decodeSampledBitmapFromPath(f.getAbsolutePath(),
									Constants.ImageSize, Constants.ImageSize);
					PI_imgUserImage.setImageBitmap(imgLoader.getCroppedBitmap(
							bitmap, bitmap.getWidth()));
					bitmap_ = bitmap;
					ByteArrayOutputStream bao = new ByteArrayOutputStream();

					bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

					byte[] ba = bao.toByteArray();

					String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
					base64 = ba1;
					// Bitmap bitmap;
					// BitmapFactory.Options bitmapOptions = new
					// BitmapFactory.Options();
					//
					// bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
					// bitmapOptions);

					// PI_imgUserImage.setImageBitmap(imgLoader.roundCorner(Bitmap.createScaledBitmap(bitmap,
					// bitmap.getWidth()/2, bitmap.getHeight()/2, true),
					// bitmap.getWidth()/2));

					// String path = android.os.Environment
					// .getExternalStorageDirectory()
					// + File.separator
					// + "Phoenix" + File.separator + "default";
					// f.delete();
					// OutputStream outFile = null;
					// File file = new File(path,
					// String.valueOf(System.currentTimeMillis()) + ".jpg");
					// try {
					// outFile = new FileOutputStream(file);
					// bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
					// outFile.flush();
					// outFile.close();
					// } catch (FileNotFoundException e) {
					// e.printStackTrace();
					// } catch (IOException e) {
					// e.printStackTrace();
					// } catch (Exception e) {
					// e.printStackTrace();
					// }
					// new
					// AsyncImageLoader(getApplicationContext()).showImage(f.getAbsolutePath(),
					// PI_imgUserImage);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (requestCode == 2) {

				Uri selectedImage = data.getData();
				String[] filePath = { MediaStore.Images.Media.DATA };
				Cursor c = getContentResolver().query(selectedImage, filePath,
						null, null, null);
				c.moveToFirst();
				int columnIndex = c.getColumnIndex(filePath[0]);
				String picturePath = c.getString(columnIndex);
				c.close();
				Bitmap bitmap = new AsyncImageLoader(getApplicationContext())
						.decodeSampledBitmapFromPath(picturePath,
								Constants.ImageSize, Constants.ImageSize);

				PI_imgUserImage.setImageBitmap(imgLoader.getCroppedBitmap(
						bitmap, bitmap.getWidth()));

				bitmap_ = bitmap;
				ByteArrayOutputStream bao = new ByteArrayOutputStream();

				bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

				byte[] ba = bao.toByteArray();

				String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
				base64 = ba1;
				// if (bitmap_ != null) {
				// new
				// ImageuploadOnServer().execute(Constants.IMAGEUPLOAD_URL+CompleteURL);
				// }

				// new
				// AsyncImageLoader(getApplicationContext()).showImage(picturePath,
				// PI_imgUserImage);
				// Bitmap bitmap = (BitmapFactory.decodeFile(picturePath));
				// base64="iVBORw0KGgoAAAANSUhEUgAAAMgAAADkCAIAAABSVPyEAAAgAElEQVR4nFy8WbNtS3YW9o2Rc87V7747+/T33K7uLVWvQiUJSSUQWAhhgYAAjCHCEGH7gQj7wS9QkvgPDkdgR2A/OIJwBGDAD9gEKKBEiVJRVKNqdNtz7mn3OWf3q51rzpk5Pj9kzrVPce6Kfddee66cMzNHjvF93xiZ8q//uX3wCI+fwAECAADjOwGgAmk/EAGBrMDaBn2JEHD3Hq4dcDrGN/4Jnn5LsgAGoQc8QMbGRARKA8UEFIDOiwYQkHQJ2AANKUAmkkMAKiCxHQggJOTqMZSC+F2BEFRAgQAQUMClDqRrYsesvR4gYO3XEW9kVBEBqDQRBMIEgDJ1XuSqyTQgITaeGordFUk3pcRP011IgJA0vu2jMQ5z+7QggiC0f5L4nLz6DgQCIUWEhM8BgWtAEBQh2v+wmkkQNEAgGaAg0jOkVgl4MKQOpIcVxEZE0uMHcmuXn7orH96X52PLxNo7CCBGAwgFlbEdNUCQffxjbB7ibIDZFM5J7JKAfKXv8RlC7Kohy7GzhfMzTMbY3kK3h/66QNtBuZpxALCA3gbyXZk8BRdUAZQwCGU1nulGhARAwAwkYBAFCBJhZUlKcXAhdT59GKCW5p4E/JVVpWES0KD4CXsDAZcMTiiMf3UCSRMcGxRcjcWVTax+M2n7205kHO9X7IEEAoTgVRupBVmNsKU5XxmfvfrX9htCgUhsx8WxEpiuhu4VewrtY2UAwAB4QNIiZEhPaCAcANAEBBzp2q6QCOld1eB4igoU5cpjxOUi6TaEiQkIMSUE7nD5Owd30N/C5RSBQn3FkUhaTQIYSBEAZljW0hthcwPLUvIcnQzPPpCLh4ABJjSIvTIeAVkfd38e2sf4RfREogas5lIBEBbnFyTMJf+i0QkxNRgnWqPhy+rJuJrdqy4HkDAkv2iBNCHSqr3yAnHKDUKQYAA9xUu0RRBq7QUBEgCDETQwgHb1iu3AkG5pQGg/Iay9hq1tmSAgNQVLF0TXAgUF0bmb/GdWDFMxhUVbF1CRVlx8H18OROu/V9189b1dfcKMlpEOyMFCmMNyMCML0JFGwiisa17MUQeYwSgUEcS5SiNOCEXiDEIEQnfP/07d4MZbQCGzGcG4SgUQU7ZupTUVgQh8g3mJ0RCbG2gCnj2Uj74jfiISlw2h7WIkQLBusHUP1z/Pk2esZqIQMUAQMpjCHExJkDnYJXMiAI0IIDGqrmbr1TfRgTvABAYxkCTBGDYD4moi23UVx9ElZxDYBtf2jxanEzARunSl+FdtiBRAJZrICjEkD9FOFnllKKYI2dVjmLbGFlevxZ8EU6Q1B3PpSdLzaPQqMEdTSZYkoIESh679ZPWGQAOG1K/0/ACtdb3tsidgSiMZR1JIAzwRAFIMDK94agM9BbK3L2t9mS9oClFCYZp8p2jrYYWZennyA4wO+dpXuFzg/BxQqABAkBRexBg9cDRUAeoFXh5j/U0MejgFXJ+hgPPiMhrF2jEWA5XBcPxA7nxFbv4xvH+KMAdUBDQFJdoCoIASeevVlRBYDgASIM2rsaWdTAeKJDsDEINCNJUMYpJgEGLjgCAGa2YwhZAS4tcJkXiNSYsuo8E5MAMAjaEq+cyrMBtv/Z8928opMgMcxABLs94uDxKACq/u1v6/iREZIDVaum+hJZI5QsCMVCJEt9YCzgyOYAUGUok0egIkFyiEmCCAjpJFg6IEAQlv0fVRCIpEWxSynXdRwBhAiippQirb5RQXmhHWAim6dwd/3zwm57JxgIPbmC9Q1tH8Y3yQ9pXiDyExUNReArC+ht09bFyDB6YXCBWiP6IQEQ8oqGw89t+Uw7fk5BEWpyJyNRwrJxQ/jDOtRoCmZMY4HCKQBKIjngcJRndFQGFOGCOdE+aSghGvcErCbQZztJwAtEEMfGByeynkxVhoFBEKoFBNQUpS6GGCbPaK4a6Mo31UKiEQA4IwB7P4AU3EcoHCMjKL8YtUioerRDyEEC/qIb7tQqAEiIkYBLSMFEqA1BAKA9jA5cwdrCYBZgYXn5lUmjLGn4QuhBSL85seOw1uNAyBIEKlCDmk7Zcpp3OOl0ZnFBpIMQrTyxmEBCl0n+p/LQPrhcymuPY61ncxnsA3oEpE2CHFiIRqEmAT0KSciQ8yHGF7F7u3mK/JZIzFNHpmJs8spKKppRG5+zm4Dl5+LOaFTtJSBuDjqAH+Cpok5paJmKRVG3unlmI6UmCHg2Vsp1YSJjNIjH1CJioUTT7ON8UEEKiYa0FbtKpALSA52QCN6MpADQigj8GLMDDAgsQuJC/VgMl9MkJtGqWBQNAhlQygEgb4xPoQGG+aLDt2LD5ny6SS/4QkwxRGFke2VDm6TpJGQ/QliRMlhGAQk9ZZMq5AtDHUWk5DQRxZKqkUtJ4eyUAFoBjV6MwcoeSrr7RIBQL3Tue3KIRifsnlEjfekN4I4yk8YS766jRhTDA3hikIxAIXpVCl38egj91D6W3x7ILzSypFuXIuIibVVNYPcfNduTjDxQuoiDNGmq7BJALFkBB9DBFUihMYGH1vhG+voIp4TcgYNOGr5NJASHJ4dKAjHTRHpjDSMogqIczBIi6Sls0BUGoOVbCKJvVKtGVifebEJE5Awj0RsEmAkCYMgDmCUA8QloEkGiYWEkRMKNFBSjRxMYEqMiBrJy9aZ+yPtMs1+qormpymPA6lcTXoyVgUIgloSBqcBPNJAdXgAlvwzXaSTcJPuCIxU1Jb21NekacrsMlkzgIB3LvZb1FpKjDMThhMb74J6WA6gyGCLcErjmplLgahSjDMl9BMBl0UOTZ2MdiUyQTzSzivAiGpgBP4pVgmdz6HzUOcHnN2wtzapgE6mMT+0VqyQ01zKVFVWS3fFZpJazeOqSCxjES/EjJGgq5ZLt0MdcMgrRNWtsycSTmIdIyw5koCaPtMqMFZUg4iAokzfsUBCSWcJeRDrAhmJIESQBOoII9PKAnJucQKmcUon0J4SOCP8RnMxQmOChsAUJPbEAFFqImXrbQSYcKGVDNHCKDCZBmEGsUACBQQikEDxKCtVTlQuaIIsrIfiRInko4VRyQ6R5Kg+7R8jaRARGEB45cClRuvgw7zGQCoiAi0fdC4ApI1qEAQPBelZBl6PYjK2o6sH8psydkZJaS5F0UwhAx7b+Laayg25OQY5QVENS13oQnMRVQY4wGoLTCUdsm2wT6x8fZnggqUNm7QdYQKi+YWPw/0DcxEIjKIjCkADpJZMtXoMLLWa8KohDM4g5q0Eww1gakGUZPkvCyhOUc6g1IEUNJBNGqGMQAJRJiDDmLJ7qFEN0ZPiz2WpJRE4yWERmNsMEZDXS1IQqIMxxYlkYKgpAtUo4JqJmbOosPkFWxugxGQFNmob0qcMRFBChTtjKepfwUiv6JrxQWdrnOfKv5e9BUELCM9Lo8gTg5fg8crtoVXmoyj076FSuUxXwCCbheaYbSJjQOZzjg9iQqEUoQC7+kGErrYPMBwS06eST2BKkwQJJkUlDBRUwiDJBxqnWC5hdzoCMLSeF2F9hVeBkFhGu6Ee9miBBgYpzxBAWMM8tFokRHdgMygFDVRixhRlOIIpURzcUJHZhDHyLehXjIvLkbnFj4omIsUokIRi3wIKtHZxL4lgxCoQkhY64BbrhANnMKii8whGISSxEZpGUNcAGlGI2AwUzM1OgsumIvUN+lfhKWXGKlJ7JFADRahd3L2EcUFsg5hYcLVjJtE9S2207ZJtvGFArq3i685CIgAQswprMb5EZDJ3h3xgvkyuVZSVv4wcqUEPwQU1B6zOYJh0IVzGIywsSvTCadn7VALrEa1lLJALdi7ge46Th6zmQtc5KmMEUWDgnGhCEC4yBKZAG0MLmpUEoqoprTrjyJpWTcSST4QeXZa+rFZEbG4NIGWdRJZgAuAiSZnk5pM4QZQBOeBytlCbUzOgBpqFIU604R9Y3oAhYiDqKljFPSTniRAnNiWkbXcIq6T6Kcj1iRb/7exId2c5ZIRMLRcMWItJuQPgQRGkORahyQtL47d1xjaWtwdIZUyxTvAhIxjK0YhAjp52Doo4eqqzkGFmAlNomm2rDt5QbRKGd3bxdfQwkMxRu3H1zx/Dsll7456kbJsA1ByUYBEYQ9tWwIV76WcC0X6fYjDaA1r23J2wel5K51TrEG+JqVD5XH7HlyB42cINVvEnTiNOaOaonX+rVKzooOiUTtphYvYOgQSg0VEJLEdJhqNljkLLTKDNgciQqqZs0Axrz74xiwECixzIUo5yrrjz0f+8UH98fX6/p5/sNM83vHPNvzL3KZBlK6gQgSqlALIaWCwENTMgYIgFqKHY9sXobS0K42wJoU6rWBHZgYJvrbKm4+AaRWeEgIWKOkYZYsr0W7Fo0UEhAYoW8SVABZaupnWmEKEBM1KES+SGZAX4drtAC5nkxzMkOxppRH8BMRiVFcA91b+dxPrJ4QwIgipCI1cvoAQu4diimWVhGADxKFbwAwhLpKVqCgIHvMl8xz9PgwYbaAYyctnrKbRr6k16IxkeCDjhXRy3H1TPHDyDKwRR1wsIZvoerhSjNpRjDllEUr6m0V0cBX242ip0dFSZlQlgW2LGfG4rGNcgRo1UiHk3WZzf7p+cz66XvW2AlH5uhFqtxnvVe/fbn58xz+4aUf7uNjCbBuLbU527XwrHA/9mdiiyQrTHkmykk5TbPrONqUXmsYCJMBETRDVL14hVlmtnDhB0SuTAEJM2NEbvAEKQ0g4qaVREpUqIYUmkr64GgnCYFHdIKJ8LW2WIvkbiRgOFDGStGZjY9oplnWZMxNDVRSXoVrOZj1KHvF7a7Yps7GCvzRCmPUz93b+d1t3AIteTmkRIjSYHMFX2DwQdKRurpoarbE/ROnFt1m8KPRQ4APqGoMeOh2IYLQli6WcPKY0AogZCKzfltDFeCYbm7h+T+YVzp4RHiKQsOovfmK8219a509KFIZbFVgSGYwAOJLDBAbjepIk30FMpE3/xMgFEPDCz//88V/7Ow9+/s8vf+bX3E//mXy4MXv4g5edyemd+r3b4f4uxn3UmpJ48XYmsC5sjYu1cCo2nono0L/9J/XLv1n87J/Xn/kv3Wd+gbm7ePJwGarMmSCIGQ3WxiaSNCbFMsJdaXsd6RwF5hLgi923zMwFEzPxQYKtclfRYpj0qxWyRuSMr+Rc0keirTnHHoVA5s6/dhe58xfnFkLZW+NrXxzWs8XFSU7pSZJAYW0GAhAqIoUBTTOM9jsZRdPEqVnkShQaxZvLJJTZ0++xXGL/p7Xfk8Ui4YP5XG7e5mCNJ89lPkcgpNUIRGRc4sUZBwNoDpdj/zV8vCnL0nJ6UGfncv6A25/TcSkPHuJTb+NzvyLe65M/CDaHqkoICYtgZcqtobW3AGCkSQASxQZAxWpKrv4JMsmVba47ClFxPIUQEVARKPAIZkeHex9tvK7oZlh0f3gma8v8eji+FsY5JAAeMdxFJxBjjgRkgPZR3W6eNM35eOf1n/31u1/5cxcIEyAAjXtx8Z1/dbcJI0aJKBYUIEA0KjeIj5dEkjZRJKIpiSYGGk0gCqW2hQeyYmWrlE1MELcj1rLbVt2+qtyxpKu0GILJCiiENAJvnsE35i/W+sNBd/N47guySqgfybAomSWGGbP8cEJgdtq4NzpfCwmMG7TVytBeDMJ0fsZmxrVN7QylNpjBB3T7WO9hfg6IBIG/Wmix0AVrQ/S6gKCq5eg+lucxviEYlzNs7kpnQ8ZTUcHaJvZvivc8f2H0ERFaS4NTcMCr/wQkA0hHdVjJyy3refWV7IeRdcKirhFZampLKAhQIzA/P83LR9vDRXkx+dY/fvGv/9f56Gx2h9OCoYFv0ChEgQq1AQUyBzGwQvAIAilgAynH9SLrn3/6px6If4jpi9M/fPj1/0sefPgpoofIc6OE0ZKgFVxKMhHEJIRYixXLPnIQCfvbVeo8Il22zi11X1TatF37N1rrz2Ii1wLMosbIlk4SSYwglYu19WpW2XicZ7bcvdFVhslR5TlsWETBC21+RENCJSkUQgziG7q3ir8rKS9tIiagiqlQRYQaq0nEWJ1LOUF/Q3ob4gHv0QRZzvD8A1iJ4ZZYBm8A4AAHeBPJZG2IXAHF6RHGR8i8gkKhX0Io23c1CMpSGi+uI1sHcnFm01OvjMVfEX0wxa8UBSP1MIAeHPTc7c2sqtgEuCS3CWLOVEQjGE1yAq8EzSS4tKIYkTKOltnSNeXzR3909Hv/NLz/rbV8rrfr5RpRwTeoeyiG6M6wKFFvY20LgxrNJUr2e51+T6umA+0oe9eWz8fj8lR2rsnk+env/uPwzd//wry+/cqyaJNwaB+oDayJxGmL4jWJJkJRKIWxSKhdF9Lm31udiQIVrHAWRWKqv4XoUW4nRFSdyxDASI7FGDURQ8Z5ryhndT5fdjLUo81ser6woFp0yyqji3K1AKqRgkSgFh2BILbn3s7/HlxKr1h6ohavMCVZo1RYzsPsnJ1ChjtKh3IJb2INzu/TZlzbRm8NwcO8KEBD6dnroddFVoAqL5/QT0QjCqJUc6ztyGhfJnNkOQCenspyIctLoIRrEFPoUd6JUiJb9hGBOIzOsUOUFQPafEWSrIwtp08mmSh9gl8pwIqlyJ9SuxDLy8XswUePJ2f6mS+7N/rzreddgnMsc2Qj9MaYTjDbwdo2+jWal5iXzr37a1+59Ytvnn1yxHmTw+290bn2i/rtf7t4/EfLl09m3/nm3ePx5wMGSbBJ1TDalsm2c7LyWtEqSOTi+i7QyBU6TEQ8jsmqLEuuoANjuoLRkURfcpWDQCujQVVEJRbGIJoUoiMPmc26WtdVtwrdIm/6RT2+rLVTdHpuvlCDi55T431jHYoTczRNajYA91bxtdYnWzTbSBxawJ9KFk0MsHpu0xMzcLjj8o7UNXpbyDK8+LEtz2TvQNZ2pKrgGwBogjRB+j10u+iOcDHh2ZMAs6jkW81yxtGhciDTOfprYhWPP4Ifa9SGAAtilnILBmVLlJMrhkgTwnjhPSN5pzFVF1lbzNbaILCqR3pF82KqkDDLjKAYRbWu8qYZv/7ueHfndPPBYHu2MbHFBCWBCeZTLDcxGqI7z+xiCNlbe/vPfPFz//0f2/nj+08+en7x0fGuG/VyWf/K5bPx6bf/nT+7uHZ++cXKX1sZQnwDYWgB1cqDxnFusyKxr0aGhMoTO0qCXoLmEeu3CXYTMKwSn62E3+YbW++W3AYs5cugDErTWDEWnM0K5+vQrazodKqM0+ncaafTGelsidqymNGGpNJCg1hEGkTSqQXu0xu/rUATArOgmqodU0YiQbmYBY6qOK3y0xehvkS367KuBJViINWU5x/b/Bz7t+TgOupGygpCLCvUXgZ9jAbQXJ8/DfNJUEEUqMoJ61rXDrUGJkvs7aMncvKJ+ZKqRpq1GaFoJAZJ+Q1hEBgMDuvDLFetanslnRAdrqQEbVrlMZ60ZelMM5IQTgQvUSrFIM+6WzvHg+Xszsvbw6Z4HE5fYLJATWCAXobOcr1T7a/137n+2b/+pXf/5me6dwo3dNNxffwHR9et7zzeXxydyezo8dbl4gsV3wyWReLHVudlwn6Wylds5Y1X7hQgEAxMsPzVxdCWxzKtMuHqGxRYpDNRanmVBqYiXwyHWa5S1SGJAJpkKIgYg7N5nvtl6DfMBr2pVLPFsnCdzmivmC5kuczZSmWttSa4qpEfUgTi3tn4bQMres3F5Vnw5sWo7eVRMklqd6r70xrLU06PQzWxZq69kdx4XZuFndwP8zmv39a96zJfclkKgLJG06DfxdamLANeHhkrcWJRf1qOSY/BnlaGRYWdG5L37OIoWEUBYJbqQihGsZS6slVVMwXrQ6cBi8qQ1gOTIpEgf1sJwLbraAWtFocIViUlEMI5BzdazGVvXr4b1pfL6oPwYoq6j6Ivg7Xb10afvp3f3b3xS6995q9/5s6fuOm2MpDInZoc/f7TjQtxTu5recb12fhLS7zbuPwVg39VDAXUWqAVjf1VWfMnf4vOKRW+XOkwafFHpTjBgBUaT79IqydQGJR0HAwciLIOzCwWjFyZblPlMs+7XPqBKte7Ez9vlr4jPbd+ozOfSznNRFfKcuI+1MjVBYwhUrK5BXNk5syHJgQjBBorxI2kABSX4GPy3SJBEKoLKafiHtU27ex9Nb/zZTc+9i/f898iP/8r2a2b+hgYT0DDi3MJwL1bfOdLblkWH3/D15eWCeHAYMcfNKGDrU9ly6U8O8Pep1yzwCf/vgkTyx3iujO1qEzFSCHClKAGT8e1BhGH1iulIWxHmZGrJ4OLJhlTFolipTrqSN2hEARjtpxfa6bPZnk51cVM2e1u7t+9ee9PvfvaH3+zaVyQ6uDT28VuFw2r58uLo5nToiu93m5ndv/S6ubZs9FR//MN3qJ0wNCWg0TRF8k9rPS6aAAxXYiw8gErNxKLJlJGrV0ybKcWbURM/ktg8YeBoqaRmLWtOlB4ManAKJAxmXgcaVJDkxU1xBnQyWuX2cSKANZLW5yVaqqSg1n7pZhql2BisTFTEQcnGUUhVAf64I1J2qEQZlHYJYIoBK6l9LERjQryLJz+oPnOmdu41VGrnfen74VvL4rP/Epx84ZKjrNLsMHxCQi5d5uf/YVMFB/8HpcXIVfVDGjs7Md1kWPvs9m4wcsL2fmprGrs8e97TDNHIf1VYXCSQ0zEYvogkAGAi2KMgCpXYg3bLGWs40xBNWVTJMpebB0CIrgJgQZ2bVbUi4tmccmlDDd1bV1vHAzuXS/2hps7Rb7mACyezJ98/+TRHx4d3z8p8sGdd69XQMO598t5vVHKTa8d0hAUJmYQIV2gRDwirYvFVbfalRElJxISsNJ3k/LSwoHUQwWCqEjec6EKITBoRFeSpDGJYng0VY3ULH6CdoNX0rxi81qpNGbizfeLuhKdm2PwXNj5B2PkWY5OgCMkmEqq7VC4DIVqIa4r2nPZyLnXb/z9VBUZ4g6bK7KiWCm2q9WBlq4mt+4IDazPbPysCfNGxCSgPuPZEV1H926pOixLBI9FiarB+pYc3FQr5OKcfkGR4ISsZXGOrMD2oZS1lA12bimJyUsgICYQV4kcSctcpK3DpxNzqcYh5cIgeDXvljAB2x0Hbe3IFetnG6koIgHW9Q9u+mcZ60kv637pzVOrPv7u+2fPl9VlNhr0R/tFdRme/MfLb/+LH3/nd3+wrJvQdbWFejybPnxhwV9Ap/lhg7WUIw0a1VsTCxoLyWMEaCiNOIhKLKDQNkXWVj6gBV2rn0IlXKqASUFSRbvOhxCSkzZd1bRFmq8pY70qP9aEsslUkmUCETHhZaFj6RQmMhotrSiafL07kHyUaSa0OngE1zUUlEJ7RT4q8t1u57DTvV0M7hb9O/ngZtY7VPcrf/nvj4+tXhhoSjNN4V1SfVsq545rKG2ZSdOw8gsqotKyNzFmxuXMjp/7ELB/3RV9KUtYjbIUH7C5jv3bmo/04hSLSQDgmIVKLo+DU9m7oXWDBrJ/29U1L0+8hFi2DwVjXXLMuF7FEkkBfsV12z5YLLoXSaVvcVBpnvRxewmVphaXSiyFIyRI3WkeHNpLQdj6/K2v/A9fzXc7Z4+mr/3cvU//6Tc3tntZLuVL39/uykZ+/+PTwcHaz/1XX37jp288/6NHR+896sItJUz0YCl7hCXwJmCbjU5xWkLm6k6XxnmAV801J9TItipqZVYrq0qGRFDF2ryBACIWaM2qliYms2J9iLQ1delqeaVgslXCAQBmsAbhNJOT3prmblm4BTLt9LO1rby/2+vsjLxfLH2Fw4P82qh/vTu42+ndKzp3su517e6IWxNkwgbNnNk7X5FQ6oNvSBhH3mkreuJjOYam3UlM/CRZVYzaLtYAItlglMGMEIfmIjz4elic8LWf6+zuy8kxmiWOXwDErdt46/Ouk+sP/x0vH3lnzDKEJZ5+24cSB593PpemI/d+Ka/qcP59Kl1MsMYlaIi1iRSXKlGk0ZSuF8YkFaK8I4gFeqT6QLMmy2vXcRYaBhX02fJFiz7RSKOiyjlXWAM/ujW898Wd3dfWXr9z6+Dd0dZrAyxZXfqg3Hqt8/k7d7TX5dLe/tyBNdX52eUCDVF0Wedhbi4EJQHLX82mAGJwpJWdje5gbbu//nL7VvngR7PJWQfIaNEa27S6tPE6GkJ0LkmXZywoNajUwiv0L1GmT9jNiETFU7o47k4SRDzjCIMEHxZSnTk7r5pLvpjnrue7rsZ5swylOre2XutadTbtHO71P7/l+k4BKjxR1+CEXLBeWBgHm1pTWzZe4I2vKMV99HtNPQ0ZqCYkkGk+zAk2c281QcC1+ZUWPDImqDSsKFfixEKhOQPnfP7d5WKK13+p2NzR0zNUJY5ewgS3b+C1n5LuqPODr+vxe0ErdnPNB/rsozCrwr2fybOeZmv61i93f1hV0/frooFpW4O9QkWRTARBWBFgCuwn6kYcLJMm2PpBs3kwH1844TV/OS7PJxaxcSs9xJIIkmrLgc0zWIVmKT54WzvovLN3DTDMAwjnJO+on1s+1C/98iEDpKtHPzybnc+IBggZLONcpKFK3GUo5kCFCpMcbuBCRGuxL3918y/+txvf+Fen//R/Ppm9XHdZ35MWYolacsNtbjnF0KhMpklYfRjBUiyPtNY/o4010ca0HbsoqsW5ilUtoWY4Ay6oFIThRnd9/+BsMqvPXqrrSrAsTDWruv0MAfWczcKaufkZmpnZzLMMbAy1qVkQutff+R3Xl83rWhvHpw1qxkhrZL6ZbX+6s/Vm4TZdyFlLsIYSqHJ1pgMkblWL0xI3QiOlDaNPM84uwuQk9NZdd1u9oa6xWCAEZAU29+OtMb40a+Tw3WztNp7f9+MjG6xJZ6D9DRnsuPMzW04ClFebgwWASaJQEs8miP6/LewkHOGAjJot3/wi/tpgVX8AACAASURBVOr/OPzp/yJ78KErz0aynPlyaeikIoPQrhQAUAlnB81Hu1hOUGNv742vvNnZzBB8kp+82BLz49rlko8cxCQDVB9/7/kf/n/fayaX6ygMvNDRNLvVIDPEQs0WAAqogVpb+bKZH2fh/Ku/IZ/9he2bb2QPfnj28mMvcFCHVVY6gsmkXEXV68rDJllCX3n+KKPGzJoKRUUFakzeKhWriyFlNlRRODcqsn5m1SnDeVHYxmYx3Bza5t2FrDkr8+3XzNPVR0WxDFJcnHbHT8Ps6bJ8VjcvG7uobVHDN2KGmGIXuEP7nXKBzpps33Xs6vTMh9KoYmbN3Ady805+7491rn+209l3S+WyDtaIC5FMwpRk3GeSIhHaymsgibkIrC9t8rLJCulvZCaoGywWOJ+yMmzvyPXbTnO9PGejvPt51x/p8/vN+UvTQl1Ht/ZksOXOT62cWkxppoxNAq4iKyFUkIo/lXSUzODMWF67PftbX9v4+V/d6rjxN//Z/ac/OOHsiPB0wyhlU2LNZIzy4pqjg3B/C80ZqvOl37m2d/2dXQhTgqvdYdJZd5oDBqhazf/4z7/7/d/7roR6HR2CE+lfZndq6RH8iZ3zjqIwP7HFE19Pbl+f/bm/cLxxaK7Yenpfnzzs1yEEGp2LfNCgclVnzVhPFo3kitACaZewtCAg7U3QBD7jjqi4eUoUmUo/dxtFftDp3uj073RHb/WyjWx59IL1uaAqJDRLTifG6SSrp01j5WJqi4tQN7NpuZj3mrLDyiRQDSYBrhV02h3l7nr929VLm55QBrL7Zp71ZXru/ZgSRCj1OIyfBF9h61Z2851i506ns5E1Ksua3qJcrHRCh4g4JRHlGJMsOgJHcYRf+PGJJ7W/5aQjTcCyxmSKeimjNRze1uGenp6FySVvvZ11hjh+Zucv2FQYruudt7TYdGenXF6aWlygCqYkcxsQIEp1THxQTTKf5U3R00/99PBX/+pmNz/XF9/88Jt/+NH3llbPoWBn00TjcQQtKCbJ3D/et8dr8MeYvZgd29Te+tyb3f0ezADUMywvIXAW6DKRHCiKo+89/5f/4F8evXxWQDdQCGwm3Yv8TqP9pHSksgSqE9HA5VM0x87pV/98+MU/+YlWT1R6py+6n9zPK98NdQkzMDdDSD2Lp1vELasUu9LqogFFXSpoJISpmjFGECOgCufQzbL1Tmev07/ZGdwtBveK/u28c+iKLdWelJNm8fCx1efeVdVGZzoo5phUGC+yao7x0s3r3BbwVQgsttkZiRO32kQURQdILNGkiLu7/VusuXgRLh/Xy8sw2srXrudNsHLuIZQctuTl43D83NdeOoNs60a+/0axcSsr1pWaBWgwiwVOiuiVkUUe1+ZSVpt8rLbFmWeQ/rrLumIi9FLOMZsDGTYOZPdQFxNOZrjxRjbc0MkFL55zes68I7fecOt77vLCZhceoFKItLs3StZ0jIW5onFXU5DCuhv9wcbw019wX/rciTv/Ru5/GBbT977XWSwKWuOyITAg2s2ZAKhqoe8/vsanOarnmM5RlWfTrd7mrXdvydDBrCkxeVovJ97lUgxV+8XiyeLf/oN/86Pf+14FL7AtFDmwQH7p7lRuXaK4HOBy1+sVmebw56G8T1Y3bvEv/TenB6+dopyjPl3f7K0dbBbrspiX5eUSJKChzT218DamepTm0EpRpuIzBCVETYAAGn0Oyx0HebbZ6Rx0uze6/bvd4eud4a2su++yoVKlKVmObfHczz+p5vdPFpdPZ2gWm0X4pX3/pfXq9W75ZnfxZqd6o1O/3qvu9ZZvDeqDQahGEgZQBVx8Ara5M6okw7qz/lsMlGA2bqYPm8nLOlvX9XuF9rUqg1UhHkBQje3iYXP2zKa15mtu9052683O/pvF2o3curpozIeggY7JhlZiWNSHVpAbDctxU86s23edkXoBDFWN8ZjjqWRd2T9Up2Iit+/p9jUNJpdHfP5JqGtcv5ft3MnGM5uemwSskn5QIu6EFonHJFFNnS+GncHW6Npe+Ys/88Gdvf+ExQNkzdo6H93nw4/7ZCMIqlsBLmhMOUChwsWm/9Ehj2uUR5jXyGpfT5+cj7L1vdv7OsqyLlVD1sf6jUKLbPJw+vV/+PVv/LPfLeuyFg80m+gWQCmY6K2F7qTCb6EW2ukVrJd1+UnTHGc5fvU3l7/8a6dOS5jCl71Bee9Lh+/83EF3rfnk/nw+ruGciUtl/LYifICpKUxj5BFTQuPuOWWurptnG53iRrd/pzd4vTe8W/Rv5t19l6+pqdSlLU7D7FEzvV/PHlblo+Xy6aJ5ObPJ5VTnk0G+3FT3du62vBXmB2ID4UBt5PyIjauapsL5IJ9vSDzWoqVQaFW3ONHubu+3tCHMmwt01pR+8ryqF9YZ5OIyPw1sAhwFYO2ry2p63kwudV46FrK2Jwd33d7dvL+fe4dywdBExhspioBRi0jSL2NUaKyahGpuvQ3X3XQ+xK3rUpaYTFk2MlyTXh+9AXf3deeGjjalnOLJx3Z6HHauucM3i3mFyUkjwRIjb3fppB1gGujqYuDuvdP76q+6X/6Fx5+++59yfQJ6kN11y1z93vdlMuuaLUQLZEOf9hWQEOV4z7+/x/EM5UssPCQDytn0+XvPUcnWwXZ3vVesZ92NnDWOf3DyzX/077/+j3/3Yna+RFVjkYEb6PahNWyaXbvM9pNhKYzWNNWyflovPyHqz/y0/6v/3eXWwSWa2AWHpkSz6OzdvP3WzmRafvKhb5YG5kAGUzUAMInbrcUUQeOZUWpOXbforHU7B53+nd7obm/tte7wVtHddVlP6KWZW3XqZ0+a6cPl9GG5eFJWL5Z+XGFRa93Aajgf4GdSVUPv1tC7lrlerNkTDaoEjOIptZcQZDLIJ+tiDm2ZMtACPInZWXGvD75Gb0YfCm+dAEeprbqsFxe19yGoR6A2FJAdYx64DM1Lv7yw0M3rTOqAtXXZO3S7Nzr5ejaprZqZ1rFkU2FCmnScqIRgKdtIE7IurSxttJUN1l3wMIMoCalqTOeoPU3gcvTWZPtADq67rCPPH/mn932nLwd3C2+cnno2lLTvHxRGPqJaZd2wc6v7l//28Nd+4+Jw7Q9yPASblMRVv7PbXJ6U93/ca0yMC8360EHMswfR3J/uNe9tYn6GxSUqgWWAQublvB43hc+lkf6w11zU97/x4NG3Pnn2/uNH9x/WrDwqQyPAGjojZA3qS7d/nt9oi8MVIsHOm+qjYOMbe/wbf2f6U18+Qx1L/S3pVfUUjWXbdzZ3Oh+9X548NVpEJLGUTQJBE29CFXRdNio6m93ejd7g9d7oXmdwu+jtZllfaagvbfE8TJ7W08fl/JPF/ElZvlyGcR23KohQImvWQA1UNdoyzOnC3vqwt67LYeWtcRMq1HLqDHmZcS2TTpGfFu5iDcxXZZVXOfGYshG4e4OvefEmPuTenBEUimQICMF7IohRvQgSQpcQpPFhEYq+dtfzyUzKCkWB0YbsXM8GO8VsxvllIBlr0SkURzGDpYOt2B621cybxSR0B1l/3TWx7BwQgRnKJaallJUEQoHRCLs3dbjrzs7Cwx+XYWGbhx0tsurCW22qIoqUDZEmz0Nvu/i5X+v92b+ITvl7GH8PqCCGzKABwfJB2N6pH31QHT0fBtbgPMv6hk506X3/ZC981MHiFLMF6nZTtCiyvRu3B2vbD99//smPjj/8T8+evX/iXHF6dvH00aMGlaExBAGG6KwhqxGmunOe3TZmkUlaOGuqD5twvNHFX/nb4U//5rEUsyhSQgjn4QJcA16gt7mxf6fxzYc/8ouxmLogLlCCiKnTPMu2i+7NzvBeb3SvN7zT6V/PijWlSDWxxQs/e1yPH84nj2ezF8vleOlnCywC6wDQKTQxVBpgNKOnGEVI75sKZq8fHN547fp4WLkif8PdG+RrZc5bvLEfrs2Lptvp78x26pc5fa7i4t5802CZ0UlUzJnT3Rl8LThv6i2LRRSpFCKleomoFTGTOOdEYEaKVZdNmCPv5RVkugSU3b5s7Krr5afHYblsEiJQIHiEQEFwpEsiCkCBNVNOL7zLtL+RMYOtDgcTCYZFickE4ynmNSTH1oGs77jZmE8/rGeXvredF8O8WYTgzRwgyAgnCzfSe5/t/5W/PTjYuo+TbwATZIQLy2kVvMu6hNUbe2G0tvjo+3IyGwSbis2R9agd0tb9/UN7BNQnmNXwcdOxgS5f2//Sz9f7d997Vr7/yfjRcVMWa/m1/WOPF8+e+vqSsIAg4BCddRQ1fCmjC3enkSFkHvzDpnq/sePNkf7mf21/6W8dh3By/AC0rNMDm7qeh7qUaqHLWVlOlpUe9PudBx/5F88QAOQ9HRT5brd3rTe40x2+0R3cKjpbTjviGyxP/PzRcvxwMX9QLp6U1ctFMy2trhB8FHwcWhleKSmAxpx8OvBEhAzVEsuma2vro+3rO+dy7pz71M5bPdeZluWt7o0dtzOpZqNisIWdy6Mm1JnCCcQyBFuSpespO4yn0GRsFXTLyI7BhHHndOJaSmd0bSY4xMMrBWK+rE4/auZltfXOmqDzPEjdYHcH115zzS8P7n8fF/eXnJgqaGJLUQ9ALE/bjpl27rI+b46+c7l+Phi+1pehS+eCtknXeonlEhcznlziYE92d/SLf6LXW3cPfrBYPJivjbpur+ePLTSNgyhrydndzX/p1wdvvD3D8+8AE2RZXdrHf9R869+5LB989c/WN+41cM2X/2R48fT4H/4v2clsXZsTYS39tyEbhZ0XKGvUVSrijyzBuju761/6wlnoHLlLDFSQM+sfHt7ev/P6w6fPzr595OABGNjAG4KCPU4czuows+YHwT8k6kGW/+Kv9P7C31xeXp79P/8IH3536/Bmce+nSr+0ixedpu7Vvlh68RjL9vc6228tpt3+elHR5ftr+Xqe91UzmKCurDrz9SzUM99MvV3UmNdp4uKubg2M2erGYqSiUlPlvwIUKNsyMIjQeZqxDsH88eTUHy/r9YkU+kHzGGCoqsfl84zFrJzYpDm/uKgxUvRhcRc1wGB+jPW5Wx800x5meWY5KSRXBX3A1f67IOaopDOBojELnpmIM6GgQ3NhejJpvlPv3ltfu9s/MW0q7O/j3ueKrcPs4z/sPv3RojwrtRJVZW3xTDNTQ9HmI6URapjz/INmdlL1rw+7B0W2ruLSSYox7WeNTC5RLTCd4dqBfOaPd0bb8uEflOPndZ6pDFUaSAioajforG87X539/v/9Q3/0rK6Lqrazp/ij746ePFoLlj18MP2Nv8a3PxPyEf/cX/fz8uj//D9sMh2qf2mLRe52cjsmmgZNDWvADDQEg/avXddhf/boxC/Hvly4YrgoeXlytnF9t3N4s3FrCAuDClgj1PAeUvOsqf7NwkraiaLZHLm7d8Vk8U//d/vkw53vf+d2qNY/ep9/8PuNE9CygMyQB2gIdCN0bgy1u170CbViN0dH65L1mV9e1stJ5WfeKjPzCJY1wXlCxQqFpV3dFEgD+OSX2O7KII2CGHagQYxQMFOrKU3oBHQ3pCN5Vw4zup4U3X6+kfXrpZHY7u41TVXqNGTxUJq4rQ7qct94z3Hv2pyjzfDJpvz8nYWxMmuCW1KDiKBRUCQEQDSIZWYdoxPXcOBdBV9mjWg6qlvgtFSXF/1bg9G9fn8vH23L1g421xAaPH/k739/dvbe3KamTUAwIuqqUTmFiCLEyiihOOnlxVa3f6PXOyiyXorJbIULAnDoD3l4KNsjnD4OH36revlgyWWVSch9U1g93NvojJyVn9RHf5iFCUmY51KaxlEAMwZ/487i7c8ubt1aHr42nk2n/+QfFd/53lrdOMIE4RDVFyA1Fh9gscAw1+7hja29m/vFnU+dhs2zZ+Pq9Kw6PertHPY3d53Yzp3dneujl9/++uNv/Yf5wjssd8DXsXaC+o9QncJqUOCGLt/fZd6rxmd+McmJgw5u9bvdwRDdoRuOZDRqusPQ6apmuqyyRbU573+hdPvVXBZj1kXRoNvMQj2pfVnHw4PEiFzgiGVwNc3BcllRGYCuFuc1FVyZAJJOonPKLF7THpIjwukMs2m/cAdvbF3/2dvazzoF9g9kMJDxJeYTgggG0MDq/R+dP37Poy5AJWGsyvpI18abX2QTOvUPb2SIO3ERt5kbKRoP5g2CuPMsGCyBpe1OZ1zbXILGI6CpFGMGb8348WQ+ng8Oh7Nbg/kyG69hawPXX8+29tbfH+af/IexVaaikSszbuFygky4tLRjSI1NqF9aOG+qo6J7o9fbL1wv3jwd3s6A6VgeLjHbwda2e+eXe6MDPfqRNadQWL7W027HBzazquO03+8gq1WyPHj4Ze1ZLlw5kY/f673/wyLPTrNh1R8uxZX97mzuYcwMdgFXY80jeGRzHKyvHf7mb/+pz3317re+df69b5f1QsYf/bA8+SBU56Otn732xhubu/kv/fq1nb/xF/+3/4nf/H8/6uGkwcSAKfwJqgY5QKBpAp+9sAaaYdCD9HvTe3dffuHL6we3ms1dbGzJaFh1ez7vOFXnfW+xmI1l71L3nz4L//5fTj/8oRnWg3eozAUPZ6ZAsMzlzLSRhnnKwacsRDQj0FKNHaExq+uULsiqWiblAyimHbcmozWVvhaFy7WjwwFGI2QZigwhT3k0R+32s82D+unDCeNBeQ3EqXS6TTUrz5ed/Ur3LjO4WDqbqtrB1qg1bl8nCPVCY/D+ZZjWmVdSPOhgzkCjk3gsejPj+KNqeVFhuR4OO/MJRkPsbMubP9uX3B59e+qPg/PxfhAIvWdgOw4ihHgIg9VWlstyUpaXg+GtfmfDqaa9LCJQol7i+VOenGFjXfbf6G7sZccfV+cfnA+3+PancPv6+Frvw+3ex52OR26UXH0Jv6hrOTsbHj9zj++HH353fnR0cX5RNReZg6nSMZ7rLTlyAWo0DWhwwbrs9C/rojs6ePen5MX336+Pj3IgTI4Xjz9889e/cu3Na+eXl76ejcd5jdHhznLD5v48rBxtlENqhH4m2+vc3qm3dsJwFF5/o/n13/C7t31bwRlLgBTIILbN5ubaHPtDC53RaHl8dHF+PkQhEgiPdFqcg69rNJF7S5sxjGpSe8RRrMF2RqUGaAOaXsFYpmJ/NTjJi0xcCEIYqYKii6xAWJ3jZYDE0y3RHbisK81SASBQmOXo1XMuni0P3yrq3jx7pZQsnmhLuoCsPX3a2o3gKtK4OSsTiyVmIMUMgOWABoHo/0/Vez1Zll1nft/aex97bfrMyqzK8l3V1R5oNBrdQMOQxJAgyCFBM5REcSY4DIohKSSN3vSmf0GakGJmZEI0M0MOh24wIOFIwhONRjfaV3X5ykp/b1577nF7r6WHc26BjIqoqIeMrDTn7L3M9/0+VlSWxf74KC0a/UZjvTlteSdDrKzRpRdapNUHfzvisdOVmEsw9yrXeu2K3AG29auWlMn9shwV7dPN6JSvImLUrJ7qNShSHKeYhOgumeWraq3rfeZl+cjHwuWw0KMj8GCOqnWogKEAJIXTN98ZBd3+vbs0PNG9Y9XvU8pKoDUEsD6sgbWwAPvgbJL9yb987Sf+6UJrefm1L35p59vf0lwCgQFmDx+88RdfXvytXxqOiz/4t3/78Ac9A7n2om6mdPS10lWaTFBA6LZldY3XTpXd5TxuOs8TDThOez1a2WrBCZwBdM0xFwI5CGM2QTlTXuPlT3e/9zeTb32jBCn2xbFDCThmNRcykSbWIKiSiCFqPhUnuKASnCkIWIkE9YS1bkpEpN7F1NdnqVAqAcF4CCOgIubN1Z5OKoMrWh3jR6YYA0TQAqtIfHKSH+fT+xyvs6mFPpVKTuYE37l2r7K1EWmjtDKa0oKqPWOlwLAQmuOpGeDK8M3lhIe3itl+Ea82/bUwnenVU1i7HJeF7L07TXsprKO5IQ4sVAqJktomzeAaOAArZZ+H0zIbNOPTkb+s4c39HPNhflbQ4REWNP/K55s/+RkCGQwTFDlcBFhQWvOsSVczes6T496+1yy2rwSbTpIp9fb13o46OqJZahjKgzbgAq67SCYqH+7yzVd326tvmpDf+ctvmHJ6/qqKmrb/QAaHuPPqayWxo8aDHx4ZwVMfzl78yfT4zfIQpoBVsBtdc/qCWz+Ttrql8ayAIcJMBJMXfHSUX5q1TViLf1Brh6vDxombkc3hSavjr53xyWcuIYrFOGGuvJO1HlXN0UIgcgquHhKxJtECQDRDV82ZwiNDD1BTISuJaQXhnUs4Ax+BX30d9XyXITLn4LaauhFR4kgCggdhIWNUJ85H/PDbvZVrnqluPahKACH48YNVb0vEQIQtF4Cwx1IJ+6QC6tX/DVUgmoqirgHFEFcMZ3ZamH4UrkX5OFzZ1qefaS6dDnbemxzeGpeTUgsJQI7BFa5f8A9gdoAw4Gxmk3tl3k+D01FjI/JaWpladEQgRRCLToTLF3V9oRSFCBE0UD4SvKFy5KI83Bvev5c6NkBpFLoL3F6UzYt0vK/vfxDu7hjPsgJC7S1f8B97Pv/qfxzsHC6/+ZXXlIxCTJ78mP78f6W6q/rt706++Ht576R7+wfvldwxxn/xM+5zXyjOnEsGN5z2abFhm5NpO5TNc66zlrBlZ6mma0KJUkwYT2yWcjMSKPcP16tV/V2CLQClqdn2tA8uQa66+GoVLepCvVr5K+cLa4ZT5Op0nQoqWaOzWLSrNIFzryYJayEIHvVjlZ8B0D7I1BRACJyCFapoXiXgBzpeqEiqlfZLiHUQrznM8v3Z8P2xYU8YzCxSKePmkzR51IqRQMGRQ8miGLqydlZRGUKia7etk5rhQhBXabOYbVkc23I8y46j9LgxPReePuc98xOLhxfiOz8aDe5PXVEqJVrNNakkFTu/fqylBhVBuBwWZTYrDuJwNfaXA9MxFIAdPANS6DRdu1Hxoxgupyo7hqq6X9WCOAVx+d7hcJxU1wE5VnBCiuEUlbYVFK98ItjQVHzTrjaihdP0sU+W7eb+H/zfqj/oGo7OPSe/8T/nC63JzVfbTz3jFdT7098PRyeL7UV89DPZr/zX4/XFSWkFpmxu0Us/3/W/O3zj+9ODHRMv6NpDC1QifFYgoqzICpsBfv1zrrfquj601DxDgsjTVHvmqyqCfizHkjnXFyLiKQlATlQFNDQkXvX81AdOdfiIItGusmZXn4Q9EVdddIBWSpGqAPQA2TkiooZ+oywRtnVzwcArqNSwBAti4+l21D7rijzr3TbQUtNsqt2FgKlav9SOyUpKLUSiSRSJBjz5e0WizIt9VEwhoDqHiVSlzxe2tugXxSRNjhvT3WDjvFlc8Z56ZWX/XmP39snkMClh9aPgHgZsbWKsi89qpAdC4YqBs5NMPwy8xSjeDBtLnu9DwEGQGW8+Ya64+iirRnrulCIQ8qzsneSFgwOzUHWtlDN174fu/gfq3PnoN35rI5bpf76R+jZY3sijBn/u18iLD//TH/ODW+2Sg8lscOoUb14pWKVenJ8+W56/ZF/49OyVn54srkyQsrbClK4+4V76x/GsEV1/M7OFEmFF1aZZ1RNJIiEp2QkKkK6/8vrBKqppDBRBKQBsOUmYRYmGGK7qB9T1GM2bQdR4UiEo4YBJ5mjlmn6LuR6ysmYqUQxH5AAjLnDOOi4rb5+IJpo/WOLmI/S5QskBUNRa1NpjThW5mtNLBXmqE8XnCy8x4hw5IQLrOfaTK68QqHJLVsA8qT151dy0Gr5XPltdk8qFfcyPZiJU1jARZmKCAsrSHk16vaR3U6K2Xt1udE9Hlz+yPjhKe3dHk/2EM6uEVCW0rSyzNQ2gdsEBJB4YLDPr0txNfTodBpsRDIlkpEMAIAUVgnQlDZ2vLFSVKZBMy8FIW2cEFnNwtRMe9UUj/qkvrF97nrN+sf6hdP/7tOh7QkUYeT/7S8n2hfS7X49vvB289aq79rg+/fzJ3beOufR/7Z+nW+cO1jZSClIUAqO4zMnn8x/VjQ258mT45EtWt0tjCgEJCzO7UkSEFNjJJCErBF8BBVjABtDz2CcCeVAGQFnKaCIWJFoqdSgbqR4L/BiqqKtXv04fqrKsVMW0rW84KBEPQo4YZCEeUIOcRZSrLH0gC3KkBR5V4T9l5cEDwGBCtZQD0O56fiyzCUirGhAogIUXttzSpqmBFDVUnumRhKr+7kRqMzJUKSLMuiIw1d4pxVBOKUusRAJ5hIkGQKQEAmbkIAvxBcYJnCQynWaTg8S/Ybor0cJ6c+vyymSlNdidzPqzMrW6slU6JkeAiCJySgAyAlfvsEm4HBYn+SwdBl7Ly5dL4aAugXUAKAjXIU8090tol2VZkjtWppLtVxM8P5TOGaxv+R96UQHTsO099fN6dDSZ9rqhiWFZafv0C3zl8eTBvTDL5P23k4LV4kL4yk90105PYHKUPgpTxdslU15Yaz72ZAwqu129uI6J4yzH9MSbDDEdqTytDnktVrebXmDj517Qp07lSytMgYP14UxdZqomVARIniOZKvl7YXoVfp2Yqqgc6DlzUgMCMBHPpYFKBAw1d59oZq3m0m4RJmiwYuF5NgOziIOqezIHFFzRKmvXa9UzuZIasQ6bLukVgKkQSGCwYRhLTd+IYUGFCwdUdXgymAjzt2FuAhHFQrauWObTuPqI/XuE1rphlLnjAooUKSGWudivsixbsYf58UF6cnMSLzc6pxrrl5bz7XK8N53uTzmZKLIqqHTUGqxhWXJS5MFokhIgaIJV+QDpOB8eoyznD7UOQD6IQQW0gxbAoZRyVu4d8slApZlUpH0HCxLtY2lbby6r5eUUhYLIxQ9HZanf+kHv6KC9dsYHAFZBQy49mXCW/+B7bjBYvHq11VwqwTnyam7iQABLWdgLl1srGwW4NIaGe96tB57W7uTQTCemZK8axikY9UCcVAAAIABJREFUBWPQ3Hmw+vWv0Znt5Mmn3Udfyc9eyGAsqmGF7kCFALLMTqbMpJUIuTmnWlXCp9rhKSTwmUhXB7RUZlRWVasAmocPVh4BrSs0KVXwWTBZATshdmxLBTJkqm3ifM/nCLZWz0OILCNs6qjDzuRKEQtL5ZViRTbysWSEhI1l5UTPWSBVaCSp+fFTq6jEOCGukDPze51AxB6qSrJOaBFoq0iTYhKIsAKIK3lFVZ5X36IiVQ33UzvZGc0OpuFa1N5sLZ9e6C56xbAPGWvfC8MYSvueioNw/9bh0U0Rt0hKkVPigZVSrIXkZKwmCRaXAVT9jAfNMOwye7xbPrirH96LDw4aN294D3cmzjoWB4BVqQyHsWPLix8xOiihNQqnjLv2cW/xTKMohK3Nsqx/nI6HRZKWpS3zIog7cng8YXjtdghP4GwlLBer2u3AW1JkJvAwSbLjA33/dkSwDorQ8EykjSECW8dQlprDLB7faTzYWXnzTbz6WvKpn+y/9Il8cdmhJPgdKA+Q8bAYjEsRHyxkq8ajnjTMpwmAUlRHSUmNtyCgFv08aoqr17o6BjQ0QxyqOxAMskzCsKwYBrpiY1dA90eUXVR2FRSMVmCaSx77pZATFjxKsVJaqY4Rsk5bESeKa+lBRQQiFlVdeKi9aFTRh0G1wahyIc2fRsxB/qjfJzgiQDTV4W/Vq4N5qaa4om7VI6ack4fT2WjcOctLm6mJjmajcWl9oyIYkDHBYvfKi77j46M7BN1BHa0EdqUofTJyvX65ve0DAr+BqOmGcuu94LVXo/feNTsPWoN+K0nUbHQivE/ISjiBEwQAKTiAfLKv/WD07HNxq2kAgstWVtE/Kd/8UW93f3rSL7Nc5aVhp7QqtT65eZsWF/X2Vrx1qrGwZLyoUkarqK2gSogbHk+/++1sf3+JYXzd2L76+Maly1G75YUBKZlMJs22WeyaW+8c79wcTwblMPFf+6F/927jvXfzX/zC4NJTXTS2AAW4gz077GvFqprxVZdYNS6QqiHwFAiCf5BcSkTQ1SKwEi7TI3oSiRATqlq/KkfrRNkqq4OpEj1LFR5VARjqe0ppKA0WmFB110IKMilQm6t9POJ9mDI7EN8n7dUMesyPKKrUMjJvUnkOUhJVzj9KKXoUEYvasFHBHeaBd0q0KE9L6cQxcS025GoW41W5sRUf0XmtIt4cewv9k9HUTdjmVDoeTVISR9AnvdHzz168/KFu7+EhlwZ+k8gAEMfK0PDE3b6Vfei5GCAErXu7rW9/MX71Owt3HjTHM4+tEDmvrduLKAaZs7bVaimx2WSYJylzWcLt7sX/+cvl/f39i2fjhbaZjkdHvaQ3suOxKQtDiMtCCbHxhR0Kh9S6cVrsHRadG8nGOjY2goVu3GwGxqM8swdHk3ffmT14+NTi9laC3pWXX37m05/qrCzOx1icWdvt+M9e9bNJ/9039o4eprffPnnrBw+PhuFXv9I4ONK//j+ce+7MKUAg7sEdNxmHrDSJwAkrYS2iWITr1BBddYUCYF5O1Q16vcOvhOIapKWWqtaA3TqyAFI5e2zBSekOb4+Dw30vCLQxXpnrMhV2QlKtvBQ5Kkd5r3THw744Ry5GVfmrugEAwXAyMmqZlQM0OQKRGJC1dQ3+Yy24yBwlIFUxT0o5RQwYSLWTrsosBSHhR7kLADOLZkKdm8qm/n7rBbtY5InpTMIzs6iT2el09nAmiUcqEMViAMXGN8XA7lw/WVxtGD7JJwYLEStTO1cgswm9/aPs539OfL987dW9P/x/o9f/5mox0axy08Cptca1Z9cee3p17VT8zg933701XDmztdCKB3sHu/ce2KzIpsnC2srMvvGjN7/54P4kDhxUYZkZSilF8PJEFTmHLevYEbQhgJkBKzw9yQ9P5L0P0kY8azWMZyQv3eGhTXtnmosvPP6T186WOP341bDTKFyuyQDEjkHSH5dvXadL20vPf7zbjFVRZF/94vW/+P33Dx7Iqze2iz9aCTezx5+IJ+P83ffzWRmLruYjxApsWMhWz1FFsavW+vNbgqEApUQEDnMya/XyVz4BqnIk65kFAFsW+awvs5Hfp07/wI1o5Hme9sgQK2eratqRIylIIHY6K3enPMh0vhFhhcjUKgnUz4CJVJjnmTMafgRDdZf+aIpUb37n+hWpy3HUvqMKS6Fq+D9X2axS3+QOAhEScopICYnoH/vj1TwwUJzIYEB535b5KLc8gpv6ZI2AlGLyCKEuFWNmb799f7fp46T0bA/BAreWxChRAsdM6sZNd+O96fC4/+//eLizd7VoHfv+zrUnuuevrTzx3NrHPnG60fIAdfbSZvzdQUGm1QiXtzdWHr9olDLaKKXf+2p68u6b3rkki1Pj+do4baQsUMxKV6ioqSy7nXd11Ig3zs0Aa3MCExkw/DyV0ZhtYYXZFap/N6Cpv34hiZ7RGxe2g2bkACYq8rSczsaDcdxq+Z3m/pGbTFwz1nFgz52Nfu23nu+u+P/6f7tR6PMPDpf/w78d/rf/vX90kNy8LsweOStQ4lUnDc/rXxIFRT+m3VZhhYRqe8isuT6fiKis6xSqGQQOFd9CwXLas0d5WGrKoyE1ctPd2iBgcnyoRHnWzEZjpiKM27AuLxLj+fBMhtxh4HSiVJcsPaqKQDD+8HbqtSQ8parJLBEqLUR1GlQqmrn4oT69pIKXSD04g6Aa/tYyXqWcVs6QaCYnykFX5zLXa+Z64jEv5A3BC92u2N2SWQiRQlBdx3Vy4wQgKEcW5WSU+2hqFDwZUtAR36+qUk16f7/8N//HjTz39kYb5OPc4+4zH1v6iX+00Wz7YazCyFQJ4I2Wbnbi0dRleaGV12l1Gaw9yq2djorD96zvuYVzVBYEBaPIOVHEUVNpr9y5o2++1uouRe0VGzcLXZFVcjAX1dBP2CszOXiPe7d95uFidxjt3t852vfbK9Haetxo3Xj11enhcZmk3VNr7Qtb8EzgxYrM0vLycR8Affjl7eu3Zu+/559Mgx+9pf7wj46yJDs67mhWzBBS1aKwfnrcfFuoFaBr4FdVEZdgW4hzQkK+IqgqLF2MgqpiP+ZniIAhJc1SjJXVbafCpLEiy88tXyMnPzpKm3F3Pdq+M7yRYXJ148lZMry/t7O9clH75V18MLV2cpiI1xKPxFU3HEG0UfFYeUvQAahaP1J1Qlb7p+qP4trHy1mOPCVyQmJFad3w/AbNqVVSdaJOERuqgrM10/y9wqPgvOqGfOQJ10QLS5zNeJx6AYGVOPdjR1E9eBPxxfhGrJPSsvMkz8SmojyBKCYtkmf6736gmyvtIMbFjdmv/JO1l1/uKlVLRPLEHh248UxOBs5a8YxyTrLpzNNaBb6D6R08PL71gStTJcrzxTEToIi8QCnjSBXTCT/8oDPNFqmv82m6uDQqc+uItK5MI8JOlO+KUk1PopJ1jnQ0PNzKzh7euf+ju3+ZR82FtdP7710vZxNPeZcanziz/PTJYPR3X361nLmP/swLWm+88XZx9aL3S7905c03x3/4J6PhZPGLXxqXU2ttk1iJaNEiTEIAK2JAnFS/GaqCRxWxgmPJh6oYRKENWgJW01JlWUejXWVFAXV0njDgqs8GSp2xpJwQbNnifjhyrmg6fyoT0l4UxNQwhWdbCw1LeRIVYStuR/rOLApRjg8e5nosLZ/gcc4oyTcdEy6b0mWJLpzxoBTcfBqkULNwSESTWOvGx23VP72mWmsN8tUkdQ8OBtPUGVomMRXwdz6nItEsZCvoBFfje3p0B9P8UqWa8xD43G0p3++s+2SpyJjJQZEmzxVlPs2ISAUq3mhBeHRrKMMAroRNRbWhAAsBKeVJsJBOs2uX5Hd+e/nxa2220h8W6YyKQg6PsoeHdpTq/sHxYP/AOSuiencfCLnm0sLi5uaDV789vHvDg2YhE7GWgiDGiCZpxd7aWhAYf3h/effh5sI6feJF/+xj1DtMDo7K/iifzthakBbSKEVntrp+3NH+bhi8/8Hh7m7vYQ7au/Nu5QlX0Mqn7sbG3fePjt8aiHivf/v6KBk99fRjO/vm5RfaP/Uz4fXbD7/218NkYLhseNqAUA/lpTLsW5QOGmKIdRUHIaxJWUK/F7pb57bTja0gWvKDViM37Q9uFXeun5QcK/EABityRCzsSigLMJS4gFg5lzvXsrN4cjA5XpPuVBczd7Qy2Z8Fk3E4GOS9EdKTxuiA913e7pmhHwsXO8PRDJlPoeeyFIULFpdNIhedKkXl4sVQRhSrdKyKxJHH2igTKc+wzWV4f5X2r27J1lbstVTQjNtnT906zv/6K7vTYWjCJeLKLiGVLwM29cIxoW3LGFrVFIvqLOS67awWZNUJ5rWafhBJPhFolKJDck6scU5yZqt87QrKs0wFAQWeghCccymEASVawWjSyrJaCme//IvLl692XvvRLM1oOLbDsU1ms/F0dnI87O32ju/vHr33fpFOlDHlOLG2UJ5ud1rheB/F0EE564jLKBLPlyDyLmw1HzsXndoM/ZD3HtB3Xms884r73M9ZarSRNE/69vs/HL1zPbVuBl0opZwj68TCMqRIkvfee/2OpLYuQ0uC0fAF1Nvb/f5Xv/32X10vEmtgdr6/50fe1vaGMvq1t+mlFxovPu/9zZd3XLFh/AZXMjRHJMSKmGG8wqdpVoTONKmOYBQtipOxX965cLm8uMERTmiiyIZbW+bUsy3fDm68l1psKKVBJIakSAN14hpRysoaZ0MiI5YtdcS27YPJ/ZmbZr6IKXezB0kwzSO5O3noSnBLHdmjIp+mUW7JcVzYwymXEYoJkgTCtj8xSavjuMlkxQFlPzAH3XDAyah/YpzXlfYGRSvu5HBBHT77bLyqJnJyaCd+wVQOR4ury9vt6fW9Xec1tfKqalyMMFgVk8gfWd0oSUMTnFQ70jqNTlTF2qtbXwcdGa2D5P6+MMQGXtsvU2cDMgEpMpX6iNkCHnnaoSCAbVGxWGoGC1FZqKsfap0/H739Tv7qW8PBYBQGwf3rt+++834+zab94Xh/n13p7MyhEJCGErAuWslxapAQoDy59pT56Kdia+VHrwcHt7uf+Wh0Zvvkh2+44SD44EYs8Pf20r/5Vh4G/MTjlMyyv/nS5s3r7bB9tHJuf+nUVCkrYAc4sAYigYAdpBoxapAHYxDs3Xiwc3NaJoFGKCAUerKX3t85KKBEe5cv8PntRrdDx33PwFRrD7gayy5CQWzaAYr9olSiKmIxGSmFpwdROO4uNSfJsMjTVsPDePbgwSD1/KX28tpquHvYQNCEJjaE2SxoDIpQceo7Z1mLZufsFM6Q1Q+Pbh8Nb9mtNhvv/vFNExkVh3eObmpHerN1NDw8HqX2jFeirIbvAkJJKBSMh5kxORP52tmxGt/2h28utY7OXrw4KNywl5Fol4ZwjvoPEPaGw1YcFw3Oy9Qe7OWD98dLlxMvL0yaFPGWxAvMDkRslBRCrmRHlg20qdH2tW6kBjwBdb60AAKb9w6NzFQY2jTzWh4ighYT++2lQHJO0yxsRJ3NznSQTJOkJENhRK0uKa/SqIkGhJ++Fnzs4/T2O3dffX32/lu7+7fuN+PWyf0Hw8MdQckoHKyrZ8gCkEOmqRHFz3huItkDC243vY+9Emyfoz/+vfir/2nLRK3ez+7u/K39V/9yq3e0aq0Ofe+N7+D2W5f8pvvl/2Kwtp6+/d7SbNiRg8bu/fjCM/ebrYnSTBWBBVhAGCFNYQmaoQw6CisCP021gBVKgSYYAPlJEulwOpsNJrO7u+Gkl6dpV+uoTm0kFl0POAVQhuIFQyclkyPjIIaYwA48LiaTG6+NXTnqxMVjFzqR796/fdwf+JtPB4sL7uCgJ7wOY9hnogI8E9sWGKcLJIVOE0+V0cj3H8ZIlKRFmLUIYeaYCgqS5qy0whKkcZ67PJ2ZY8oza3NWADsRQHsNzgsRZ5R0y+HN2L7ade+a8rCTx1F4eexnxg7R3NaLG5jOUIzG2fj17x0N1+nKdniwM71+KBx3WuyUFpQzcFE5ilgBpKi0vipV7NnSiFYQlmpF66oFvJIqq7leOxI0OUfJUWZEwDFnXIwtg2hohz0BScnIwzI/GRZp7hIFz3C7aRZWFQUiQp4uGS1v+HM/HfzkZxe/9qXdv/797x/f74srexgQWGO1xLhAj1FWohNACFbDBK3nVftZPv4yI3fgRtPcvi9/8hfBnfe317a9jCXJ8nff8IYnC4vLdjSQMMbjT6azEd59r/HWm+4p3Yja6md+YeTj5A9/N337e9HKpmdtWfdBQAhloBwUQQM+YUmw5kCAJjggl6rpB8RyI2o1uoHn4d17ox988+7x8aIxAdfhOVqqZDmBWEHuIim1sgJXkR9FS7U4tFYGs3IcZIfjfrQ3W24HO/3eSbzU0smi04qmRZCrQAMCyY3nCma2JRepn5Rxge7p049dfWX9/KJnCArQCprkNDt2pAibYi10QN45Z11ZWsdS9jrH3/vy3w32epr9YGGr6B+XxdAUww8C+9ULp24uNaezVLE1RlHkF63uNKEhu1LZXHFSLLuZV3SycjB0B5NR73R77UzUWdC7DyZWuIKJgStHmOZyHAWJ57cnpa6CK6UWZ1QzCQdF9RRGaQCKDLXW5KjvsqmqOlYYhgGUBSpXFiPPkBCUUoYaGs2YfcWGFWnx/Ni3n3yBzp6e3nhz/xt//n7vzhTwCMwoAUWIADBGDtNalwQBxI+veGufcZz7rk8QBRqP1L///5Zbp5785/9CFbPvfOmLW0tR+7P/KLt4ZS+OZr/7fy0mxeJv/s69Rph8+UvtK9fMaDTTxj3//PTK5d2HvfHffrXx4L7XAoXzSWEPxRBgeAqaoATOIa8iSAkGCCy0Q0kwiTX37k6vLIalKV//4d67rxclAqO5zpirA3Zq/LMtC185zyu4dLW6XYigGFCeNE7H7kyBh2OVF0EQdk43J1vB2B1H/UIpz/kWPnHO5DIvVqSIJ7mfcMtfCn3yF5Za65tLa6E3FxYQzfP6BATYEn6EuAVoiMAz6K1v3br+cLQ78sPFePmi39guk75ZvvClte6DuGnH3BxMdXncWFGhY54FWnWWSXd5sK8wOXV2uXXp1OrBeCHNnB90Hzu9efGMuX006vcYS6RCqEA8FkVwZItBsJyTIRaliABV77DM3CYoBNZAtdQmaCLtk/YIDGQACyKGUXDzQoy00UFsylnhbMGFAES+1pEfxDHp8ENXkt/+7YWNNf+P/+D73/vmnkOhIQJHsAwHOAWt4JX12JcFouHr5jlurkrvPeXGBK0hozGET/3m//rxz/7833z5dw99s91u0aUni8efO75/PSPdaSzyymKycSb5zXUKGtlf/FlKujAm8Uyyebl/eZS98+2OGxkBKagc7gBZUs25AYGz6HnINBSgGQHQDWO01sOVM6d0vPTg3mS3n/tRMeoHrrikgwZYCJpIgQjsQDVTu7Rl7ElLTY7SNrwFiOIqyElK7Zfnz6421ptOdDMvo9g8tbl4Zamd3O2N8wmkA3HCRrJclZModImzqqCu6XaChnPONFowZKXeWasqUJHni5i569UK4FBFPfuRWd3ePHxQ+t6WF24FzViWnFn4yNTNcHvXS4tm7jgUj3wzmZmjwabprBgR5GOidDsOrq6dmkwYabHVbXUai5jRzngaNyN9lLnRPhg2CuBpFFm3O9s4H+4fs0ATHpmaa0janHwzHxbXy1QrPtBcZhKZ9sQPSvJJLMURyMA6E9l4ifgh24noqOMtrQbra14zImNMPvzw5Vks+Z0b6avf2p0MI2UU2bEgrx5KhxmguBaj1JsmQkB+mzXBTUgyVNEQyJ94fuHFF0pkb4+nEjZc2JjAFlAuSaxltbyofROgKINYQNaWRkSVpU/a97S0F9LFbjga6fmBBVuFIddOiRIoBIVG3G345664i0/a81e87Wtn1h77rJD/e39w9z/+BYv1g2DZC1p1F62IiEgU1YGfREoV2cyO++vNWb+/Z0cpsRMubDqj7KG/wMFsEN4ehkoNk+K4N1w5vXB6QaXd1v1E6OQIx+/r9kyVdnXdbW7Fow8ylelY+QaGiYM49H2NOQtc6p13fXoxoAy0rv8tgGVoQ2ubK/dWnLjToFCESAXmzdeDMFxWGkVqir3R4ijJ2sPOUmtt6s9O7hUnb/pZf3nRtIteed/dv/FQw51bJz4+LLNp4JGnFYqjNr+93h6obpPiWGnVjjQV2aQ/U6Ya0atqq/1IA1GPs7hW1wuzeLlqArTuSGM2Ec9wI1KtBrpdgiclxCQU9qWfuVRH5y80rl4Ba1eU+Wj0wtXJK680TaTSu+9udb5/5SLu3O6WNlbIHVJG6GALnJQYySO6MliZlorWRUO7gUapoAR2uRH/9C9caUb39m7fOur5QTMJozGYIZTlqhQTxfAMAQTHEAZp5fTBHp/eVIEOQ6/stDkBuypOEV4L4ZB4LDNCrgABddr2xRfo5U/lzz0/Pb0lJnboXka7CwTntu/7oceyoEgLXCUnIabatVKzSAENLvLj+webp+PLK73e4U3FNg7NSf9wUKRit1EUYeS0sLV2NrNZZvMiVwEFHryyvxjevHjRhq0wasZlks1GY0hTwXNEzqioHYeRFoJVj6R39btYb+uqIX+lcyUhgSZaWm5HrX4yrBflcDCTkZnegx8rbz2QM/7sg/HxznB1qX0uPjncHZyMpirw1tdPLTTJpdOoFfmBIZ+YyvZyJ/dMfv0QzoaBXWiMmzKSmRmXfNwv+nvZJLzknVZUieYrwSYJK4FDxYOq7KdV5q/S7DfhMlg2IiRcqk5TL6yw8QRahb4SxcWJMDOJE0nHKU9LLmy7mfz0K/HZC8vg7MlzB+d//e7HPlL80R+tfO0rK8UsJFhCQXAlJg4JQc8fLNLBGkVrYq3KxhqOQNqYD3/2IxefCk/2/wqWLQd+YD3fgiyUTjNVlJqJRTFgAQ/irKPAL9udtORiOhJbmPaaO7lVJqkCyIesUHPNO5fZ95h3PISbp82v/0bw8z+bL69NoAuZmd077XtH5a79YDrzv/13E1HLpHSVDQ6CVDhIruAwqJ1bHlnP7R8VRSLs0mw6JCkQknVTxsxKUkpTlDNGra42mt3Yj7UrLJeumGUQVjKjdK/M7P575clBNskD7V2ADp2CGB21Qt+jObj70UMFVTFiKpopCVd65Xm+U7vTbC4Fk5HT0AoMJSY+FXMjKa+P+GHT295ypbd7OOvdy205zfOkdElo2kIuL1UELDWINLEoCcJwaREOOTsust79vfH+iacCES8vXW7BuqkfW4DnU1HpVl09dq/O9fkmu/q6CEoTmYism2KWsNG0uKgWlsX4Kgy8qKFsyYOJzQs2RnwuRiM+6ms/ENjnnlQfeaEBKJncpuwHzcb0hRfzU1tp1Bx96Y83sqyhkZaY1ZGe9YvHikLTvURBR6WpKrNq09HpdC++eCWOdpfiHU2+Up4mrbUPlZYlP9xrZKk/mdhpgk63TpYsClUKLSx4Z055YYjpFAhs0HWTVDSMgSilxSyi0EsNtXnevvBK+YVfCLrr6WSvePud8LW31t65fvagvJw0JZul2SRUYSRColUt/kRdoILnvgkCkeLOwvGoPTycKOcYWqSU6VAwAmzklkrpZmXukVEkw9GJyYNVcq5kCAulRw/7w4OGSCu3mgE0m4EhaCKB9o2J/EpRMA+vqta2c+lKfW7O1RLV4k3QiIOF5fjgflGTdgGT354GZyjYbpidS2HxnATHx3SLXQndhjkRLiLtCZskQ5rlB4f7zXZrY2FlmpUNceLr0rLzW66xlpsO/A5JqCnSOjKNrlrcIDaV5JlIo5IPQrESESES1pVXg1C5KZmc8sib6lYg3VVqLptmHLSaVGTZ/p7LZiXDGSM+gVkDjqjbLT7zycbiYgSXYPy6lHsgoUxOb+a/81tH2aT4qy+ec84XWEH5yKVJYD84ZbqPQfuCsUhR/azYSRiYjbWZjySfUDprsDOaHAx2d4rX31ixeWNwPD06wOZmFfNARWmywssKX0fFykrR6KjeAZKMGSqoopzDUK9Eq85du5wtnJ6sb/vjxLz1ZfP1r6+//sNTDw7WZt7q4uVT7VaoMSHtQ3tcGUicQmWOUJWCo7bYVOxz3VpyF56wycwU1VNQUDlV0z4mx85mTgTGHxeOObu1f+CFYTMMXF4UZamUQfNSGZ9T/qJWRhklysNoUlGotae8wFeEipM/lw/XlTBXNmhdK9ErGJIIrIg2tLwce2FPckfkC2DkrbEdLoXb18LzT3h6k+kMr11SYgEgy1zvjkpv+8pJ4E2S9HA4MIsNDijNk67NbJ6WqcPK0/riS6R9EqNYKwmNxERaUFIpEEUMZoZhYi0EUtVXyKwYpBQppIVyU5GSKPK80KxALznEmfZIkgmP93XaJ2WMFGU6pNyQ7pLRUtJzj/svvtABPKQ/RPptICFiMJDJ5hb+6T8bPnzw4I03tlHbAuoCVMOPWk964RaTsKrGGWDIlRcff/EzF/zwO5ikaRonE8/CgAiaFSkhxUytpjQaQKWKVGKdmqbq4ECXRdTthnF72irFCxmgCDqEn3O6uPrmxbMPl5d6RclvvW7e/Ib/wbsXHu5tKC9ePoXnn8bjH43OnW2++U7yl980pXikhCr7FYxoqVt9DQaBKxWfIvF1tIRokdjTKiYTKvG09eTBXen9deEy68wgcVHTtAJDipVvMmez0jo0zPIL+tRHREORdZ4rx8cySkU8RzCh9iKj1I9fwUf6FSaUVdiCFsytQNX6VwRKYWml2egcj48LrQI4MVHjnE/nfDytvCaTCBnVWKzOPtUywr46PrZ0Iq1wIWyfHywuLC25TiiAFTc9GXMJ3ViUcInEUQkiDWXqPoJNhWJSmpVhdlYsiQg8ZuMsZy5PANIm5GIYoGeQBu0IxlOEL2ipAAAgAElEQVTTJCj33HTEE1/ywpeEtaGmDhzbnYEtNakNx7K+WPzC51qdDu3cejtMvrYSPYCXC5dUgb5KeeJx/sUvHN67HeyNS6q9lgDYC895yx+GF1YQEoJYuLMXtn/1f/rVoDX47lfe372rxkfB3g3MYv+NG52XPpKe2XYvvjR4uHP48sfU9tkMbOHT4Jh39hr5WH/jz/Px/ZYNnM3yZtcub5fDA88TjxC4xeNT516LlwaTE+/Bzc7OB+3pZF1hfesx+4lPHL304uzqM8+uXLto/Oa0yOLrjckosGWhKvhzHe+nSFQdq8gaIIImpatA0Kr5JziQpsDnRrvox7OZKwPOCxVFjcevnCZQ0AxyJXpUkAgrq7zK/uwAoCgUk9KKFZvYM5HnVF051YcWakgqLIwWU2fyPOoUIYBjNDpxdyUYH2VKYgZM69rn4IXKWyCpQ0pqnyOJKCc2d45nohpGLUbN1tkz3tLSgS1F68zaw/4gcwKthCswjgcGkbCULpnAjZfWPBQj8DSIMRtMnAtd4TzPnNpeWV8NfdjDvaNbdw9dGHicpb2ZpRXmgPt2duI4NKbVhmnmgCqN5D4Fvm7EOitJRGx+/lwRmdmf/bvh17/62nK8948/27h6aeZpi0obxZYMf+IT9ktfvvvwWw1VzzzI04vxysdV+wwrW/mk2HG71f38b37O5if/+n/501s/cnGnS3lo++gf5f/q/1w4PpZPf7L/yiemvnf/mSejoDmDk6MD+bM/jd743kLbZic35I1h7K/OHhy3O9uuu2lb67nabwNEUWGi7Gg3+OCHC4e7LStR088//lMHv/RPJs8/NW7GJdqPwfcn4/wo9U4/vZ4kcvwgnfWEcwUQiVJsCIqZFRTDQYiUhlYEgzn7EVVoAIEDVZKaJDLrKFZQQdRqEpKsFIIyTIodi8sFDmSIFVmYmTNMZEDgMAq80EiVFg1SUhNSq+ECC5SuFafVeLe6CConjx+YxZVox5uyOFHamOZpQU3SEQJVyr7K3Q4iW+a5G85oSXugQrVi6jRGd/a08R2B2pF0AudBQNpB0hOtcwWn7OziGdNqTJQ3nO4esJVWtzWZHoaNZnO5nWTTx8+Wj11YL5NJ8Fh8/NTS0tap+/eP//APXhs/3PEDHyWV4aorcjsYqOayDsLYS9xkbI2VICDjxCXs9u7sZv/7/2N6e9nOPfGx7GT0z9p8bovF1SsQiJtkk+biONBq5nwABBW2LnsrT0H7JDlrgTaAF/phloz/6t989/pf3/Uanc3LcGVmU2mHk6NR+Pu/137/unz2J/Qrn8wb4eFkWF6/7n3ta+0PbiydPpt4i0Pb98gI53Jys713z556Im12XXCAQPjgcGn/+zg6HvV7LcB0m+Xlpx9+/lf7n/q4oCwkj5A7gpvNePfhbDQplk432qt+/9CNH7rshO2MWM07nLogrSS9pp4BkiJSIE1iVDWOljJJ3dSRE8thxK3QDvcc2SwvEfgmEnFF5Z0SAllWpTOkFJEo8luhF5hKOPUIk1n9XbkqlEdV1PQj4zZVNB8Ro2h1sRVGkzSxpLWpNPT1h1Hl+1A1nkGBJc9Tm8TKDwKfS2+pOfMw3DtodNvKyJnHtvd37Gxqlpc9Gbyf3Hk9XvAdylaXXnr5ybVG8L2/vJlMJovb5xa3Oujv8nA/WOLcTffv35gePzjYPb5w/sxTz1y+fGX94rmlOw/63/jqDZ3P4tZq6TX7x0PJczcZO1J+s2xv+CaS4Zitz4p3t1fksctbsykNxg/bce9DT/ZefCHptgVcQpSQIk3HJ9l33hxYz+t201nfq34zyutI1IAHOEUa5AUmbE76wy/+yz+Po7C10DLdFCpb3Jpd+fCHrz65bt2XX/2h/e7frd38oPHhDw8XlrwHd/j+nXajGf/yf5lduXRy5/pkeBD0D9X7b7I2mE7pg1eDdoDzEgAyMk/cPWikg68r5CurxZXnRutnTvqDaHyy2o6VgIhnQMbs52O7fzsZ9qW94nsLfveyKSacDDgbOjcBUnIgIl37t5Su0J7EiqCUM8ppRVqzIZGscGkReNZlpS3by87rSV6MxiPymu1ucDJI4EoxHgA4B+ugNBMkMKYdKZ9qdfyjsajAASVDFJRXx9Q96gmplqXDCTqLjfaSN0syUsbk+TvG7yq1JBJWNamQA1XGaGGXsthCXAa2Yeh120kyScf99kKAIjUqNr6/tkqL3hvHD7+mhtNMrSPyljZW0mRy4739/EEvbnVzHU9zJpLp8fF0MjQL7XQwGUWBU97dmzujfv/O3vkg9uMgu3qtFUfLxvjtTvzu67079zQ32yrqlFplQbm6NCtSPc1Hl6/if/wXzzz97NmH17/1l/63T6+PX/lYb2N1QiywBmCl8tnMvfrW8NaOC9teZyk/6ItUG0phqn83BNLwDPmBhqeg16/ox59Nzl7Kt7YzG3QWLnx+ZfkAJ73Hr9pnnrZf/WbzW9/pWl7bWJ199KXspY8OLmxPYGwzcpbLbtt7//30j/7D9N03vd6+J9MwhLEwycIF3Xrey/bXF199/IXR0qmhiH14mO/3XHs7BInwgNxEq5VmN2otRFnKezcTE7iwGwQLOjqlvGVtJ+JGko3EJo5ZnIbTFYSBlNMCTayIFEEraBCxdZNEmuJOHjxY2GpFxqOigFKKJAxF8QyuBAGkhB1sqQBHIoEysaY6qnPeCfK8onIwBkbL3z/AfnyeAczwG/7iRnS4lwuJGb/9tWil0750Sfnny9mysJLaSq3gLOUTKfN0MB6OotOXNnWjiXTmNePG8oJmMEOpPNn/5u5OMRqxxOsOHQgdPhz3YhWwVr6XjIY7GMRJYzkrlMvtOM1t4Zh5seXHsWU7tlOrZDpNLNvVlYX28oLR6vRmZ2nprPvKvYNe3y5uqtZmVgzS2T3TjNqr7he+cPaTn34SctjZ+sHZX7sVR1b7M1gr4gGGlLUl/+i9/ts3poX1vUCai4VW7NgDwFpq8z8RFGujWUcXnn38C//dy+evfH+1842oWbo83Rs9GTcvSpExb0fxzRc/Ojh/cfi97x8Ld5+6lp47PVa+RWFQKO1xNnNxg599Jr/Xn9qOWz9qDl/T3r4rVHMWbqqlc9HZT5/afHdp4x4cFEyalnu9/OJWg8jBDVCexNHG0jLaE9PU/mxq8yHyE05HME3y20q3KFhCw6p8YLNhKalxuRFb0zsq7VGdy+QZkBHm6agAUioO8mSzEWkljWg5LJNZHEKjsGJJK0DgWFXkDiMqYhPODVTzOVXNwxJwVTWo+fP09x6+uccRxtDSauxH0yJ1xvXKpNg3W9Ooc1clV4BrLJEoBpG4ksqRL4UuiwCqsbLoyny4d6gF7XbTU7ooJTvsH797S7XX7KnL0ujCkYg73ju5Pdw9c3ZFNfzZ8SApTpKUvTRtAmBOeyfKh+n6HntS2oIlu7PnWLxGOMmOPd9f2d7aOeilSfrYtZDfOTkohhrbhMhZAZ988hMrP/P5J4Ccj74u+fdanUmFwK5oFaRKJ/LuneS1dybTtNqEcNBko1XJWiFQfpe0qdyZSgkzRVsbn/lvnvzU/8/Wmz5Jel1nfuece++75lprV1XvG7obQIMACQIDgiJNStQujTQTHoUipLD9yQ5/8V80djgcsiY8tkQtFEmNRA1IShQJrti70XvXnnvmu93lHH/IqgYYdkZFVFRkfsjKunXvuec8z+/50w0Y/Y2f2smYD4YY4kvJ5HHhjxL1ZkIrEX203p2++pnR+koRtbCeu+MDNx5z1fhFzQrTXrvT6WdXzzcf3h+ZZJYQuO8l83Kt6p7BGGX1M3O8Zd37MbEiCQGOx7ZsQjthdiOoHuSdF872m+qHRy7eMJryvkqEfIOuAX/MTiNmEncx2TbpGZ3XUTNLy2lVFzU3IgGDwmUhAyZibRBVCMFx1WnT/PFh//pFvZ5Pf/JB6n1mFELN4EjpJdhISLyQKDS50rFmBH8SAwDPjMYBgAlUJEIQTpulnwZOAiKKKMFeN027oa4bbTbPNsXu5FFY+IM8maZpTvUtFiUkDMxSrW2ZzfaKHB0vqrqYTYaP93aubK/1O/W8ODo8bMYLaZ0PYmRRSJZLZkRME7LR6MCUY5d1Bz6FYmAbmHmrEZG9qwutDWoAYO+drUpxwRhTTSc6T1rPXS7KYnA0rEbTYly5kJkkQRJQwYq7fSP70z99cX2zJ7N/luLbCFMRBA4gAVEYEcjff7L4wTuD4QwDKkYBVERLhz/oaDvp3SaVyDKRlAixevO3nvvSH1wO03+YPB3O5lFdlRWcO3v1VzvJuDz6P+syqmXTYA9x7p0ejezgfv3ooBqMK9ewMDcObBNNFs2t6/nGev/yWffenUV7ez5+MTp+tO7iDQIvqj8vX6qb/xK1jhlRSZhMy1nRtDODbg7VXey765fyFo6fHuQoKFrrKNWRVgYEUDyEKZQLaVKIWqgzrTKXJUk/7kjjuQY7x2YiUIsII7q4TTFKmHlgOnjviWmtZJvRZDg7u7OmnDewaMICQYkEAR9UCIEI0CSRNhpOZ8/Llpmc1k+oQJsTBdTSknpqz3oW6IzCkGdxb0WPj2udnr0ePhj4vdLmGd9wgr/IbB94BwB4McRmunoOrmynD+4//fG3vhvn8erO1va1swhYTubNYGiw8a1uwFiKEYxLanUx6Vcap6CSoil9NYC2bgZAqiZpNGlkJFEKg6u9Y2TE4NnZui7ZhziP69F09mSvLhbzw+nRVFUrV6i9gZF4dhsd/KN/9+ILty9IfZ8nX0e5i0vhzfJck0AEh+Pqh+8eHw48gxGBAApQCyhkJsii7c/rtauiEICRIEi4eJ7/4LfXFN9/9OBtVbtOmrZTbJI3Vtav6MWfx/kweFdXh/OqGs1mB/vC4p2vtIarF3pJrLUSFt4/ts7znfvTOIqRyJACwfzq1OC+uDlIDDq2fqf2eVsdspACqSo7r7xgJOjBPkWent1pb60O98YWJPY2OOtREBFJkTKACsCBr8VN0LnRdPQhJXrzuRudM504BgxgF9KMYfrzITZHactlqS4W2pd1p9V6/OMPrKaNle5G3zRz19Z2Md2D/g0EQGdRTnJoVaqV0Xi6YuREN76U+GISAekls/G0todTnfmzGl/ARLq3nut7C216m6a1EYZ3oMzcAkt/N2l6lK5xIH9wNy7HWneU8f317uzxfryztn3zkne8CIGIuq24kxt9cCiddYgQhiM62se0BaZV1rwgBSqIWHZNIiOdxoAs7Dg0oZFQlkmGAsi+EVcjoDK6LuaP33mPQzAgziehfwVXb0DWDSA7Z/i//7fPfeELV8GPZPh1cD9G9KcWKGAARdA4997H0ye7NgTNfOLXEED2CNBO1r+Qnv8SxBmIJ0AkFdBfvnDckp+N7/9ry3zU3XAR+eEsS/uvKZqG+m0SVirJ8jrJreWQZWp7o9/ptuMItD7xFBOFjfVGOMlifLznhuMaGJpaUcy93j8f7f2bJvyeKMM69agYmAQBufZuXrEAC7H4PVXd7XVe2dwCeeAQ4xNVZBAI4MqysTUgcK5VFJPoYrBXjx+LwQNLxfhq2mrFOUUtijaBzaELpaa43SITeqtJfeHS6nDQTCbl2c00SW0zrxNyNL3nq1vUWodgtQgpBsMQEUYa6ETl8+za51kQIDYAyy7zEoonn+pGnM4SBUUIO/3MtEZapet68yLMn3DdcKWdq3D4c711s5acp091qlmZpir6vWhl+7ppJSaRomqWaFtSSps0r458c8SUoqTYjHnxhLOdmjpToSQ4HWbYzIEl6q61Oqbb2pKwNhwMk83+5evXQuP27j8c7c6DZzIJNN6Vc1RUA8yiNdu9Ra11Ib+9Nv8ffq/zm7+6pVTNg29A8/cK5yAE2ABaEQRhQH6wP//w44W1OoDyojwoQGEP88W62vrV5MLvq3QL2AMCaRXEX9yRL73iVqIfrCYfRpElZG9tULdavc8gPBEZCHoAYPGIIU/w/I7eXhdQjmUp+gFgRYixVmXd9Lpxtxuf3Vk9s5n84MeL/ePQy3bPtP72afWyz15io/gTly9bJ9Oy9hwrFPBjmP803rh9+WKW/LCuXE6ohZWAeDcvx4/t5AC1SDeNW9tptAmuAQ7AHOp5+Xi/KBvUQhlSrN3jXYwzjHWWc6bivpF+V610es62SHtCp2PutLU6njb1EFtrAhYgIGjBgAQqIlTAS7UrACwdIAFjA2SETzsQ8qkl9UurCwAE2+0064pGk0YbF/TBh014Clr7UkWz3az1YAY7ABY6WS26WNiO5vb6qrRTj5Iow5WrnHXiayE0JseZ8AIowVwLJiFPur0zq7luRYWvF2m3v3HuzOVrF69d3WhnLQR1cPB0Vjafeen5frt17+OHu48eLmYLBpgMp/sHx9PhhJKk1dspYjdx93a2sz/6/dWvvbmplQ3jf5TFXxMMAAGkAawAGJgVhtk8vPvxfDALHiIv5IGYEFCOjy4M8Y+j679D6RqGgICgVTBiTPm7X4p/982UpiO2FYhmwWkVFk07KWaUZDr+Kjbf5rAnYAHEBwSSIB4CIgkqBlEh6Kp0g0lQikQYOKy16Vdeba+v0be/OzgeuovR96rH/8dRswOkCT0iM1IA8EEWVeO8V4ZELFd3CKYvXO1udIf3B30kBEEJgZsaESAWxxOej4KdwqqHWCNqlIDONov7rhgr0pS2SbdxamOlfYCq9GuZ5Km2dcM8b7dbikgEkiTq9tjMJNoy0U40ORAGEWFpLFJQEZ3sVSzLbmwQIAQTgegTMuLSaoqfjKdPYQyIy2osSdNeP9YiqOOejjvWMM9LKKC3g6na3Z0kJKIAyyZMA+SgQGlGAgSlpYHgJHgIlQuo1dnLZztrnenxxGQZxkmre+bS9VtGQT07CPUia7c3d3YuXt3eOb/66P7+dDy/cet6uWiyPEpa0a3b11989YaOSGvVNG40mkyn4yiN47y9vze9+2j3lc9ufe6VcwBzHv+DzL6O8hRpqTOzAl4EgDiIfLy7eLhXW1GOwcvy/kLHw1uPjv+70PptnbQBLCwxy0oDV6++AL/95UTL+96PEPtFVU+K4eHYt9cvgsxmR/8SqTyiL2n8PtE9kRBYTjlOihkXcx6MqqPBYjAsDgZ88dzaWj/uJgDsicprF2H3OP7+jxa5ml7a/s/N42uLZmuJgGVgJggitvYhCBoW9OKfQn3vwtbnLm27BwOHlAkDkorSvo67FKfV/KH3M2fHZfHAqD5qrdJu8NY2A8pM0r6YpOcV5PVxI4vHTRGGSKu5ilO2zs+LucniXCciwqJQM2ihXHQniPJs2INjWyoDOtLLLZUB/akAS2vQkSzJgZ+Yjj/pn548+FQebDT1e6kGAKAI27k0Ig8WuqPn54yfT6ieogvK+9riQmGIDYFCpkAhuMaXBQTnAzReUq16nTSOTCVKOxa7MOp4cveHxaKpZqUKDIgfmXd+kCVJmlRVHbz70doaKq3bUZQksTF5O2tv9Ptr/dV+e22tfeP56+1uXi9mnbh55ZXb3fU14AWP/k4Wf4HhKaEDYZEA6IFBBJHUtOC7j+tFJYHRCwioIHQwuPH46H9q6LdUlIp4QI2agZhBLp6xf/I7/e1tDIN2o15dTA8X8/ukTG/l4sbFL3bbqi6/a6vjhd8h6Gq1EsWHiGB03AQ/ntjR1I0m1XTWWMekVJ7R/tHkR+80WyvR9lrU7qgok0vnonc+gqMhrPb2zi3+t6e7ryuwAWTJABaA2gW3hOSRYx7S4v1O/7M3ribf/6ByrgPMAJooIoCkdU4l3ap8Ehb3/HwkWIoBJu/LuYrypH8+za4Z7FDQNu16lsbVbE3jESEIYlVIVTZZGkkAzxyUiOPh3YEet6SYo+HAwmDjzGizpDXLsnFFgEKgYyANQU4dbXASuCsnldYvnYRL+Wa319YCDJrMuevR40P36B0C3DvK48LKdC/iRkFgh7U0juNAgCDC4srGzmpp0DkKHoyhal7MD4auDBaVoCqHJUAw2ijUJynnyMUIg0OTaAR8eLSAWEeZVrFG1HGWm7VeliV5rNo9099evXh28/xWb/Vsr7OyAn4YJn8j878iPiIJcpLh6AR4iddlkScH5d6R5UAiCkGLqOPJc4+P/merfluZSDAAERISIiIJT197yb96u12Xo/G4ZWdk4GB9rUI0tXo+b29L9e2IDuN86v1BUUTjWeOhKBcSx3Za1a6mOKKz2+nlC6kxWke6aWD/0Gmtyqr84FGVptH2ZpJFUb+rj0cNErSS99tqpGAiACC8/JM0LtgQltFryFbK92l1eO1q2skWx+NVQg0itjx21YSyXKfdPLoCCOXxB84d6m6PbTCtbtq7HEUbJC1Zzo1NFAgs+sBQeGkCGUW2bopFsbrWEgkueAbHAm6xwDCgskZlJATUHGeRVifMAwGBJcdUgTbLWAgAOcmUpxOSAsCnRopLQPhyzJy32xqQgEDHKzGvAmVQKd5tWR/RYhwS77UKPsyLxXhE3fMcJ4gBfRMq573VpUXw7ISnU6udi2JNytBy7C0alyhpFBY+4YaggATPIAoNAYlSigQRtYoQxDVVCAw4d5NO27386sXOyqq4A578NRd/hXCEAAAO0YEwSBABBiEli6q582g+LcSLYkEBP6o2Ho/+xOrfwTQWcAKEy9wWBYxhbaW+sHN4sPukHr1twi/66WEaL4iaaZXr7svaZGFyH3mOpI2pup1JkqjBzOzNql7bbfdNay0yBpfgQ0FA8o2VoqF+J2rHNJk1hyN39/H8+DgcHnoRqCr03qbZESzjRUQCIIo0LjjvT/7Vidg9pPqjC1svnVmvD8eBkOrqaHH0jvVjLDNlWnF+Jm2dZVtUBz+Xqta9NZW0vC3ADyIDRG0iRFTOCEekAo8bmFndj31jq8FIVjeIPU0XobaWxbGbiWTALBGwd8pIlCYnrjw4ufsJgiYgRA6ncToAiEsi7klLYrmwTlO5l8xGSSKjgYMf7VVP3g6jhyrNSTS6CCFiVXFbNcZIxZmJqtH88O6DbKObZCk2FnyoLM0teldRqJxlnSSozTL3EE/wIACyRNj4wMhMnh2A1vkyz4VFUVCGmZWr2KNJU50Yoerq5Z0vvPnZzkpP7MMw+Uso/gvJ0WmsjwNo5DRWZynu2Bs2Tw6tCxAYmMG6aG/y1UL+QMVtwRpOaJpABIio4/I3vpS8ev24OvqbfnTQyqdaNcLgPS+atEUboHKVfz64t4O/j6wATBzxWo+s071W1O8ys2VA4KV8FyEgqUA6FLXu5rK2EvV76aRwSVwejvRwws5J3hUVN86Cb4gUs4hHqB03PgggIAuQ8ESKd9a7t6+cl3fuFU1t59N3nQwwSZVO2FWL6YepuqjiFlTiF8dCpq5mIk5hHGM/js+02ldQKVGKlUYfZqWbFU1TFXvzucwbCwdGxZ6iumDfWEkLxtky1TlASPIoaadLPORSYCQAWkGkgAAcnzghT29+8AzkuHSB4mnX9MRDiqjLj97ygyc83aPcYN5BQYiU2MBiERKoIRQVbunmbPTYOCkXUM3TaRVVVJfxbK5d1URqAaCZjbWOBIKw4wDCClAhCICHYJkDC4TgxRiNEicIGEDQe+QAzrJyCdnMRFcu93/ly69ubG9LfSdM/i+pv0MwRmQWRmCQwCJCzAgiSIhl4+4+qUYFBEEWAQzzxfnJ4rdInyUQCEaIkXgpLQlcvXaz/vdfSXaSg0o9VVgC2ACilGJH0znUxwdaf5Smt3Tnj/3sf0f/YPkBsnAScxT7wMtzYKllDIgBgQQVMzZeMdjAAgK9Fr1yO0Utw6nFBQAxGZ6OsSko7SCDIIP34FlkmXwGQaDx9Z24N7l1OfnGW49GCytUZqtXTXLGRH2xYTr+6WL4PhaeixF0Y9VuAYdgLYeimo9qe6fJ7kR6hYLCxoovbJuHF/uyucW3LxPTveF48XTQgiyqXFNW0KoBK5QYAkjwcacTt1K/PM5OJzWkgehkW+Ll4Yeg5bQjjydmipPS6vRkXAaV6PrRj0kpbOVAIrYBUESxeA/BhsKLqWnHHdzuFC9cTFtr5HU5GzWjqbEix3b+oyFYm+SmrKFalEo8EQqC9w4AldaAmoUCg3dNkAWiuJBTMJJFECtta0CLqLT2sYKcBp2NjTfe/LWdixelfuRHfy7VtxFKAc/o5ASXKYK4BJOLOFJwMLT3nzSNBeEl/S2g3s7T7nT+xEmmk0yREhI0EjicWV/8h19vnV0/bg7fQZwBMqFqvJR1NZ0s0Ly6vn7ZTf+2PBom7dfz6PdR/hOHewhaWEmQE0ABBkJBIBGsLU/m7nBgH+/yhW0TNpVRgYP34om524J2TkUVAoNSkGRYlWKCaA2OwTKGJXwHlEgACGz3wD+6dv7WevfoeNxuZVci2kJMQBASSJNz9eKR8yPJCQ0GrqPOllZXJbBN9uz0cVE/cNN9VY1995humfqlrccXOhJnqe4o0GW9KG6OxtMZvn8gu6WZjjDvgkmEGSCkK6nKomVe2XJogwCkABQEgMCnyVHPnIanNsMTkODp83IK1dN6vYsQow/Bztg3pDQ0C7Q1+XnV9p3bcWstv1fV458/zmiWloqc8x0IZ3rJrb4o5/9lHBapEyWuNHrZEvGIzJIp0JFRWungWYIVD0wkSSZaOe9VNzcmIgDP0DSVW5TtlfDFNz/7/PO3wU/c5C9C9S3i6kQfiwFOfYhwwkNiQPbMTw7r8YSZgQVYxDnY3Fy9+YJ/unv30Z45qvrWr2nIUas0Kn/zDfn87ZjrOyxDneyIN9N5MZztEdUBs9XN1/srfc8fVPYH5egXLn4tS17TWCvYC8yCepn8AsjW+cWCj8c8mPjRxNUN1xUWi0HZ6J2NdKWnk8Qr9HnGaQbPkvCiVGoLVYmtLjBKYBEMJ0TN5RESZlA9PLv20pVz60+GFwyuQEPMIkslp2gKCiOD0FEYhceoAskAACAASURBVPnCmlG0thNn23H7bB2tCJc4nDX2aXNDycvdcjEJX/9QLyJlWihKKYYVbLYin4kxCzV/jPEOxEowgJGs2460XlZMywMxUqAMMIELYHnJZT7ZvU6zuU4OPgBZJgE8ux4igEYmaGq2tYRK2ItYdIfoR5ggnlmxzs6+dyRPG4DE0pg9kauYZtiCsq/VTt+sx7gYt6NItyKKklkpbBdJrBg6dS3OLTKoTfAqcmyiAHG+2kq72WwUiknp17DVSRLvOdZFbV585aU3vvgmKe+H3wnFt0gmRPqkqAKHAEAWgECIxYkEIpmX/PTAlTWwYBBACd5l84XO2/e2N9Ir586UIXl0NHx8MB8s0vNn6994sxVFwHYHsz+Yl9Nq/r5rPmq1KTWm5pu99c+DvY/+USvlJL5bNHuT2UUlvXYy9jwHiLQK1vvxTEYTrmoWkFau1leiLCXn5ONHdWP5/u78/q7eWtPbGyrLoNdT9DQsg0eAhAAHD+NprCmxoY4mMxF0UeR80CGQcM32UbvvL54T9V4j4UTVu+xKBldKsBQZ3V5JV69JnFbl02L0IaFJovPRWo9QVdX3G2Bz6MM3P4b9xkxJa8WqkBAklMIF5T6iSheWKRXtQBGTJU1Zt6NQhdOoSxZYDihFwDlYooqXx/9y4kDPXIfPVIGn1f1ytWl/sEuuBKnFRKgiw3XEYyfH6Hvm3cGiGqLrtNpnkm4umiBWYV6EiXbDoR8OwoPCtFLSGOeQtZR1yi8YScURq5RVbJoZlUWhDKW9lgvKWlRZlHQjkxpdeoiksYVGSFaSC/3O1776K93eOjQ/F/d1UrvBY9nUzgcWy+IIEEi0wtgYo71W3uiwN3B7A9/4JSRraY+KZxUN7j9xZXN+Y/j8czdffONC7ZLvv18yPgI5662ZF6GYjH3149Tc6fVmWvOsAErfTLIdP/kvEkYASIrzbEj1aDxTVWlBmEhPCjddVOyTTt7aWqMsDdqUqAKCDwFYdJakcVzvHrjdA/fwCSijxlMAXDIksCkBUJynez/redGtVWWn6Xuv25duLZ674DsZB98Ed2y0O7cVJaaurCg6YewzAccgEaoo1/GK6V5Q7TVY5MXuD8vjD8x6L0rWwEEod2mxjx8sFEw1dNfP7qxvr9immk8ra5PgVhezsW1YgUGtkBAIGINKlOnmgiABACEEQQRlEAjYAzJ8ggz41Gj6kwCyZ5SVT71KSxyW/XuDbKCOsdC6dFJ668CygXz9/Na1Wzv9rdRl7bK25WgYBp3Ro+7huK7rUezmOutVlmVROa+CV8C2qb0SCLVoo0FnvrFV7THSKtOiubZlHCdthdYGC1xDxC78+heuv/LyNYDF0cE/Prj74eOn2d6hORroskB0gsCAyIQqCq122OzZ7c3i8vnZ8cSNC/FwEuIZGE3a76yeG8xy9rA3HA9+8rPLR3uvPHfhq690VBpVs5/u10fAbyd0r99qtA4C4oNzcq7bexOVYp7LEmnNCEKJcRs9Oy9l98hZx6srvt1WnQQiJSBBwDFQYASpECSJtfVuYxW6V1WxRU8P648fh+lE6gKrRgKKCEY5rF9pjp74wZO8LpJvHZ3/zg/al67O33x9/OUvTJ6/UqzlM6BivddPjFvwEtWzfC/C6NHEWrcxADJjYKPa2qd2/NCaNbOWQIjF1ggDAdBmdWtr9ZVXWufOYFk0w6OiqUKUdQ5Ga48epLNxHXQOKhJglhBnkWnljk5KDB8giUEbQABhoKWKRk73pmcQ2VO/IZ7IHJ6VXQAAGlQi7ZxsEdVPIkBjlIAXaxkwj1fPbG3dvNm6fjXE60nV6h4fDlU3b+/Acav80fvt3cMmy5xB8bNyLpUFBpV4V86sl9k8eK+TSCnlmiYsvFIq73eqYVocS5KmEWmTxRzrovCddvelW5f2huO3/uWH3/unn3x8Z+vweGVeGWsVe0XLnKUAAoG1A/KJCq2k2lqZt7O9Rg7z1abVswjsREobdaL+Fz77maO98U9+fqeYLj5oit3dj37j1z/3u7/yQjP5hi1/lGSFohoFggAp5RwUdS+2LvHeZK/75mfs31sO6wGUIuy1uWygbnCzr01UgDRB0pNPVZKlD5fIZ5kbTrB2URzVWY7PXU0vnOMXr8Mv3rNvv1fPayEtSNDu+LM3ZyZBIheoGs777/18++6DM//w4/EXPzf4/a9mr6+XmyvdPHGHz04bAgARCSrtaWxzMxd0jAWaOF07x4tHfvKLbCfz5jlKegyGov7OpZ2b55vtXpmqIMb2e046nOfNmb7KGvzpPCpNH1UMAhjYRLFKkqWowbN4AW2ICJhPqng+lZIugwZP83v+fx4nMTkCWhhIhQinWboQ7503oJwggF5duXzjuQt2oz0GVLasQFVZp0U+VzPfbtuWAcNE1nKoY6MBDbsauYoVeKUbawP4UAVCRA6ARAoJAVUksQkKUYELoVkE8pxnnbd+9vTO3w5+8pNHu/d7vl4BAkWk0ZF2gF7Ag0cUIUTf6FETHdnswZ1e6jEx3F5rzt9oNi9NMWpmRzDdu3uhe+krr70aq9bB9Gg4edDJ09vPP5/ofcA7cVqIeAkBEBG0bfxkHAS3rK2O9n/Ubq2l+X/L5f8q/oNlq4SRBVgb340pMhAYEBQCAgVhQPEKw0myHuJkKq3UpOscvHCQ2LjL532nq8el+uiB9wCeAQBWzpdx33a70asvALnhD95u/eJ+//6D9QcPVt5+F//o+MmNq2meRgDMSp3cuQTT9tmo0wc/qefzKKrTHKqm1bvY6XTOFwejVrpb4Wq8ttI86Yekv7LTWW8fiLU+itkroyMkYiZFPsuDMihoANSyTaDjiLRCABZxnolIGxAEH8DxaZ0uwEsR0uku9cvCmU9KrpNkCmMnEZQxHiuqgtjAgqC0pD5ZVWudJDkUYe8BiwWgjrqrDepCVGGpCkwRxa1WTD5LDKJEVYwiAXwg1qg4ysgYTZq9B9JRK8lX8ni774wO3quqJjBJHpfWPxq373yHyqY1Po400dbZ6uzZxc5m08/KSBVGlZH2wGQdBJfMp61HT83Dx/HgsapGVWlNuadmo2Q6NedfGMtcj56OvvXX34hj+rWvvLE/PP7+TwdvvPL89WtbzeTvfThSCKhahFveymwxKqpxUXfOXPzi2tbt6d5/HO++O08+104+H9NUZDdAw8ggyodlq+lZn1lw6TAD70OYLdzRSB48kgePm/Pb7sWb6eaqxFEp4q0PcSy97hIkAIFABKJEvHMsbm1t+LXXi699lX/6Qe8fvz/7zo+2f/rR+uGfFc9dfjg4XlXaM5rldRgYo/SMSecmWhzvSiyHPa1CmeY47mx0fZ008ydCPk5C1s5mFmY2NEgxsJPAACAkTB4AhS0GMQkwArEYFgEdR6RIRHxgHySNgPRJfg4/K5pOv8unOA7y/11Yp6/UiX1IWBA5LxLAkwJNFCgFSeppKHJuxwpOb9kYSkEKvqkqLC0rJTpSGlkpkcCE+iT5LDiFpFApSlDrwD7pZGk7RiJUWqcamRiRUOnciIqL6ppz24aOXr728Ws3H3zmxsHF7eFK28YmKPJKWUIGxBCEA1qnx/N47zD/5rfkm38Fg4PEebWo9YNftDmYtTPdfC0ejhff/ObfPTl82F7Jb1+98ObnXkEomZ3OX9D6nPWtajGoFx8KY55lUfuFzuptBZOOuRul359V747L8xrTLI6iuAIIzHZ52gk0AIwoQOiZ5nM4GrrjkTse2+lc6gZcwN3jYlJUW6vJhbPR9macpi7WrtcPyoC4JaochME7tWjyd+/T7avu0hbvvFG/enP/tc8WX/9H/+Hw1nsPO9CErNMIpidWZ/EsHsJuhnsZFLGd68Kq2iisiFqGWnHCbn7oPWSdeLYfRiO/WKUsksCAdAp+VQLCrvEMKMAiHkBQwGQxacUizgcR0gpRLd2Cv7Ra5Je/4GSmeLpV/fIwWie6UhoUxcF5IIriSJP2YtBrP3NFqwldQs2gWUnDTRkAqrqo6to3rLwFa1lLXTOzaCQAQaWUUQzIfhlayYgQZQYUzI4n0th4NWutd3SSIKEIB6sY1Pkt99WXZ1+6cefS5kdZNCa2wuCAA6vgvBWHgApREbRSu9L3/V79wYPq5ps02G3v3snGhy3r6PD+am/nUu9ip+NdquXh/Q+jMd+++ZudXge4TNpfa9xivNhv5j9V/hdpfJgkvrJKRS8l6Vkuv8P+oTFx38wb++54yrNF0WthJ49AqhAEIqdUCAEXFR9N7HDMk1koKjBK9dvJuU3McxzPNKJCrAcj+7MP3cM9fWFbr63HSdZEsS+cqCUrsgRm7O68Nmnd+te943l4spnurrb53325fvnG8Bs/Lf7y7erwMAYJhFawQUipPTJYhtkTck9TCEmCrYznVWkUg3gdynbemRUhSRZxPz7cndrSSSAO3qHVMYAPKIC0DPti4QbEnYTyJhB3U6MJggTPhGQMAoI7nQ8ua/WlGYgRBJFOzRTqdB0JnextghCWx2uSxYgg3kmkIpNFWosPAsQGQXw5Ld12QjEDOQiAnkPwbKtgy2ABsSG0iDoICIo2iESiNSn03gswa6RIK2RMItYAmSJ2vD+FNFe9mETYATr3xsv6j//9+c+dH0Bx5JtJVfu6IefQBWAmgQgJRNAHAkatwXn19s/dex9yq8vt7rh3prz3c39wtxWnW0lrNWtHURxasWn32ltnWv022XKOCqaTvXL+rxR+kqqDOPOEKMKlXc07LyJ5Z+8KFwAZgk2ieqPP81LNi9o6yvMksBWh0YwOjvzxODQutFK1sxl326aVqSQio1kp/3TA1tHOeqtu7HDiDo7ko/vuJ+/6ccG1AxH0VmwJgNBu+ygtzfqv+rVrQ/zoycE/rE8+Prta3rj50oUXX9s4t/iP/8/hbObyXglUFKNGko8IpnHUdHJsOJiYE5NoBBUrJE1+LiF1EUJeYuUFawSrETl4HyptjKilD5BBKzINBA+0JkpYBUx10muTorr2gUOcGBODZ3Dhk33okyNPTrYxhQAiAU/Sx55V98+6WRqRlrgQVIZ1zCdmfCRNRnFoSuYYSAMTeJEIrQ0QnPfBC8XakQmEihgZQEeIMTCROFBaWRCljGnlaECtxEwYtXSax1h4irUgIHDg5sWbZ/7HP3nx6rV1mJeDoplNVWXJowvB1yWWVcRiULT37AIQRtOFe/dO+fgJW8Y4kTiC7lpz/dURWjaZ7naNRialWYfNzfQ3v/ylCN99/Oj/jqMa7fda6jCOq+X8ixCtAy+baX4e8CQ9FJb/b540wEqeZIaG8/pgEOYLqmvVOPAu2d6AXke1cjB6CfMNAihMQUxkqrIWZm6lTStTOxvJeCYPnlTzO74pQ92IBKAIogSIoJm9Mx/+K19+fvXMFzG6NinuV5NHLr966dJz/+Er4zv3Zt/83lThwMSLuQRX30Get7CdJqRrVADgtfiAQaIowmWAJToMZaSdoUaJJW2EpaoapRWeoBMBlGiDijwjsxIVJDZx3u4wcsUNI5mISEETIPAvE/1OOuonJ+BJqjvKMz/Fs1Nw+SItIEJIqJnihlsNeB2GPlgiiFIxilSseRlMhsLAzgcWqiwGJFQEiKKJHQCAGA2pCVqB50TFUighk/XTuBehoSYIe41RhBgpEyGRDx61e+UzZ69eOQPCe4Px3sFCA9Q+vv+49d576cf3W0fHma2MNBXXI8TCROKCrrzOVnxvo3Hsg5W4xVnLnb8tATqQmSRrk9j1dfXF114/fzaZHvyksj+LUfKkVADAsYgH8o2HSb0+cVfVrN40Pm694f0D1/wzQIlEwEa4iaJ6fSXsHst4RjubycWzmBiItGU84TABKCGFYBAUgDPUBIfes1GagyIMq/2wumounVN/90/y3n23JOUtifcks8XhW08OXr+6/vq5zZ1ecsXa2b39R4t7+xdX9GpbDO/Vown3RqIwjhtoGMMMQirMAZlSQ6b2tsnaORkUDuhcp9VOTU+HfQoNGsWWx4dFCNTODNLJKDVSWRx5cAzsRXmTd+I88YG9E43aRMgA4dRF/+zqh6dUUviUWgaWsOxPOSueVfc6ICFiAFVjv4zPAHtdTpwvlPa9dd1Fg1oBeFgmy1AI4r2nugEWQWRNwIhOgkKMjFHKQIRqJY1dpprgiaK2UYn2zs+fDJghWu24SYm9PNnoeMtI3O+1AdXh4cO94awJrfsP/Xfe2viXH27uPu0VhQosKKDAI2iGY4EawBjI4yj0N93WlSr0FqA4jqC/tZb1rxel9p7a3fzl27deuHHDF/+EfGe9U9KJB04TofV+5nuWXo67X9nJX6ws7B2Nuq2NTusPDaGrfsC4IGJhYSFFYaUHlY031pJ2XknAIJHIyZ16SWkBERegbrpzf7OQdFQ/3IyGGkTEhoAgfrVnL18ID4+gsiCn1nStEMp3dx9976etC5vtzmY3PtfPr52/djQ6eOu9Bw+Gh2QqLSFJbOEsJkoQuW4AIu+siaSzlkSDmXMVkqVEKaWFKw/CsQ4yA0+kU/EyOZij0q28hwQg4hoUn0WmMtaTdWCC6cQUGd8wBDAJgQErwvLJQPBkRIgn7OtlmzQ8M/EInPZGT735y9+uYRRQVq9YvcqUQD3mprRStOK630tk0FRFDeQlIJP24qyrQt0450QSUZoJxHsQFgIndTleYB61+iuSRjrWy5hLAXZl0Rwcx1GkIwqLxjaWfGiM0i2fptl49HQy30PY+qu/7X3zmyu79843dYToY/KYKqwdsTgge5I+Kw7IWV09UXYBF19qtA6hFojaUZeYK8d45dorKxvb87JIwlCkEKQgDKAYZFFi4V5Kur++vvZmO98CQuFmOl+MppMBrK0mv5MniTT/zP5QkBhbDAGgylMhxT4wiAE0J0h71I6xtmnpNgSumuRzq2uv9LZbR+PvPZn/xVr8UTtCEuUZUdl2G5IIKyt42ghCELbzo8MP2mf2jyv7/pHvRvrSav8zZzclMJld0ANDSRLHtGhMSqxiKV1rvZM5FVOV6BDKirRS4kKoa18rrRZN7WzhoPFQsLAAVpWP5k0QIUEA8V5sIFGCGAQcaEi6ORpqggcQrVEp9MsdSIQBllzPpTfn042GZ4vuRAt/Mkb/5GnNtnQUe61UM4+LfagPHVai24GgdJ5Km1mPTBwQlEC9aI5nzaRpau9FpKWsqsR6UNrWlR+WjQ2mToCt6eR6rceMvDw3ZjbSkRYdrGBu2IV6NGty6vQ6lV9M5ruB5T//xVt/9uc0HF+InE4giDha6yFFUA0AQE7yHYyAX0pkI7KhrqeHmLVVYxNrwdJu2m197qVXf/crX7KuuHcwvtTbMtF5H/YRTNk0w3kM+s3NnX/b7193jEeLCoTzSPc6nVaW7Y/Gu3Noq6/2jDLwfeADhiCgrHgGRgQAhaIQkUFqqxa+7/FGFH8+bb+cZ1fSdL1hBIBL7YtHg1v7g/80t2+tpCOjENEmiY+iwLQE80kI4DDHzuuU3ZgtjrXWqPSwcUdPymnd/OYLFz773ODHb39XK2NQEUg7T6vgq6qZl9OqsKD8fDb3Liii4K1SYl1NJgHWs6l18eacsg/35ULXdNZbdeWbyuvMLEmKHoQpBGTRgXIV9zuioak8kVKR0NIDfFqM89I+vyywToIgTlaVfKrskiVX9lMPbXSpwIN/SuwVVME0jG0yl0qfvT9otpN4g5aeUBAUW1bF4TB41bBfCurrslRBE3KwguwBxJVlM5uaVq6nDaExTQiEWmGSJuIZmEkRGS0IUUZpS0+KXWU2v/vWt//yb/51PruuPCn2CAx5Qme25OkRiD1t+S7RmADAMYVWWkaRDRUEi3Hrcpw+XxS1SvWViztbq10OqfXweFxc7P+eSYbTxWLShLT7wvbWm6zajyaLewfjJ4MhIPZbrbOrrcur7e21zX67O5y0DguKQbXUDyLaV2iZ0TGTQsLEsVSWinDB84tJ+43Vldey5CyouHbh7vHszuGo9n672+3EN9PV/6UuPvNw9o1+9P5qi7O0iZIAWgmL92LNdrr1W9sXfu9oPP/xP3xX+XTz0qXLt2+k3eyj8bD1MJ0F411wYuumsE3t6ropGteU8xnZGk3Gi7rCKGIFrFklhjT4umHvQ10DmoY774xwvpad30z58chbB1kECKAEDSqFiBiAVazjLOfAwKK10lotQ5rlNGr3RFqByEvu6LPO1qcW2SeV+6d+1qRZSwNgQQkAEhLYgJaEk8MO1nnS57DNoBWCChhBlqnFFFinpA3VjRGlFCKKTiMRH4JHpZBQGi/TClXgxjcA2Em1JmFL6ANHABh3U1xPrXOKY1s33/nH746etFTRRV7iuQKtraHO/Lx49m4ZZEmQ0xgy00TGAgBpwKiTrr2Qt5+nw8fEcrSwD0aLjVZ0bmN199h/ONCJuZhodWb7TJKu7U6b948ePRzO98eD6WSw0lndreoPB4dtwisb68+fO3NmfbtsrQwmKwfzPrn/2s8eAlgC9hZnPp35yyp+rb/yhXb7Zpav1R4+PBrtz/aH83J3Mq8BHfM7e7sQ7M3zV1678Ied7mcPD/9+PvomhrcjM/JsGuiqlRdXdn4tXXuVok6Ow9Dgx9/44ZPWB/tvPvjcH35pdXPzZweHu4+eWG7aOkFgAKqqyganWyZKtYlERYzERBiYYZnuCKx8Qyh5C6Lq0FvwZ1fGN881D5r+g0MywsjMoGNpt0KsALQAhihRcRpxYGSItTKKGCWQLDO1PnXuyWk19UmdvtzHnhX5J684rcz0SRDXUmUDGhCdjNg+MPqKXGjNWvnTh8XFabW22hYJaSdf3+7WxZzSLlSAno0xJzZkAgGUICIS5TkhRkrHscZYSeU0BE2KiZYkCkZQxpgoLSx38h0QP9qf23kLmBQwQQBC6q5yyWDtp66xp+RLAcQAKBRh1IOoe8lk5xRib2XleDT54d0ntrO5mieX+q1ra+txFInwercvaH7yaPi9u/f35sMoTQ4G+3sf37v53AvGL4rZaDgdvru69XB06/rW2qWN7tr6i9w5Nx6+9Gj0reHgLSXi1fW49crKxn/T71xPo7R04f5w8e7e8MFgOCpnw8lQx0lTN91Od16MQnB8lATgr12//vxzV/YOP/vo0Z+Nw3dt60y29m/a65832boHb0ONkY67bRXEDUYPvvkjdvbcizdUHh0dHLnC6hyVoCLFGALysrcROAgEpEDAy/oWBAkFKBCSJiIpVDOi/ZonHffCC7NxWUJIkQVAazbKia0CJmA47iRRqoWDAtSKFGGAk8nziQrsdENSS8nys5LrmeniU5vWyeGIAAIagE+iSBBJITIwoo9EldNot3HP6+lq8vFwEKfYaecOAFKl0wgwArRixMakhJBZJCyzAoL1ybqO8hQXHjUiiFu+BUUQp14vcZSEkUYCUmZe1b2VrU7/auVHGThchhRrDXEk8wLAwYlIn05TzYFBOdYCPu1Cb2dr5czLhjKRkozSSeYd39vfe6j0w6P8eL7+4vZqZvTDUf3B0f7d0eT+6PCd7/2zariqq3o6XYlUt5v6ogn1YszytLcxaOoPjwbn+92LaytnNr9cq0vfv79xY+vs5sU3jFkdVPJ4b1LXh8PCHhTFcVUI0aKajg4fzob20U8+3rl+0Rl77spFWJcPDvYNyauXzvY231DRpXenb/WKKGlts9KOvUgIRKDM/8vVezVJlh1pYu5+zrk6dMrKkl1d3WgBNNCDgVzMzHLEgpzhkmu7DzQjjS/kz+ELfwZt+ECj0WZJGxuFEQuADaC1qi5dqTN0XHWEOx9uZAPLrJe0srKsjIhzj7t//onB/m7U1zxtpbFP/+N7F58e73/jleF4X/QOBwnspeshhAOAD3q74obQ2U53XQpoERJARXGKxQ6HAc3q6p8eZv/zLf/dt5/9y6/6mk2sCEFIfFtDk4KSpFcoY3wICrXSKATAQEyMwttA5i0Fq3u28foEyddkrGsNxdeH7Jo2022TkAhJURcTvU/6pnOC7y/UuKgeHDydn07O5v1+37e2XjbMEQSkEKKdyKQxqQiD9fMN16hSowoT7Q+Sfta+2AQH4sUi6UFPMiPAaAjKVhpmZBCNQb589PyVBzttuwtQdQkRDALohbz8lgWECAZBA3jZJv9S2oPBwWB4+L08ewV9sD4IaZ2YzWq2fGaHg2GAyc8ebx5fLYzW87p+ubhsrXv4Lx998X/8gmeVNqq3U6z7xb13HwyHo6t5mK03i4szq+lc48Vq79cnL24Ox/04M+OftOn4w4toujk5X6/OV7PlapXGqUqjQDC/mtbTk+lHjx/+7IvN8Wb63iPJcP2d8+aPfDLsh+CPF/W4yG70ezR820AZAIK3PnilSKMSFJ0aSslDCxATq14xOti9E6dDazdCLQQH4ojUdR5R5+KJAsiaEbTSDgmAkBEZSY96ONgNpVGR8U/Plz9/v/jeG6dpb/+ivHmnABQ0pCKAqkJycZ6zRCFYikkiFBJhkU4c+DtHBQCEJPxW5rV1pu7OFl13XV+zSkVASwigFBAqJIUYABlyoyDbjcszCp/Pih9/R32jePmLD28FismAE8voRDANyVFMFGsdpz1fvmB72fYOB9FuD7RCpcy+yFUTPKte0j+aUKrX6w3GOgSsruZ8VVKeRRF9+eT0k//1//roP80TyHDrHScMbUQBlBGgrq8iMAhGoOlsHvNdPXl9lA6+oaM7nskQG6VbUewDN1V2MF7Uy3pq27aeDyZxni/W89Vy9uK9L5//1Yd8uRYI1sn8ef3hYlOebb73k28d7e/vWnd2ebpqKmeU8q5KzHK1zIq+JlxdnbhzQoEQ3Pn0dDq/MEKktTK4Or+8/OjZ5S8fr89mAtTMK5jzs3k1PZvvvPtq/OMfRlHvydXZL9lOBqOOcYM+NLZMkjQ0noxKsxQjHSAQeCITZ0USJaSAkclEhK0IjwHjZwAAIABJREFUIykiIRRCL+Bt8GXTtJuKIdm0oW1ClBXCwbuGE0ag2De7t7PVcbb+2VfFO6/Au68/+79/MSptrx8HZpWlqjFk0GSxDywCyihlkEE8dNrH6/SJDpoiEcROOoG4rXG/Wy0JkHC7k+5SRjUjIBoGxUCdXlECIG72b/RKLC5Oa7qo7G52juGq8jfHcZTFjFCTNyOdDtV6KeQgjWM9LjDJi50eJUlnxkJDXZbordaxFgU6JTkv2SuVqiB2czm1qeodTaq5f/+vjzeXZIAYgoBnsI6rRAOM+kyIbLf1EYCBOxp4/+j+4dsPItkhkwQKiEiAFDCN47KtlG/6w8FytZpeXE7PL8b7B0mSDdLhp0/OyssrA+iBO724Xa3e/4ffXB5f/vDffP+tb9y+TXgY6xbCycnzNaJLhuHodtrLKUDDtrV129RnJy/6oemTqtpSmMvPL4//6YvVbMWAvC3cEqp1+94X5cmsh1nzxrqUtjfuU7mCICIhjTPn3GK9XG+WeS9fXFw18xpAMwRkZobQeuBWbMsSBYbghIOEECSEJjSgrShovENsnK97O3dNCsvNLGgX52ncz0wWyeWqH/Hk7ujRs7D+5Cz64f12Z3hxvinyzFrrUGGemTxVsfLSIkEcq9gQg/Dv5qP97nci2NnKd7clbZGtDoXXKEYQvpZ/IWhCBQoYIDDQlvmDIYScFndeH4QPe1cfPOcf3sji+GqzGu0M29ZbT44gS4lIVeuGlB5yZPqFzlVQChGNwiASAopXSZF5dvNZOR5GDDJ/MQOj66bl1tFlDL10vTHWDgCqjqbhoXaw6mSIdLAr/UQWcwTNYBkcgAiEKO6N9h/08ntGoUXp3g4hCAi6F4er1bPffLz/1oPdG3d3hker5VxH0cHeUbVegmMB7wDD1jMsEGAAfPbV82rdVD/9/rvv3h/maJmtc7kPttmsXz6ZZxlHsYojAjDajPqDA6fupmnreu//6tHjv/98sVx3sfXX4E73k7k6Ofvgf/uPn9/YeeOnP0YfHl1+HOeZc7ZX9PuDifMuSeM0TevlRuoGgMmkyc1DmhSqoKyId9JBkuN6fQnkwMqgSHvjvVv7w1E2Gg77UUKL1fJ0dm40qmxdIwPBaNzvF8nT4Zl9gd5Xr92PwA4/+fCkfXOc3Bqd/OLyqOqL5yChcxxgFCbRmqKEyIB3DIAk25ixrdr+P/vaJjDjtdV2Nx522XnwO7bvWlSKGNAHVCBgQhBvjMqLXC+PJu1qN1pdzNEf0O7uoqpbzh07BoOCitBbbtYhzhULRlHXIAlGCpAkMAsGMVm/H5u2dpYFe4d79cIvz2fRKHeXG25stdqsmlFIMlyuuzrooWxhSQy8nunxbXV0CHWNrZXrzBICNb57ONjrN+s6pLGKtN4qkLpcL+plOYQw//IJOzp88NpR7whRkcKmatp1wxAE4JrBJgyMoBHg4vz8Z//nz5qq+t4PXsu12kl7qNGLPJrOrFbJZCfWSRZlpBWyc2cNgDl5fvnLv/voarkUUAyBr8NBBFjAEQgBlIupWzdP9HvMvPPt+5N3DpzzaZaNh6M8ybxIsylXnz8NbVns7b390z/ce+2BBHW4NxyPR0EogFuv94rjlyh8c+9WGuWxUpHSGPxytlk3Varj3/zN31/NZrv3biUq51Ia10rbUpZjikVev/aKnn3VzB+eZLd3rwoz3dRZapI+0BxDa0NwgKiNUUYLgAiSAG2dvkE66ipA2EZTdL0U/paRtZ2nkAUdSze9dX90HR2Z9kJJiVp5jlzwgpSM+umItan2dtX4rAmzpt4blg8XUFUxCQKQgygjQXFt0HlARdoQKeh8IVwn0gZ2QLWOR2OFjkDYpNH4/l56UJh+Xs/KalNvZtWaJpBnACcMwUPtYC3QsmD19DepsNm7404X0l4AGAQlAL3B6PY3X8n7qW1sAEkoM1p1yC8KK8Qk1uwMrzanH320Xlzs3Dw6uH0ftHbBetvytv3/Wu0HAB6BCGg2n/7zX/0TE/zoB28UKOw5EhrpDPJBb7RDjkPnFhHHjaLL9ea9f/rw5ckpAzF0c0YHRHdaLxEIAbYN1emHH2tIXv/x77315jdZAqLywZVV/dUvP54+ezn97KlJ05u///ar73yDBNaz9enL9ZNHTxbz1f03X337W69/9dXHEcTc2PnV2m6apqyqcrmczxlQKzn76LkUBxR2Yh2FGqu6BstiCPJeULY/tkcZhCez+J0H66OD+cn58ChKDDIyS+t9TSSRIVIYQKSzSABhEEb5Ouare1pwu9AJX2OiiMCAsA2XQxLpZBcCopu2YDuLNGkx3hVOGhYxKajcOESTSoJqab2bZK2t25kPSjUmcoapIEFx1sXkkzxJIyOe2HPAENxGKUEMLGgBg1EAxMTOgN6N+zsJCCajNJqtNh8dC2qVF0LIbB2sA1QAQYCa5SO6iPMbDyRJuxGEARhCuluMDvtpktQ2eNuG2EQREkoQECQGJEPCzjYbV7rNCWnEKM7z/X1BL9LVU+rmymswr4sfVwC4LNcf/adPXn3jZnFjElrLQSQSZZCDBwhEhASxiUqTnL04PX56FkAEftcyg68VUrr7WAQgACMQEATght1qNbVlW26q1dXiy7/65/XTk5hU73AnSfXxpw+bpiWktYXzq7JcVnuTG9PZ1enz5wnk7cqjE+0DWLG1bVdc1VXTzltVJPFhno32diJFyodGx5m4GQT2Tkvs8qGG1lpUcrS/OjtxGAA8kPdog28JQCtEAoYuuGaLKVzvnkWoY1UDKdxeSl1MNGzBiK2mHrcvPiCzgParKVJNzBUaawrvBQDZVfVsxROrImQJc29pkNskqsuVNz0HmpWlCFsrbmPRujiJEpOxAkyhadr1dJH0tQg4EUAMnXKLwHRAAUrwSoCSXjI6mixfWIgV7e7L+ctr5w8AEBbn3Qq0wp09Pn0hsuXK6lhHWrI4DlHYlEvb2DjSpHH7IkUUoY5T5zBJ0pQiXfsXn34kz78iJ76uZJta1bVE14qm3w7OeHV2/sEvPuLvvj4Y9YsssVU9u3jh22owGmdpDlYQsQn88uGz5WrFIAwiEK6hRCGIDPQBFIMNW6ktEwhDOD+++uXfvmeXF+RYHIuz/X6WHB64NqjIUMnZoNjZPzKprrzsHWDgMJwUTz79eH02bRKPJt0dTDKVeSshaZKoWJ+eVfOz4BMGAnSC3lqp17W1LYhs5su53uRHcZKKXTZ2uYlvHywfJTU0xiilkIP3zqPSykTd5SNb06ttNCIgIAkgcJfUBoBKuPt4ttErXcYqgIDfMh+2l5n22innrOQhHociBV8Bkm/K9bSRccKBmqqtGxtHhhODDtTWF5yBfLXxYr09vlyc7sZ3eybxgf10ur54Xu2/MgDRLXhhZiVaKwQgBgK0gdqKlRKV6+LWsFiuKyA/2ffnLwX89WcsABDKBTdldHSjfvaFLGfbSxiVb6VpWsftajmPQ5P2YgWJR0HB4INlr7K8mIzbzVqaqjm30TCyrr56PmumVkMfQAMoDxVD2zVnAoBbFqRqbf2rv/n5kw+/uHHv1hvvvsYGThbLWp+mg0Ex6BGJifTVZ0++/PmHta0B1LUkHRC0hiKCkYIigAC0ARoLS4BWQFj45MOXyys8GJg8UsoggI4mudy4cfH5OYoteqM8yYyKEVQvhsLAfDV//MUH5189aabN6JuvFINDr4qZpabl1plKklV15aYNYuPzs5Mn6+UJSCPYsl1uALhu/aZsxWUxolq04Wym3725KTLr173UJJkqMVjfogFKMJAEJ0GYUUBIwTUeLSDQGRh0oHtH2u+ilr42CsFOLLzFtwAFWAMoTnc4vsFZj1A4KIx0nCJT4jBuA7eVh9qx9cG1IprYQPAQQJxvVyxmUG1mz79qzJgmg7Au2+fPquVp2d/RiRFm8EIgYmib4AsqWy3C9HI23jVpmlAC6U3CVcxXvvPVvtYXAQKFdtM8/Hm09xbkGlaMAgJiHcwq7bHksFldzPBSDScHaWI4BAUGwJfLaZboYb94cXrattIu6uGtUbo79JeNdv0YiAEU5AEqC3MPawEHANc3GQNwZZvmuJwdX5x89vTg3Qf779xPKn/56Iz7i9FuTLFpnp21s2VXAq6XZ6RhmMC+gpQBBGoCE0FBEFuYMtQiIrXq0binWbdls2rL6aW1a3Urr5vUJNGicuHlCRIKmbbhZrMpF9MqNF5pMLubq7jdtM43rnWudb5uXFP76Rkslmgadqurk+xKJcKxAu3aknp5lKakKvZCpFTt1bxSkWm0CTUVRRJVxJV3vqFIKIIA3gF7EWEk4W18N1CXTLA1Ku3czoi30SVd0cffAhNfbxQBWEvS4+GE0l1EEVczpZRT1gOjfRBsvGtdY5RJTSIBA2kdiC0JCDrkdZDenk/25jY5O2dx1XLqqlUhSds0ENkGgxEGosAe2zYyKI7Vy2PezCUaGA2R9i6AZhWLXwB0gx11HhMAxOKrF79ScY9ULpIgKARpNvblk3lMdd5HSrL65dX6apEO+8QYRRFHZnZhL87O9145jJPk7POnoazrq7kZDjfHS+V1gEjAIpCGPkFiIXcwYyjht+8NAzABCPBiOhttmuGgTwOOICk33q3s4b2Jvn/4/Odqs247oQqCiWAQwZ6CTLYodNd1KQ0DBOVgwdBSGse9xGTsS6kWVVk2Ah6Wm9Cw93h6Ws4iAAmBVbP27Xoty5lWXo17oZbpccughD0Ei77F4FkhaiU6843FtvGRgaIPOktzoqYCwjiLVKJaQBYAkuCcGJLYNAtfpKQNCrJwE0eAhNx9BWTPQoKqaxCFAVkkiCADK2GUAIKIX3vPdKYgnX34dpmLAiCa40zSnujtbeeJ0yya7GFs11VpygVXBdLtgdGoOWiFPoANKFpAeXGt6BjSwku4OCvXl2W79k52VH+/bFfarrgFaRrEuKzw+AUxtz5s1msjNp1OY4yiYVJ7WwDH0FYA/rdqou13ASNURS8sfADPEACwvDqvF0txTTHo771+U497tS2JPQgD+jwfAJvLr07dph7e3o+ASmslBtssIQIk2vKMIAAgQRJDrCFp4dLDCrZ2Ylt6rQDoKB7d3cuzREI7GCVlCZcXUwwvB/0kGeayXgloBIpglMA+QvI7S9uukQ8ApKFQEAVYCvIqBPDGS9TqxOf9sGjk/ExKH0yxuPCd/Bq1kRDEC+hUfAVrS3HEtvGWVUzFQRLHHf0qspC3Fz3NY9PvQzzGKCNt77yWhqb32W/O857SmT5fW43aJaiGqTKGSTXiRQVjIq00io80EHFwLIGFxYWA0llQESB3MLxI6OzBOyspgC09DbdP0fW2eruRFgHQgIqlRVui6TF45krnqY7sanaFarCqyzr1nLhw9vLA1TqLPLtAWrSwiZ20Ah4DQAjVol1XtdQblUckelk7cuxa58L8pLfjbDS9bCw7pSNBFQSvLoLlYPO6rcdAxGUlYDvb8etRAwEEtQKtmcsAqwAtANh2Be0aAVaXjS+bfFJk/dQHT6hmy8uDJB/u7DzV8cXTUlIXjfuN9Wo3Uz3d+kZQbXcR2yx7AAANBYG2kDlYMFTXwAExSHGw8+Cdt3dH4+XiSg+g3PB8k754Op9nC8iSLm9GQz+GMUB0rRkWuIayEBoATWAIMgQf5rPlV0/WCsQ2YGtkC22DHEQQVQpaQUBkQFRIMWWxGSvUXkWYJDE3FSosxv2dWwOtq8XZ1eyqbKdttj8c7AyyQa4kZeC2naYDkSJD5X0Tlpu2cq437jUqYKx0ValZCSwKnUYgjIQdIRCjMIuEEAJ7T2RCJND9KkDS2XICB6Tr4/Q7jvXIWxrgdahw9y5oFJbyAtjBqBAB9MEuls8ez7hdb7QsYm5u9qVa8fMztA0lWlmHbIBD02qrEmGRphZnRTuuK1hMuRWO+xKHBZvgQrtYVvMWi773AYSpSIC9BGd9u7ywra1Fr/Gg6VgMBFEAtXUXx257IMhBwHlYA7SAsI26BmCQplpJVdf3dhxCBElbz8vVbLK/UxwdnT0sj1+EuEj2vnVXZUkyyLB6uX44A9ACPoBF0N3aVAARoggmCLGFC4Gmswxm4PvfevPP/tWfavEfPvxguZntHZqLWXa5HoXNaWBWQAJkoEeQ8jaCdLvrvCYpMQAz2K0Ob7UK9ROeDHSaCGhWiZrc0EkmzKBYkJSQMQZVjNZkaRyZNt/VjiteVL1RkQySfDTs7/QuLytCvP/2q/NNnVCRm0h0Q8Z617a1qNhfPbsMm8Wzs42JNqQhmtblYToYqviD9/WXL/hQEzktAuA8OwFkIWZwQazz7IUUAkDozgt1pqmdB6/A/7+jwmtOgACyXPfxgKh1eYFNq7nxSV9ChD6sQ7noiSqSl1UTDoZ8ZwCrhh+dqTGRRkvoFdp1NX24CW4HyHKokUDqltmqIkMgYBJRNamANSsPiw22jvIi1DVXLTrBIgEC1zZsPYcLlQz00YDpnly8DNAy1Ijd0onYulCuhTxDKxBgq2MTFtGoIjOgJAtRNJ+XbAHJtM6O43h4dPP4+KxtjVd2ckMf3b832bsZyej0w5/ZspsEAwN3uChCJ7oDDTnCTgtrASfAJireePedu/s3lffA8OHjDxwsDvbi+dTo4kacXhK8RIAOtv36Z8rWEtYwBAJNEAl4hkallOyNW69wPIa0wBpIpSrJ0WjFLULDoY2TfpwOkDCCNteVK6f7ezf7+zefffIoYugNdTKQdXW5Xi9ZSzIw4x704xga3wSGSGmO8lEEii5ZuwEs7rAMMqwdohl8553B3Zvx3/zSvTxrdg4BCD0DhMDWozcEAuy8DyEAAtPWvhYQBJlRWDp7agARvk4P2BbCa577bwthJ6ZgBYqMt22YnTmTcR7hUaLu5U5b9h7S2PSGdFbGoR2NxhigZnSIoWJ2GhIFxECMRECKTA5Ko1WAJFoEFMQJxQB5DEigIwSQqhFkpZBIc2g9CGgIpw/1cGwePOAG9WrlwIu03c0avGunzwGKrS7umqO45SmbDOLx/Nm6tY9cZXrDJP7OUSCfj2KmxouSUr/86KnymA/2qIhUHkMZEDRCLAAMzoNDcAQKgAgMQEqguqJm0qy3cwPRMIf9wc237+Hnxx8Hd356qs8vNeW3MXoslq9ZScLbrl8RqG7DjVvv6shBq+Lo7T95+/J09fizBWEc9XZQpyRBkUd0CKzjNCoKBDBcxaaMUmty4yNLCZlhbKt2GZbr9cYFjjKTFklZL3r9gYmi0nlUmkXKRenaGrXUrfeHA971gA4G2ejNNw9efzcCs8wehggNqRAIpIt4aXxoAcX74J0TYSRkvYWoCJFYpPMUFOEtKgoCfO05+ju3F1z/hQgAKn3wA44LjvscRLwHiqK6jhcrbm3wXo/7u299O/dmsrh8bRQb56e1uZpTvbLQH0KSIwIQIAKRAlTERICgt/wzoi6zWCNqRFDakEkwjTnSgAgaUSkyBppGNhvc2cFsAtO18Eqg/a0Kst2I34RQdXQ/2C6yCIFUMlD9iXNYtVyvmmphwcS6oNX08uLJmVdJAFH1y4P944vz6ePfPKnPS7ZRV3MJlALqDBB528trButhBmAVKMTM94a3X7m1NxpY5+IoHhaj2jcvzzfHJ9bayM3PvV1p6BEkDB5BFBgC2v6e0GXPKAYMgBybP/gv333rG5Dgr9CYBo50JAm5CIPYxtcr8VZp1e+rgx29sxuNdvvD/R1PtqlKRcQYats4H9gDKYqSKNKRVoYZlY4iY9q6nF9MgXQx6M3OFouzCmelfrw2U0ms1gFCoiWNYrs5SONU6UXZzNsIsv6dN96JkkFZ1tZ6FCAk0lvZKoAASWD2PgCgNoiEKIDICApQXc8oHeyAiND5riOAisbfIVBgYuAg1ZLqChcbP2e5YDqulOP85o29N15PnJ8s55HIeZleLrFdl5CmYGJA7uBXgOv/S7F04FrHPZLOCAOxE71qEk1AAID0tQ+isPgAimFnR2qRzYlcz/8EJOy8r75OLyNUJuqbeBSlu2owwSLHOAUVYWxQm2oT5rPNcrG2ToHKvaii5//Df3Meu5Pf/AvaCphNt8nu6mBXChFAtov5JsC8Yz0oiqer+qqe37i5d7CzOy8X83WlVf/xcXlyWYXKuqvz4BYIhiAiIAJD0PlEk2wPFgloQQhGcYyvfmP15//FB3d2P/7kE1NV4562BZYRNxpskoY0xoRgkOleTwG2dVUtzq6unrycPjuV4PIk2x0dZmmBQOPx7miy0+8Pi6KXF71erxfHkXM2LtKjO3d2dndOP3+8/PipmTM1fSzj5sls/cmTqlr1vvNaf3ffHJ/l1M6asHBJY3Fy43YyPiirxgdGRCJBg9oo7JoOxE5KigJaK1KdiwN0GzbZtpUAW9+YbvMmAqC3dukiopD9mkKgnRu9/QF6WS/L5uFi8dcfZDuT7I03FvPLdLnxLgodCyFYvPZpFoWgCLRCTYoIiUiRKARA9CJdkiczsDALS2Du4n5QgIEQFKHCN988KO6OfnP1wp6G7hgRIAB2dikAgMAIqEBHvR0a7JNKunUfCiJoJMJIvIT5ZU1agDRIi4ZoOL5z13/3rZcff5z++v+dMJNy3V6CBDr4QQg0ghJwDBsA11UxDUBN/cu//WVo6v/+v/v348nk8cX5V08vr+ZWaR0NwGqwIAwlwBghFiDYull3+DMAUiAVIvAxHd6q3n397965/ey9WZFGMkqOB70bhjWSIkWohAVQNLBfXrXelaG1q9MpRtI7HEwO9vb2bxWD8bopM1vlaW60Uqi4iwZCxKCiOG59S8RxmkxujZ/9vGR1sPPaYb8XlrNwdblp/uX5eZqEH7zpb+7Hpy/EEwFU8+mjT95Lj+77jtEA27Ch6/JG16dFGIJAx/YDEWBk+Z04MLre53dbiG5XCkAiFCCKKBkh0eTBwRt3vdjqq7Pi+Bj8F5cnf/ef8p98EyZjNWs7ez2JUtBAMUZJHqdx1s97g35WFFlR9PI0jrXSmlF1fBZ2vvWtbx27UJdN01TW2cuzabWsDanQNvVFCQl9/3sPvvuj+/WjX3/6NEY5qMq1k0YBKiAAdd3EAIsPqxUNdiRS0MV7AqLv6rHgdXoVgCBZIhpN6mG/efN+/dM/fvzocXS5yIIN7IgdaGUjkQ6TQUAAIyACHoERIowHYAph8+tfPFqWf3n/zdequvni4ZPlDNlF0VCSvX6zUgAtgBegFsh3+w4tYkBYAK0pIBlYk9F/+Pfzf/uvzyJoQE8O7mXu8SxND4hGgqjYI7Tb/oVIKSAbBVT5eJgPkuHueGfvZjoaBoTGNXXViGARZ04sGTRxDEAioV6Xbd1skioZFONbB8n+XqtGt+7Qq735spd8FvWfH4fyZ5+5QhY3BuaCkrYmHwDV7PJsObvKit2O8yJAGom6JxoQRAkHFu5ytLciQ/htaOG2i5evySJdgQK99UTsTl/cpzQaDf1hfE6RU7f6TZtePovDp2ezB3s+NgwIoQ1e6WHv9R+9c/uVV/v9/nDQn4x6o14vS5PIxKmhiBBAmNkDd21gJ2FmAOdFPHtvnx+frld1mkb/8LN/+tXfnsUJZUWyW0Tvvt17PX4ti+KrJnzxdP70s+fNctXdi0U+Ht4YA9dpr3/w1oN4sCeoRZH3tiqrTVk1pbWt9d6yMLOASKzdu2+d3z1Yxgg/+fHm739x9rP3e8gmjfz9V9dvvnb5/j8UX360b67x/i7pWCAIUkiHYIakBQi//Pj0q4eXKF5sy0zgozBjlFTpXvBThg2qwcE9F+ftao2BJMqhn8vebvPGG+vxeFOW/O/+pB1la+ugbVZVdVaXWuCyGAwR0IFF9ERIjJFCjOOAqvWtmkT5MB1MdvKiT4SVXZ+9eFmv/dGdW1YxYEhVJCSEXK+X7XqTJGmsI4UqSpJoOLJBI7KCsD+eY9xvbX56XPoPHq12Xr8yOAqlUIY6D8yubjBnQQYgIQQKgCJICCIiWxNl7Cy2hIm7VooEBbuCKFveAALwVhqmQRB5O8EjKKWVotqKTUFuJNVyotfHcbWswnqzJI6m86IyPsSDm7f/27/4s997601jIq0g1HOuZ+CvfN3gqkH2IbAKVoMXVKTzKCl0VmDeM/2RwjRw8vrhIInTjedPPvsYUHREpJV1YX+ER2/Fo8Tg8MY8+6NfPV5+9OtPX375ZH4+Pdid/PhP3h72lFF62N/TSU5JbvK+zlJWVHq3LJv1uq03ddPYprHrTbNpX7x696OIGudhOMZ/8xfxK2/NFZW3b/G731rtDVb/y2bv88/GwRsAESgBNltFkNKgDYARCKCIFAbreVmjD2iAtfeNVzrGtI/rqwBLykf/1f+w/OlPnl6dS9uqOOXx5N5koA6HTyS0z05hb4irir2HNKp3RnCVpavLE63GcZoJMCqFQGh0AHFVja0gQz4okl4KiWYKzOHs7OTs6ZnRPdtaG1GUxqCMIDjn1ptVlJjhZJwNeooiioxKI1ly1WCZpj3VTvrLV44G68tkdVbKumyRNuuWTSlFIvuRaC+yJWgIMXRh7F2Rw8BbDzZh1B0V6JqNBQKCncH715vqLdYFuvsnKJoCAXhSyOgdcgSYgL07qk7H6nmlseYQOQ6ehcNQ6b5fPPr0Bdejnb0iDtXFe27+TPkavCBz19B3dDEAAtStiiGOJU+jrK8gZhZQqtfbbc0IyjUCGlBaxQyopVHSSkBYnN3MJ/t/+r1v/8G3r6aL07Pz+tlXdyPsdcvt8kIqAKIQxdQrssFoPNq9uzthE0sQDCIitaOL9Yuw+OKzx5/fPULL2U/+1R/+13/6jNyHccRagfdw6/YqKrxfJBrWBDOBikEQkIQksBcGBgyChMQEQSDRWCS9QcHB12VLNpO1Ziij+OruUfOjb87DaywMqFQlfrPZRKoWBZHG56dgIhUrMdrv7UU1Pfjy108306c8PIyuIupKAAAgAElEQVSLsfZGKRJDjfccRIOgUTqNg4Vy0UiANpSnT65sE8WD1HkLGGudMpBiWc2r1cqmWarSmIwCYK01JjosW+vZsvJoDDU3d8v5bfXpMeHxRifaUcTeS+55V7Hu1FCCHSutW0tIJ8n5+lOE6507bSmNW3LNNX93K6vYsgF1EhINSqNGdqVYMBwRG0FADOT6Wbm/n51+5mjl/IERZumneDBUrTv71cftZ0+LSb63T/vFIoUaWYIXDgG2kZHdWBRAHEsFFeACLHa0aWaRVicNJXy+Mh4joiSOZHOl1ktCBah88NXiNOdqZ7Q7Hu28+frbzz8q1g8/a7xXnSxEgH3gUIZmY2eX+vRFMtrJ9m/qwQgVsYRY7O2dnbX+fTTvn181FO2MD+8zcaqMuNY6UBHcvduOxpvpUozMABre8kIgG+aTtw71YDeEUC03bcXeW0YMKFkW//AP3j67OP/gvccqjrWKOFQDXfYUtY0OrSUA0srJ/qo8SxDSGAih3NDdWzDpi1Ewffn8+UlPFNp6ZopBovYgKBQQLxIEGQFRRYYZfFMLQFWumratF1rrmCKjI4U68t5HsWma9uxsEWrIEiXQcVdEaR1pDVKBeAUWMYhgGjd3bsUvX5J7tMHvHW7GmbKk7w6FEMM1coVCiEjXCjDshi4Rge0MttUVbj2N/jM+/Nf23J2YYk+GGkkztaGsQSDyqcJIANF7FE1u1ONY2C0b/dqEbh3w/lhKl89DkaJe1a1dT2vo3xuP9wdaNxy8c961EhrbudBcd9OCHYdn69PFwJ6laSvnF4AcVKKNCtWLT2A1p9GOCCCQ2No430t3aucSXYwmdzenx1DNFHfLdFSC3fIHOUi5bpuGK5vdfiWeTBrXLE8+T0CZFHf3DYkcXw3SNie8ndG+hOcdxvfK3fbVO6f18dCE1vlOpI0B/N1vvfIX/9Ofp1m/aZr5fLlatL/6+SdfvTf1oOMkvnUrxkieHmcrX5NJVLD9gR0NQmcD0ElZsuSONhT4QxQZ9qFxUKSSxtzPgHz17OFK2n4GkFEAZlJAKM6LD0gCConIuNaH1nvx9aptK8U2QYM61lESb6qGpI3i4vh4Np814wwjw2RYAIABCQ0xsmWIWw8aOySbhwVPJun5tK2jqP32K7vJACd5/ajFXRZk7vQ3OrDuIClGpCAQMHQ6keu9wjUweu0+I1vldHeutuiDzlArQQS0wgFbxaEuZclNfyTIkVeSZBLFtDmZorpBP3y9ulr6T84KmSQFRSJJikkirl5sFpDEYIw2caKKVIJA27Zl5esKvSdQQMLYvfXUMfJJeQYMnpkdkXb1ajp9EpxH1YFAwuzYt6FepAJFsRsObq0udprjqfYBGAEI5et6LyAEwnY5o1kvHg7ANri6sNWce2s8Cr2B7OBupIdZpBQXzgMABA83bsh3f+/y+efYLCP2jsF2ZMN+P749SfIi9ZDyzaLZ2NMXLx7+ihEENdbO7t4s/uTg/gf//OXLq+Ng18N9O5mUIiFc77eTeExGNSFKpY1TMRGsKx3HHKVyeARK9Vf1AGE+0qQi7GjzdS2eKYoRNQqExjF7DsybdWhbIU0KMNYxM1xdbUb9fD7fvHy2ICYsUCmjVNxl+oEWIA51XVfxhQ07OWQ7iIyRCeM99fLD1p5usj/+ti522g8fytTKq1sWTBdT5NELkgGFEFhE2HfRawKhkxALdhqL7ukG+HojIth5GSGIRuh4WypII2GNLS5WkW5CVGA/RgXBaBvpIGvrq6Y1iJ+eJCc+OUqFoWXKFe4M2yyehVrqJmqQ0GhKYhMXaTFMd2/bxtXTF369UNaBeN4eBgQMCEFEBwcYOAZxm8v14kJJoA6ng0AU3PrFcv4sV3nxzWE2HI1u3JsuTlW5kNAJ3QDBAwYAQtEA4MW33Lbsy/WFuDVLy/YSEQJHaXorirNEV2B9JxRghn4K3/1u849/v3pe9UFZCQ4AFFCzmj39+L2sN6AootA2C9+ezRWDUABhH6Bctfmg980ffd++fHzy69PJbRgObWAJ240Ba41JfOg3MUhrNKQxPz/DxYbSJKRpm9BmDbke9qJ0RKgswLKUci15BrEShtD60DqwpWNhB0oMCnkgZeJkXVrbeDU25y+n9Tz0ehoQVBILaBYhxQggQcT51ropsFm1kxSjjIjcaBAiFdzK5nmxnM+XHzwu/EHoCKAoQGTFX1XTOIpvJBMCFOkGbGYQLxzweupDIgFBYQQCARAm6Rov2sq/AICCCIZQY+R1P637eAZZPW32k+pgKInGKAX0JDbAp8f04dTwTsQGugBdGzDUBlsEszUNbNvQ1B6WdnGZ79zMb7wV33y1On/qfv0P2G7aXsGoEJUgKkAv2AACsBEfh8qxFQq4fZlEbNqL59CWPunZ+VtwY6e/94q9eOnaNYWl8955ZEEW71kAjTGRTpI0T4XINhvywQcIdn1yZYd8czx5U2nUqnZcbhFYARB48EC+9f21T1rvZDXl5aluykjE29UlruYAqCk4q6SpOgiPvXNt+/JsMau+2ju8iYOhTqM7918djE69+7Ij/rF4gjJNbs/WY5EVCBQZx4ZSo4wK8wWyXQ73x3u37iVR39qwqJqrBQCZmEJAcEGalq3H4FkEvEIgZAYmqJrgqjZOorJsL87KIBLEKkoQsKmbKNcaFXvvNjUqktw0Iz6GQVU3vaY5HPhBFg2HeuaDbDbLX37iH83DrX3r19zRAYmqsFrTog3xkOOhylk4yDVrY8tvwGsBa4endvRlEWIRACJhRBENgiDKC1vXUCFml9Y3ojo9vLTuYrOZNuvDCLt4PmydPq00pARa1YEImIN34jwIahACaAEYUHXDgm/a1fGXzWadvfIOOccvnxuy0eS1hhGCBySA2LJqpWUgCBKRBK1ZiDvmNJCAARcUB9/Mm8sv05030t5+MtpJLmeH0ZTEt16aVk834WKj1i7DqB3tSl4kAcWg9oAgnCbkODT+W1F8B9ErvKrDsru5icB66Pfx29/1560HDdbC6tyfPZQoY0VkOACIAgGNygRBBlK+dcvp1C7bpuWT03XdUL7ve8PdPP8DaP+y2Xze3VggV7EZNeHA8lONEMfYK8AYtpaeX93mwd5gd5IUI/ZUls3FZdtyXBQQRJqWnJPGd4lJJCiCDEogYAg8v1igYp8386tVXXtUEIAEsKmsc6Fvcp2oum7qdcuJgUmE93rh9t2Lli9PLmaLk11fqnTNlXXLEuo2yXKB4FwJKCSgEQUkiLUCHoIgBmHmICIoSNA5doNRRAQi3N10DoWFSZQChZ2SGkULYBCom+Va2/beSD9Im5xKY2pSVu1bD4unx4vyBQ8LIEzHvWjX89O2OZ37SQGJqduw2cT9gjQ2Ag0AI+ouFASRANgtT1e/eAkvXsDLx5RnPf378f7t1elTrFZIJEHACyqgqI2UF51YVeM1iM7kCZmQhRt/9blaf9dP7jd8+sZo9tZYayQR571sanWxpC+P3VeXYbX02fqCQEQkYKKIKQxSdePW/vcZIFbi5KmHTRcU07S43GDV8uU5zF4SaDA9GNwIyajKdMUBAioAUFuxBWJAVOC8m84WtnYqQhSMBrS3Uy6qf/zV5zfefe1/TAZ/VW5+KWytezbI8snwB8yfIK5ZZLYKbUuPTg6+WvwoPuiL0lXDyO10UVeVjrJIK3AcQsPgAa7priKIrDqFkmNW4tm6clOLSggMAqKiVdOsvVd5lo4zAFjMyjVl/vVsfSfJ0iRNkuTGHt271yzmL754OPvgFyJORUlyaxdWsxA8ty0wCAJTyOMit7lSJtZxI852pwoEGBbNctEua6r6ce92epBhFEBWYX1Snde+TVV6kO6N1ZAEWFgHFC927VfVgGwvbZcczZ2JrO+nm8TjwUGse+X7F24/1UUWNWDijXd2M9tshr3i1pFDv5qGQQ/6RSOCAHq7i0YCFkLixpUPn5iz40RJPV/T508n3/xjv39v8+nPw+ZEXIvMaCRKbKocFn2tIxVF6DwSISKhcGfCW055/lQNDjI/3091nGXAFgR0hEmudnb9zZ0m/rh6fxYtZuc9zxgQQWlU9SY+frn/ymuHiNjPJm0TglDV8GqDrkEBPl7mn50fvXi8sZcb1JzvhtGRG94UCho1igQWCqJEEFkwAHtoyuAtYyQkrLQPLS3m5UeP/vfN+ofvvP7T8eBV3/zCOTfs892Df301/Wt2n1xM8XJhyjr/9ePXFvVRHAFwaBnLdTlftYip0YQqeGEUJFYESAqDMAtpAWTxzEwCGNqqRiVaY1eO2Pv5LDCqSZSQEha/nC2agB5Ve7Ky88vqg3Pz6t3eG/doWOA3XoOvTjAENeglbuyeLmTJwTkQCMgX/jJO9I3BIQDO7cp5n4U+ovbSzvzVzM1VgkrhRXMawN4t7mCAZ5vjWZjHKq1cvXKrm9nhYbKrWGtBCaFpeQOtVe+vReqAQCnpu33/IK+Lpb792sG//cPnizNKU5WGIEsUaZtm+vQs6w/NOGkaWK4lTZVRSgC/FmoCCrNfvzjbPDkbpkYdjtrZ7OqT9/Hm/1P82b+LvvNH7ul7YfXI61qIkBDZYX+U3zw01TpcPAcRrWOKyHmngIBB2imFukc6JgWAgDGAvmYHYb/fHA42n88LXVfGbDjOg2YUz6G3mM+fHb/3jQd/zKwul8X8sodcxwaygT1eTJ6GPy9+8Puj6T8++8t/8LVdXZrZ06j4cYJH3c41MOiACDqwESEURu/+P6be5EnTLEvvOufce9/xG30Mjzki5xq7pqa7i1Yj0WotZBJCjZkkQ7DDTGjBih3Gjj8AYwmGFlogw0wGgjbGBpUQXeqmsro6q3KMzMiMwd3DZ/+md7z3nnNYvB5Z8rWbu7m/73fvGZ7n9ww8MECivkpfPbs1vfPO7cy8PPuXV9efP77/228/+Du7o4lx+0WSlfV7T08+O7k0870fv3h+7/ML8SwpQGRqa15dtkFdmoBJBBGVDSgqWkWO0ktEIBIzzCdRA7ebKvqQjBwAK6Egdn0fmZS4Wp93LSZJWa+u4fLaXF+RdtJri1J/fNF89ty9PXGPbk//ym9h21Pmksmoz4kXvY+dIhDRRpcXffMwecRBjrvjcTIe61wEFvH6TM5n4+k8H0X1rWmvw9VW3LKR1lJPi1mumcuM9/5wcxQ03MnvWgLwEMRf2C4Yk0mSiDHaqDaVuVpyXZ+7dOfxnfRKyVpr+o4lSRKbpKvlojw63B/f6dGsFm4y5unID7sjUQ+IhNRfLOtnR8je7u7RNLcybpeX65/+b2Tq9Id/pXjnt1233f/ZnyE2BIixN+My27lXAq02Z+obO7qlSSJ1bUFQUJoGQ5cl1pkEnLu5o5QhemAAEWtsggkEr5E1c4pGRK3LkYp6/a+ur8+OPff+szJ1s0JYq4v1Izf9T0zyGyn73W9fHP7x+9D2CNQ3tqlIKDI6UbIAA5N20NcggDAgChhRUgrg5nem3/i9ZvbAuANp/+dPv/zvn796+2D/33jjztbObP/4ZPTk0E3mk0dv/v1zfLT5438s0ntfNHXcXFaxCZQmir1JLOIw90ZFbupNs1iaJDGZcUVqXO5D8ItVd7k0sxRDdJQZIGHpQ2AwKt36otlUk8V6tTxbmMWZBtL5No1LDT6upf/5K//xE3xrxj/+je033zZZAl2OCSlXq+a8wc0smVo1Xvs2Nj6GALWVFDmsY3URzifZdE5b63YRsLXWGKAmNKmkBg1H2cRVYu1usWvJHVWvBMEyANgkM5PeVnFrNt4vJmOpa9ms2B+eweWzle82by/daHp370APl22QtCi2bhX90fHl0Uk5LdI7O1VtNrUrSnQQB06lIQrrtnp6mLWb0d2ZKaA/PDT5ONudwvVF9+d/0sUq+9Zv9lAIWxLNitxaGznWx7+yD35j/OZ3pVpMDt5uuoouToBrzApMx4LWGDDkwGU3HmaJAAGEAcHYVMGw74S9QgTVYXwaebReR+X/c5x28xwyZ3yvR1dvz3f/8929v/WrLz7RdrXz4O78G7dPT69ACUFBOtGgnJso5ARUiAcb3WCzuxn4IjWPx5fr7nq5Pt7CB1L+QUezR+afbeGvjg//4ulXe2l+y+D6YMtNZt+azX5vZzdM5pOLVfRV1zWxX1cAncQA3vUZcJpQYlCkW7bV+Tpcrc3IUE7p9gSg71atVA2qosawamFcuiKRXrtlFclR3LRL//SDk7apuktAclKkuDvfv+fSrq+atOJp9UrgJ8+WVdUleucbPyjzeaUQ2V/XL8/k+Vb6HRccRO2l7jV2y+tNvwDaXGJv02zX7Ld9XZnGOFJQQeikN0AkEiUSSR1r6+290X0NfLh8ZkE1NYUW99d6hmOzfyc83L/uO3OxHB++2Fo/r83H5zwvaDJxxnWbFqOgb8v5rmvmq8uLixfne5MR22KxtpO5MWkEJkInrayennSHVzt7ZTov4mapQUPs062CY8LLS/Ps897p5moi644Etg7ulrP9rnrVX39Va8wO3qByW60zRZ7v3+YqTfYfuTd/s7ElSFAjYHRAUSBZYAdoASko+Oihb1mjKA9IBYMxgHtxsfVmd3lvtxfGtpPDy0fbu//ZW2/8+4d11YUlawBnZu8m4YVffgUsJCysBkJnFku7lfdJyqrACESACiTGUASwxn9jf/nZ2fXzjz7ZOtjfufvOOvvRF12WlH/03fc+kHC2bi7GZZqkGeR/NckfxfTz8Z2tF5+9iK1AAG2DyVCFfe2XTW+muUktVq2vMRoriVE20KI/XAASJjliZqcOUw6bbv3qcnpHKEn61UpYLLZ9I6ujNeZIAXl0i0mS1G7P/TYt2r5g557l21cfN/J02Xzw2cVkbiYHhtmKStect1/d235oyYrIMqzrzSpZrP7yO99d+83qYjGy33bg1tCR1dc6UWUIBrJBsYWKBkwTGh/Dg8kbfaytAlpVRAKxaWLmeVNCk1kc7UeGcXc1g+Nzfb72e8uzk2ejtk5E/XLRqtvZ2+421eJ6WRxdTdOsXlFV5UXSGupUtHp5UX12kthUd3bqdZ2nBT3YCy/OXdvYycg3nZ6ep/M51MQRxMR8Ns62dmT1zLD6q/PQLCVK8zKj3LnRrLj3rfL+D3R+v682ELwYAQLUgWlDYCygAQEfQh+YuIvqRQe3vnrf9eS27/+Nmv84xD9hxtPV/cnOf7p392+/3Gw+uzy6bNeLTXX69BOpnjz6YX9k5PRJ0vfEkOD6Wl8+t/ntPtsGFWT9eptvkEgJxY5G5CRcv//Vszwt/vp2Ntnt8ZsfNmkbi9+68+EbB+vA7VfLt7amv3/Zxg+ff1zV54gKagCVRiWmFhgIOSLHRuC6wyA4LSFPlSKoQZtD78EaylLlqKnTBHWW+D60ddwt4c7t6vqkOapThgTbmrooaR7G+1pdYRQDUFiw7MvJgu7m7dW8Pun0w6vlzqfh7nJCHoFHIdeoh+FliSWbuJF101zuVJ7b+o3tPOCD83qsDIiQmmTQQBgkUEhdRj0RAgg5SjptO+13kmzsUyugDMrAatQ4tZZRKLKi8aNRKOaZdAWfx/jqet1zHj0KuKzoNg2N8u07s9Nnp8vjs3w6pnSyWpr5qLBJbC5Xy6fHGKR4eKuNTB3mj+6FnREx8LPnNktgMg2Xa/fqlTUFhJHGyMtj4VkfN9pXweQUekAJ0MDGxNUavNpsa9g1hHqjKYEaAAaUQZcNCCrqvXiPVjErppyN/OYCQDadL3cOfvi9P1wuurP1n3ehzMb/8ePHf//Dy9OfPv90HbrrzfLoycfdq/dLPhlN9N73gkQF20HszHIBiwVWM5zuqKDeiENFiMWAooAagESWHL64utz7ePXjHxXjqQLH7N1P2un6q/lfuvPTkT2N8CPMHr3/6otPnn++ePYlhl1jnZDi2KGSGkULiKoc0Qga1NSAmiTLikIpQZDMpAjkm3VUtanBbJIZddNc/u534l+9H//ZvzD/6Ge7nSZAGYeG3UizCa2vwQtHtjkxQgxcpGG8n/abMS+Bn1xUiXHqc2DbBHctx4sPTFCGgAjctYcvr//Hl1/8td9+Z+vuv3m1MYzoTNr39aQYM0jVVkmeFG6SYFqF9Va2GyPHsCHEo+effPr//MQCSgAJxOwUEkXTWvIAtg2qqqM5dSvn0aGihMhdBA/7+9MdT4vLxfRgXo5HzbpaHZ/jJN9sTNNbK7j88qq/bub3dnTsutPrnTfuynSHmWlrW67XcXVlxjmsWzm5QJrYvrDB2MWRv8o1hhj6yN6CAzREPMyqmpOn7dWRLedsres/p93RrwkPKMMbpqA+RlEqZ7uzB99cdZ5ZQABdtn378e7uHcF/++jFk63Je/t3/95J135w+sWr5rrru+NnH11/9JMpfDkqNDJkY73/gwD+OrYnurlC9VI1GGO0gzlTVHhwc+qNJBxjgGyL0+zyy5/9vwmlu7fvRemh2Dv0f/OPjuZvFcc/evcPO7W/eP7Z2eV5rDfs50qooKg3zmxFBlUwBLkZ0BsEPN2mnZ0OsY1srENrupXLQuDtSWVANq28uZ38O+9u3j6oP/lyvvXlTtViSEhbEjvVJEdkEe8jkQMnvmLpYzIqs248iqsaVl3oO48hB+6ev6oul70EDpGUkAUxyqhc79H56RfJzrcUtwBgmk7ruDhbvwoq6NJpMk8g2cv3N+vFq9XLwGE63tmcnj753/+vqy++tEyqAL2J7NQYtdQRekZqBUPktGBnQhYQKWvaWtcdtpLvQOaoOY8SAMgIiDCrQFD0KoFD7HqJihY5MnhAZ/U1CaCPlES0EhEiMcRAytGiWI1cLYwPrC5KQAKDINIjBiALmmLP2rcxLO3oGu0bgAqCIHwjjCUd9Pwmt8XdxzKaVVefQfSJAkR5eXl6XV/tbX+vSP6L+Wj++Xrzp89/fnh1rC6rlpvTT94v+88mRU9DzodCNgYHK9gcSeWViOuWfHejziBGsmgIHSGBAvgglIfdvaZ0/vSDP7u6uz+/cxsIRFpIRtfwB8mte3v73/uz05eff/HV8198RtQmpZOYKAmwalBjjUlTJEREQ5aMIacki3HWJmnselVUEk4tz0aK2k3Kmn2ou/xbB/pgXkPknan9xuMty+XF6nz5YhPIihmoitJDyqTAXc+JakgdjCZULzsLiR6fRxaJlpfcHV1iURgwCAZYqOvxbgqzbHN5utmcKD1GhdIUd/NHJ/2RKO+Ut0ozYZFZMn80ffuseYVWS51+9ac/uX7yhfHBAgx764iqzmpCNw73qCawtcabWI9jvSPx5HnQqwxx5Jf1qpXxfNxu6s16My7Gs3v7krsIIoCU2PGdrXC+WR8tZ29mpkzWL8+356UdJctXl+3Zcmea8nKJvsODO4v+TnUECYSUCu46rjcIwBJJAwKSoooRQARGBEHLwqoDZef1rv01+muwlGhifZJenT6rT75A9kKm2zTPFr/aefcXo9ntW6Mdb+JPv/zVx6dfhabhni9fvXThbD7uEzPQCF9bT3zA1aXBqW5vh+Cxqg0n8PVvQ0BLgMoaA/STg66ddqllSq/aiw9efHbv9uP3XJbFKOTcUpKfnZ38y09/fvrll+svqmxqt+5O8vSWgET1wpGss2mChAiEZMgYjG1cnyUYg8euRwRwVkWdJbYUCOs+yDwd/ehBTIsAHW1N4BuPE0p24Kvl8mVUlxpyiEYlRDVeDDQMIlQoOk4KIF+zIGxEDlcx34LRyBpUH5URrAE0kOQAVgIuVpv04jnt/ibZAtDMkq1JPhnsXyqApES4m+/O8m109OKXH1z+6lOsOuprizcDZY8mkqXBEIMqJIAGRSBsGpvJHFPtu/MuDQLtxdLMtgHx8uycCLbu36a9WWe0jEqRFGx5d7e/qtafn7Tni/H+fH25rl9duq1i/fzMuqRpQ7ZqivlYH96vXmae6xw1sSQ+hKpKXAnqZBDXUACwIEZVgAKhF4n12rd1U47GQK/htIO3BI2KKmFTL/zVCXW1kukF0ny047v3/+JfbNLp4/vvzSaz54tXva83l5env3y2+OKT2/fP0wko3wQ6DrqiWAGferdb6J29/vmRXG8kzgcXqooiD343ZRRKtdyKlthmMCt0cfXkyVfif1A9/NH31VhD5unVs88XX37085+9+rOP4so1bb59Lyl3HEcU4+DG90IAQ7wHInHXXAOvMYGuDRKtsQjOsAGrmjvvsFvG9Js7zfduB8CgSqmBeVYX1mOoUUyazA2jKMTILCSKGlkDxxz9gGpv+tBm8v176JGvIpYu2UnD8ZI3DJiBM2IVBIWVvV8dfx4273Myo8ymeWIQEYejHQEFKKiIKjHL8a9+FRYLqlqw3hIICwfwauS1ZUuQGNjF4NrrqvPMeXpp8z5XSC1swihL07K8ODrj4G/du5fe3WlSFY1RkBlVAbN09uZBXGzqy2Uyyt0kv3h2Yl6iQScmuXq12BuP6eEDPxvp04BBQ07qRLTlGMgOBlKLZIAYUEQABUQ1Ihtrmtb96qPj25cdCiKgMQAq3LWhbtcymj94M4jHZpWi7UEVuByl1MvzLz47sVvj8W1Okj7069PTZ3/8wdUvXvBqk/ey/R1MnA736o2zRFLCHBInZQKI0vQ8hKwBgAiwoCIqiCKaxJAZ/ATkgDbV6udf/OpjOf/qaOuNO49/8H20ybPPPjj6+Z+3L2vFCav4JgZlQQTRAQj0WpkpiOD7ul2eZi70VdutWk1KiKqFBWBDaqjjrsOu/85ue2sGoOCjBu5L4zON3DQU2HRem0baBilqFCA0DqBlEek9tSuJjkIKkjvaHeFqDYEn9x7Sw3fO/r/PpfEYIiirOgEyKv3Jq+MvflLce1zu3vE+hWHXNqyalWXQlgpK5G6zxL4D7mFOVlAUmYmArDOISEpEDmNFy7Nm5Vv//a3+bvkFZKbdZL6zVBbzyXKxqqr1zmxr9uiWjEgxgLZM61kAACAASURBVGiv6CUCMEeXbs/Hj/frZbU8W83f3JcNcO1Hu+XyeAk94Lv3wv5eT9qpBKfOamIUBj0+ooKwRBawYhUksidAYxxDQAN5OT9bVRerK4OJMQmRisamb9tecHo3zabaNA4co5L2BgCNMPeXL9aHddzZe8XSvvzsw+d/8svl+8e8aQWSs0/2rVndf+86TUVeCwBNXtjdbem9Lmv0EXNiohv73GA4Rx0ogK/qVIOlFNTA6hKPP0zr05T5fPPysrw/21xczx7eOvv4Z2bzF+PdB1cLMtJZ7I0T8TcUoF+nISEgmVC39fG5TyIqohmV43IrXc5G3UHp72U+M1G9113/4zcmxjoQ6oLf9K0l1RDaZafXK21fKgNDVO5jFEZjUyvKGEN3sboMTfejXb6dQKi0q4J4Dh5xnO6/5fY7/+QJ+k5GToyIqsaoTdM9f5Xnu8nMccwYQQiNKirdyGkQhvme8QW2PVgGUssECkOkJhlLw+lgDXFTrbxuHie6W4A47hW2sxDDqFqtL6Jb9UWR7bx9QPOckVNkIWTAngFIYmRUk92b5adbm68uq/PV7M4WB786XcRVvf3GQfLoFjuS0HdA0TIYJUIAUGLFCICiGAVQnUiMUQkjEiISCBKSc/NhkjQg7SMGSA24pFETLs+MkKJjYByGRYDLLl5cUp4umsM/+ux5v356tv5AuSvQFAQ2dntHv9wOvTz8xmJUqgpEBS5yybf5+bm+PCVWO5tqgIG6qwAMyjJgtOxHl7eKbmuaL85f0osP8+VxDuAIVH2on15//k//+c73IDPr73+3ytOT/+X/yK+vJ+PsfGc0P1veErH/mmh80PRyUuSjO48Q0WWFzcr5DN8tPzswL767FX73HqYJguYERWIB2AMmgXnVeCYGUS33aTdBVwiQ+lPxG2EJYJKEBL22ddeG+l4BD6agBJXIOK0eIT3bnH/y5+b6UmIDI9GewBlUBVYRNhil5fNPnq3PzqPnHgSMkurwmFQHbpuCF2gqaFtVxpKtkDJ6Bg/GKfWRGa2GJi7OfDPf0q0SF4xnAQj11jS+3bftcvXnqz0od9+8m9yZeXlNcVM0CsJECIq+8y4v7fTNrfZqU50s0zKLGuvzzWx3uvXebRgbkV4Ew2B9UWCrooTq4EYuKFFUGRC8ACswqrdIqHSjm1fDEBA8oBFRjU4w8wAMUZFY+bV7H5WxWoa9rer3/9oZt0cvj7n4PnzK0w8+2g+hMF2HHmKfH3807Vb9g2/2s/0IANaNcLQVDq9htcjmu2G6zVftIEsCwkGoNJD0K7tbr3eunx9ePsvqTQoABAEBgDKxZjpeP5gtnIN3Hri//LsLQ8/+yf/0Hq8Od7vlmn6/lulrHB7AkOkHbPN0du8xqlEERSOmNyYpnTptZ0VOGYAyoAVmUAbDSnJVbRrXGgcSQ+g7qHoOXazXaEPw0DYuZ+g3vvKhsTuwXUJHeNzB+Qpy0Tdm/FaApo93lqIGcRc/vKbnHcQEJEEgWxjVvn11xqtg9g1vJdx5RIEyQWMALVoCa6ANeN4lvcVoVCuLqqBeKaBxrNGDkZidHHZXkut0DleRPr6mkxqc04sJv53xeI5Zs7ezM3+wy0TIoooCbtANISCR2MzHxjLb8e1J98be5c+Prj9+xYT5KN/75qNkb9pDrSJVU7Y9aewJyFhSERUisEgsqsPNbY0YowYIgFEUFREYCRBJkRQdizAjM0UwPMgQ4Ka5GwZCkXmcL//e3zz88W8vjg75d74Le3vw5Ieb//K/3vzy4y1AQ7Ej6SPr2fOsXthbj7vJLt9+NKVyquPSLFc0LpoRBY03nhCLSqrqFaOSUILr47z6YsQ34AYGEDAlT7a//cPV3/5b9a1bsN7gpMCDLfgP/92LvtcnTzKqrqbZaYPTAXpAKoBESKoiw3GgMmTW1L1fWTak7H3rkzKhQXcOAIAWQLxvN13khINq1azreoGYKhpwiRH1nhfnbICbylw1hh9OMRZwuMZPLmC9ISRoc35YSplMbr1H0+kSjwQFr06AWQAE7Gg3/da/9Xaa3GatcKaynUdWMpRuTfror05f9NW1SyEHR6uktIkdjdZnx3bQ0Sp5wlzEVH3SXjXPz7h6Y4RdT1+taYk42gYEOulw2Vqbz3e3b31j14wwtENcDxEMByciGYlxVS8vlxUlyWzXyN0pXHbN6QZI88c7qy1XN+tRrjGaZeOaDgTFpJIaEemjtkqZQjJcYl3XLquWgyvLvJi6zFhrrJJEleC72HeZxSxxIlbECCKqIBhVeB28g0AYuH341sXv/s6lwZBnePcARoXu/FCulkdX/216+GLfcUb1EmOHgNUqe/6B3b1tH72xpbmJ44JcovNcXE/iB9iHAnJUiENciBgl1UTAvDZ7K4JDl377d/gf/kf1730vEMByA8+P4nqj92/BP/i7l//0f03Pr2cI14gyRNQYVOMMK0dhHXK0Bt2oqKCertxRmyc5/uzL9tbUWWsMkSUicMz89Mj1yR6UW413fTmTXQDMAQxqJasjjbH3dN1QVUMzSnjf4fESn23QjPBgBwPDkYfLa/fo3r1vfjdJ9p9u+vXDRn6b8aNGRFndbHf+3tvv7mzffXV+enJWreqAQATWNHlctf5lDeuasLMYLFHITNdVMahlgsCt+AbZVVWoV5tm3VUHOU8j/fwarybpwd5k1/oA7dkqvng5tv3tHzxw22XHwmgGmhuiDCE8LqFlFX7yJxenF+hm0/KgzEuCrfs4YdNv0sKGV6cPM/Ot+1shJl0wIQbFiJaJImtgEAUhFCRCA6dn65/+4vKyHqd7eOu9gzv3bmdJ5kO7WFxVV6+S6uq7B/m9nW0VjRCVEvp1fzXYcHtBYupm802Whc1mINyo79US/MHv9ler5//on8Dp4b4NViMPHWFUTPLd+XiOEP04T3a3eVYEIEYDBgARBdgLWjSIoEBoKLVwcz6SgiU0P/xLm3/wDy9+85sdRQXAwkHqNAaVAPd34bvvhf/hp8m6H6CZRkmMc8YY76MOWPWBQnEDW3cbuv9xP7+AN56f+zs9TTLnUBFF1LG3tUq8VXZmr/aoqGAdoEFGBMAYpFqvM3Ox5s5S/6iQpDJH1+C3s7t7s13pvdYnPrx4mU7aTPOR2d3nR42cxvfm2gi+ZNEEAH/5p/93s15fb0ZX61kfjKhRUEAGBRBjec9oD9AhKpNhJhS0JF59K0bCqG3Lnp3owQRKA0drWMdkd373AW3Prhd9vkiz1XqyNUm3b+VRQBQImdUIgApE1DRXNN2np+aX5/NqE6zP4dJCgoAuxcSJCjcHO/T9H08Rky5aVVENqmKAUCkEAkBRuq58z0wGKygqmpw0STiBZ/V6/DkUzpJ2vlt1df3u3WI6nZCqqggBkypYvCHrWbwxVCphKNJoASSCtYAGQCEylCn8nb/eWPrqH/93fPyRAzACDBANmtmDnWJUYiQcT8y9rM2zXoEHCguCoqiKshu4iGAIk+w1FhCdlQffaP/wP2h/67tRevACBtUQJhZ8RAFlhbywNd6+asq8CEBoHaaF7brIrK+TAGVAEwsQALEtl27cm/06cRubZ4h2IPgIIRl1GpEZYNMtY90BEBBh8KSIk912ivXc4+0p5InkCC+voRU7m92+C7fnV5ddvshGy36SzWfJaEzG7EweL44+uwzXWKgC9lGFw+U5vHiRi0yixyE/EUIADeCsgolgolFA0ui1bUEChmgRRFC4SMJ+ArsAkEAT8eNL89KP9u/cfwvuz866pq/Q5rlrJqPZ7XmSO+51iIIYhMMCyKDWxVfX7b/6WK5CaaegJlOgxEoubY4xz8FXzmIBSXZyzafXsYnas6gyIYJSCMCML07sz1/idSRChJBXLfHEKBjxPqlf7iZVlviQ6oqTcT52xgH2gKCEgpGBEURAohIBEVpEcNrlSYMIzOCsWBxoXhCjjjL49/5Gl2fP/pv/avLyMyIwApqV2e79MTkIHjBL2yTrrUhkFFBCtYKIiiRkbnzlhJQlAAaBy1k8eKd97/v93QOVeIOjVEBHmqbgGVnRGE0yk47vg98WDtYmRZFnObV9O/iwhwmEGXZVxKCgrAHZpSam6QrNWgDF3GCqAJRJASDVlr32PfaIiSJ3KAGSPM5zOOBpArGV+tNrerLOJgd337bv7Z9C3KzATcq+nydQTPp17ZtTQU6rKb1/gusK7J0YBbsld+PuxNp+bdpOEYEQusrujk253b94Yben2eN3k9G4fflV/fRDmtZAYD13QWq5ODUf5zJBVKOtwatkXI7eetM+3Du1XK91ImIwEKKjJPWAaNkzMEHqIhCve66rum6aq6o9POwwIcQUELfK/s5ouV00GUXy2k9oEfM/+UXSbWCxdGEU2rTYmriEOoEYJbY9f3gsvzi2PappfBo7V6CmRg2UmTzevrrlFogMiY2zrG/Cz38Zt2Ywn6SucNF0XlGBQVRZPEjECEZtcuHSXgDWDeQZIMFNy6jogxYZ/NaP/Pt/6arvsuVR1kU73i3mOxZiy2IVkQ0RCg2hS6pDZ6CAaIiGVTQhjbJsRrO99vbbfrYfi4neLH9u+MyKiHkKXQWRIbWQupAWnPQzFEyypCxLY7jIM1SIwkhIwhKZbyx8w3oay7LI8nRg2skwyFYAVXUgikgQmhZODs1VxCQBjtpcK1/ix6k5T9FY7QkvwPL27Xvpe3dO53S5CIVIAsyGuTp59fJn/xwpVYUY1/mhSi9+TyVyWF/AdaRTJFZQ1mKkJkFibUEasUlqs5IkDasYrnpQhMxqRLvmw8gLIoJLwguiPHOzcXrfbpebg8kqxT6KRUHQqBEJDNi0EUyMUMaTqUwtXzVN82rZ+Gq5lpNLFMopccB4MG6+tX+ZxrXuPkxLxOOXReKx6z4/nF9s4M49ZliMUvPwnUm9gSBBtI8O1xYZ1HRS9pu9/baDbNlbRLtbxNuzMHU2RHIOs0yqNjz5cvMXn4aHj4rvvDUfjXxOkRAUVJl7iTX6jgOk6zRXFti0lGTIKAhqhtg0gMBwfgnZRN/5cXv+VTz9Mh/tE3DVrHoAp2SBIDGGpEzTJB+VUQkgAqm0fQcLyEEogQx3vs/37zbjQkGGsvsmMZlAESCCmgRF1HsYp2BNBPk04Ig0wZDU/baRDNgO3CBSF6Pvu8BCYG5S3NLUqZiqiQhqNDJE1h7VEwqACCXSj5cn17xc0c07bcFlII42FipTmYTG49GtNEvbnfn1yDQsQEAWwEeFTU9lLPa8IwMsaBK8+/3N6dnFSS2BfN0Ym5lspC2CSQAtBgVIeNFpXM1+9G65u62EXbVuqAZHsklUrA2yVhCcbbmtrTTFcoIHOyGndWzXFlWZRDUqBxAPwizLy9WLqrMUyjL2hV+Edt3VoanqC72+ckwFbaXRuYmD79zt393vzi70Sz+ezs3DPZNoUlR6vZZlY8q0n6fx+YmcaZJOEGwSe7k689WrpVmk2slo1oxKrlYmYjJP46PZ9TQLxmBQ6D32nRfs9vaKy5X5/Djkk/4N4zKjYCgSeYNinbW2lJ4KSDOIDEhJmhqE5uvinkjbHp4dQ91CPoO73w75LpZbRTma2IgExIiAYFAT4jcez7f296qN/+rLV6KgkVU6Mi0YgTTNijRLFV+LLSKjDIlHA+1VgQwoYogqANZAEZ+66kwkaSvoFhlCDuqC5moeEGwLG2FQ6IAqhagKPkrwLUFUiSqdqldt0rRPMzDESK7ng/VmHuf7qoTgQAlDCWulaZaOs7zQ0bbZ2Q5pv0qgkYgW7RBXGsEBKtddc7YxQ74lRiXnFwAsChTbQKEhKtlY0ABdD+TAWCBS3zenK2EHiKHrtFdkkk0C82ijRxLCJNm+l9zeqQpqt5KWu27TR4epAfAQW9YgjoVC3x8fL06dVURLYihq29+aNoWNx5dl7Q3tFOhSRL2zUz/ab8sSi940y7bsbWrVCo7G+PCgr7viqycCJtaKzcXmwbsTg2Uf6POnfHLYow+oQQ1eXyUbzU1GB8V6DKerBYuYoSpr2yDUT7bCvf3Jk+PsF1/4z88COmRjoqHoEA0UiU+wfeetziQYI5DJ0gRV62EVAwqEuFrB6fmw7gIksAW0Hg6Pe6NowIEgARBFAd+HFiQ1HVofRHJJCKxRjgoKlPhQhgjW3eAiA8PrPIEbKOXAJ40RFcAZnZqwba6DgZ4hMAwmd4eJ0pnittoMAUgr1DVqUFFVYV8BBFRBHboEmBSaj8SgAEHU41V2+6I9kBZJEFRFAsTWpuODN7KD2SXYbmzVcd12jJKABUUOPgYdm61ZfLpavv8UMAMVQlAW5Ki3cjEao/CyliYHSDT2igaAQBgMApjm87Pm8zMiRVX1jSpDglh6K+MSut4lZj7u58lSY+wZI+PQkCCgCvgOAlrtVRXbrQkUOQz+XCRw9TZ55dimJRcplIUibWXNvdGxb5oLj8uNDV7b1eq8X1nBrEx25vFujScX2eVaMLfsMBrXR1ou7GEzraZz6nsjTd1F0xGXmie8lS0hdO0ATVUDyMYCgIk9j0Y+SeHlwrxs05BkQAYR0AE5MJlHm+z4jTESAgAW1nh4Xf8AqiJeLGC5UUQgABZYLouzZaLYKhjEiIIIpFY5KtQ1VC2jVWene+TXVb1aIA7yBKay5zECKQCwaOfxdXUEQ1mGpImFGDEqJA6yG3/5YDgFUhRA0ADxJeOxghEAAk3g5jUCQEIZxhuDxpEMAqghQQtCYLEt81Mb01jdAGhNbBiZaLss+yLddAFizJw6CcxKRIgYte+iAiQ5yoqkVyMmNEU68b6JoshpUPRCAoLpDV5UjANElKhZqkRaVxg7W6Zk8zYEVibH2qqldC64sNY4UhXoORHkjNikAzEJgod2ybEkiV7HFr/1hpZTYEYgslZXq83TRdp2BoNPCnHGgO4XVSHXl5cKgHUofGyacLnBzhB2npNUre2nW/sLTr0mYkTRVF3y/CxbhSCpA47kytC1qpFAS9MVtgEl+DrvE9RZICWJYLOQTkQ0MXdv43QKpAhCBqxVtGowljuNocM+IpqZsct/bcwFnuFsoU0PRIMIEeP2Q9j7phcnzgFZJYMuUULaNPjsJDSnkjhKHCNqkdN4rsZJ1yPHDruGT0qsFSEy1DUwgyEQvSHFEkKSoPcAoqmFzA2hpfq6txvYZUiiBphQCXCg1tzEjOhwSQ0cFFGg6LXvJY9AdONXYgeaWLghIhACgjKEwExeTJBEgbIUkkgCKCgKIuuGKSgNNbSCIiIkSc59DBqRSdhw1NGeffTObUHbnBwKVMlkYlxm0rLIt7KsTF2eJWNjRqv15aujJ8ujL+Kyt3Z0t4dDIhfEDQdyVJtnoikKSUTsWm0uPRuUECUB2ZlxsQXMAMMsUC902m7amKeUOVUsE7+bXxroGBIEQAiuXhrX0MQiQWDp66iqxUhpUwgn0ndG3aYuzi/XgRFEiBDTVBC5bQloJ1uNqAdxN6OpX7PqEUBQ1OWYbG1nP3hHp5OBqk0ABhUJKLbl7jNAiUJJOrK0GX4AAxrStoara2AGRWCA3qTu7W+Pt74pnUayYkjxhlWfrCu8WsQjRLU49J77Y3z0SNJClzV66jf7m/7ZttRkgRVWa+g8jAtQBoSbYD9jgAFYwBAk9msCv8Bg8B0wsQSgakAB8ebdQhjIJzfxxF8TqgiSFBlUIqQEqMCoijiAgPUmbzdq5BiyEApiYaUkh5E1gmKFPVDcdJ5at50jddAxGTXWEDggI9CpsIpVNGke4ygGk6TqFNgmnqwax/kEx9vZdDrKklFRlnsxuCcyKTpYNRaAIHiVpvUTn2TMkpOkhhiBQERcUIc+8PlCmh6SFOOAO5UbNHrUkI7Wdm6MGiSMmuRt5jY36GJUSzSCJnMBjaCaoV9PIGbWIyYqiKjkiqpN6w0AA6qCA0CAJBmys8qkdtgLGhxEajdd/DA34MgkqlBkMMkwdyjm11IBA2IkTdWgKOVFObKkGofZkhLAptbFeliiQ1ToknFf7tZk+2RQ7wx6dIab1N4b+OZNMpiBkJuYZxqULCHs1WE/yksLIAqLFawqmJTDbOJmaGAsMIAXGPimHSctYowgQqI0bKNBcciAGOImGYRufOUDkFxv7iMFZyHNMUZggSGPwaAAiA7/04GxDyLtul6OncvytPHRBMDcIQEZ0AgpFpEPz7TPNNYKpJQxQCudtyoCijGCY2OxC0cfrGpBY2aIoCKgisBEV+iuTfoErTUpGmKMvcFdRztWYqu+9udVNQ7XU9ZEcRbXkblXz1T3eH3pQpTOX4lPADNsexzr63MDgQw4J2WG7M0N2U2/5uiCIhnO0mDsTT4ZDI8K1ZiATrQHRMHUKCojK6reJDmAooHUgEUAZeQh5g1utmjDqyNRhJl9kNB6673J80EaMXzPwGhJrDoAg3mWTb6OYh++rje4alRvngD02XaDo9bDICX6OuJSQRQHpstNxaQ6KBxkGBCLRDBpjbd6NimwKCw3cLGEu3uvgWSgqmAMoIIwpAmgNWfVQR23JSKzUbEoMlj0hmeGwICKqBnZOWjJEQDURDJeAYKaxAG2ETkSQ5cJGIVXRIEYv0bOEoD45aurz0PXjMux7za+pWp3247ntg/h9CRsNhj6Jl44wQxTFENsteelZikkKZBGIMbUcNufdr5b4vBnDGVj8BB7IERL6hwURJlQJEwcTkdWmhZnE1/QsnZrQd3RQ8DEl4UPsY3LVplI3hqztbDJqMkRGCEqvo72SwgSBwZAWQ0AEAMykgLoAEpCMc4SgejgtwAAICURUMNgQM1wRKlYVstqREVuXksgIRPQCgKigEb4ustCVRBjgZWCQIzMkREAQG6IlKAEROgN9aKgkKa2AJCbWHAEFrheaeuRYWDeQO/2Wi3ja0qdDtmOQyYAKpooTtQqoYJRMK/pQlaUIljqcFqFvMwrA1C3cH4FPd+EXtzc3AiqEBnJaJoqoEZJWKwKARthqwIkPOCu4abHIEACNqlXENIBtQtkEnCW9QJcII2GEdUIXq4p9IAR4OvPPWhatjTqqmShlh2cUv6CNasxdrb1ADsp/sZ+jFa/RHq2UA6SZmAIHUEQlchsenSGGChCYkAFbsBqCETaBFRGm6B1bq9zO1e8ipAZSUsrdQ1FIXszzjJ2ANRDv4EknY04H/m+R1aDiYEWtY0wTrEodIB7A4CqJlbKlC5ARZEElAeO5wCXH9LGsjTcEJsHuhcMQ+MoCkIGrQVDlTOdoZuPmqqoECmoY9SIX+dqRITXbEIAJQGrIurJKCoAMwyCKb55kkoGYmKbCBCgtNaqer4ppdEHWK6hjzcc8whUw1anqdjBrvg1q1UAb+4ocIN4QtQI2BsQJxgYJvCei1Uz2xpVZMBHPbvCuodpMWTD63Ad9gxdwCnpKJOt7BXaSxaMbL0WEaeqYHhFCo7QoIKKKhFoDepFUMkAGSUAtImYUUwNtpu0rl2RmTwPMBa5HoT5A+4KERMc7cj2LUldMAK0hrKqZ1gnABOFbQBjaJbCBiTtARkSA1kKww5UAwiCAAuZRChFNQ4sQIggDDFiUCADDDqeusdbW988Gt0G3yQrD37dWRrnfLzCbiPFCEYJFAhOAbSm6NFDFPRgGKERqQR+47GUI72xIAOAABHkqRoCQDFDGMYwfB7woEN0tZDqAH4dai9E9UQCgCBEwMacl1glw85v+HQP2dUgpKKiGG+c3a8nQwpmiI9nkB4ScClaoxAVog6ZUaAEYJUL0ysD0a0ksdq2X9f9PkDdgvCN0qWXosEdsQ4o3pRFN/f9kIoMOPhzzMBURDAkQ9WFiDRE9dqqyvs5OAMscLGG5QZmxa+7viFMpGUFhMypr9PVOqPIkTEqiO0BJfQRRQMqxQgSh4avGgJhDSCRQSWIgJCkkhD3FTbe5DmkeajXRZQRQDLcvYgGTC7ra2g2aDMDpBBhArpbQAZAAXwEz+QX2AAsOmAD011AAuHhEzUQD1mNoKgwiIJxIACdx6bDEEAZskym02RuMttNqbyznzx9FZ53bM1kzscGMWZZMDaomNg6jUZi0gdLEq0g2ERYyHrdmisRcHj9YhEAQerAoA4cVAcRTRCnBgVlqFMIBF6fOsOr9f9T9WZNl2XHddjKzL3POffe795vrrEL1ROaDZCYSEANEDAYIZESLFI2TcHmH9Cr/ewX+y/4yVOE7QfZsh2iSNmUSJMmQQZFEiQBEgIxd7MbXV1dVV3DN9/hDHtnph/2udVwRXRHVUVVfd89Z+8cVq5ca7B4NUxzctLM2XqRNoY+ANENRBkUHExK5CatVuocoKPYePGNLZWEkYKyO1fCFatnuJXr6mTZxT1XrGqI8XYU6a3HtnBKCW1HVhiwitb3+voahMgcYx4aA295xgSHDiQC4hFWZzh7SfdERJDuKrQbnjYGx+UKp1d09zrct9NqpllTJ0vueTJBF199ml+TnJCt5Hl3Q+GNmPmQASN2UsfQuebRVswVlk0VasiKQUGESDDQSmkAR7fxcgqYMVyJe7U3lRm77BgH27AtTVVcHersDAqkJ2590WzHMJSUACowa7CqcstYrhxTYkI/UM7M5MQ+X3iUw8XZjRvg3g/rYX8xbN6yAJDP5tMX40uvtLuycYVmGpSyVm0/WfYx1FCWqye5XWfUcWTheOE0uJMjCIJAM7JKzb3Led69Fq+Iko9i/KWQVC7WWOB1ml+0O2rJHITUB88ixih6Y65mpiwCcVfphiZZFajf2lkbiMf+yDGYZMs8cY/bloKsCMKok8GbCusOLc+FUXyFyslKCW2P7FAgZWzicdpZWFmbKv/E9o86lZUch2mJ1mN/SCAGBSpLFgTKy7C5lMM9Y0fX+ukZmTqNhjJgeFM3qyWc8t4c1U7jqyMlc/ORclHml2ZQAwHTBiGgy1itkQdkxTAgJ5gBOko4FbAEBGdPlzKsgQKRkOei5V/LYnHjlVupRQAAIABJREFU0/Pj47VnY82euM+xs7DqJFlVVdi0fPl3lp88w+UVJhXcMCgZqCIzd28mR7s7LwjJOudhWE8oGRNzjA5Sk0jD9cXZ7QOZpUl3fn690c/dzMH6U+c0mcnxdLknm1D6bACgTY4PNwsL1A+xpbhxdyqWcQGjyDcREwJ5GTKpMjyZPB52X5g8mVNfcJVCEy7YHgHq/GTYPx8a986dXPsUspb6CiBxp4wSA9xAPlidUDeeCcAYt6gUW0SuZU5QNyqVg4p5IgHkMsCYLpEu//SH4Szo8fFqJtA8dh39QG2HbABBFX2za1U0V3Ir5mgAnMd0zqpk7qoGDw1ThJt56tFV6BKlhC6TDu7T9XqR8lnDnno8OcW6x7wZ6+go/vS0+4Ov4/xTcuuaHkzPkU+8YkT34v1XtuyF4AwKiMo8kWaHdvfc3XPSzcpXSwwDUkLfwRUicIYWqxEdTySPoutO5jCQTep8vemD9ZGcnRSkZCeb6nKYVjE/u5iuYsgGbltKqns1RXAvRTi9txr19OBOe2PvrNvUj96O/Zp9NOoldwsT2gnOa+wvdnjhJ2fLaqcPevK2ayaeZkXvMKJggQCQEndNzRuNanBnb9d++ozqmbsATsXJlZmGFXEiZB/UDEZ60ddP0t5OtZZRaFcLt4icmfXcqkf9YhhyJZkbcWGNUHJMhSaJNEGzZ/MqkBs49xaSVSyrUcYbtK3BGGTZQkohaablCmUAS6jIBQim6erBv3nb33rvE5trB9PFO1/9FOqApADQDd4mqIMcWVmHiMtTYiPX4KiMIlgqlA6tyYmPqvb1W4PUCEQN+dBXZ1fOhiEjG+ccguDOS8nbobuoZmqO8yusOlo07g6pcNnia389/Vd/+MLX/rT9uU8+vXzSob8CTRCk1DIgQAR1BarIIyywEjGTCIhQ11JXVtXWdbCBdOnWYZnQKUDIDs0wcylNxei+BbibJpVeK/GMMYsbk01jmz1lY+IJBcCzCIHI1ACFCFjgPljM3jRxM683O1HPZ6k7/RAsdEqOBMup1y6HW9O9oacfPu4DdWuudwAxKwNAV0Kxm0vug6M3yV4ZCF3mH9/HpuWdHWJxN4OBiGHN/kRX0/TkDINSI33mB6vDo93LQ1naCCxR4Xtkx9Nh97xrxNbzVw5x7aDdtLqojJ1v74T9abrX+oWgG9CYsznbxsKVNYeRaGuB4DQa5rbenLaTAdED8sWFuIOIhSt2IZ14d/qj9f/znZt8cJfj3v/6W828m/2HX1iHADi6gbrkZmBgsNoxbfo1MLBbcNQkFUKTM6BuLg4cVGH3Rp9o/eysbVfWBUkMtJ7VYdCkmqwadIjrIUynqo6rNa7WfmsfVYQa/t8/ld/5+qsX6aVnP3789g92wh7xRxfY2wdFzw51lkh1pCoQRRiTMkFGhjIREVOMypTcjNkPI/ol3rlEa2AHF+9IG3MEbxMKRVfvB1u7MZK4RABwcyFkkCVroKBgCGxu7IrBwD6yCOGqRAZmMgMlQ8oGJnMq2E7KquHxVez2u4jhCNPdqqqlCohMTXBBZxRIpqyBjNwc2KR6NUx7q7MFs0wsnB2rdRCwBBs9MEDMIVaYT/rHxt1GagHRed/8aHXnI9PT43ARMTjBnDqVJ/3uw9WxdsqV5ZsLur6bz1iz0mqDEOigwlP1ELW9opRQV8TeO93r95vYHcsyIpObMSXnq1Q9GA7eP58NExJ3Ww7Zjck7p6UTwWRoNw9dNztyxLh68v4D+R9PXlhdvf+VL29u7UHVhwGuALDahFV7pd0Dd4Np57YGk1N5fm4OdWeCFHHZzrWHM9Z9Mb8EE1wdhoYQJ5c2O8g9GOcrPLvin658aP1rfxb++W/cuH+xH8JG0kr7mGLNcQfVAtkpMk+bEGIZ8Y2znTDWERhPFksQlsqt0Zh1p8dFRrtN7VQkcEoYUQpSOitQhWy5G/rsE86ZRZwN7qCU6nWqNrozbNxzRghoN+YDQgRFA1PB+Z0EamROlHrrl8lNYMlKCV1Ps9bvPVxsDjuxS2qrWR+vlpOQc+IAY17lKciDt0xWFttbjdmqrNwnMzM3ZGV0qumqzKe9wOCOKyIk0ybQkHgYOJBC3l8dXPTzm7OzeVgxtHe+TPVpt7/KE03LhNS989TvX3hSEgKRJwUJrtRj9t6RVZpiE0TP2h2z29eq5SJuKkqmWGv1rJueLZv1asjo/M37kMq5TGDZwWCg6+npmqdzPn5Fz054eXFvuf/f/y/85ptP/8k/uDxbZnUwAwkrTNbBgc3oaV9MotXhBGYIg8sYOUAY8wibYBQMHAcMUAYUgTHM2m5mfhYIw4CLDd0/sT/4g+o3/s3t+w+OuOp8nhCUN06dU5+ImQITRQlV2dodIwU+PFTb3zNVAyhOdsh6O2m9Japrp4zggKIhdyt3g5ygBiY4ec6593XaiYIq5MBq8AzqnHoL2Tm1nfcDSbBp9LrCpKHVBlldImmhrmfKtLms1s+sU7fDKdFkzEOx8r5bPQp0MPPDfP/Rit+3zZMmGJwF2WRIOx4cnhzq0N5pMCeHG6sGt2TRfBIQBEXaHoUdqWCAGSHi5mHuO2NndkCd0qmEy3wUbJfc1JAzq4tT9houO2gFmwxmlMbJCWYIgoOpzzibqjiCgc3InurkRGeBVZANUI8pBJv2VgVfTDBtUDMiuwQEQSQ0hMcrf3/tO5VNFjqcYOgZcvZo8Vu/Of3rvzo/3Dtp03qya1J5f7iHW7ch/KGbcwZ6QzI4oAx15K1siCrUQYwCXxX34SBwLvyYrtvZJJrC+yv/sz/ir/323jf/5vByuSvZRJSO7ua1eLuh4JKUWSQGeCxM/BEALj1p4dIUFjy7ucFHN23KJBvxatdvH6FdOi1BGe3MzTUygpAxcqQMrLK1uVvqpl3U02riJ5EGAiUNrVUGJnLN2bqOImx3zxcHMEcypIFyC1dDdB2GDvc+OBo6Sge7WBy5xDFMrlZoWyg2j3ZTTXbapu8tkUNAm5C71E42UeZNRcLMbsq9IVHhVJMN6qenmDM+OUeoUUhxTONrKFUeFVOVrDpoShgSgXynyYHHftgM2ZAN6rB9eNiO0Ur0LkY/Y0cHuGkGOUJAYIQiIC9g3hZs5S0qmBADKkFVpP0YxAgOMpxfzG/qz/38/oOzR29fPSWJ3CdRNQv3fnB8r43Bn9TzIexT9wsLLAL6DAJCBAS1oCkoM4CATEgJfYICfULKKPuUIijJSw1mSECo1/2Nd9/5IA5td05/u571dBthFriHq1e1h71q7xnTpu1meYCAoNv7Wdq5EgeLj8jWK7CgtKUPIneu6rC7r7kzEm8EHuHZPzrH3RbegcnBUEPOeHTK37vqH16um7p6abpoAoKRE5TUxT24Rxjb8tIsY/cmpALMD/d9uqHlyk3VJ5ct9wNdhGMcTMEVCtcP5QAEWA/yvKryWxHZcHgEIJCqn54M7fJqXoc7kutUu/I0bEhWA/pN17ZdOk/pYo1ZQwv1XUYUTCswQQ3FmLOgicLgCGX0EUN2VVRc4HPEgMDoU+EIwwm9ohLUcbSRLxhXQT41ox+gAXU1yjVJGKfX0NIUjk8tBlQRDrhhJJqX+kHQ9T9z88Ev/rz/yi9+5tvf+d5/89ZbD7tjOKEdeEg0sGXJWuVT4DziY4KzS6wUxqimkAqekR3OCBEgDD0BLg1EeAJwDzNncWNAYI6c0Sd0A3YmytdP3znG1TkAQuBZIu5JyJoqu9067L7wZbvJ733n25MfbuqNvMShIjMHE3FB6raYDzH51vi7AFcjpk4wZ5ixpQyLwAJw1IRpDxpKV+gpIXVoNhRoWLeXP3qQN7t4CUvtaiJaSM+TPqN9umyXa4/qvaNPmChigE0hDS4CBs/GD5c35OIZEflkAmIEQi1IhlYxifDGUwKAecDxEWY1iANu7OmkNRkkt5dn/KxL5s7XCRUMKZtZ47hWQW9haP3+FaYddmq8dIw6oldsevQDUgIBQiCgDpjUECEoWscq42rlOSHKOJIlQa9Y99ifYNZAIqoAIaiCGUzIGcsNVi1mU+zOESpkUPFn4QFkEIIpkqKOmFBhQJQXAXKoosOhXP76F+6/8dLHX7t2/eYbv7c5ufc//5/63rvH1DTs6pqJlbU0H/Bl9gdLtA6uUAvqirOjM69q1DURuM9kRLM5x1qIuSlHGDmDIESCCZE7adZALpc6n+nVGZWkxdmaaLFCnV/6qct/9uvVT90+3KWLX/3y6W/8Bf3fTz6bqoV4cggVyd4SscoEZyyvDFAH3CznnPt+2GxUDSFKHS0rhn6E2y2AgxemWpzE+sBlZbsL28u9tikt1xcTDJMgVM3n2N3PU243z3Qe8ZHb9B5DFQfJFxGImARcgt5rfRjWzyJfVNghXKvQMKaMRcQ6411FG8ATrAAy3Kjxs0eYMJQDJrV/pPGDTKKWtW3NI9AwnEY1YjhWAU+MWWR2TJM5YkPtQlIUCyq90qVp66qmGVlp0jDvx1gHZorsUyNq9epy6DZaCwIzIgVg7ibsAwBBx2DAbQRsLFkWt4iVAaCKeGNs8ICcB1SOQBBGPUfveNoiEE0rBKE6UMVgWM6f2Dt/dc+mkzeyrTS/9ctfTk39/r/4reEHP7yWfEIcyZMnBpyEqtmeTl/wihEqkprDTIxph4gFTMQiCxInlkgiRDxm5NK6MG1JnON/Op2tjvfs4fuAwyVzgybu3Gzf+Oz5V79y+Y+/9LP3Hx+eXYbXb+vPf/L8b77RP7U5pHMEKig+oXheOEnpIGDurg5TVazW3WqVl0vEKBJCHamqvapcM4gMRkRMLAwWqaaNXlwuz07sNmGvV5g2E9Q1oMTCVe2V2+0dkOFS6WFPrNirMY3IGZw9ApTocu19qzP4qzs4FBBDgFqpEu8neNxh7WAGA/MpmgAxIIdwfKSptcuh5YxsHh2VotDhklOXqWfaGJjk5vW4f4emeyyBlUMWdlGaWjVFyKZu6lAnER5iSEzPGU2ThVfHlSYtTihgAYMpj5dztFHcQv4E9xxznmaCEkmgKNPCG9S+q5UMDuYYwgJuli98s6Fc82QiVnEiqsKkWv+966eL5qWj/c97+uOhfVIzvvKFdOv44b/92tWffP3GBw/2fMOEAGRQmC6O6ls/47mycRNQSlLmIm2KwAgMd4KNxTUX0rCbZ1iZtwEE4cABsybfuDngTUf2CjdeXn3iS1df+vz55z+xubGXKzqfNC/o+X7bP76+096dP1320aM43D08J/YVM+ZC/nK20oQzI9R1s7db7+xQjBKCcCAWLuRagpOzhFpiHQIhD5r6l+9mHdrVibWdk2KtCBnBPSXtBnSKBASnlGWnjrevpcPj7AO8H2kdLw6+M9C5eyBUDlYo4ISUnRy7ARzx1JAD0gBytIZ2QJ/Czqd+Ol8t83qlaXBXcDbtLW2sH1i9irtxcSTX52gmFGqwWA8OYAbBzDJQpK0iA8Tbq6zF+GQcqJkDRBxrHkvzsvvnkba3nLddNTucCBwjU5DIgYjNDGYOGCPuHmCs21lYvHKtFqYtMTHHQCwkDv7o3r1XDpcHh788m/DF6TcYWlrvn33dX7p99can2z/75uw736wffC8NS6PoITbTZt9zkSQAFcthpi3eWORGir3jdpjEXPhVDBSJOwKDiZk4xubWte4GN5N2967+2j89/9W/r4dzc0NOyDrszG7GcKz58f5U7+zde/NiUK5ck3oycHH+szJZGrk77hgHlLGuY3NUCKZEYOcCd4xkEhQRNKmaqqmmbbvRfjr/6CvN8lZu29SudNOS6lYEzCBGlfC0kUktuwtvmnz+DLbBLqMKEGC6wFHtT1foMiggP592GcwREibAoSBO0AWkHo+BNiN5SP2Aqo6TnaaeFBa5+ZCGTbo6s9WV7B1WBy9wmBDI3cfTQjw+Sy4GnINpJhYiJhQ3Z3Iwk4CYiJy9yKY5TIRjaOCsOZdzOQ5NnAB2ZgIxAGESiSQgqKuNrNQCQCNg3H9zVxMhTJidSIQCh7qKZ5+4/r3rB69cO/4PUv/naXizyBurQQfMp/gHX0hv/OzFN7+Af/lb8sEjSpxoMZCE8aSMgszsxMROICcrlqKCapR25JKuwESyJUGU79zhxiK39o++5Lfm691df/UlHM6hacQoyIdpMw/VUZsxb3Bj8cHk8nyNGxijE7AFcozGOAgACFvqvINgZQpBI0Lu5FakAbdB38hNKFRVXc8iR5ofGyjllPtOU69qnhWqBStxYlKy3KerDbKxRu/Z1xlqcMcqoyXszzGNAI+kc3cYozfAIIQZY1ph6PE0IVSoJGyePWn2d+NshyiWAEIUQ5g2cZHnG5JAYOSkRS2aiIVJImIIEgSsOadhrV6UsYSe1xxlBEFcdkYKNFM206WKHCo2dR1DWiGEGsiKfouX9hpa2kQS3jK5AKLyPRaIx8isgw3VrA6xIq6J4os7P3j9YHj51j+dVP3Jsz+E9+NIlsjdC1CwP8FPv4rXPq71PgbPJ5P1oCCikXuFMaaO/HYvClacqMgzU9FIow+3G0rEcCNzd3KR/cXk9mTh3gj6lvrkRYuBCGrrSSVV/eKmk72p3lk8PQjvt3rDR35eaY4ZTjLeuYI/kPtI+S/MN4xHiLa4F8HNPZfGcXl+cnF1ipRcqjCZVbU4kbuqpcGGrJnUkEv5Yq7wlG15qu0m3rgu08P+4XtoN5hO0W1weo6KUQWUCqFrMQwgBjcAoanIGesVYD5lBMI0IEpojo9jMwEHFB6SuxPEmWUiswnMy3hnnP0aQGRkVGYdGOsdkmBZzZ2ILCcnSIgciQsHhQIK8SwEpmBOWqIxjWlRMb6kkon0Qwfrba58fjLLraURVHVyj2IKEuIgTHEa3vvpw+//zEd+6fr+T10u/0VKP2ICtqjnc0h7yEVGgWpxz6DhytPauXYvGXD8YyUqlC+lVhiqIGZ3g+cQKw4NSxita2kctsKdw9TDriUwj5kMW4ROrRWWyeTl883M/OrGYn2teufR6pNEZMVM1XgkD2IcbmCL6Di5O6Fs7/hzgGurigows8LzMLTL5ebhY+tb2d+LWaXrTc1ST1AjsJdjK+bsBCKxrtU1ZDGNzdTBObmFiqopMrBPnhPWQvUMQ/IrhzKmDR8c0WSKycRFbHZlusHOAEkQRjeEen5ApUIdq+hxkc3cSYsq47iBYKVkdbWknpJ6X6oRJ9ehT12L2VyqKqfB3TlGLpOvkjC4FCWBELysLowcIt8uEGzXmkZAt/yw5zufhRc13lEal0GdWKIAxCzMDax9of76Z+7ceuWFr/T9N9ar3yNP2LLHy8EcGWyAEELwMrAhPVFdEkcAXugBJeBipAeYmVl2dWJGCEO36TfL2e7ebGciBUBzUNHrIGLywDW4cQa4LA8+vxwwG9zz3vyldvOJZH+5O9WXFu/+qL9oaR/ZXNmhjFIs0vZ2jaxVghMTQdhHKcwyVwLgns2ym+WUUt8zV7Prd9206GC7uTuYq0KVQFn0kbF6c3JdLrXP1dG+NDvqaF5+jUDEsWQOy1nhVAdW9+vkDBZCVbbEKJHbfpH8HAAgK9xDoUiXmm+sFMsuy3bv1sdfAkBp6wpfxlxLVnISDnU9YwrBwdLUBDhzyplpLJ+ImSWCRnEzH2s1w1a4BxivnBU7qZJ7PDM5iQACoLwnPF/CA2DZzAjMHACd87c//4J87tX/hP3ds8v/3fV8iwPh+av17c8DgwOUIIKJPdvgiprbcEOxgS/UAINpdjNzS5rJrJImSqTJjKsYqrqcPCYHj7tDZA5ypkBcMwGjxiqocA+A7J3p8nD+ai2/trx45sNbd/cf712dDnybPXMmzyCX0lNvN7nKvSOQFEnw8nXNrWwcuuY0dHnorE+m2dUAp2DiwhkKKz6XkMK7JUIpL8aeQE3j7n61u88xUhAmoslsW64R4IXxZe7I2YbBkA1uloBUHJY9LBHWIIJVADBrgtR1+UpObF5My+FWBOKZAHN2IhSzl7FG5VHxvLwoK88x+LjUxiC4aVbb8vEAYiJ7/lxK8FbP7kbwsdZ3KaJhJf2Yu+dWUx9irOqpmUMgVWWmIAaHskwqsRKphUNlP/rcnYf/8NP/eDGbnJ78D3l4uyTBcmyZPsxHtl3S29ZrmPh5pc+s+QyVGZaPju2AEwUjdxrtobPBUnICS6XmXe4iSmAuAqpgkJMaEdHMS734fOWMAIfapftJpNcWs09o/uXNen1j7+zowTun+XUEogADu5KVWFW+B6AoHjhQ5N1K+jMvlZ26qhiBogmpQVm9/ANOTAEMIYCgpmXrUCSEGIkF2G4+Cpecbp6BYs4BBjvDkFWTb9p09nR49li5t72I2cSbQAKIF7gLELhAiyQfhziZoTRNKSNnL0QbKVwQODEQiITI6Hli+okymooqZ4nUY8grwuVltDJmFJRn/nwbnLZ5cCQ8MougmEwWUeLyNSLnEAAwi0MNlnPSnGupQs0gQAILmFjzs9evv/+ffe7n7xzdOTn/jbb71nME4yfTkD/f5yjm3UU6khB8oOXfDYvPZCVyVJMJUSCT0rISSeAQYpHZFi9YgKrB3dSzigiLkFhhP457ioU15mNrVYolAsw7wnIYzp6su6vhUyu9rCZ/cnfx9o9PHme/C88OQ+mly0F6Xg2UVO4+fgR38mKxQA52EhLKRMxc9shYWEiAsX83tzQMbkbEzFxVtYToNG5WF0UIgzvXAJyMAUu6WZ10m7O83gxPn+p779rqCtd3MNsD94U+Cq4AQXYkgxaqtIM5jDzjEqvUiJhCIBbQFp7hwBRAwIjlwLa8H3IQ0XaYRaNghZUsacWGBETEBgKNq8zb7AeHVwBKs/kTT4yfE0cIMTYTH+XO3E01DUGqqmqYmQI5mVkr6Gfx4adeevHmweub4d1N/w1DAm9F8baBFQ4GufvIYPCR9mIOCQjrt9qn38/hqK5mGkN5tSAGqbmaaVkhYgZtATpmGSt9IqdRK8HgREJO5oWaDrUipDyeabfhyfK90/4FCq/F+sXe7pzb3qT5ZqRHXb9nEJEmIzszjH2scMuWg40xtjSgY0HMTmSMjOTuwuAmUNmfI2YJxMQsJZvGOKHtzuIIOjpKO8BjvUNunvp+yK1aTv1mtXyahzW5YRHllTuknXHn04BRe7MoKDqMoQZmBIZmDBqcyLOpKohC0xCLs2zfNvNzP02CgW0MVb5tdbaoD5UKqJS9zmBiJRKiQAQnLVsUVhxjSjVCcCv3yYsMPYHZCTCwOQFU3mCpCco5D7FuiEquhKGvY//pF6598taNo+lndybdtx9fLGrbC3dCuAftnseqcoxt+2sFmGC25b8DzKjsVIbTavF64GgJSpnHE50BlJ13ERYRAkWQcyBmcwVs+0kLXlFiljhYx6KKbLuKKYSzdX+5sr2jzx4sPh652ZnduZy82jRv7t/0Hz45/8t37mdcE66zZ2cnlzHxjet0P/mjUEQBJvEgMkHhI8PcYRlmRSvVGCQhbL+rsc7U0UfGS5hRzXnohtOT4fxsyF2mrKVqrAk7FZoa9Ry1QDLSFbwfsZOye8WFph4wJGxaVIL5JJAjpX4YNvVkwjEWBG4rhjmegfEYjTDmmFUKQDgmu21scJTfxBaQGv/WaEtaJDIKMDNK1fmogQeA3LbFcDnZzDAiGol2oz7HmBSIVpdPjmv96s/90mduvayGNg/r7uJ8c/9Rlw5oPadvwbrndWBpfB2uPjIrNx3a3sv7sYxmZ3FwVJ/3pykecVWVpRnA3cDiMYYYQ5TAwiAIhRKMzazgM0TsDiMtuKZETYHL17ftUwDhssOF/uL16//F3u7PquekPTMfzm9fW9z5GfLP3D3bn/7Vn7/98GJgCkwGAsOpvJBt2Td+Fhqh5fIzYg9uRjArTZePkLKZDdq1bTa3UDUx1pqGoV07uTN5dlfPQyJ313718MFw8hSLiKbC0GO1Qeqw1/itA1yfGhxmYAUL4OgyVj0iIWdcbiAFKs6Y7mASgrtTFaowpRCzM5mrKhFTpFLD+giNotDJiYQKu5/ZYKbZhpyHgQixrscukEvzLe4jJ7J8RiL1bf9c6ginkaQ5ik9T4XWNdTURuIxKjL08wRHZcnWd70y/+Pqtj+wfZfVBExHtTo/nk6OT1fHTi5jZDvlbsGEr9DCer4K+KnC2xKYv0iLoIfNXv7h37WOX3304+KKmRemJmDkGCRVCLPgxPc+tWwSeHE5WkHA4GcYZl5aQng3rFpZRVbjc4Cq9cf3Wf72//0a23l1RTi4yAVn9YLb4Tz/35aH7/X/9rTfl4EUiKVtn7s9VjMZxSqkP3c3zoJr61bpfL101NA2HylVdM8wMMHPNaeg37lbtzJvJXIc0tEsJIiHaKDlHsY5xMpu99GK8ds0J2S0vL2z9EO0KaDFfYY/BgCY4IIz1BlcdYsR0Rllh7AbMJ1jUaAjQ4MxEFVNFI47nwh8WUKX0KccEDC+seoLDzJHdLA25b4euDSFyXbkqAcKRiL0MQ5wYMFOzzHAJASEaMZdLzExlO6/E9C2aAaJtR4AR6Cqv0kfAy80WFd86nHtA77nEFvNEhOOdu0xffXphwcOu/K3bsigFbe+6M6NLeHyCIYEJrUKPXqtf+EKcHB3dSI/PuexsOIHIOZJU5dMYfBQGMaiZ/kQ0pC3WWx4hWU5uZoQMPD3Dk3NMIi76jx3c+K/299/I3m0LPA4svfYr7abVDoP2ZHZzMasYjpqIyxdkJxdnBpMwSQn5mvPV+fmwWpJ7f3XZr84oSGW7oZrCy3kt99NYECc1QExI/QZGsa5BBcwACwAyzanPIrGZ7bp6Jk/V1Oomt5dmrdeMloAMAVLCVYdlj4uObhx4Cp4ZEtBmnBGuOniLeRNInMnIyxsuDQmDWSQQMTDOQse05QrPOavlXo0cgGZ3DSFQkJQGU4VDVME0PrvisWPZyFniZDILdeMjp8E/3GIc35iPfjWmM5roAAAgAElEQVTP4ScvJwbu6mpWCJ0ieXV1tn7/3p3p1dXl7nzxcy9+4mi2DysGKMPhzg2Rrz473xnyv96VP490hVHpxkEgxpNTevTEHcgZeef6/Kf+Udg5NtPpzkQu3Gn7VshVE7JIKG0IfTgH+LArKFVoqRDK5yEqAAcBjIu1/+X3sHr91ouv/JcHR/9QPcMVYCZm4fdOH3773e9f9JefuPPxu0e3+2HtjmvXbi3DvgQBuaEYURcwENuXknPXDf2UHdYn3uVmsRCRUk+4u9mw3Y8kggSPGGHoEZeEU/knUVZFrExISz5wYcikot2PpGGzefS2Xp7DAjTAQJcDhxloV9sn/niD8wyEEdFJCRdXFBkv3wgyqcYB/liPlZEBM3FBlB1q7mZOrqZZ1dTMCsMYDpQNhsoNlhJREZKygmfxmDC8mjTSNAiRYyQSUrjbSLYdY9KInZRy0saxsxdZInVzdzYjgCWkbrN8dOKt/l//9nvrk/s3Xzw8+GcHw3G3Xm1ev/WKmWVNe9MbQv/Ro7NF1zcH9EdTuWRyOITQDnj7vp9doXf0u7cWH/2V+ug1z2l5cfL00Sn8Tnl3hRXlal07VHWUqsKHXl3uHwqhlaS+PbpwApsRWWICE1LGW08Xr37qP79+/asAXA3EkSWG+Pbpg//2t/6Pb/37Nx10dPDtj3z05cuLZ1cXq3D7NToidYVm9WwFu3Ii13LLAgs5dmbzFOqhXaYcJNZVbLzkEne3QXNOOeWcyzzQ1c2fE1LELLsqttzLbZPjY92ibjpYu0pX5zg5ZcqYTinvBwvSmEym2Sz1ZJcnzgIJ7opKCJFqxOs3MLseuGrwE2V5sfEA3Ezdsmk2VXMzGp2PCgUhSC0kGAfTIBYicnESFnBBpEYoFQ425kASP9TZYCqeouxlV5+JClJmZlldfTyxBiEAbAy4CMh9WC9XHzzrV2RXYX3vHWzOLu9f/MHrf2XVZr85mP79xbyZzkKTfNiZ7N89/pXTq9sPzw+r/vd3w8MGJoLHJ3jnEc5llm//9O4LX54cfuzs/Pzq7GzT+rqdyGzCvG3C4W556FbCU2+iw5i4XG31Mi4CsTEZgd0YxGA3eN8u+3Y9OHqBNYvbL/3aR17+dZZ6SD0zO2htw+nFk//pN//l7/3mn+ULQuYP+Oy73z3HwTXQ5IZdHYTHzqKpK5MrMDF7UCYXY+8DU3mIgmZnWhMRBSL2saABWQO4upqrK1yRczbTdr30nJll6FTVSF1G6xYfgW0z9Zy7LrdrTxvSXB/fop05qpnEyrvW2pX2gzTTcP2OH14r8+/syQgCpkOO07mzBCo9/jaUo8xyAKNslJ2URYTq0kqxCBHAEBLmyNsdCvA4DwQXfADjvB0ONgK5jVTuUpCMCCmAcbMyMBXJPHc3LfiDjWwQdmYSg6f11eV7968ePE40oUvDj35My1MPZs/0t//572qVPvbJj71/+vCNj3/6V7/4i2bIOtQh3jx4Yzq5e778ykn7Rzx8N3q6kJ3hepSbt6f7H6/r/dT3F6fPHj08oeaFuHvLOMDA4AIXOSxWNREPfdunjhzNdBpC1OTdphdiJ9fcN00joTLAjVK3TstHuzfv6rBKDe/d+uTshS8OIhk5RAbJZbf6V3/0u9/41r//1p+9lR9mZEEymOPqCd2Z0J0XTz/oODw6uHuNyPquUwdx5LoiEVMvr8zchAiRgErKkYAaAPaxTURhMyQz7zebbrOupjuW07BauUO1kCWIIpHATTVly1mqKk4m0lQeQCmGesoxOgVAdH3RnTyG5nr/KIaKiL2elXAnW77RyOsxD1vx1A/hES/piyNECE4SmGT0qSzzFmYWDlIFDih6imW/cswEDrKyGY4Rc6bt1mdB87e19NgWgiFkxefBHODtAoEyG4FU02a9OT27fPh0df8kf3DOsfU24fSEhq7syawuLzDB9y9/8Ld/NTz94sXdj1x78ejmcXOs2QA7nt/Znd1q0y+k4VzIDm9WD6dvXTx9aCAnujw/f3T/qU5vyOQaI2JsINxhgIKIqqAgy953g2py4qYyMxd2InZTOFJK2ctQEanr6kgvvfa5KnyWkOt6ulR7++T+zZ2904unb77/zuVm/Tu/8wdv/fFbSFPqgXYoDoegDssfYb3RV18+ebQhPNq9deywfrNhriIHFx9yW1HlhiENLFRVlUhtcB9BRnFXy3loU7tedusrTQMcOfUwCyGQhIL/S4AYwTyntW3a3CUjl1hVe3uT/UN17YeOHFJPiypHWp/2509IwuToemwWgLiPVAtsWVoFoSrnK4wA1fZkjUOZQjkqBRMXb1PeVqdjEetmvQ7m6oUaA5BnteSjwrmFGOp6qtnJrYqRq6pAK1IAbWc3cYcxE8k4eiytmAHuWYeu7ftNuz45WX9w1p53+byjZyt68MzXS9ptyBwbJzYnJmJk6u+1CP7D6t5/N/3ffuFzn/2lz3/pcLZn6qvufKeZN9V8Jx4SyMgWzRPz+0a0XF09OznF9O7ixsckRE9uSupqbl7uAhxbAWMJ0d1L+SxEUrGRs3NVx9LcOjFTyNVMV+dD3y92j02zw3vTH5y9uzOZfO13//BPfv/PI09Wj1e4rKgzSorsZHAQBD6s/eEjunmUBn5yeZ4TH790c+/gWExE2AjTpgJzNosxOpxFhMWJsvnQd/1mlYdek9rgljLMRAJAUk+a2byeTtq2DZOpgEIUArery/bxSb44k9l8cvtWvXsYZ7sIlafUzPdlVyhMTQ25CxU3u3OuJxIm5KKu5tnHuVUhDIHIRu4CODgVpi+sjM9GvZbx/MkWg9Lxf2OPWM5lKQq9qGUys7l7QlHyTQM6SnkY+kGHYTKZNPNdVSXz2WweuQbIWYmgm/XyYukeCqJuqtqlod10m013sRzWQ/bKlwO99wQfPMX5FQ2ZsuGyAzuUQEbspRvi7B559c7JXz5+/N7fPvnBe+/8/Kc/de3w2l9852/+0d/7ysdvv5ytjyzLfnWuF8bJwfe//f33/vrvrn3pnywObromczcjHmcBbsjmVnTmRgCtPBkbRe+ctbQ8XKpMdzUMA58+TY8fXM32FiwBasuz87OH90/fe/z13/762XdPS+7jzBiUFCU4svGI3qrhxz/GkPXu3ZMHaeg+uP7i8f61g1gFzQ4idwRjRHYy245ixLhfr9N6HWOc7CyYq8AR7Opqqg6QNApQys18EaQSYSfIbIerOh1c1YuFzObgEDiQm3tGlGwunmIzlThl3oe7maLoY7gbzC2Rm2/7zZHKaqZmwbcUOIc7l1EKP3+Equ4lcwNUxPpA6gK4FMS00IZESwUmJIXb2UxmgLBw3UwYzMISI7mzU5CiaO0QA+Psxz/84Lt/h707LA3M3dSSWsqmQJtxcc5H1wgR7z/hZ6dsXNYKHAYzIMMJFkBMo72duRFa+uBvPviddz/46xe/8+LHX/nR228vT8N//MtffPzs4YvXX3rw7L1vvPXXp6dX528/PPnu/avvPtx9/cxed1ImchEWK9MDB4WSFosSDNOI8ZqxWy5bpv9/OqnBvBY5vPHS6t1vv/3N7+wc7C7vfXD14Fn3+FRTvLqn1AbqQYmg2zH1OB4zMhA7Tq78fAlmbMxNLvVgs3pwfvT02u3j+cGRRBm7dC8CpQSABDHEvb2DxXzOLO6sqkldLbmDAruhHwZLWSRCgrtnciKSWE2Pb9rxDQEXyycudmbRKbftZjkMfV819c6CwLnv3VXqOjQNiTALU+VA2VfGFs1wd3ELcC9hmODyky3n+INK/zYCOOPUbpzhCRxkTJAgYDgkCEnhL7NI4OID+pyTOSZkc3gmgIly6lfv3ks/uEevzGnvpj859+UlNFHKNCjajs7Pce0Er71G0wnncb7kz6dHAJAAL0pOlIkUbuTG1qst7YNHjz742yeI4WurP7736Punz04OJseTib75/R90jzfLH32AROSwdU+hEgCucDhZMfoaob1xBgcipxGQKWhb0ToBoKVkJFAgosjNtYOzv7h4+O/+ot7ZSU8v81Xrg3mecXNMmimNA6bnn2Jb4mZYhgklQSB/fIo+0ed/boiLJ995dPXu04OXb+zd3q8XC0jlRlZmncTQEahxI+16VVUzLbgDRjirX28s5WYxy2bD0LHEEGrywS0TMSiWl6yAg6Sa7DTNdGe/75ZttzLtBBLYOTahniAGMAnBiW2sX3hsAJ3K8DAwbTVnnWVkZpSnOBJkts/yOb5szNmJmYRAQaiumxgDFXtoWBARltIkkrNvJxBj1CMHrCCQxCG1XX9yicsW997DKxGPHtF792FKfVGQMMqOy5XvTHx/4qOUDkZq1TgCd6AHAoFhUsYelJUEHgk928aozier909++C5zeLv7saQlzs+11xJ2QVi9/+Di4Tsh1CLEHMZ9KgeVaFVoia6Dq5l5Vkspp2S5Mx3MjTwXeKpAkwUIWL/3ML99nnFGW0IWo+beYExeOgPfVhxemilHTyAgAE5qpfDy9RU9eoSLi+4jtz6gzenT5c5xs7h+tLN/GOpG1VTdzA3qbpZGBrUTsQi2yKCTT6Y7ZlYK7SCNOTSToNh7FrjQnaBgWGZLwizMYbKYNRMiZ4iIgKTASA4iN2cYEcw09Q4TCcxSh4qZgvPYIo4BeYRkge3pcypbCIU3NhZqYCWXwNWkqau6YaYhdWloQRRlMnpEO9lIT9o+Q4ydhI1YJfWXy+Fshcz++BRg9BldVwTnSAnGcKI22Tv3/c4NNNE3mwJplANaKl4fqXuFb2NwhgqZwzJYGYaBsXaHORlZ0s0F0I8TXMAdq7ff3Pw7k6qWGDlUUkcORQGnyEO6ucPcTK3s5SYzVdNU4EoqMwlTQMetgaz+6DGNHJfy+c2xdj0FdoBmezEwnjDHlhDI42dxBTMaprfu4fEJCXk3oMv9jetD75fPHs7CvaMXbx689lOAmG3nFfb8m/XR2snVTE1NzV1Lu+vbVEoMh+VCviiunVTgw5H0K0GiBA6RmSVIKItEIxe74JGMrl0v28sYqtlkQiHWzTSIhBCqrfjq87hSTItErMCeBIJlVc0OGMxHQ2+SUIFD1uwp9/0mW5IQhqEnVFFCWcPwUtyN09tCsx2zIoH6q2VetQAwJNx7SMKwknAchYADAoRO1pic+pRoYyM5EM+DVsGOsz9fJoU6evj/V9W3LEmS5dYd4F53j8jIR2X1Y6bZM9NDmUjR9OCQXHAnrfQZ+k/tuJGJK5lMY0aa0WQcI6053a1+TFVWZsbD3S9wtACuRzFXVRkZL3dc4AA4ODixnYkluuLhLSEqvgJLj/dNAJlGjN7+8ENLcmy8tspQpWx+OwNjPpjKji5Zm1MEFYXWu9GC3SCb3WTxojkW4FlxDzwIao/pTM00DECntwHgiu9/lIsAgiL8/g94/yzzij/7t+vXH57+6R/Ob4fhv+3f/ul/4pxOUTSAtPRiOmOI2mkGh7FQQhoKCoET5jQzjz4IgBJUKYhIkaJFS+dOJZMiE7ZYtCqAcD+Nh/2hlDrUEZ2wUB9v73okZgy/stMKShQtIavZvC6tRROmrmujO4SX+XSZATGh01tDa+fGZrf729u7+zJOqvVyOZr5frcn6W616NrWMkzDMJr75ek9Ly1Fwy6GdRVUyNiDXTgUgUF+eOEk0NyG25FJ3Oiwv0YAWIhX4gjMSLoQurJGYI4N0whU5HYvv/6Enz8ExyxnXpYVi2E/4manWrpY80ZsBSQwWGY9ksO54ZZcRCkVP3+QXx7x3Tu2hTkSGM2Ls2MREHibHAkSaEDhdXIXgMFNLgSqQGCmDsJ5esLf/1a+/ZFtnr95/va//83Nz3+5u/3Ml1VEQgdIEM2y8OeVKipW1eEspJDzcnl5fUe0cZq0wIXDuB+GSbWUEqlbEHO6Vw2EmVddKTE+lAeu1jrUG4BO24hVddpV6bAlT2MOTgbJAE42eimw1k7H16JDKcVAupq3YD4UFdFapdQylVHHcWomYipStO60mEhw+RXKUosoKGKtre8+YHagCByF8IJmYCPrJoudpnM2zBdSFMN2pBMJJi45Ec/ECTgThqzaSidvFqjqUDBWDAVDwVTl4Ubf3uJhn0KI0aAWchxYQv7KgxEVADXfdpshQmcNf0TCS5Tv5N2N/OYX8sUdf3rmuxc+X+gx2KaEAx8EE3grIb8en/Ba0DDCBENXIM+3pbr88COOCxwyVp7l5W9/+8N/+J9f/Of/KiggC+HNINBSCIiJo8WsJ0iNeCFc7XQ6vh+nUfc71TqoVt2pjALAY3Ahw3OeNHSWL0TiZcKrpUtGF/HUjCNk9Ux8ojQvIErid7FU/0IZhzKMu3Ea67Ssy/lyWtZ1qPsiNb517DSSIlV1nPZ1GGOBrDUrUqX00UsRF9bQ7HJXkenxjdxNnE9UlYH0Aqe0FdGos+tlBWPjhBHD1ibIzwwDFuLZ8Yy+lgIQQHUccJjkbo/DhH2V3YDdgKGiVqhgKFSFO2hShLIlzsjImTy63ozqdk58bEjsvgqCEvMC+YeHHf54wpePcpzlhyf+8w/+dEQa6Ox4r5iIAWh9TRDSs2IFyke/ibc1ucy4KDBIicVFau9ef/yb/3HzJ18dvvgVmw/EfDlSMIx70VKkmK3N5qEO4zBu5cmb3X73R78uWvrrRAWn9xk3/g/iRMUF2VKNjhbyVOnHFwIS7XlU69F4e0wo4kqVrnZOIaCsMuzuH915ms/zPIsURTWz83xa1xmgohLO5WxmQx2KVBVqnxOXJCTHnJsRLlp+/ld/oaX++Lf/+/K770kIBqiCM3xhrT3Zj0+ngoGYkWTYcFUCrsQz8UwckbOugnGU+71+fquPN7i78d2OtUD7ugvk6Ge/UBJL2yCFHV9I90s9o03mUF7ja1pKwOSaPXQmMfNJAFAHeRzl8Qaf3srff+PfvWe6gSPxQXBPUIKgH7S+lBwvQdjK36IRTVCBQSCwMAUVYP3d9+t3/6989VUzrmuLNN5tqVJVAdjpcmxShru3okpYs9WdQx1Fk6R6JQd38JrZnACsaUnduHqoSC5P/Ee7FYYgflvnmi/SfxQIacaOcKDXYaMYvNHd/ma62cOpLk7XE5dVSimqQ0I9LVpiPi1syt25mrvRuHrwYd3bupYij7/5s+Fnjz/9r//z8tvf8YcTC0UGmRuCk5xtlf4lrZEmqrkrnAvxnvhArAC07vFwwCcH+fRO3uxxGFwFQaHvEFk2K0EUpsKFD0GSkV5riRpApJ0fu8cNbnTo4FmXkJSSzSLX1rzI2o6JEp/f10FNpX3zU3dyH4AiuBOEdD6ZYi4big/vaN2H1WSOUJI9BdjTy+vf/eNnf/nnQ52IqtOkIh7NMUUt00N9S2eoTBC+2mJmohxkEslBTWYt6QqBNL9nysHFbiZm8hgJVmTmm1ciozbp9np8rpreRKK6oIRro1BycHM7pUpIIJdgdocrUsHN4XBb7gk5z5dadKiFcDcuUadzd1/NmzXYGiIiDsC8nV7fY73cvXk8PD5+9l/+evfLz0//9+v565/WH478cMYSnRSVXvwXKpYq5l4U7rKeiA/ECUORww3e3sjn9/L2njcDqrgwiqXo7pJ91iAPUJTKwzlFsh5XEFvE6LgCDt+sG1uUJAgUAqlFk2QhCnMOINum2/V359u9/sWXKs7fvydIzMCr4B4o0TEDTDBKej7vtrWGw+5+tDuKtO32+nf/+PL7fxl//kduPgxjLQOp3FKGUrRIFMIBHYcbjEGqznVGm7PaskknVEuRkilijAPlsFDAnAQEmhGA6AV2wvfTVLPyyZx4aB1XKUOkgPMyn07PVevh9o2oEgxeRhRwBBAtUqe2tvPrB2G7e/PG3W31y8qlNZBdqaIACimlVAFE6+72LbmUaTeU3f7h9u6vPrd//+eXP7x//vrb4z//fv7up/Y883jhsqIpotoyKRYXLuCC3YmHUd4+4vGANzscBgwDUUAjGyAsceok1Cwk5CQj/csLQgIwZ1EpV5fTBykCPijSGpmU0H+F3jubOnbowYIzq1GAQEvRXgm5B4URD4fym69cqv/LO6IBC7AKphzGDJSWNbn4HA0QkREoWRdKGODEAjaBLN8/ffjmu7s3D9asrWOtIRMKQKkiYtG/KVoGFdWCf5Vv5NCTCEVgjstyXte5LcthOry5/1RVu5+WLVlPtyY5FSMuPRpgGMb9OFaExouk9cf04rLOIJU8nl5Op9N6mYsULdNuf0CpPVtQgs3meT6eXr+bT8fl+UnVKTToUPal7qcSLx1UX+2NEXQ/AIIq0FJRVKQMd4/Tmy/u/vjfLX99nJ9flqfj8vTeX195Wdbn1/O7n9bjs89nsmEw7t/KYcQ0sA6IqjcAimiQqqO8lMk3NhPZcjsgpqP85SxueLyVCglzlBDf6HiKOXidt+o6ntH9UT7mwhZn2vPhBPISC1SjgGTg/R1+85WUEV//CG/EMzEl+MtMkN1qjWiCKem/V1tw4kI8xyGxy+Iv53HauzgdtrrDkLPCm4NrXkoZhs33dD8VAwXhufl6fnl5eT/WsYTUFFxEUxYmvXy2ADe3Kd3DxH2OYbDqojF+FbT9DvfVvZ0ux+en9yrj7d2nQ51YDoZ9wSiI+QYlGSLl5KtIPbz5QhVa7nbT3TDsRKeuMJKHLw1epFf3I63Nk+4hwkkX5Xh3v7v/Er+ILmMTNl/a5fX59PJuvby09WhtbvO5XY42n701X51uRGMAQY8iniMmxgQFmhro2xUhZV3JJnXAxWIdHUsNoWlxAm5mNMPSYCi7EeOQTPiO6AHk8DMUULL0frIBEJYwKbIiaPtqcAccD7f4y1/r/YH/9D1fzo4X0RtIJSq6VxLGqPgAHTZEBZAFkEZ7Bs8J89eFx9e7w13Zj97oNOPCK0CUnlejQGOmr9tCGl54Nqcp9O7mzX53M02HAMxkBxBMeRt2Vy2EehC5UwfHhQaQWtkHQAUFsapQbJomQMbx4XDz80F2pYyixXt637NRQDHoWIfDzeEtQv4IDrLIAGrO6gZ6jQJKht0OkdWYBhqV6Vgp6UpKI7NJqYKBojru9p88TJ/8EjThCje21tpsbfbWbG3mq3EmG8y5rtaak1pLaHSpxuAfXGpCcxKrh/48fYEIUBKge4OTbMs82+XUXp+W5ydXei202P5m2zx3DuhE8TSpwaGCRo2psOZgk1JRKlHgJjFCczfiP0742Z387kd8e6ZVjofQfmCsKWoOKyiVRfr4dXgogV/AM1F7WbXZ6XUQDNPESoCKKf6WAoF6YhEiy8SeUYQZrfLASJ3uJ1XdygziHRVcs8KSyBPJaIhNSZ02nGe3iowSSYB0MZdIaAVaqKPCKl1IuNCFAhRXIT1WW6cWTQl/KChJzglWL1xAxXWpU+ZNijhPcSME8VUsz08vpHn6svgbYyg4ImivRcex7m4EASeVgIkRobgQnYTegwn+FFRYTLZp7FA7oYOu1zRP3AVUOkCj0ay1eZlf19NxPr2cj0/L8Wm5PLtbibNqMZUQoTVgrZEWbo3NyAtImUapJcKBZ5vDWSp+MeGTe37zAd8ecSHM2AoImLk6ygitqc4Y7qUR5xl2DFkxKHFX9PO308++FEwOsphSoh+vPaQGiipUaLYLohSn7JNHlCi3Z7ZDBtHjqqCQekkJMbcmhAECC6tyxjIFKFCl7Njzb6F1y862iQfdjZlVfYQtohNoYfXo9S7tE3lZ1IDBvX8x245cwpIc/spzxYzl6bwlk/rtcEEkWNNuaACVCovsziK1sf7hytbXywy5E39IiOW/CBNGk+FaBRSEmIUHUBfBIAqtd18UAr4uy6nNx7aejBa7Fpb1YlwgInE7XaNDJXBSzZ22RiVKUsPBSaHBWmNbvM1ru6x/8mpPL+3dyZ9O/HDhcfbnIy8rtGDTeopTPy9STXcPevdZfXs3ffnpza+/PPzq39z/6k9XuYU3ERGKQgpYIGAst6YQ3vWGNQovGYAYUeR68ck+0ilpTJrQSuEZX4O8EEsjIoi5A1oQraSheoIPi/3gESGYo09wUJl9fgER4pc599ef0JPU5F3mLyIoEISLabaeBK4e14gijBxUuul1BafMeiJuJt5IawaBnC60LOZ0C4yhGvTSHvMlOul+s+C8nujdiu4ke9jO95eYm+z+xSNhG6abcboR0S0UOM0lmIGhiVH62QFzMAVCJam96KWiuYWYRo+Bh7nZ2tbmy8zL7K/H9f3T+d2TXxa7LLYuOkiZxlJqHep0d1vePJSH+/Hhdrq7HW8Oqjuu0myF1NQgkFbEJ7iGLFGPVQRLv6TaJfWUMDFeew3hK3r6l0mRJsB3ON2srevq3lSC+O/N2roubsvt/UOdHqsvq2Scyjw7qmuEeJAK4QUKBFjwKJal8woqDQXI2oYBIh4f2COBignKtJTOlIsT3GsfnoZr12JTd1ySc5agSGFJzTzEvuI0UfTbqPjoqVGiiiGzHoU/mgwCoy8Y9i/d1OIFPDusZHQQWRBqAIzyYCzyzv29Ukvm46Hv3QEHw3dmTz+wDhIxO8UgVAxSRISqGPtN7XUQ92ZiMbfVUKUE9tEQuIsuM4WrN3Nfe0OzhHoiMThs5RJ7us0hgtbObT1VoUqpZYo1tuMwOmS15vChjIVbyrxdSZJi1GbNaWZY1zbP53k+k1ZUqlYIHFzWy3J5dUgdWMVXYRLyojLl/UqD7PtKHRJKNxRA1WOKJQxfhCIGoO99IZP0ck060HePi1Dp2qkUhIDJ3kqf2PsJW6IcRh4D+JsXTnMK2iBA0V6+3FwJOrVDmDP28bj0PK4gNN+QWW4yPhCLzDP7ExFltFfEhdQQ2eoDlaHKnfZSessvpd2CVpL+N01GOoGIgHhPs5gFWEZ8CqGGlA2rw0fscQ82XyS/GjmpGxnjc5DYfRazhM6ZAqrZelnm+fJSMO92o4kPWsHS2joMVYdBgIriKDnqGbAsewju4Gp4PgJVGPsAAASmSURBVB1fjs+KoixON28eZQSHM8xDa70ZbnektuVUVU2oLoB4OKJOMs1m45aU9jt21VfbyHX5DCHolI+ekd6XnXgUizNVvQTtDmKxf5VxY3rkiOd5zhIxmE8OgJYcm6iDB20t7DIUrbI6VoQo8VBITsQZYPpzZE8wqKIhmBVqNkDycuLjV4Qnp1z5DdKVlSJ6A4Fp0r2Glaf/i02w2E4aNhKlJLzpV650+GuIPYkwUBnVio8idZonhCIFUGoUjJrZ2uZ5Pl3OH9bLEa2Nu/3u8LC7ua/DMJK2SNXh7vA4FNVIzEdCxBlMf5QoFlxvfeBkNLfzspzPJ1st1jsBCfx6yI9EyVS0iIpKEVRVT1Z7X3edKWiPK9K9Q567YAGmu053GeoCkeYZMl5QLWNebDwO+WWBIBJ+iEY4i7p/OI2PmirBXM2A088zBAjJ6sx4KRmlwjuBBRG/MtpSpMScLUW6E4lDqQWiQXiLex5RS2q6mU433OpWJQ3I+3cF4/W8b8OSnuFodgu3Ajc7BI90OJt0njmRXDHq5onJENkh+kbZ/OkFAgBw0oyrXZb1dFku8+XVLkdv81DLNNVp0lJcVKfdsJ8+U/HonBJRLM+orLErJWNNFk6AcKCyGOalFdSpSAvBwWugTNrmlkoaPF6r7rVDi9447aepF7Mlu6Ph1hk6Behgp2eKgdgZxWL2Y56iA9qrPpmomTRCqvZ7Hzs/w7Xw2s0ouJIKpCcx3Ow62frsUU8k5CcZOE9UTEW1DFRtvrq1tjTSyzCU3a6EzAoUbOtyFpWh7hRVWBy9Kri5FEZbN6O89sZQYkLJkH3t9TCv45YsdJvobj8BdWjRxbSpd4ly9DoAFBTxDIEd+ESKlRCcpLuKjON+rHs5vEkOropiIOg0tjj5xUUAad2JgCjgkIkcNho5SE8PDCdbjGaQlkmMk1t6tMEPKUkdMQeFWseSqTC2bCx8R4a+/JJx/yThr8uVoNTxCQLLBPxINTxBIbQl2TjHPk28j09voU4SWkWMY0fk4p3s0zGGJI8fQPRIRIgYhSSAlom5kGykOaTNp/MyX9a5zfN6vtRxvH/8GVUvNguwq1Nb5w8ffljWyyef/uJw90lss0lzhgQtjUl+zLjeXU+iGgggzp7pARLpLXgt8/f86workE6fPSflFh6EBqyg0eGU6Bk4JOWDEc31sDwohKoao415yIJbFcoYRWLKTKJsVNJ2HQSbmBEDNBfThg8SACVxha5Dld1YV2IRpFqLx9sEQtjOCksfXQJZA0VuKUAE/Gu5X65fXMJPpVpcUurTdYS3QG4ViPBreSECFqkQvs5GL2WU6J4lo0fZmT1ZqogbmRe+h4lrGQIdBqnGkWZhYsT4G3dv5rNGL4c2FB3qAbtD26+iWoo+P/10Pp2L6lAHrdUd67pe5vPNoYML6T6FUQSOwehMFTxHxTUiRzp8iuckqyi7L+/uSqW3Mjcfljl15i2pbkGhN/oFXEnzwLECuDgr1dDZWmH10v1WAM7IuDNIMz5UpPhxHNWjpZwVGIOIhVCSBrQOSa78OkUE4sOgtUw2+eoeUjHiUT12MOVDA/Mqek0W+P/IbLPKVcsYTQAAAABJRU5ErkJggg==";
				// if (base64 != "") {
				// new ImageuploadOnServer().execute(Constants.IMAGEUPLOAD_URL
				// + CompleteURL);
				// }
				Log.w("path of image from gallery......******************.........",
						picturePath + "");
				// new
				// AsyncImageLoader(getApplicationContext()).fetchDrawableOnThread("http://webneel.com/wallpaper/sites/default/files/images/08-2013/23-3d-beach-sand-wallpaper.jpg",
				// PI_imgUserImage,new BitmapDrawable(getResources(),bitmap));
				// PI_imgUserImage.setImageBitmap(imgLoader.roundCorner(Bitmap.createScaledBitmap(bitmap,
				// bitmap.getWidth()/2, bitmap.getHeight()/2, true),
				// bitmap.getWidth()/2));
			}
		}
	}

	public class ImageuploadOnServer extends AsyncTask<String, Void, Void> {

		// Required initialization

		private ProgressDialog Dialog = new ProgressDialog(
				EditProfileActivity.this);
		String image = "";
		String Content = "";

		protected void onPreExecute() {
			// Dialog.setMessage("Please wait..");
			// Dialog.setCancelable(false);
			// Dialog.show();
			try {
				image += "&" + URLEncoder.encode("image", "UTF-8") + "="
						+ URLEncoder.encode(base64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.e("image", image);
		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			// new ImageUploadServer(bitmap_, Constants.IMAGEUPLOAD_URL
			// + CompleteURL);

			BufferedReader reader = null;
			// Log.e("urls[0]", urls[0]);
			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(5000);
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(image);

				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {

			// Dialog.dismiss();
			Log.e("Content", Content);
			// byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
			// Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,
			// 0, decodedString.length);
			// PI_imgUserImage.setImageBitmap(decodedByte);
		}
	}

	public String getCountPostionForNumber(String number,
			ArrayList<HashMap<String, String>> CONTLIST) {
		String i = "";

		for (int j = 0; j < CONTLIST.size(); j++) {
			if (number.subSequence(0, 3).equals(
					CONTLIST.get(j).get(Constants.COUNTRYLSIT_DIALCODE)
							.toString())) {
				return i = j + "-3";
			} else if (number.subSequence(0, 4).equals(
					CONTLIST.get(j).get(Constants.COUNTRYLSIT_DIALCODE)
							.toString())) {
				return i = j + "-4";
			} else if (number.subSequence(0, 5).equals(
					CONTLIST.get(j).get(Constants.COUNTRYLSIT_DIALCODE)
							.toString())) {
				return i = j + "-5";
			}
		}

		return i;
	}
	
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//	    super.onConfigurationChanged(newConfig);
//	   
//	    new ChangeLanguage(EditProfileActivity.this, session_email
//				.getUserDetails()
//				.get(SessionManager_email.KEY_LANGUAGE).toString());
//	    
//	}
	
	@Override
	public void onStart() {
		super.onStart();
		// The rest of your onStart() code.
		Tracker easyTracker = EasyTracker.getInstance(this);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "Edit Profile");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
	}

}
