package com.connexun.main;

import com.connexun.main.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.graphics.Bitmap;

public class NewsWebview extends Activity {

	TextView textShare, textBrowser;
	WebView NewsWebview;
	private static final String TAG = "Main";
	private ProgressDialog progressBar;

	String newsurl = "";
	Bundle bundle;
	ImageButton imgbtnBack;
	ProgressBar progressBar_n;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newswebview);
		textShare = (TextView) findViewById(R.id.textShare);
		textBrowser = (TextView) findViewById(R.id.textBrowser);
		NewsWebview = (WebView) findViewById(R.id.NewsWebview);
		imgbtnBack = (ImageButton) findViewById(R.id.imgbtnBack);
		progressBar_n=(ProgressBar)findViewById(R.id.progressBar);
		bundle = getIntent().getExtras();
		newsurl = bundle.getString("newsurl");

		// WebSettings settings = NewsWebview.getSettings();
		NewsWebview.getSettings().setUseWideViewPort(true);
		NewsWebview.getSettings().setLoadWithOverviewMode(true);
		NewsWebview.getSettings().setJavaScriptEnabled(true);
		NewsWebview.getSettings().setBuiltInZoomControls(true);
		NewsWebview.getSettings().setSupportZoom(true); // Zoom Control on web
														// (You don't need this
		// if ROM supports Multi-Touch
		// NewsWebview.getSettings().setBuiltInZoomControls(true); //Enable
		// Multitouch if supported by ROM
		// NewsWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

//		progressBar = ProgressDialog.show(
//				NewsWebview.this,
//				"",
//				NewsWebview.this.getResources().getString(
//						R.string.Alert_Pleasewait)
//						+ "");
//		progressBar.setCancelable(true);
		imgbtnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				deleteDatabase("webviewCookiesChromium.db");
				deleteDatabase("webview.db");
				deleteDatabase("webviewCache.db");
				NewsWebview.clearCache(true);
				finish();
			}
		});

		NewsWebview.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.i(TAG, "Processing webview url click...");
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				progressBar_n.setVisibility(View.VISIBLE);
				NewsWebview.this.progressBar_n.setProgress(0);
				
				super.onPageStarted(view, url, favicon);
			}
			

			public void onPageFinished(WebView view, String url) {
				Log.i(TAG, "Finished loading URL: " + url);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//						if (progressBar.isShowing()) {
//							progressBar.dismiss();
//						}
//					}
//				}, 5000);
				// progressBar.setVisibility(View.GONE);
				progressBar_n.setVisibility(View.GONE);
				NewsWebview.this.progressBar_n.setProgress(100);
				super.onPageFinished(view, url);
			}

			@SuppressWarnings("deprecation")
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Log.e(TAG, "Error: " + description);
				// Toast.makeText(NewsWebview.this, "Oh no! " + description,
				// Toast.LENGTH_SHORT).show();
				alertDialog.setTitle("Error");
				alertDialog.setMessage(description);
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								return;
							}
						});
				alertDialog.show();
			}

		});
		NewsWebview.loadUrl(newsurl);

		textShare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent sharingIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				String shareBody = newsurl;
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Share");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						shareBody);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
			}
		});
		textBrowser.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(newsurl));
				startActivity(intent);
				// finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		deleteDatabase("webviewCookiesChromium.db");
		deleteDatabase("webview.db");
		deleteDatabase("webviewCache.db");
		NewsWebview.clearCache(true);

	}
	private class MyWebViewClient extends WebChromeClient {	
		@Override
		public void onProgressChanged(WebView view, int newProgress) {			
			NewsWebview.this.setValue(newProgress);
			super.onProgressChanged(view, newProgress);
		}
	}
	public void setValue(int progress) {
		this.progressBar_n.setProgress(progress);		
	}
	// @Override
	// public void onConfigurationChanged(Configuration newConfig) {
	// super.onConfigurationChanged(newConfig);
	// if (new SessionManager_email(NewsWebview.this).getUserDetails().get(
	// SessionManager_email.KEY_LANGUAGE) == null) {
	// return;
	// }
	// new ChangeLanguage(NewsWebview.this, new SessionManager_email(
	// NewsWebview.this).getUserDetails()
	// .get(SessionManager_email.KEY_LANGUAGE).toString());
	//
	// }

}
