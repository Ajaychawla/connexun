package com.connexun.main;

import com.connexun.main.R;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RegisterLoginActivity extends Activity {

	SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_login);

		session = new SessionManager(RegisterLoginActivity.this);
		if (session.isLoggedIn()) {
			Intent i = new Intent(RegisterLoginActivity.this,
					HomeActivity.class);
			RegisterLoginActivity.this.startActivity(i);
			finish();
		}

		LinearLayout register_button = (LinearLayout) findViewById(R.id.rowregister);
		LinearLayout login_button = (LinearLayout) findViewById(R.id.rowLogin);

		register_button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(RegisterLoginActivity.this,
						RegisterActivity.class);
				startActivityForResult(i, 1);
				//finish();
			}
		});

		login_button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(RegisterLoginActivity.this,
						LoginActivity.class);
				startActivityForResult(i, 1);
				//finish();

			}
		});
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		 if (resultCode == Activity.RESULT_OK) {
			 RegisterLoginActivity.this.finish();
		 }
	}
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (new SessionManager_email(RegisterLoginActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE) == null) {
//			return;
//		}
//		new ChangeLanguage(RegisterLoginActivity.this,
//				new SessionManager_email(RegisterLoginActivity.this).getUserDetails().get(SessionManager_email.KEY_LANGUAGE)
//						.toString());
//
//	}
}
