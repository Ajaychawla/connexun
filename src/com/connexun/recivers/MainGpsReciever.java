package com.connexun.recivers;

import com.connexun.utils.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MainGpsReciever extends BroadcastReceiver {

	public void onReceive(Context arg0, Intent calledIntent) {

		Constants.GPSLatitude = calledIntent.getDoubleExtra("latitude", -1);
		Constants.GPSLongitude = calledIntent.getDoubleExtra("longitude", -1);
		Constants.GPSSpeed = calledIntent.getFloatExtra("speed", -1);

		Log.e("GPS Demo", "Lat & Long --- " + Constants.GPSLatitude + "--"
				+ Constants.GPSLongitude);
		Log.e("GPS Demo", "Speed --- " + Constants.GPSSpeed);
		Toast.makeText(
				arg0,
				"Lat & Long --- " + Constants.GPSLatitude + "--"
						+ Constants.GPSLongitude, Toast.LENGTH_LONG).show();
		//

	}

}