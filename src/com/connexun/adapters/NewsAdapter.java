package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.main.HomeActivity;
import com.connexun.main.LoginActivity;
import com.connexun.main.NewsWebview;
import com.connexun.main.R;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

@SuppressLint("ViewHolder")
public class NewsAdapter {

	Activity context_;
	ImageLoader imgLoader;
	int point_;
	ArrayList<HashMap<String, String>> GET_NEWS_ = new ArrayList<HashMap<String, String>>();

	public NewsAdapter(Activity activity,
			ArrayList<HashMap<String, String>> GET_NEWS, int point) {
		this.context_ = activity;
		this.GET_NEWS_ = GET_NEWS;
		imgLoader = new ImageLoader(context_);
		this.point_ = point;

	}

	public View getView(int position,final ViewGroup root) {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.news_layout, null, false);

		final RelativeLayout newsPareant = (RelativeLayout) row
				.findViewById(R.id.newsPareant);
		newsPareant.setTag(position);
		// News bg img
		ImageView ImageViewNewsBG = (ImageView) row
				.findViewById(R.id.ImageViewNewsBG);

		// lower part of news
		final RelativeLayout relativeNewsLower = (RelativeLayout) row
				.findViewById(R.id.relativeNewsLower);
		TextView textTitle = (TextView) row.findViewById(R.id.textTitle);
		TextView textDate = (TextView) row.findViewById(R.id.textDate);

		// Upper part of news
		final RelativeLayout relativeNewsUppder = (RelativeLayout) row
				.findViewById(R.id.relativeNewsUppder);
		TextView textTitleTwo = (TextView) row.findViewById(R.id.textTitleTwo);
		TextView textDateTwo = (TextView) row.findViewById(R.id.textDateTwo);
		TextView textDescription = (TextView) row
				.findViewById(R.id.textDescription);
		TextView imageViewReadmore = (TextView) row
				.findViewById(R.id.imageViewReadmore);
		TextView imageViewShare = (TextView) row
				.findViewById(R.id.imageViewShare);
		TextView textView1 = (TextView) row.findViewById(R.id.textView1);
		TextView textView2 = (TextView) row.findViewById(R.id.textView2);

		textView1.setTypeface(getNormalFont());
		textView2.setTypeface(getNormalFont());
		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_NEWS_.get(position);

		imgLoader.DisplayImage(map.get(Constants.NEWS_PICTURE).toString(),
				R.drawable.logo_text, ImageViewNewsBG, 0);
		String title = map.get(Constants.NEWS_TITLE).toString();
		if (title.length() > 15)
			title.substring(0, 15);
		title += "...";
		// textTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0,
		// R.drawable.ic_launcher, 0);
		textTitle.setText(title);
		textTitleTwo.setText(title);
		
		
		textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME).toString()
				+ " " + context_.getResources().getString(R.string.News_by)
				+ " " + map.get(Constants.NEWS_SOURCE).toString());

		
		if(new ChangeLanguage().getlanguage().toString().equals("hi")){
			textDateTwo.setText( map.get(Constants.NEWS_SOURCE).toString()
					+ " " + context_.getResources().getString(R.string.News_by)
					+ " " +map.get(Constants.NEWS_ACQIRED_DATETIME).toString());
			textDate.setText( map.get(Constants.NEWS_SOURCE).toString()
					+ " " + context_.getResources().getString(R.string.News_by)
					+ " " +map.get(Constants.NEWS_ACQIRED_DATETIME).toString());
		}else{
		textDateTwo.setText(map.get(Constants.NEWS_ACQIRED_DATETIME).toString()
				+ " " + context_.getResources().getString(R.string.News_by)
				+ " " + map.get(Constants.NEWS_SOURCE).toString());
		textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME).toString()
				+ " " + context_.getResources().getString(R.string.News_by)
				+ " " + map.get(Constants.NEWS_SOURCE).toString());
		}
		String description = map.get(Constants.NEWS_DESCRIPTION).toString();
		if (description.length() > 20)
			description.substring(0, 20);
		description += "...";

		textDescription.setText(description);
		textTitle.setText(map.get(Constants.NEWS_TITLE).toString());
		final String newsurl = map.get(Constants.NEWS_URL).toString();

		imageViewReadmore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EasyTracker easyTracker = EasyTracker.getInstance(context_);

				  // MapBuilder.createEvent().build() returns a Map of event fields and values
				  // that are set and sent with the hit.
				  easyTracker.send(MapBuilder
				      .createEvent("Read More",     // Event category (required)
				                   "Read More",  // Event action (required)
				                   "Read More",   // Event label
				                   null)            // Event value
				      .build()
				  );
				Intent intent = new Intent(context_, NewsWebview.class);
				intent.putExtra("newsurl", newsurl);
				context_.startActivity(intent);
			}
		});
		imageViewShare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				EasyTracker easyTracker = EasyTracker.getInstance(context_);

				  // MapBuilder.createEvent().build() returns a Map of event fields and values
				  // that are set and sent with the hit.
				  easyTracker.send(MapBuilder
				      .createEvent("Share",     // Event category (required)
				                   "Share",  // Event action (required)
				                   "Share",   // Event label
				                   null)            // Event value
				      .build()
				  );
				// TODO Auto-generated method stub
				Intent sharingIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				String shareBody = newsurl;
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Share");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						shareBody);
				context_.startActivity(Intent.createChooser(sharingIntent,
						"Share via"));
			}
		});
		relativeNewsUppder.setTag(position);
		relativeNewsLower.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// relativeNewsLower.setVisibility(View.GONE);
				// relativeNewsUppder.setVisibility(View.VISIBLE);
				//Toast.makeText(context_, newsPareant.getTag().toString()+"--", Toast.LENGTH_LONG).show();
				//String tag=newsPareant.getTag().toString();
				//ArrayList<View> views = new ArrayList<View>();
			    final int childCount = root.getChildCount();
			    //Log.e("childCount--", childCount+"---");
			    for (int i = 0; i < childCount; i++) {
			        final View child = root.getChildAt(i);
			        final ViewGroup root2 = (ViewGroup) root.getChildAt(i);
			     // Log.e("Tags--", child.getTag()+"---");

			        //final Object tagObj = child.getTag();
			        final int childCount_2 = root2.getChildCount();
			        //Log.e("childCount_2--",childCount_2+"---");
//			        if (tagObj != null && tagObj.equals(tag)) {
			        for (int y = 0; y < childCount_2; y++) {
			        	 final View child2 = root2.getChildAt(y);
			        	//Log.e("Match - Tags--", child2+"---"+tag);
			        if (child2.getTag().equals(newsPareant.getTag())){
			           // views.add(child);
			        	//Log.e("Match - Tags--", child2.getTag()+"---"+tag);
			        	((RelativeLayout) child2
			    				.findViewById(R.id.relativeNewsUppder)).setVisibility(View.VISIBLE);
			        	((RelativeLayout) child2
			    				.findViewById(R.id.relativeNewsLower)).setVisibility(View.GONE);
			        }else{
			        	((RelativeLayout) child2
			    				.findViewById(R.id.relativeNewsUppder)).setVisibility(View.GONE);
			        	((RelativeLayout) child2
			    				.findViewById(R.id.relativeNewsLower)).setVisibility(View.VISIBLE);
			        }
			        }

			    }
//				HomeActivity.sharedInstances.AddNewsInLinear(Integer
//						.parseInt(relativeNewsUppder.getTag().toString()));
			}
		});

		if (point_ >= 0) {
			relativeNewsLower.setVisibility(View.GONE);
			relativeNewsUppder.setVisibility(View.VISIBLE);
		}

		// relativeNewsUppder.requestFocus();
		// relativeNewsUppder
		// .setOnFocusChangeListener(new View.OnFocusChangeListener() {
		//
		// @Override
		// public void onFocusChange(View v, boolean hasFocus) {
		// // TODO Auto-generated method stub
		// Toast.makeText(context_, hasFocus+v.getTag().toString(),
		// Toast.LENGTH_SHORT).show();
		// if (!hasFocus) {
		// relativeNewsLower.setVisibility(View.VISIBLE);
		// relativeNewsUppder.setVisibility(View.GONE);
		// }
		// }
		// });

		return row;
	}

	private Typeface getNormalFont() {
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(context_.getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		return normalFont;

	}

}