package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.connexun.main.R;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Movie;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PeoplearoundListAdapter extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	ArrayList<HashMap<String, String>> PEOPLEAROUND_ARRAY = new ArrayList<HashMap<String, String>>();
	ImageLoader imgLoader;

	// ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public PeoplearoundListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> peoplearound) {
		this.activity = activity;
		this.PEOPLEAROUND_ARRAY = peoplearound;
		imgLoader = new ImageLoader(activity);
	}

	@Override
	public int getCount() {
		return PEOPLEAROUND_ARRAY.size();
	}

	@Override
	public Object getItem(int location) {
		return PEOPLEAROUND_ARRAY.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.item_list, null);

		// if (imageLoader == null)
		// imageLoader = AppController.getInstance().getImageLoader();
		ImageView thumbNail = (ImageView) convertView
				.findViewById(R.id.imageView1);
		TextView title = (TextView) convertView.findViewById(R.id.title);
		ImageView imgInterestCountry = (ImageView) convertView
				.findViewById(R.id.imgInterestCountry);
		ImageView imgCountry = (ImageView) convertView
				.findViewById(R.id.imgCountry);
		// getting movie data for the row
		HashMap<String, String> map = new HashMap<String, String>();
		map = PEOPLEAROUND_ARRAY.get(position);
		// thumbnail image
		// thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

		// title
		title.setText(map.get(Constants.PEOPLEAROUND_FIRSTNAME).toString()+" "+map.get(Constants.PEOPLEAROUND_LASTNAME).toString());

		// Log.d("Imageurl", map.get(MainActivity.IMAGE_URL).toString()+"---");
//		thumbNail.setImageBitmap(imgLoader.ConvertBase64ToBitmap(map.get(Constants.PEOPLEAROUND_AVATAR)
//				.toString()));
		imgLoader.DisplayImage(
				map.get(Constants.PEOPLEAROUND_IMAGE).toString(),
				R.drawable.default_avatar_single, thumbNail, 50);
		Uri uriPeopIntrestCount = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ map.get(Constants.PEOPLEAROUND_INTERESTCOUNT));
		imgInterestCountry.setImageURI(uriPeopIntrestCount);

		Uri uriPeohomeCount = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ map.get(Constants.PEOPLEAROUND_HOMECOUNT));
		imgCountry.setImageURI(uriPeohomeCount);
		// rating

		return convertView;
	}

}