package com.connexun.adapters;

import java.util.ArrayList;

import com.connexun.main.R;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ViewPagerControllers {
	public ViewPagerControllers() {
		// TODO Auto-generated constructor stub
	}
	public  ArrayList<View> getViewsByTag(ViewGroup root, String tag) {
		ArrayList<View> views = new ArrayList<View>();
		final int childCount = root.getChildCount();
		Log.d("childCount", childCount + "--");
		for (int i = 0; i < childCount; i++) {
			final View child = root.getChildAt(i);
			// if (child instanceof ViewGroup) {
			// //child.setBackgroundResource(R.drawable.btn_slider_outline);
			// views.addAll(getViewsByTag((ViewGroup) child, tag));
			// Log.d("addAll", i+"--");
			// }
			child.setBackgroundResource(R.drawable.btn_slider_outline);
			final String tagObj = child.getTag().toString();
			Log.d("child.getTag()", tagObj + "--" + tag);
			if (tagObj.equals(tag)) {
				// views.add(child);
				Log.d("tagObj==tag", i + "--");
				child.setBackgroundResource(R.drawable.btn_slider_full);

			}

		}
		return views;
	}
	public void CreateCustomButtons(ViewGroup buttons_viewPager, int maxIndex,
			final ViewPager viewpager,Context context_) {

		for (int i = 0; i < maxIndex; i++) {
			ImageView createbtn = new ImageView(context_);
			createbtn.setTag(i);
			createbtn.setImageResource(R.drawable.btn_slider_outline);
			int width = (int) context_.getResources().getDimension(R.dimen.dp5);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			lp.setMargins(width, width, width, width);
			
			
			//lyoutpherms.setm
			createbtn.setLayoutParams(lp);
			// createbtn.setPadding(width, width, width, width);
		//	createbtn.setmar
			createbtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					viewpager.setCurrentItem(Integer.parseInt(v.getTag()
							.toString()));
				}
			});
			buttons_viewPager.addView(createbtn);
		}
		getViewsByTag(buttons_viewPager, "0");
	}
}
