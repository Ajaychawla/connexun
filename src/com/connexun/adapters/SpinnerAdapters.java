package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connexun.main.R;
import com.connexun.main.RegisterActivity;

public class SpinnerAdapters extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	Activity context_;

	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_ = new ArrayList<HashMap<String, String>>();

	public SpinnerAdapters(Activity activity,
			ArrayList<HashMap<String, String>> GET_COUNTRYLIST) {
		this.context_ = activity;
		this.GET_COUNTRYLIST_ = GET_COUNTRYLIST;

	}

	@Override
	public int getCount() {
		return GET_COUNTRYLIST_.size();
	}

	@Override
	public Object getItem(int location) {
		return GET_COUNTRYLIST_.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.spinner_country, parent, false);
		TextView label = (TextView) row.findViewById(R.id.country);

		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_COUNTRYLIST_.get(position);
		label.setText(map.get(RegisterActivity.COUNTRYNAME));

		ImageView icon = (ImageView) row.findViewById(R.id.icon);
		// icon.setImageResource(image_[position]);
		Uri otherPath = Uri.parse("android.resource://com.connexun.main/drawable/"
				+ map.get(RegisterActivity.COUNTRYCODE));
		icon.setImageURI(otherPath);
		return row;
	}

}
