package com.connexun.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connexun.main.R;

public class spinnerInterest extends ArrayAdapter<String> {

	Context context_;
	Integer[] image_;
	String[] Object_;

	public spinnerInterest(Context context, int textViewResourceId,
			Integer[] image, String[] objects) {
		super(context, textViewResourceId, objects);
		this.context_ = context;
		this.image_ = image;
		this.Object_ = objects;
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		// return super.getView(position, convertView, parent);

//		LayoutInflater inflater = new LayoutInflater(context_)
//				.getLayoutInflater();
		LayoutInflater inflater = (LayoutInflater)context_.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.spinner_country, parent, false);
		TextView label = (TextView) row.findViewById(R.id.country);
		label.setText(Object_[position]);

		ImageView icon = (ImageView) row.findViewById(R.id.icon);
		icon.setImageResource(image_[position]);
		icon.setVisibility(View.INVISIBLE);
		return row;
	}

	public void onNothingSelected(AdapterView parent) {
		// Do nothing.
	}

}
