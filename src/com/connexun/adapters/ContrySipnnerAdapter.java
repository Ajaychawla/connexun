package com.connexun.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connexun.main.R;
import com.connexun.utils.Constants;

public class ContrySipnnerAdapter extends ArrayAdapter<HashMap<String, String>> {

	Context context_;
	Integer[] image_;
	ArrayList<HashMap<String, String>> Object_;

	public ContrySipnnerAdapter(Context context, int textViewResourceId,
			ArrayList<HashMap<String, String>> objects) {
		super(context, textViewResourceId, objects);
		this.context_ = context;
	
		this.Object_ = objects;
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		// return super.getView(position, convertView, parent);

//		LayoutInflater inflater = new LayoutInflater(context_)
//				.getLayoutInflater();
		LayoutInflater inflater = (LayoutInflater)context_.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.item_cont_list, parent, false);
		TextView text_contname = (TextView) row.findViewById(R.id.text_contname);
		TextView text_phonecode = (TextView) row.findViewById(R.id.text_phonecode);
		ImageView imageView1 = (ImageView) row.findViewById(R.id.imageView1);
		
		
		HashMap<String, String> map = new HashMap<String, String>();
		map = Object_.get(position);
		
		text_contname.setText(map.get(Constants.COUNTRYLSIT_NAME).toString());
		text_phonecode.setText(map.get(Constants.COUNTRYLSIT_DIALCODE).toString());
		try 
		{
		    // get input stream
		    InputStream ims = context_.getAssets().open("flags/"+map.get(Constants.COUNTRYLSIT_CODE).toString()+".png");
		    // load image as Drawable
		    Drawable d = Drawable.createFromStream(ims, null);
		    // set image to ImageView
		    imageView1.setImageDrawable(d);
		}
		catch(IOException ex) 
		{
		    
		}
		
		
		return row;
	}

	public void onNothingSelected(AdapterView parent) {
		// Do nothing.
	}

}
