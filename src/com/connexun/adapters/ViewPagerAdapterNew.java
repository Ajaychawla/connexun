package com.connexun.adapters;

import java.io.BufferedReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.connexun.main.HomeActivity;
import com.connexun.main.NewsWebview;
import com.connexun.main.PeopleAroundList;
import com.connexun.main.R;
import com.connexun.main.UsersDetails;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewPagerAdapterNew extends PagerAdapter {
	// Declare Variables
	Activity context_;
	ImageLoader imgLoader;

	String whichOne;
	String CompleteURL = "";
	private final SparseArray<View> views = new SparseArray<>();
	ArrayList<HashMap<String, String>> GET_NEWS_ = new ArrayList<HashMap<String, String>>();

	public ViewPagerAdapterNew(Activity activity,
			ArrayList<HashMap<String, String>> GET_NEWS, boolean isExpert,
			String whichOne) {
		this.context_ = activity;
		this.GET_NEWS_ = GET_NEWS;
		imgLoader = new ImageLoader(context_);

		this.whichOne = whichOne;

	}

	@Override
	public int getCount() {
		return GET_NEWS_.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_NEWS_.get(position);
		View row = null;
		if (map.get(Constants.NEWS_BOXTYPE).toString().equals("0"))
			row = BoxType_ContentExpert_0(container, position, "news");
		else if (map.get(Constants.NEWS_BOXTYPE).toString().equals("1"))
			row = BoxType_ContentExpert_0(container, position, "topnews");
		else if (map.get(Constants.NEWS_BOXTYPE).toString().equals("2"))
			row = BoxType_ContentExpert_0(container, position, "Expert");
		else if (map.get(Constants.NEWS_BOXTYPE).toString().equals("3"))
			row = BoxType_ContentExpert_0(container, position, "Event");
		else if (map.get(Constants.NEWS_BOXTYPE).toString().equals("4"))
			row = BoxType_ContentExpert_0(container, position, "promotion");
		else if (map.get(Constants.NEWS_BOXTYPE).toString().equals("5"))
			row = BoxType_ContentExpert_0(container, position, "promotionfull");
		else {
			LayoutInflater inflater = (LayoutInflater) context_
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.news_layout, container, false);
		}

		((ViewPager) container).addView(row);

		return row;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		// Log.e("destroyItem", position + "---");
		((ViewPager) container).removeView((RelativeLayout) object);

	}

	public int getItemPosition(Object object) {

		return POSITION_NONE;
	}

	private Typeface getNormalFont() {
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(context_.getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		return normalFont;

	}

	public View BoxType_ContentExpert_0(ViewGroup container, int position,
			String filter) {
		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.news_layout, container, false);

		final RelativeLayout newsPareant = (RelativeLayout) row
				.findViewById(R.id.newsPareant);
		newsPareant.setTag(position);
		// News bg img
		ImageView ImageViewNewsBG = (ImageView) row
				.findViewById(R.id.ImageViewNewsBG);

		// lower part of news
		final RelativeLayout relativeNewsLower = (RelativeLayout) row
				.findViewById(R.id.relativeNewsLower);
		TextView textTitle = (TextView) row.findViewById(R.id.textTitle);
		TextView textDate = (TextView) row.findViewById(R.id.textDate);
		TextView textDate_n = (TextView) row.findViewById(R.id.textDate_n);
		TextView textDate_by = (TextView) row.findViewById(R.id.textDate_by);
		// Upper part of news
		final RelativeLayout relativeNewsUppder = (RelativeLayout) row
				.findViewById(R.id.relativeNewsUppder);

		TextView textTitleTwo = (TextView) row.findViewById(R.id.textTitleTwo);
		TextView textDateTwo = (TextView) row.findViewById(R.id.textDateTwo);
		TextView textDescription = (TextView) row
				.findViewById(R.id.textDescription);
		TextView imageViewReadmore = (TextView) row
				.findViewById(R.id.imageViewReadmore);
		TextView imageViewShare = (TextView) row
				.findViewById(R.id.imageViewShare);
		TextView textView1 = (TextView) row.findViewById(R.id.textView1);
		TextView textView2 = (TextView) row.findViewById(R.id.textView2);
		ImageView expertIcon = (ImageView) row.findViewById(R.id.expertIcon);
		ImageView categoryicon = (ImageView) row
				.findViewById(R.id.categoryicon);

		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_NEWS_.get(position);
		final String Expert_user_id = "/"
				+ map.get(Constants.NEWS_EXPERT_USERID).toString();

		final String newsurl = map.get(Constants.NEWS_URL).toString();

		textView1.setTypeface(getNormalFont());
		textView2.setTypeface(getNormalFont());

		imgLoader.DisplayImage(map.get(Constants.NEWS_PICTURE).toString(),
				R.drawable.logo_, ImageViewNewsBG, 0);
		String title = map.get(Constants.NEWS_TITLE).toString();
		if (title.length() > 15)
			title.substring(0, 15);
		title += "...";
		// textTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0,
		// R.drawable.ic_launcher, 0);
		textTitle.setText(title);
		textTitleTwo.setText(title);

		textDate_n.setVisibility(View.GONE);
		textDate_by.setVisibility(View.GONE);
		textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME).toString()
				+ " " + context_.getResources().getString(R.string.News_by)
				+ " " + map.get(Constants.NEWS_SOURCE).toString());
		if (new ChangeLanguage().getlanguage().toString().equals("hi")) {
			// textDateTwo.setText(map.get(Constants.NEWS_SOURCE).toString() +
			// " "
			// + context_.getResources().getString(R.string.News_by) + " "
			// + map.get(Constants.NEWS_ACQIRED_DATETIME).toString());
			// textDate.setText(map.get(Constants.NEWS_SOURCE).toString() + " "
			// + context_.getResources().getString(R.string.News_by) + " "
			// + map.get(Constants.NEWS_ACQIRED_DATETIME).toString());
			textDate.setText(map.get(Constants.NEWS_SOURCE).toString() + " "
					+ context_.getResources().getString(R.string.News_by) + " "
					+ map.get(Constants.NEWS_ACQIRED_DATETIME).toString());
		} else {
			// textDateTwo.setText(map.get(Constants.NEWS_ACQIRED_DATETIME)
			// .toString()
			// + " "
			// + context_.getResources().getString(R.string.News_by)
			// + " "
			// + map.get(Constants.NEWS_SOURCE).toString());
			// textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME)
			// .toString()
			// + " "
			// + context_.getResources().getString(R.string.News_by)
			// + " "
			// + map.get(Constants.NEWS_SOURCE).toString());
			textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME)
					.toString()
					+ " "
					+ context_.getResources().getString(R.string.News_by)
					+ " "
					+ map.get(Constants.NEWS_SOURCE).toString());
		}
		String description = map.get(Constants.NEWS_DESCRIPTION).toString();
		if (description.length() > 20)
			description.substring(0, 20);
		description += "...";

		textDescription.setText(description);
		textTitle.setText(map.get(Constants.NEWS_TITLE).toString());

		imageViewReadmore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context_, NewsWebview.class);
				intent.putExtra("newsurl", newsurl);
				context_.startActivity(intent);
			}
		});
		imageViewShare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent sharingIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				String shareBody = newsurl;
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Share");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						shareBody);
				context_.startActivity(Intent.createChooser(sharingIntent,
						"Share via"));
			}
		});
		// relativeNewsUppder.setTag(whichOne);
		relativeNewsUppder.setTag(position + "check");
		relativeNewsLower.setTag(whichOne);
		relativeNewsLower.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// relativeNewsLower.setVisibility(View.GONE);
				if (!relativeNewsUppder.isShown()) {
					relativeNewsUppder.setVisibility(View.VISIBLE);
					HomeActivity.sharedInstances.refreshAllPagers(v.getTag()
							.toString());
				}
				// Toast.makeText(context_,
				// newsPareant.getTag().toString()+"--",
				// Toast.LENGTH_LONG).show();
				// String tag=newsPareant.getTag().toString();
				// ArrayList<View> views = new ArrayList<View>();

			}

		});
		relativeNewsUppder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (relativeNewsUppder.isShown()) {
					relativeNewsUppder.setVisibility(View.GONE);
					HomeActivity.sharedInstances.visibleBulletes(v.getTag()
							.toString());
				}
			}
		});

		SessionManager session = new SessionManager(context_);
		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (filter.equals("Expert")) {
			textView1.setText("Community Post");
			textView2.setText("Community Post");
			expertIcon.setVisibility(View.VISIBLE);
			categoryicon.setVisibility(View.GONE);
			textDate_n.setVisibility(View.VISIBLE);
			textDate_by.setVisibility(View.VISIBLE);

			textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME)
					.toString());
			textDate_by.setText(context_.getResources().getString(
					R.string.News_by));

			textDate_n.setText(" " + map.get(Constants.NEWS_SOURCE).toString());
			textDate_n.setTextColor(Color.parseColor("#ee4f3d"));
			textDate_n.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent intent = new Intent(context_, NewsWebview.class);
					// intent.putExtra("newsurl",
					// "http://www.latlong.net/Show-Latitude-Longitude.html");
					// context_.startActivity(intent);
					new PeopleAroundListserver()
							.execute(Constants.NEW_NEWSandPEOPLEARROUND_URL
									+ CompleteURL + Expert_user_id);
				}
			});
		} else if (filter.equals("Event")) {
			textView1.setText("Event");
			textView2.setText("Event");
			expertIcon.setVisibility(View.GONE);
			categoryicon.setVisibility(View.GONE);
			textDate_n.setVisibility(View.VISIBLE);
			textDate_by.setVisibility(View.VISIBLE);
			textDate.setText(map.get(Constants.NEWS_EVENTDATE).toString());
			textDate_by.setText("Posted by ");
			textDate_n.setText(map.get(Constants.NEWS_SOURCE).toString());
			textDate_n.setTextColor(Color.parseColor("#ee4f3d"));
			textDate_n.setTextColor(Color.parseColor("#ee4f3d"));
			textDate_n.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent intent = new Intent(context_, NewsWebview.class);
					// intent.putExtra("newsurl",
					// "http://www.latlong.net/Show-Latitude-Longitude.html");
					// context_.startActivity(intent);
					new PeopleAroundListserver()
							.execute(Constants.NEW_NEWSandPEOPLEARROUND_URL
									+ CompleteURL + Expert_user_id);
				}
			});
		} else if (filter.equals("news")) {
			textView1.setText(context_.getResources().getString(
					R.string.News_news));
			textView2.setText(context_.getResources().getString(
					R.string.News_news));
			expertIcon.setVisibility(View.GONE);
			ChangeCategoryIcon(categoryicon, map.get(Constants.NEWS_CATEGORY)
					.toString());
			categoryicon.setVisibility(View.VISIBLE);
		} else if (filter.equals("topnews")) {
			textView1.setText("Top News");
			textView2.setText("Top News");
			expertIcon.setVisibility(View.VISIBLE);
			expertIcon.setImageResource(R.drawable.top_news_ico);
			categoryicon.setVisibility(View.GONE);
		} else if (filter.equals("promotion")) {
			// relativeNewsLower.setClickable(false);
			textView1.setText("Promotion");
			textView2.setText("Promotion");
			textTitle.setVisibility(View.GONE);
			textDate.setVisibility(View.GONE);
			textDate_n.setVisibility(View.GONE);
			expertIcon.setVisibility(View.GONE);
			categoryicon.setVisibility(View.GONE);

			// ImageViewNewsBG.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// Intent intent = new Intent(context_, NewsWebview.class);
			// intent.putExtra("newsurl",
			// "http://www.latlong.net/Show-Latitude-Longitude.html");
			// context_.startActivity(intent);
			// }
			// });
		} else if (filter.equals("promotionfull")) {
			// /relativeNewsLower.setClickable(false);

			// ImageViewNewsBG.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// Intent intent = new Intent(context_, NewsWebview.class);
			// intent.putExtra("newsurl",
			// "http://www.latlong.net/Show-Latitude-Longitude.html");
			// context_.startActivity(intent);
			// }
			// });
			textView1.setText("Promotion");
			textView2.setText("Promotion");
			expertIcon.setVisibility(View.GONE);
			categoryicon.setVisibility(View.GONE);
			textDate_n.setVisibility(View.VISIBLE);
			textDate_by.setVisibility(View.VISIBLE);
			textDate.setText(map.get(Constants.NEWS_ACQIRED_DATETIME)
					.toString());
			textDate_by.setText("Posted by ");
			textDate_n.setText(map.get(Constants.NEWS_SOURCE).toString());
			textDate_n.setTextColor(Color.parseColor("#ee4f3d"));
			textDate_n.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent intent = new Intent(context_, NewsWebview.class);
					// intent.putExtra("newsurl",
					// "http://www.latlong.net/Show-Latitude-Longitude.html");
					// context_.startActivity(intent);

					new PeopleAroundListserver()
							.execute(Constants.NEW_NEWSandPEOPLEARROUND_URL
									+ CompleteURL + Expert_user_id);
					// new PeopleAroundListserver()
					// .execute("https://api.connexun.com/peoplearound/user/536/QWYvT0hoZldqdGZFTEJIMUp3SXlMMzBIZDRnellKbTlxLzZQTk5QL29FR1FVaWIycmxWTEFNOWVCbUlXM0JvbFJpSThsOVZYNDMwNWNTMDk2S2hXeUE9PQ/1884");
				}
			});
		}

		return row;
	}

	public void ChangeCategoryIcon(ImageView caticon, String filter) {
		try {

			String filter_n = "";
			String iconString = "";
			if (!filter.equals("")) {
				if (filter.contains(",")) {
					filter_n = filter.split(",")[1];

				} else {
					filter_n = filter;

				}

				if (filter_n.equalsIgnoreCase("entertainment")) {
					iconString = "entertainment_ico";
				} else if (filter_n.equalsIgnoreCase("business")) {
					iconString = "business_ico";
				} else if (filter_n.equalsIgnoreCase("opinions")) {
					iconString = "opinions_ico";
				} else if (filter_n.equalsIgnoreCase("sport")) {
					iconString = "sports_ico";
				} else if (filter_n.equalsIgnoreCase("food")) {
					iconString = "food_ico";
				} else if (filter_n.equalsIgnoreCase("travels")
						|| filter_n.equalsIgnoreCase("travel")) {
					iconString = "travel_ico";
				} else if (filter_n.equalsIgnoreCase("art and culture")) {
					iconString = "art_and_culture";
				} else if (filter_n.equalsIgnoreCase("fashion")) {
					iconString = "fashion_ico";
				} else if (filter_n.equalsIgnoreCase("lifestyle")) {
					iconString = "lifestyle_ico";
				} else if (filter_n.equalsIgnoreCase("politics")) {
					iconString = "politics_ico";
				} else if (filter_n.equalsIgnoreCase("technology")) {
					iconString = "technology_ico";
				}

				Uri otherPath = Uri
						.parse("android.resource://com.connexun.main/drawable/"
								+ iconString);
				caticon.setImageURI(otherPath);
			} else {
				caticon.setVisibility(View.INVISIBLE);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public class PeopleAroundListserver extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(context_);

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			try {

				Dialog.setMessage(context_.getResources().getString(
						R.string.Alert_Pleasewait)
						+ "");
				Dialog.setCancelable(false);
				Dialog.show();

				// Set Request parameter
				// user_id += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERID);
				// apitoken += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERTOKEN);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data

				HttpClient Client = new DefaultHttpClient();

				// Create URL string

				// String URL =
				// "http://androidexample.com/media/webservice/httpget.php?user="+loginValue+"&name="+fnameValue+"&email="+emailValue+"&pass="+passValue;

				// Log.i("httpget", URL);

				String SetServerString = "";

				// Create Request to server and get response

				HttpGet httpget = new HttpGet(urls[0]);
				Log.e("urls[0]", urls[0]);

				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is
				// established.
				// The default value is zero, that means the timeout is not
				// used.
				int timeoutConnection = Constants.RequestTimeOutLimit;
				HttpConnectionParams.setConnectionTimeout(httpParameters,
						timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = Constants.RequestTimeOutLimit;
				HttpConnectionParams
						.setSoTimeout(httpParameters, timeoutSocket);

				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				// SetServerString = Client.execute(httpget, responseHandler);

				DefaultHttpClient httpClient = new DefaultHttpClient(
						httpParameters);
				SetServerString = httpClient.execute(httpget, responseHandler);

				// Show response on activity
				Content = SetServerString;
				// content.setText(SetServerString);

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog

			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				// dataListner.GetNewsListners(Error);

				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						context_,
						context_.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				// Log.wtf("NO ERROR : ", Content);

				try {
					if (new ConnectivityServer().CheckExpirationTocken(
							context_, Content)) {
						return;
					}
					ArrayList<HashMap<String, String>> PEOPLEAROUND_ARRAY_New = new ArrayList<HashMap<String, String>>();
					// ArrayList<HashMap<String, String>> PEOPLEAROUND_ARRAY =
					// new ArrayList<HashMap<String, String>>();
					PEOPLEAROUND_ARRAY_New = new ConnectivityServer()
							.ExpertprofileSpinner(Content);

					// PEOPLEAROUND_ARRAY.addAll(PEOPLEAROUND_ARRAY_New);

					Log.e("PEOPLEAROUND_ARRAY.size()",
							PEOPLEAROUND_ARRAY_New.size() + "---" + Content);
					if (PEOPLEAROUND_ARRAY_New.size() > 0) {
						HashMap<String, String> hashMap = PEOPLEAROUND_ARRAY_New
								.get(0);
						Intent intent = new Intent(context_, UsersDetails.class);
						intent.putExtra("hashMap", hashMap);
						context_.startActivity(intent);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}

}
