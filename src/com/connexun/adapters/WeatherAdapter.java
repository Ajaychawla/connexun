package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connexun.main.HomeActivity;
import com.connexun.main.R;
import com.connexun.main.RegisterActivity;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;

public class WeatherAdapter {

	Activity context_;
	String filter_;

	ArrayList<HashMap<String, String>> WEATHERLIST_ = new ArrayList<HashMap<String, String>>();

	public WeatherAdapter(Activity activity,
			ArrayList<HashMap<String, String>> WEATHERLIST, String filter) {
		this.context_ = activity;
		this.WEATHERLIST_ = WEATHERLIST;
		this.filter_ = filter;

	}

	public View getView(int position) {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.weather_layout, null, false);

		TextView textCountryName = (TextView) row
				.findViewById(R.id.textCountryName);
		TextView textCurrencyName = (TextView) row
				.findViewById(R.id.textCurrencyName);
		TextView textCurrencyequal = (TextView) row
				.findViewById(R.id.textCurrencyequal);
		ImageButton imgbtnCloudImg = (ImageButton) row
				.findViewById(R.id.imgbtnCloudImg);
		TextView textTemp = (TextView) row.findViewById(R.id.textTemp);
		TextView textCurrentTime = (TextView) row
				.findViewById(R.id.textCurrentTime);
		ImageButton imgbtnicon = (ImageButton) row
				.findViewById(R.id.imgbtnicon);

		HashMap<String, String> map = new HashMap<String, String>();
		map = WEATHERLIST_.get(position);
		if (filter_.equalsIgnoreCase("home")) {
			if (!map.get(Constants.WEATHER_HOMECITY).toString().equals("")) {
				textCountryName
				.setText(map.get(Constants.WEATHER_HOMECITY).toString()
						+ ", "
						+ map.get(Constants.WEATHER_HOMECOUNT)
								.toString().toUpperCase());
			} else {
				textCountryName
						.setText(Constants.ARRAY_COUNTRY_CENTRE[new JsonLocalFileFetch(
								context_)
								.getPositionCountry(
										Constants.ARRAY_COUNTRY_NICNAME,
										map.get(Constants.WEATHER_HOMECOUNT)
												.toString())]
								+ ", "
								+ map.get(Constants.WEATHER_HOMECOUNT)
										.toString().toUpperCase());
			}
			textCurrencyName
					.setText("1 "
							+ Constants.ARRAY_COUNTRY_CURRENCYCODE[new JsonLocalFileFetch(
									context_).getPositionCountry(
									Constants.ARRAY_COUNTRY_NICNAME,
									HomeActivity.sharedInstances.GET_USERDETAILS.get(0).get(Constants.USER_HOMECOUNT).toString())]);
			double value = Double.parseDouble(map.get(
					Constants.WEATHER_INTERESTCURRENCY).toString());
			double rounded = (double) Math.round(value * 10000) / 10000;
			textCurrencyequal
					.setText("= "
							+ SubstringVaules(map.get(
									Constants.WEATHER_INTERESTCURRENCY)
									.toString())
							+ " "
							+ Constants.ARRAY_COUNTRY_CURRENCYCODE[new JsonLocalFileFetch(
									context_).getPositionCountry(
									Constants.ARRAY_COUNTRY_NICNAME,
									HomeActivity.sharedInstances.GET_USERDETAILS.get(0).get(Constants.USER_INTERESTCOUNT).toString())]);
			textTemp.setText(map.get(Constants.WEATHER_HOMETEMP).toString()
					+ "�C");
			Uri otherPath = Uri
					.parse("android.resource://com.connexun.main/drawable/"
							+ getAtmosphare(map.get(Constants.WEATHER_HOMECODE)));
			imgbtnCloudImg.setImageURI(otherPath);

		} else {
			if (!map.get(Constants.WEATHER_INTERESTCITY).toString().equals("")) {
				textCountryName
				.setText(map.get(Constants.WEATHER_INTERESTCITY).toString()
						+ ", "
						+ map.get(Constants.WEATHER_INTERESTCOUNT)
								.toString().toUpperCase());
			} else {
			textCountryName
					.setText(Constants.ARRAY_COUNTRY_CENTRE[new JsonLocalFileFetch(
							context_)
							.getPositionCountry(
									Constants.ARRAY_COUNTRY_NICNAME,
									map.get(Constants.WEATHER_INTERESTCOUNT)
											.toString())]
							+ ", "
							+ map.get(Constants.WEATHER_INTERESTCOUNT)
									.toString().toUpperCase());
			}
			textCurrencyName
					.setText("1 "
							+ Constants.ARRAY_COUNTRY_CURRENCYCODE[new JsonLocalFileFetch(
									context_).getPositionCountry(
									Constants.ARRAY_COUNTRY_NICNAME,
									HomeActivity.sharedInstances.GET_USERDETAILS.get(0).get(Constants.USER_INTERESTCOUNT).toString())]);

			double value = Double.parseDouble(map.get(
					Constants.WEATHER_HOMECURRENCY).toString());
			double rounded = (double) Math.round(value * 10000) / 10000;
			textCurrencyequal
					.setText("= "
							+ SubstringVaules(map.get(
									Constants.WEATHER_HOMECURRENCY)
									.toString())
							+ " "
							+ Constants.ARRAY_COUNTRY_CURRENCYCODE[new JsonLocalFileFetch(
									context_).getPositionCountry(
									Constants.ARRAY_COUNTRY_NICNAME,
									HomeActivity.sharedInstances.GET_USERDETAILS.get(0).get(Constants.USER_HOMECOUNT).toString())]);
			textTemp.setText(map.get(Constants.WEATHER_INTERESTTEMP).toString()
					+ "�C");
			imgbtnicon.setImageResource(R.drawable.icon_marker);
			Uri otherPath = Uri
					.parse("android.resource://com.connexun.main/drawable/"
							+ getAtmosphare(map
									.get(Constants.WEATHER_INTERESTCODE)));
			imgbtnCloudImg.setImageURI(otherPath);
		}
		// textCountryName.setText(map.get(Constants.COUNTRY_NAME).toString()+", "+map.get(Constants.COUNTRY_REGION).toString());
		// textCurrencyName.setText(map.get(Constants.COUNTRY_NAME).toString());
		// textCurrencyequal.setText(map.get(Constants.COUNTRY_NAME).toString());

		// String title = map.get(Constants.NEWS_TITLE).toString();

		return row;
	}

	public String getAtmosphare(String code) {
		String atmosImg = "";

		if (code.equals("0") || code.equals("1") || code.equals("2")
				|| code.equals("3") || code.equals("4") || code.equals("37")
				|| code.equals("38") || code.equals("39") || code.equals("40")
				|| code.equals("47") || code.equals("45")) {
			atmosImg = "storm";
		} else if (code.equals("5") || code.equals("6") || code.equals("7")
				|| code.equals("35") || code.equals("17")) {
			atmosImg = "rain_mixed";
		} else if (code.equals("8") || code.equals("9") || code.equals("10")
				|| code.equals("11") || code.equals("12")) {
			atmosImg = "rain";
		} else if (code.equals("13") || code.equals("14") || code.equals("41")
				|| code.equals("42") || code.equals("43") || code.equals("46")) {
			atmosImg = "snow_shower";
		} else if (code.equals("15") || code.equals("16") || code.equals("18")
				|| code.equals("25")) {
			atmosImg = "snow";
		} else if (code.equals("19") || code.equals("20") || code.equals("21")
				|| code.equals("22") || code.equals("23") || code.equals("24")) {
			atmosImg = "fog";
		} else if (code.equals("26") || code.equals("44")) {
			atmosImg = "cloudy";
		} else if (code.equals("27") || code.equals("31") || code.equals("33")) {
			atmosImg = "night_clear";
		} else if (code.equals("28") || code.equals("32") || code.equals("34")
				|| code.equals("36")) {
			atmosImg = "day_clear";
		} else if (code.equals("29")) {
			atmosImg = "night_cloud";
		} else if (code.equals("30")) {
			atmosImg = "day_cloud";
		}

		return atmosImg;
	}
	public String SubstringVaules(String currencyValue) {
		try {
			
		
		if (currencyValue.contains(".")) {
			String splitvalue[] = currencyValue.split("\\.");
			//Log.e("splitvalue[]", splitvalue.length+"--"+currencyValue);
			if (splitvalue[1].length() > 4)
				currencyValue = splitvalue[0] + "."
						+ splitvalue[1].substring(0, 4);
			else
				currencyValue = splitvalue[0] + "." + splitvalue[1];
			// Log.d("WEATHER_INTERESTCURRENCY", "--"+map.get(
			// Constants.WEATHER_INTERESTCURRENCY).toString().indexOf(".", 0));

		}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exception", e.toString()+"--");
		}
		return currencyValue;
	}
}