package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connexun.main.NewsWebview;
import com.connexun.main.R;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class PoiAdapter {

	Activity context_;
	ImageLoader imgLoader;
	int i = 0;
	HashMap<String, String> map;

	ArrayList<HashMap<String, String>> GET_POI_ = new ArrayList<HashMap<String, String>>();

	public PoiAdapter(Activity activity,
			ArrayList<HashMap<String, String>> GET_POI) {
		this.context_ = activity;
		this.GET_POI_ = GET_POI;
		imgLoader = new ImageLoader(context_);

	}

	public View getView(int position) {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.poi_layout, null, false);

		ImageView ImageViewNewsBG = (ImageView) row
				.findViewById(R.id.ImageViewNewsBG);
		ImageViewNewsBG.setAlpha(70);

		final TextView textPoiCategory = (TextView) row
				.findViewById(R.id.textPoiCategory);
		final ImageView imgiconUpper = (ImageView) row
				.findViewById(R.id.imgiconUpper);

		final TextView textPoiDistance = (TextView) row
				.findViewById(R.id.textPoiDistance);
		final TextView textTitle = (TextView) row.findViewById(R.id.textTitle);
		final TextView textCountry = (TextView) row
				.findViewById(R.id.textCountry);
		final ImageView imgIconcategory = (ImageView) row
				.findViewById(R.id.imgIconcategory);
		final TextView textSite = (TextView) row.findViewById(R.id.textSite);
		final TextView textLatLong = (TextView) row
				.findViewById(R.id.textLatLong);
		final TextView textPhonenumber = (TextView) row
				.findViewById(R.id.textPhonenumber);
		final TextView textView1 = (TextView) row.findViewById(R.id.textView1);

		textView1.setTypeface(getNormalFont());
		final ImageView imgPoi = (ImageView) row.findViewById(R.id.imgPoi);

		final ImageView imgbtnBack = (ImageView) row
				.findViewById(R.id.imgbtnBack);
		final ImageView imgbtnNext = (ImageView) row
				.findViewById(R.id.imgbtnNext);
		final RelativeLayout relImagepoi = (RelativeLayout) row
				.findViewById(R.id.relImagepoi);
		final LinearLayout lineariconlower=(LinearLayout)row.findViewById(R.id.lineariconlower);

		map = new HashMap<String, String>();
		map = GET_POI_.get(i);
		textPoiCategory.setText(map.get(Constants.POI_CATEGORY).toString());
		textPoiDistance.setText((int) Float.parseFloat(map.get(
				Constants.POI_DISTANCE).toString())
				+ " km");
		textTitle.setText(map.get(Constants.POI_NAME).toString());

		if (!map.get(Constants.POI_FORMATTEDADDRESS).toString().equals("")) {
			textCountry.setText(map.get(Constants.POI_FORMATTEDADDRESS)
					.toString());
			textCountry.setVisibility(View.VISIBLE);
		} else
			textCountry.setVisibility(View.GONE);
		if (!map.get(Constants.POI_WEBSITE).toString().equals("")) {
			textSite.setText(map.get(Constants.POI_WEBSITE).toString());
			textSite.setVisibility(View.VISIBLE);
		} else
			textSite.setVisibility(View.GONE);
		if (!map.get(Constants.POI_LATITUDE).toString().equals("")
				&& !map.get(Constants.POI_LATITUDE).toString().equals("")) {
			textLatLong
					.setText("LAT "
							+ (Math.round(Float.parseFloat(map.get(
									Constants.POI_LATITUDE).toString()) * 10000.0) / 10000.0)
							+ " | LONG "
							+ (Math.round(Float.parseFloat(map.get(
									Constants.POI_LONGITUDE).toString()) * 10000.0) / 10000.0));
			textLatLong.setVisibility(View.VISIBLE);

		} else
			textLatLong.setVisibility(View.GONE);
		if (!map.get(Constants.POI_PHONE).toString().equals("")) {
			textPhonenumber.setText(map.get(Constants.POI_PHONE).toString());
			textPhonenumber.setVisibility(View.VISIBLE);
		} else
			textPhonenumber.setVisibility(View.GONE);
		if (map.get(Constants.POI_ICONNAME).toString().contains("http"))
			imgLoader.DisplayImage(map.get(Constants.POI_ICONNAME).toString(),
					R.drawable.shape_radar_center, imgPoi, 50);
		else
			imgPoi.setImageResource(R.drawable.shape_radar_center);

		imgbtnBack.setImageResource(R.drawable.point_arrow_left_off);
		if (GET_POI_.size() == 1)
			imgbtnNext.setImageResource(R.drawable.point_arrow_right_off);

		setIconForCat(map.get(Constants.POI_CATEGORY).toString(),
				textPoiCategory, imgiconUpper, imgIconcategory,lineariconlower);

		textSite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map
				// .get(Constants.POI_WEBSITE).toString()));
				// context_.startActivity(intent);

				Intent intent = new Intent(context_, NewsWebview.class);
				intent.putExtra("newsurl", map.get(Constants.POI_WEBSITE)
						.toString());
				context_.startActivity(intent);
			}
		});
		textPhonenumber.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:"
						+ map.get(Constants.POI_PHONE).toString()));
				context_.startActivity(intent);
			}
		});

		textTitle.setTag(map.get(Constants.POI_ID).toString());
		textTitle.setOnClickListener(onclickwebopen);
		relImagepoi.setTag(map.get(Constants.POI_ID).toString());
		relImagepoi.setOnClickListener(onclickwebopen);
		imgbtnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(GET_POI_.size()==1)
					return;
				
				if (i >= 0) {
					if (GET_POI_.size() > 0)
						imgbtnNext
								.setImageResource(R.drawable.point_arrow_right_on);
					if (i == 0)
						;
					else {
						imgbtnBack
								.setImageResource(R.drawable.point_arrow_left_on);
						i--;
						if (i == 0)
							imgbtnBack
									.setImageResource(R.drawable.point_arrow_left_off);

					}
					map = new HashMap<String, String>();
					map = GET_POI_.get(i);
					textPoiCategory.setText(map.get(Constants.POI_CATEGORY)
							.toString());
					// textPoiDistance.setText(String.format("%.2f", Float
					// .parseFloat(map.get(Constants.POI_DISTANCE)
					// .toString())));
					textPoiDistance.setText((int) Float.parseFloat(map.get(
							Constants.POI_DISTANCE).toString())
							+ " km");
					textTitle.setText(map.get(Constants.POI_NAME).toString());
					if (!map.get(Constants.POI_FORMATTEDADDRESS).toString()
							.equals("")) {
						textCountry.setText(map.get(
								Constants.POI_FORMATTEDADDRESS).toString());
						textCountry.setVisibility(View.VISIBLE);
					} else
						textCountry.setVisibility(View.GONE);
					if (!map.get(Constants.POI_WEBSITE).toString().equals("")) {
						textSite.setText(map.get(Constants.POI_WEBSITE)
								.toString());
						textSite.setVisibility(View.VISIBLE);
					} else
						textSite.setVisibility(View.GONE);
					if (!map.get(Constants.POI_LATITUDE).toString().equals("")
							&& !map.get(Constants.POI_LATITUDE).toString()
									.equals("")) {
						textLatLong.setText("LAT "
								+ (Math.round(Float.parseFloat(map.get(
										Constants.POI_LATITUDE).toString()) * 10000.0) / 10000.0)
								+ " | LONG "
								+ (Math.round(Float.parseFloat(map.get(
										Constants.POI_LONGITUDE).toString()) * 10000.0) / 10000.0));
						textLatLong.setVisibility(View.VISIBLE);

					} else
						textLatLong.setVisibility(View.GONE);
					if (!map.get(Constants.POI_PHONE).toString().equals("")) {
						textPhonenumber.setText(map.get(Constants.POI_PHONE)
								.toString());
						textPhonenumber.setVisibility(View.VISIBLE);
					} else
						textPhonenumber.setVisibility(View.GONE);
					if (map.get(Constants.POI_ICONNAME).toString()
							.contains("http"))
						imgLoader.DisplayImage(map.get(Constants.POI_ICONNAME)
								.toString(), R.drawable.shape_radar_center,
								imgPoi, 50);
					else
						imgPoi.setImageResource(R.drawable.shape_radar_center);

					setIconForCat(map.get(Constants.POI_CATEGORY).toString(),
							textPoiCategory, imgiconUpper, imgIconcategory,lineariconlower);

					textSite.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// Intent intent = new Intent(Intent.ACTION_VIEW,
							// Uri
							// .parse(map.get(Constants.POI_WEBSITE)
							// .toString()));
							// context_.startActivity(intent);
							Intent intent = new Intent(context_,
									NewsWebview.class);
							intent.putExtra("newsurl",
									map.get(Constants.POI_WEBSITE).toString());
							context_.startActivity(intent);
						}
					});
					textPhonenumber
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									Intent intent = new Intent(
											Intent.ACTION_DIAL);
									intent.setData(Uri.parse("tel:"
											+ map.get(Constants.POI_PHONE)
													.toString()));
									context_.startActivity(intent);
								}
							});
				}
				textTitle.setTag(map.get(Constants.POI_ID).toString());
			//	textTitle.setOnClickListener(onclickwebopen);
				relImagepoi.setTag(map.get(Constants.POI_ID).toString());
			//	relImagepoi.setOnClickListener(onclickwebopen);

			}
		});
		imgbtnNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(GET_POI_.size()==1)
					return;
				
				if (i < GET_POI_.size()) {
					imgbtnBack.setImageResource(R.drawable.point_arrow_left_on);
					if (i + 1 == GET_POI_.size())
						;
					else {
						imgbtnNext
								.setImageResource(R.drawable.point_arrow_right_on);
						i++;
						if (i + 1 == GET_POI_.size())
							imgbtnNext
									.setImageResource(R.drawable.point_arrow_right_off);
					}
					map = new HashMap<String, String>();
					map = GET_POI_.get(i);
					textPoiCategory.setText(map.get(Constants.POI_CATEGORY)
							.toString());
					// textPoiDistance.setText(String.format("%.2f", Float
					// .parseFloat(map.get(Constants.POI_DISTANCE)
					// .toString())));
					textPoiDistance.setText((int) Float.parseFloat(map.get(
							Constants.POI_DISTANCE).toString())
							+ " km");
					textTitle.setText(map.get(Constants.POI_NAME).toString());
					if (!map.get(Constants.POI_FORMATTEDADDRESS).toString()
							.equals("")) {
						textCountry.setText(map.get(
								Constants.POI_FORMATTEDADDRESS).toString());
						textCountry.setVisibility(View.VISIBLE);
					} else
						textCountry.setVisibility(View.GONE);
					if (!map.get(Constants.POI_WEBSITE).toString().equals("")) {
						textSite.setText(map.get(Constants.POI_WEBSITE)
								.toString());
						textSite.setVisibility(View.VISIBLE);
					} else
						textSite.setVisibility(View.GONE);
					if (!map.get(Constants.POI_LATITUDE).toString().equals("")
							&& !map.get(Constants.POI_LATITUDE).toString()
									.equals("")) {
						textLatLong.setText("LAT "
								+ (Math.round(Float.parseFloat(map.get(
										Constants.POI_LATITUDE).toString()) * 10000.0) / 10000.0)
								+ " | LONG "
								+ (Math.round(Float.parseFloat(map.get(
										Constants.POI_LONGITUDE).toString()) * 10000.0) / 10000.0));
						textLatLong.setVisibility(View.VISIBLE);

					} else
						textLatLong.setVisibility(View.GONE);
					if (!map.get(Constants.POI_PHONE).toString().equals("")) {
						textPhonenumber.setText(map.get(Constants.POI_PHONE)
								.toString());
						textPhonenumber.setVisibility(View.VISIBLE);
					} else
						textPhonenumber.setVisibility(View.GONE);
					if (map.get(Constants.POI_ICONNAME).toString()
							.contains("http"))
						imgLoader.DisplayImage(map.get(Constants.POI_ICONNAME)
								.toString(), R.drawable.shape_radar_center,
								imgPoi, 50);
					else
						imgPoi.setImageResource(R.drawable.shape_radar_center);

					setIconForCat(map.get(Constants.POI_CATEGORY).toString(),
							textPoiCategory, imgiconUpper, imgIconcategory,lineariconlower);

					textSite.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// Intent intent = new Intent(Intent.ACTION_VIEW,
							// Uri
							// .parse(map.get(Constants.POI_WEBSITE)
							// .toString()));
							// context_.startActivity(intent);

							Intent intent = new Intent(context_,
									NewsWebview.class);
							intent.putExtra("newsurl",
									map.get(Constants.POI_WEBSITE).toString());
							context_.startActivity(intent);
						}
					});
					textPhonenumber
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									Intent intent = new Intent(
											Intent.ACTION_DIAL);
									intent.setData(Uri.parse("tel:"
											+ map.get(Constants.POI_PHONE)
													.toString()));
									context_.startActivity(intent);
								}
							});

				}
				textTitle.setTag(map.get(Constants.POI_ID).toString());
				//textTitle.setOnClickListener(onclickwebopen);
				relImagepoi.setTag(map.get(Constants.POI_ID).toString());
				//relImagepoi.setOnClickListener(onclickwebopen);
			}
		});

		return row;
	}

	public void setIconForCat(String category, TextView cattext,
			ImageView uppicon, ImageView imgIconcategory,LinearLayout lineariconlower) {
		// int image_orange=0;
		lineariconlower.setVisibility(View.VISIBLE);
		if (category.equalsIgnoreCase("cultural and art center")) {

			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_cultural_art_orange, 0, 0, 0);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_cultural_art_orange);
			uppicon.setImageResource(R.drawable.point_ico_cultural_art_grey);
		} else if (category.equalsIgnoreCase("education")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_education_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_education_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_education_orange);
			// imgCat1 = [UIImage imageNamed:@"point_ico_education_orange.png");
			// imgCat2 = [UIImage imageNamed:@"point_ico_education_grey.png");

		} else if (category.equalsIgnoreCase("entertainment")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_entertainement_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_entertainement_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_entertainement_orange);
			// imgCat1 = [UIImage
			// imageNamed:@"point_ico_entertainement_orange.png");
			// imgCat2 = [UIImage
			// imageNamed:@"point_ico_entertainement_grey.png");

		} else if (category.equalsIgnoreCase("government institutions")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_institutions_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_institutions_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_institutions_orange);
			// imgCat1 = [UIImage
			// imageNamed:@"point_ico_institutions_orange.png");
			// imgCat2 = [UIImage
			// imageNamed:@"point_ico_institutions_grey.png");

		} else if (category.equalsIgnoreCase("market")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_markets_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_markets_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_markets_orange);
			// imgCat1 = [UIImage imageNamed:@"point_ico_markets_orange.png");
			// imgCat2 = [UIImage imageNamed:@"point_ico_markets_grey.png");

		} else if (category.equalsIgnoreCase("place of worship")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_place_of_worship_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_place_of_worship_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_place_of_worship_orange);
			// imgCat1 = [UIImage
			// imageNamed:@"point_ico_place_of_worship_orange.png");
			// imgCat2 = [UIImage
			// imageNamed:@"point_ico_place_of_worship_grey.png");

		} else if (category.equalsIgnoreCase("restaurant")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_restaurant_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_restaurant_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_restaurant_orange);
			// imgCat1 = [UIImage
			// imageNamed:@"point_ico_restaurant_orange.png");
			// imgCat2 = [UIImage imageNamed:@"point_ico_restaurant_grey.png");

		} else if (category.equalsIgnoreCase("business and services")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_services_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_services_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_services_orange);
			// imgCat1 = [UIImage imageNamed:@"point_ico_services_orange.png");
			// imgCat2 = [UIImage imageNamed:@"point_ico_services_grey.png");

		} else if (category.equalsIgnoreCase("travels")) {
			// cattext.setCompoundDrawablesWithIntrinsicBounds(
			// R.drawable.point_ico_travels_orange, 0, 0, 0);
			uppicon.setImageResource(R.drawable.point_ico_travels_grey);
			imgIconcategory
					.setImageResource(R.drawable.point_ico_travels_orange);
			// imgCat1 = [UIImage imageNamed:@"point_ico_travels_orange.png");
			// imgCat2 = [UIImage imageNamed:@"point_ico_travels_grey.png");

		}else{
			uppicon.setImageResource(R.drawable.international_icon);
			imgIconcategory
					.setImageResource(R.drawable.international_icon);
			lineariconlower.setVisibility(View.GONE);
		}
	}

	OnClickListener onclickwebopen = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Tracker easyTracker = EasyTracker.getInstance(context_);

			// This screen name value will remain set on the tracker and sent with
			// hits until it is set to a new value or to null.
			easyTracker.set(Fields.SCREEN_NAME, "Point Around");

			easyTracker.send(MapBuilder.createAppView().build()); // Add this
																	// method.
			
			Intent intent = new Intent(context_, NewsWebview.class);
			intent.putExtra("newsurl",
					"http://connexun.com/en/Point_of_Interest/" + v.getTag());
			context_.startActivity(intent);
		}
	};

	private Typeface getNormalFont() {
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(context_.getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		return normalFont;

	}
}
