package com.connexun.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.connexun.main.NewsWebview;
import com.connexun.main.R;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ViewpageAddbanner extends PagerAdapter {
	// Declare Variables
	Activity context_;
	ImageLoader imgLoader;
	boolean isExpert;

	ArrayList<HashMap<String, String>> GET_NEWS_ = new ArrayList<HashMap<String, String>>();

	public ViewpageAddbanner(Activity activity,
			ArrayList<HashMap<String, String>> GET_NEWS, boolean isExpert) {
		this.context_ = activity;
		this.GET_NEWS_ = GET_NEWS;
		imgLoader = new ImageLoader(context_);
		this.isExpert = isExpert;

	}

	@Override
	public int getCount() {
		return GET_NEWS_.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.news_add, container, false);

		ImageView banner_add=(ImageView)row.findViewById(R.id.addbanner);
		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_NEWS_.get(position);
		
		final String newsurl = map.get(Constants.NEWS_URL).toString();
		imgLoader.DisplayImage(map.get(Constants.NEWS_PICTURE).toString(),
				R.drawable.logo_text, banner_add, 0);
		banner_add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context_, NewsWebview.class);
				 intent.putExtra("newsurl", newsurl);
				 context_.startActivity(intent);
			}
		});
		
		((ViewPager) container).addView(row);

		return row;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}
	public int getItemPosition(Object object){
	     return POSITION_NONE;
	}
	private Typeface getNormalFont() {
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(context_.getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		return normalFont;

	}

}
