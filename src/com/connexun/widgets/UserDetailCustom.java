package com.connexun.widgets;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.connexun.main.HomeActivity;
import com.connexun.main.R;
import com.connexun.main.UsersDetails;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserDetailCustom extends LinearLayout {
	ImageView imgUserimage, imgInterestCountry, imgCountry;
	TextView textUserName, textUserCountry, textUserBusness, textUserEmail,
			textUserMobileNo, textUserDob, textimgihomecont,
			textimginterstcont;
	ImageButton closeButton;
	ImageLoader imgloader_;
	Context context_;
	Activity activity_;
	HashMap<String, String> hashMap;
	View row;
	LinearLayout linear_cont, linear_bus, linear_email, linear_phone,
			linear_dob;
	AlertDialog alertDialog;

	public UserDetailCustom(Activity context, HashMap<String, String> hashMap_) {
		super(context);
		context_ = context;
		activity_ = context;
		hashMap = hashMap_;
		Tracker easyTracker = EasyTracker.getInstance(context_);

		// This screen name value will remain set on the tracker and sent with
		// hits until it is set to a new value or to null.
		easyTracker.set(Fields.SCREEN_NAME, "People Around User");

		easyTracker.send(MapBuilder.createAppView().build()); // Add this
																// method.
		// TODO Auto-generated constructor stub
	}

	public View GetViewForUserDetail() {
		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		row = inflater.inflate(R.layout.userdetail_layout, null, false);

		imgUserimage = (ImageView) row.findViewById(R.id.imgUserimage);
		imgInterestCountry = (ImageView) row
				.findViewById(R.id.imgInterestCountry);
		imgCountry = (ImageView) row.findViewById(R.id.imgCountry);
		textUserName = (TextView) row.findViewById(R.id.textUserName);
		textUserCountry = (TextView) row.findViewById(R.id.textUserCountry);
		textUserBusness = (TextView) row.findViewById(R.id.textUserBusness);
		textUserEmail = (TextView) row.findViewById(R.id.textUserEmail);
		textUserMobileNo = (TextView) row.findViewById(R.id.textUserMobileNo);
		textUserDob = (TextView) row.findViewById(R.id.textUserDob);
		textimgihomecont = (TextView) row.findViewById(R.id.textimgihomecont);
		textimginterstcont = (TextView) row
				.findViewById(R.id.textimginterstcont);

		linear_cont = (LinearLayout) row.findViewById(R.id.linear_cont);
		linear_bus = (LinearLayout) row.findViewById(R.id.linear_bus);
		linear_email = (LinearLayout) row.findViewById(R.id.linear_email);
		linear_phone = (LinearLayout) row.findViewById(R.id.linear_phone);
		linear_dob = (LinearLayout) row.findViewById(R.id.linear_dob);
		;
		closeButton = (ImageButton) row.findViewById(R.id.closeButton);

		imgloader_ = new ImageLoader(activity_);

//		imgUserimage.setImageBitmap(imgloader_.ConvertBase64ToBitmap(hashMap
//				.get(Constants.PEOPLEAROUND_AVATAR).toString()));
		 imgloader_.DisplayImage(hashMap.get(Constants.PEOPLEAROUND_IMAGE)
		 .toString(), R.drawable.default_avatar_single, imgUserimage, 50);

		Uri uriPeopIntrestCount = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ hashMap.get(Constants.PEOPLEAROUND_INTERESTCOUNT));
		imgInterestCountry.setImageURI(uriPeopIntrestCount);

		Uri uriPeohomeCount = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ hashMap.get(Constants.PEOPLEAROUND_HOMECOUNT));
		imgCountry.setImageURI(uriPeohomeCount);

		textUserName.setText(hashMap.get(Constants.PEOPLEAROUND_FIRSTNAME)
				.toString()
				+ " "
				+ hashMap.get(Constants.PEOPLEAROUND_LASTNAME).toString());

		String userCountry = "";
		int possionCountry = new JsonLocalFileFetch(context_)
				.getPositionCountry(Constants.ARRAY_COUNTRY_NICNAME, hashMap
						.get(Constants.PEOPLEAROUND_HOMECOUNT).toString());
		int possionCountry2 = new JsonLocalFileFetch(context_)
				.getPositionCountry(Constants.ARRAY_COUNTRY_NICNAME, hashMap
						.get(Constants.PEOPLEAROUND_INTERESTCOUNT).toString());
		textUserCountry
				.setText(Constants.ARRAY_COUNTRY_COMPLETENAME[possionCountry]);
		textimgihomecont
				.setText(Constants.ARRAY_COUNTRY_COMPLETENAME[possionCountry]);
		textimginterstcont
				.setText(Constants.ARRAY_COUNTRY_COMPLETENAME[possionCountry2]);
		textUserBusness.setText(hashMap.get(Constants.PEOPLEAROUND_HOMECOUNT)
				.toString());
		// if(hashMap.get(Constants.PEOPLEAROUND_HOMECOUNT)
		// .toString().equals(""))
		linear_bus.setVisibility(View.GONE);

		if (hashMap.get(Constants.PEOPLEAROUND_EMAILYESNO).toString()
				.equalsIgnoreCase("N")) {
			linear_email.setVisibility(View.GONE);
		} else if (hashMap.get(Constants.PEOPLEAROUND_EMAILYESNO).toString()
				.equalsIgnoreCase("Y")) {
			textUserEmail.setVisibility(View.VISIBLE);
			textUserEmail.setText(hashMap.get(Constants.PEOPLEAROUND_EMAIL)
					.toString());
		} else {
			linear_email.setVisibility(View.GONE);
		}

		if (hashMap.get(Constants.PEOPLEAROUND_PHONENUMBERYESNO).toString()
				.equalsIgnoreCase("N")) {
			linear_phone.setVisibility(View.GONE);
		} else if (hashMap.get(Constants.PEOPLEAROUND_PHONENUMBERYESNO)
				.toString().equalsIgnoreCase("Y")) {
			textUserMobileNo.setVisibility(View.VISIBLE);
			textUserMobileNo.setText(hashMap.get(
					Constants.PEOPLEAROUND_PHONENUMBER).toString());
		} else {
			linear_phone.setVisibility(View.GONE);
		}

		if (hashMap.get(Constants.PEOPLEAROUND_DOBYESNO).toString()
				.equalsIgnoreCase("N")) {
			linear_dob.setVisibility(View.GONE);
		} else if (hashMap.get(Constants.PEOPLEAROUND_DOBYESNO).toString()
				.equalsIgnoreCase("Y")) {
			textUserDob.setVisibility(View.VISIBLE);
			textUserDob.setText(hashMap.get(Constants.PEOPLEAROUND_DOB)
					.toString());
		} else {
			linear_dob.setVisibility(View.GONE);
		}

		closeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HomeActivity.sharedInstances.linearEditUserdetail
						.setVisibility(View.GONE);
			}
		});
		textUserMobileNo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectImage(textUserMobileNo.getText().toString());
			}
		});
		textUserEmail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("plain/text");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[] { textUserEmail.getText().toString() });
				intent.putExtra(Intent.EXTRA_SUBJECT, "Connexun");
				//intent.putExtra(Intent.EXTRA_TEXT, "mail body");
				activity_.startActivity(Intent.createChooser(intent, ""));
			}
		});
		return row;

	}

	public void selectImage(String PhoneNumber) {
		AlertDialog.Builder alert = new AlertDialog.Builder(activity_);
		// alert.setTitle("Alert DIalog With EditText"); //Set Alert dialog
		// title here
		alert.setMessage(PhoneNumber); // Message here
		alert.setCancelable(true);
		alert.setPositiveButton(activity_.getResources()
				.getString(R.string.UserDetail_call) + "", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// String value = input.getText().toString();
				// Do something with value!
				// You will get input data in this variable.
				// AlertDialogExample.this.finish();
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:"
						+ textUserMobileNo.getText().toString()));
				activity_.startActivity(intent);

			}
		});
		alert.setNegativeButton(activity_.getResources()
				.getString(R.string.UserDetail_addtocontact) + "",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// String value = input.getText().toString();
						// Do something with value!
						// You will get input data in this variable.
						// AlertDialogExample.this.finish();
//						addContact(activity_, "Sammer", "Delhi", "11-9090909",
//								"Going to Meeting", BitmapFactory
//										.decodeResource(
//												context_.getResources(),
//												R.drawable.ic_launcher));
						Intent intent = new Intent(Intent.ACTION_INSERT);
						intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

						// Just two examples of information you can send to pre-fill out data for the
						// user.  See android.provider.ContactsContract.Intents.Insert for the complete
						// list.
//						Uri imageUri = Uri
//								.parse("android.resource://com.connexun.main/drawable/it");
						//intent.putExtra(ContactsContract.Intents.ATTACH_IMAGE,imageUri);
						
						//intent.setData(imageUri);  
						intent.putExtra(ContactsContract.Intents.Insert.NAME, textUserName.getText().toString());
						intent.putExtra(ContactsContract.Intents.Insert.PHONE,  textUserMobileNo.getText().toString());
						if (hashMap.get(Constants.PEOPLEAROUND_EMAILYESNO).toString()
								.equalsIgnoreCase("Y"))
						intent.putExtra(ContactsContract.Intents.Insert.EMAIL,  textUserEmail.getText().toString());
						//intent.putExtra(ContactsContract.Intents.Insert.PHONE,  textUserMobileNo.getText().toString());
						// Send with it a unique request code, so when you get called back, you can
						// check to make sure it is from the intent you launched (ideally should be
						// some public static final so receiver can check against it)
						int PICK_CONTACT = 100;
						activity_.startActivityForResult(intent, PICK_CONTACT);
					}
				});
		// alert.setNegativeButton("CANCEL", new
		// DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int whichButton) {
		// // Canceled.
		// dialog.cancel();
		// }
		// });
		alertDialog = alert.create();
		alertDialog.show();
	}

	public String addContact(Activity mAcitvity, String name, String address,
			String number, String mNote, Bitmap mPhoto) {

		// http://androidtrainningcenter.blogspot.in/2013/12/android-contact-content-provider-api.html
		int contactID = -1;
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		int rawContactID = ops.size();
		// Adding insert operation to operations list
		// to insert a new raw contact in the table ContactsContract.RawContacts
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.RawContacts.CONTENT_URI)
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
				.withValue(RawContacts.ACCOUNT_NAME, null).build());
		// Adding insert operation to operations list
		// to insert display name in the table ContactsContract.Data
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
						rawContactID)
				.withValue(ContactsContract.Data.MIMETYPE,
						StructuredName.CONTENT_ITEM_TYPE)
				.withValue(StructuredName.DISPLAY_NAME, name).build());
		// Adding insert operation to operations list
		// to insert Mobile Number in the table ContactsContract.Data
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
						rawContactID)
				.withValue(ContactsContract.Data.MIMETYPE,
						Phone.CONTENT_ITEM_TYPE)
				.withValue(Phone.NUMBER, number)
				.withValue(Phone.TYPE, CommonDataKinds.Phone.TYPE_MOBILE)
				.build());
		// Adding insert operation to operations list
		// to insert Mobile Number in the table ContactsContract.Data
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
						rawContactID)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
				.withValue(
						ContactsContract.CommonDataKinds.StructuredPostal.STREET,
						address).build());
		// Adding insert operation to operations list
		// to insert Mobile Number in the table ContactsContract.Data
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
						rawContactID)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Note.NOTE, mNote)
				.build());
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		mPhoto.compress(CompressFormat.JPEG, 100, stream);
		byte[] bytes = stream.toByteArray();
		// Adding insert operation to operations list
		// to insert Mobile Number in the table ContactsContract.Data
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
						rawContactID)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, bytes)
				.build());
		try {
			ContentResolver mResolver = mAcitvity.getContentResolver();
			ContentProviderResult[] mlist = mResolver.applyBatch(
					ContactsContract.AUTHORITY, ops);
			Uri myContactUri = mlist[0].uri;
			int lastSlash = myContactUri.toString().lastIndexOf("/");
			int length = myContactUri.toString().length();
			contactID = Integer.parseInt((String) myContactUri.toString()
					.subSequence(lastSlash + 1, length));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(contactID);
	}
}
