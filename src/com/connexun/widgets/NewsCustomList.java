package com.connexun.widgets;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.connexun.adapters.NewsAdapter;
import com.connexun.main.R;

public class NewsCustomList {

	Context context_;
	Activity activity_;

	public NewsCustomList(Activity context) {

		// TODO Auto-generated constructor stub
		context_ = context;
		activity_ = context;
	}

	public View NewsSingleView(ArrayList<HashMap<String, String>> NEWSLIST,
			int i, int point) {
		View view = null;

//		if (i == point) {
//			view = new NewsAdapter(activity_, NEWSLIST, point).getView(i);
//		}
//		// View a;
//		else
//			view = new NewsAdapter(activity_, NEWSLIST, -1).getView(i);
		return view;
	}

	public View NewsDoubleView(ArrayList<HashMap<String, String>> NEWSLIST,
			int i, int point,int cols,ViewGroup view_) {
		View view = null;
		LinearLayout LL_Inner = new LinearLayout(activity_);
		LinearLayout ll1= new LinearLayout(activity_);
		LL_Inner.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams layoutForInner = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LL_Inner.setLayoutParams(layoutForInner);
		 LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
		    lp.weight = 1;
		    ll1.setLayoutParams(lp);
		    //ll2.setLayoutParams(lp);
		for (int x = i*cols; x < (i*cols)+cols; x++) {
			
				ll1= new LinearLayout(activity_);
				ll1.setLayoutParams(lp);
				if (x == point) {
					ll1.addView(new NewsAdapter(activity_, NEWSLIST, point)
							.getView(x,view_));
					ll1.setTag(x);
					LL_Inner.addView(ll1);
				}
				// View a;
				else{
					ll1.addView(new NewsAdapter(activity_, NEWSLIST, -1)
					.getView(x,view_));
					ll1.setTag(x);
					LL_Inner.addView(ll1);
				
				}
				
			
		}
		return LL_Inner;
	}

	public View NewsTripleView(ArrayList<HashMap<String, String>> NEWSLIST,
			int i, int point) {
		// View view = null;
				LinearLayout LL_Inner = new LinearLayout(activity_);
//				LL_Inner.setOrientation(LinearLayout.HORIZONTAL);
//				LinearLayout.LayoutParams layoutForInner = new LinearLayout.LayoutParams(
//						LinearLayout.LayoutParams.MATCH_PARENT,
//						LinearLayout.LayoutParams.WRAP_CONTENT);
//				LL_Inner.setLayoutParams(layoutForInner);
//				for (int x = 0; x < 3; x++) {
//					if (i == point) {
//						LL_Inner.addView(new NewsAdapter(activity_, NEWSLIST, point)
//								.getView(i));
//					}
//					// View a;
//					else
//						LL_Inner.addView(new NewsAdapter(activity_, NEWSLIST, -1)
//								.getView(i));
//				}
				return LL_Inner;
	}

}
