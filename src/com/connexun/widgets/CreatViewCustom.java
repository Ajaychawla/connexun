package com.connexun.widgets;

import java.util.ArrayList;
import java.util.HashMap;

import com.connexun.main.EditIntLangActivity;
import com.connexun.main.HomeActivity;
import com.connexun.main.PeopleAroundList;
import com.connexun.main.R;
import com.connexun.main.UsersDetails;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CreatViewCustom extends LinearLayout {

	Context context_;
	Activity activity_;
	public static int ImgSize = 0;
	public static float innerCircle = 0;
	public static float outerCircle = 0;
	ImageView radius_outside, radius_middle, radius_center;
	FrameLayout main;
	ArrayList<HashMap<String, String>> PEOPLE_AROUND_ = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> PEOPLE_AROUND_INNER = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> PEOPLE_AROUND_OUTER = new ArrayList<HashMap<String, String>>();

	ImageLoader imgloader;
	int paddinImageview = 0;

	@SuppressLint("NewApi")
	public CreatViewCustom(Activity context,
			ArrayList<HashMap<String, String>> PEOPLE_AROUND) {
		super(context);
		this.context_ = context;
		this.activity_ = context;
		this.PEOPLE_AROUND_ = PEOPLE_AROUND;
		imgloader = new ImageLoader(activity_);
		SeprationCircleByDistance();
		paddinImageview = (int) getResources().getDimension(R.dimen.dp2);
		init();

	}

	// public CreateCirvleView(Context context, AttributeSet attrs) {
	// super(context,attrs);
	// this.context_ = context;
	// if(attrs!=null)
	// init();
	//
	// }

	public class MyView extends View {

		String color = "";
		int Size = 0;

		public MyView(Context context, String color, int Size) {
			super(context);
			this.color = color;
			this.Size = Size;
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			int x = getWidth();
			int y = getHeight();
			int radius;
			radius = Size;
			Paint paint = new Paint();
			paint.setStyle(Paint.Style.FILL);
			paint.setColor(Color.TRANSPARENT);
			canvas.drawPaint(paint);
			// Use Color.parseColor to define HTML colors
			paint.setColor(Color.parseColor(color));
			canvas.drawCircle(x / 2, y / 2, radius, paint);
		}
	}

	@SuppressLint("NewApi")
	public void init() {

		ImgSize = (int) getResources().getDimension(R.dimen.imgSize);
		innerCircle = getResources().getDimension(R.dimen.innerCircle);
		outerCircle = getResources().getDimension(R.dimen.outerCircle);

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View row = inflater.inflate(R.layout.radar_layout, null, false);
		main = (FrameLayout) row.findViewById(R.id.main);
		radius_outside = (ImageView) row.findViewById(R.id.radius_outside);
		radius_middle = (ImageView) row.findViewById(R.id.radius_middle);
		radius_center = (ImageView) row.findViewById(R.id.radius_center);
		TextView peopleAroundtitle=(TextView) row.findViewById(R.id.peopleAroundtitle);
		Typeface normalFont = null;
		if (normalFont == null) {
			normalFont = Typeface.createFromAsset(context_.getAssets(),
					"fonts/mic32bold-webfont.ttf");
		}
		peopleAroundtitle.setTypeface(normalFont);
		FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(
				((int) innerCircle - ImgSize * 2 / 3) * 2,
				((int) innerCircle - ImgSize * 2 / 3) * 2);

		lp2.gravity = Gravity.CENTER;
		FrameLayout.LayoutParams lp3 = new FrameLayout.LayoutParams(
				((int) innerCircle + ImgSize) * 2,
				((int) innerCircle + ImgSize) * 2);

		lp3.gravity = Gravity.CENTER;
		FrameLayout.LayoutParams lp4 = new FrameLayout.LayoutParams(
				((int) outerCircle + ImgSize * 2 / 3) * 2,
				((int) outerCircle + ImgSize * 2 / 3) * 2);

		lp4.gravity = Gravity.CENTER;
		// Set layout params on view.
		// radius_outside.setImageResource(R.drawable.shape_radar_outside);
		// radius_middle.setImageResource(R.drawable.shape_radar_middle);
		// radius_center.setImageResource(R.drawable.shape_radar_center);

		radius_outside.setLayoutParams(lp4);
		radius_middle.setLayoutParams(lp3);
		radius_center.setLayoutParams(lp2);
		//setLayoutParams(lp4);
		// TODO Auto-generated constructor stub
		// addView(new MyView(context_, "#ebebeb", (int) outerCircle + ImgSize *
		// 2
		// / 3));
		// addView(new MyView(context_, "#e1e1e1", (int) innerCircle + ImgSize *
		// 2
		// / 3));
		// addView(new MyView(context_, "#ff6733", (int) innerCircle - ImgSize *
		// 2
		// / 3));
		int numViews = PEOPLE_AROUND_INNER.size();
		if (numViews > 5) {
			numViews = 5;
			for (int i = 0; i < numViews; i++) {
				if (i == 0)
					addPlusButton(i, numViews, innerCircle,
							PEOPLE_AROUND_INNER, "in");
				else
					ViewCircle(i, numViews, innerCircle, PEOPLE_AROUND_INNER);
			}
		} else {
			for (int i = 0; i < numViews; i++) {
				ViewCircle(i, numViews, innerCircle, PEOPLE_AROUND_INNER);
			}
		}

		// int numViews2 = PEOPLE_AROUND_OUTER.size();
		// if (numViews2 > 8)
		// numViews2 = 8;

		int numViews2 = PEOPLE_AROUND_OUTER.size();
		if (numViews2 > 8) {
			numViews2 = 8;
			for (int i = 0; i < numViews2; i++) {
				if (i == 0)
					addPlusButton(i, numViews2, outerCircle,
							PEOPLE_AROUND_OUTER, "out");
				else
					ViewCircle(i, numViews2, outerCircle, PEOPLE_AROUND_OUTER);
			}
		} else {
			for (int i = 0; i < numViews2; i++) {
				ViewCircle(i, numViews2, outerCircle, PEOPLE_AROUND_OUTER);
			}
		}
		// for (int i = 0; i < numViews2; i++) {
		// // Create some quick TextViews that can be placed.
		//
		// ViewHolder view = new ViewHolder();
		// view.v = new ImageView(context_);
		// imgloader.DisplayImage(
		// PEOPLE_AROUND_OUTER.get(i)
		// .get(Constants.PEOPLEAROUND_IMAGE),
		// R.drawable.user_avatar_default, view.v, 100);
		// // view.v.setImageResource(R.drawable.br);
		// // TextView v = new TextView(this);
		// // Set a text and center it in each view.
		// // v.setText("View " + i);
		// // v.setGravity(Gravity.CENTER);
		// // view.v.setBackgroundColor(0xffff0000);
		// // Force the views to a nice size (150x100 px) that fits my display.
		// // This should of course be done in a display size independent way.
		// FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ImgSize,
		// ImgSize);
		// // Place all views in the center of the layout. We'll transform them
		// // away from there in the code below.
		// lp.gravity = Gravity.CENTER;
		// // Set layout params on view.
		// // view.v.setPadding(5, 5, 5, 5);
		// view.v.setLayoutParams(lp);
		// view.v.setBackgroundResource(R.drawable.shape_radar_center);
		//
		// // Calculate the angle of the current view. Adjust by 90 degrees to
		// // get View 0 at the top. We need the angle in degrees and radians.
		// float angleDeg = i * 360.0f / numViews2 - 90.0f;
		// float angleRad = (float) (angleDeg * Math.PI / 180.0f);
		// // Calculate the position of the view, offset from center (300 px
		// // from
		// // center). Again, this should be done in a display size independent
		// // way.
		//
		// view.v.setTranslationX(outerCircle * (float) Math.cos(angleRad));
		// view.v.setTranslationY(outerCircle * (float) Math.sin(angleRad));
		//
		// // Set the rotation of the view.
		// // view.v.setRotation(angleDeg + 90.0f);
		// view.v.setPadding(paddinImageview, paddinImageview,
		// paddinImageview, paddinImageview);
		// view.v.setTag(i);
		// view.v.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// // Toast.makeText(context_, v.getTag().toString(),
		// // Toast.LENGTH_SHORT).show();
		// HashMap<String, String> hashMap = PEOPLE_AROUND_OUTER
		// .get(Integer.parseInt(v.getTag().toString()));
		// // Intent intent = new Intent(context_, UsersDetails.class);
		// // intent.putExtra("hashMap", hashMap);
		// // context_.startActivity(intent);
		//
		// HomeActivity.sharedInstances.linearEditUserdetail
		// .removeAllViews();
		// HomeActivity.sharedInstances.linearEditUserdetail
		// .setVisibility(View.VISIBLE);
		// HomeActivity.sharedInstances.linearEditUserdetail
		// .addView(new UserDetailCustom(context_, hashMap)
		// .GetViewForUserDetail());
		// }
		// });
		// main.addView(view.v);
		// }

		main.addView(CenterViewCreate());
		addView(row);
	}

	public View CenterViewCreate() {
		ViewHolder view = new ViewHolder();
		view.v = new ImageView(context_);
		if (HomeActivity.sharedInstances.GET_USERDETAILS.size() > 0)
			 imgloader.DisplayImage(HomeActivity.sharedInstances.GET_USERDETAILS
			 .get(0).get(Constants.USER_IMAGENAME),
			 R.drawable.default_avatar_single, view.v, 100);
//			view.v.setImageBitmap(imgloader
//					.ConvertBase64ToBitmap(HomeActivity.sharedInstances.GET_USERDETAILS
//							.get(0).get(Constants.USER_AVATAR).toString()));
		else
			view.v.setImageBitmap(imgloader.ConvertBase64ToBitmap(""));

		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ImgSize,
				ImgSize);

		lp.gravity = Gravity.CENTER;

		view.v.setLayoutParams(lp);
		view.v.setBackgroundResource(R.drawable.shape_radar_outside);
		view.v.setPadding(paddinImageview, paddinImageview, paddinImageview,
				paddinImageview);
		view.v.setTag("center view");
		view.v.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (HomeActivity.sharedInstances.IsEditProfileClickable) {
					// Intent i = new Intent(context_,
					// EditIntLangActivity.class);
					// i.putExtra("GET_USERDETAILS",
					// HomeActivity.sharedInstances.GET_USERDETAILS);
					// context_.startActivity(i);
					HomeActivity.sharedInstances.account.performClick();
				}
				// Toast.makeText(context_, v.getTag().toString(),
				// Toast.LENGTH_SHORT).show();
			}
		});
		return view.v;
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// RelativeLayout.LayoutParams lp = new
		// RelativeLayout.LayoutParams(widthMeasureSpec,heightMeasureSpec);
		// setMeasuredDimension(300, 300);
		// init();
	}

	public void SeprationCircleByDistance() {

		for (int i = 0; i < PEOPLE_AROUND_.size(); i++) {

			if (Float.parseFloat(PEOPLE_AROUND_.get(i).get(
					Constants.PEOPLEAROUND_DISTANCE)) <= 30) {
				PEOPLE_AROUND_INNER.add(PEOPLE_AROUND_.get(i));
			} else {
				PEOPLE_AROUND_OUTER.add(PEOPLE_AROUND_.get(i));
			}
		}

	}

	public String getPeopleCount() {
		// 5 persons &lt; 30km\n9 persons &lt; 50km
		return PEOPLE_AROUND_INNER.size() + " "
				+ context_.getResources().getString(R.string.PA_poeple)
				+ " < 30km\n" + PEOPLE_AROUND_OUTER.size() + " "
				+ context_.getResources().getString(R.string.PA_poeple)
				+ " < 50km";
	}

	public void addPlusButton(int i, int numViews2, float Circle,
			final ArrayList<HashMap<String, String>> PEOPLE_AROUND_ARRAY,
			final String filter) {

		ViewHolder view = new ViewHolder();
		view.v = new ImageView(context_);
		view.v.setImageResource(R.drawable.people_around_plus_button);
		// TextView v = new TextView(this);
		// Set a text and center it in each view.
		// v.setText("View " + i);
		// v.setGravity(Gravity.CENTER);
		// view.v.setBackgroundColor(0xffff0000);
		// Force the views to a nice size (150x100 px) that fits my display.
		// This should of course be done in a display size independent way.
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ImgSize,
				ImgSize);
		// Place all views in the center of the layout. We'll transform them
		// away from there in the code below.
		lp.gravity = Gravity.CENTER;
		// Set layout params on view.
		// view.v.setPadding(5, 5, 5, 5);
		view.v.setLayoutParams(lp);
		view.v.setBackgroundResource(R.drawable.shape_radar_center);

		// Calculate the angle of the current view. Adjust by 90 degrees to
		// get View 0 at the top. We need the angle in degrees and radians.
		float angleDeg = i * 360.0f / numViews2 - 180.0f;
		float angleRad = (float) (angleDeg * Math.PI / 180.0f);
		// Calculate the position of the view, offset from center (300 px
		// from
		// center). Again, this should be done in a display size independent
		// way.

		view.v.setTranslationX(Circle * (float) Math.cos(angleRad));
		view.v.setTranslationY(Circle * (float) Math.sin(angleRad));

		// Set the rotation of the view.
		// view.v.setRotation(angleDeg + 90.0f);
		view.v.setPadding(paddinImageview, paddinImageview, paddinImageview,
				paddinImageview);
		view.v.setTag(i);
		view.v.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(context_, v.getTag().toString(),
				// Toast.LENGTH_SHORT).show();
				// HashMap<String, String> hashMap = PEOPLE_AROUND_ARRAY
				// .get(Integer.parseInt(v.getTag().toString()));
				// Intent intent = new Intent(context_, UsersDetails.class);
				// intent.putExtra("hashMap", hashMap);
				// context_.startActivity(intent);
				Intent intent = new Intent(context_, PeopleAroundList.class);
				// intent.putExtra("PEOPLEAROUND_ARRAY", PEOPLE_AROUND_ARRAY);
				intent.putExtra("filter", filter);
				activity_.startActivity(intent);

			}
		});
		main.addView(view.v);
	}

	public void ViewCircle(int i, int numViews, float Circle,
			final ArrayList<HashMap<String, String>> PEOPLE_AROUND_ARRAY) {

		// Create some quick TextViews that can be placed.
		ViewHolder view = new ViewHolder();
		view.v = new ImageView(context_);
		// view.v.setImageBitmap(imgloader.ConvertBase64ToBitmap(PEOPLE_AROUND_ARRAY.get(i).get(Constants.PEOPLEAROUND_AVATAR)
		// .toString()));
		imgloader.DisplayImage(
				PEOPLE_AROUND_ARRAY.get(i).get(Constants.PEOPLEAROUND_IMAGE),
				R.drawable.default_avatar_single, view.v, 100);
		// view.v.setImageResource(R.drawable.br);
		// TextView v = new TextView(this);
		// Set a text and center it in each view.
		// v.setText("View " + i);
		// v.setGravity(Gravity.CENTER);
		// view.v.setBackgroundColor(0xffff0000);
		// Force the views to a nice size (150x100 px) that fits my display.
		// This should of course be done in a display size independent way.
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ImgSize,
				ImgSize);
		// Place all views in the center of the layout. We'll transform them
		// away from there in the code below.
		lp.gravity = Gravity.CENTER;

		// Set layout params on view.

		view.v.setLayoutParams(lp);
		view.v.setBackgroundResource(R.drawable.shape_radar_center);

		// Calculate the angle of the current view. Adjust by 90 degrees to
		// get View 0 at the top. We need the angle in degrees and radians.
		float angleDeg = i * 360.0f / numViews - 180.0f;
		float angleRad = (float) (angleDeg * Math.PI / 180.0f);
		// Calculate the position of the view, offset from center (300 px
		// from
		// center). Again, this should be done in a display size independent
		// way.

		view.v.setTranslationX(Circle * (float) Math.cos(angleRad));
		view.v.setTranslationY(Circle * (float) Math.sin(angleRad));

		// Set the rotation of the view.
		// view.v.setRotation(angleDeg + 90.0f);
		view.v.setPadding(paddinImageview, paddinImageview, paddinImageview,
				paddinImageview);
		view.v.setTag(i);
		view.v.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(context_, v.getTag().toString(),
				// Toast.LENGTH_LONG).show();
				HashMap<String, String> hashMap = PEOPLE_AROUND_ARRAY
						.get(Integer.parseInt(v.getTag().toString()));
				// Intent intent = new Intent(context_, UsersDetails.class);
				// intent.putExtra("hashMap", hashMap);
				// context_.startActivity(intent);
				HomeActivity.sharedInstances.linearEditUserdetail
						.removeAllViews();
				HomeActivity.sharedInstances.linearEditUserdetail
						.setVisibility(View.VISIBLE);
				HomeActivity.sharedInstances.linearEditUserdetail
						.addView(new UserDetailCustom(activity_, hashMap)
								.GetViewForUserDetail());

			}
		});
		main.addView(view.v);

	}

	
}
