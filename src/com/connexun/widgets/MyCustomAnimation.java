package com.connexun.widgets;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.connexun.main.R;

public class MyCustomAnimation extends Animation {

	public final static int COLLAPSE = 1;
	public final static int EXPAND = 0;

	private View mView;
	private int mEndHeight;
	private int mType;
	private LinearLayout.LayoutParams mLayoutParams;
	public final static int duration_ = 200;

	public MyCustomAnimation() {
		// TODO Auto-generated constructor stub
	}
	public MyCustomAnimation(View view, int duration, int type) {

		setDuration(duration);
		mView = view;
		mEndHeight = mView.getHeight();
		mLayoutParams = ((LinearLayout.LayoutParams) view.getLayoutParams());
		mType = type;
		if (mType == EXPAND) {
			mLayoutParams.height = 0;
		} else {
			mLayoutParams.height = LayoutParams.WRAP_CONTENT;
		}
		view.setVisibility(View.VISIBLE);
	}

	public int getHeight() {
		return mView.getHeight();
	}

	public void setHeight(int height) {
		mEndHeight = height;
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {

		super.applyTransformation(interpolatedTime, t);
		if (interpolatedTime < 1.0f) {
			if (mType == EXPAND) {
				mLayoutParams.height = (int) (mEndHeight * interpolatedTime);
			} else {
				mLayoutParams.height = (int) (mEndHeight * (1 - interpolatedTime));
			}
			mView.requestLayout();
		} else {
			if (mType == EXPAND) {
				mLayoutParams.height = LayoutParams.WRAP_CONTENT;
				mView.requestLayout();
			} else {
				mView.setVisibility(View.GONE);
			}
		}
	}

	public void SlideUP(final View view, Context context_) {
		Animation anim_slideup = AnimationUtils.loadAnimation(context_,
				R.anim.anim_slideup);
		anim_slideup.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				// Called when the Animation starts
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// Called when the Animation ended
				// Since we are fading a View out we set the visibility
				// to GONE once the Animation is finished
				view.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// This is called each time the Animation repeats
			}
		});
		view.startAnimation(anim_slideup);
	}

	public void SlideDOWN(final View view, Context context_) {
		Animation anim_slidedown = AnimationUtils.loadAnimation(context_,
				R.anim.anim_slidedown);
		anim_slidedown.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				// Called when the Animation starts
				view.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// Called when the Animation ended
				// Since we are fading a View out we set the visibility
				// to GONE once the Animation is finished
				
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// This is called each time the Animation repeats
			}
		});
		view.startAnimation(anim_slidedown);
	}
	
	public static void expand(final View v) {
	    v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	    final int targetHeight = v.getMeasuredHeight();

	    v.getLayoutParams().height = 0;
	    v.setVisibility(View.VISIBLE);
	    Animation a = new Animation()
	    {
	        @Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	            v.getLayoutParams().height = interpolatedTime == 1
	                    ? LayoutParams.WRAP_CONTENT
	                    : (int)(targetHeight * interpolatedTime);
	            v.requestLayout();
	        }

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };

	    // 1dp/ms
	    a.setDuration(duration_);
	    v.startAnimation(a);
	}

	public static void collapse(final View v) {
	    final int initialHeight = v.getMeasuredHeight();

	    Animation a = new Animation()
	    {
	        @Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	            if(interpolatedTime == 1){
	                v.setVisibility(View.GONE);
	            }else{
	                v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
	                v.requestLayout();
	            }
	        }

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };

	    // 1dp/ms
	    a.setDuration(duration_);
	    v.startAnimation(a);
	}
}