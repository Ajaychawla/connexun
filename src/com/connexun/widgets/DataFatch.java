package com.connexun.widgets;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DataFatch {

	public class ForgotPassOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;

		String user_id = "";
		String apitoken = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			try {
				// Set Request parameter
				user_id += "&" + URLEncoder.encode("user_id", "UTF-8") + "=";

				apitoken += "&" + URLEncoder.encode("apitoken", "UTF-8")
						+ "=4ssiw3B29F4BD5I0iPkSE4BD41jq29F4";

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(user_id);
				wr.write(apitoken);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);

					Log.wtf("error1", Content);

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {

					}
					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("email is not registred")) {

					}
					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("ok")) {

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

}
