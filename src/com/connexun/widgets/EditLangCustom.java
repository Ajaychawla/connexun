package com.connexun.widgets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connexun.adapters.SpinnerAdapters;
import com.connexun.adapters.spinnerInterest;
import com.connexun.main.EditIntLangActivity;
import com.connexun.main.EditProfileActivity;
import com.connexun.main.HomeActivity;
import com.connexun.main.LoginActivity;
import com.connexun.main.R;
import com.connexun.main.Register2Activity;
import com.connexun.main.RegisterActivity;
import com.connexun.main.EditIntLangActivity.EditLangOpration;

import com.connexun.utils.AlertDialogManager;
import com.connexun.utils.ChangeLanguage;
import com.connexun.utils.ConnectionDetector;
import com.connexun.utils.ConnectivityServer;
import com.connexun.utils.Constants;
import com.connexun.utils.ImageLoader;
import com.connexun.utils.JsonLocalFileFetch;
import com.connexun.utils.SessionManager;
import com.connexun.utils.SessionManager_email;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EditLangCustom extends LinearLayout {
	SessionManager session;
	SessionManager_email session_email;

	public static ArrayList<HashMap<String, String>> GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
	ImageView imgUserimage, Img_editimage;
	TextView textUsername, textUserCountry, textUserBussiness,
			textUserInterestedCountry, textUserLanguage;
	ImageLoader imgloader;
	ImageButton imgbtnEditCompanyinterst, imgbtnEditlanguage;
	Spinner spinnerInterestCountry, spinnerLanguage;
	ImageView imgCountryIcon;

	ArrayList<HashMap<String, String>> GET_COUNTRYLIST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_ORIGIN = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> GET_COUNTRYLIST_LANGUAGE = new ArrayList<HashMap<String, String>>();
	public static String COUNTRYCODE = "countrycode";
	public static String ID = "id";
	public static String RELATED = "related";
	public static String COUNTRYNAME = "countryname";
	public static String CURRENCYCODE = "currencycode";
	public static String CURRENCYNAME = "currencyname";
	public static String LATITUDE = "latitude";
	public static String LONGITUDE = "longitude";
	public static String WOEID = "woeid";
	public static String LANGUAGE = "Laguages";
	Integer[] interestCountry_image = { R.drawable.interest_icon,
			R.drawable.br, R.drawable.ca, R.drawable.cn, R.drawable.it,
			R.drawable.in };
	String[] Language_list = { "Select Language", "English", "Italian" };
	String[] Language_list_NICNAME = { "Select Language", "en", "it" };
	String[] CountryCode_List = {};
	AlertDialogManager alert = new AlertDialogManager();
	String CompleteURL = "";
	ImageButton imgBtnLangSave;
	Context context_;
	Activity activity_;
	View row;
	String langcountcode = "";
	int valueofLang = 1;

	public EditLangCustom(Context context,
			ArrayList<HashMap<String, String>> GET_USERDETAILS_,
			Activity acitvity) {
		super(context);
		context_ = context;
		GET_USERDETAILS = GET_USERDETAILS_;
		activity_ = acitvity;
		// languageChangeView();
		
		
		// TODO Auto-generated constructor stub
	}

	public View languageChangeView() {

		LayoutInflater inflater = (LayoutInflater) context_
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		row = inflater.inflate(R.layout.edit_language_layout, null, false);
		session = new SessionManager(context_);
		session.checkLogin();

		session_email = new SessionManager_email(context_);

		// GET_USERDETAILS = (ArrayList<HashMap<String, String>>) getIntent()
		// .getSerializableExtra("GET_USERDETAILS");
		imgloader = new ImageLoader(context_);

		imgUserimage = (ImageView) row.findViewById(R.id.imgUserimage);
		Img_editimage = (ImageView) row.findViewById(R.id.Img_editimage);

		textUsername = (TextView) row.findViewById(R.id.textUsername);
		textUserCountry = (TextView) row.findViewById(R.id.textUserCountry);
		textUserBussiness = (TextView) row.findViewById(R.id.textUserBussiness);
		textUserInterestedCountry = (TextView) row
				.findViewById(R.id.textUserInterestedCountry);
		textUserLanguage = (TextView) row.findViewById(R.id.textUserLanguage);
		imgbtnEditCompanyinterst = (ImageButton) row
				.findViewById(R.id.imgbtnEditCompanyinterst);
		imgbtnEditlanguage = (ImageButton) row
				.findViewById(R.id.imgbtnEditlanguage);
		spinnerInterestCountry = (Spinner) row
				.findViewById(R.id.spinnerInterestCountry);

		spinnerLanguage = (Spinner) row.findViewById(R.id.spinnerLanguage);
		imgCountryIcon = (ImageView) row.findViewById(R.id.imgCountryIcon);
		imgBtnLangSave = (ImageButton) row.findViewById(R.id.imgbtnLangSave);
		imgBtnLangSave.setVisibility(View.INVISIBLE);
		getPosition();
		createLanguageSpinner(new JsonLocalFileFetch(context_)
				.getArraylistposio(GET_COUNTRYLIST,
						GET_USERDETAILS.get(0)
								.get(Constants.USER_INTERESTCOUNT).toString()));
		// spinnerLanguage.setAdapter(adapter)
		spinnerLanguage
				.setAdapter(new spinnerInterest(context_,
						R.layout.spinner_country, interestCountry_image,
						Language_list));
		spinnerInterestCountry.setAdapter(new SpinnerAdapters(activity_,
				GET_COUNTRYLIST_INTEREST));

		// imgUserimage.setImageBitmap(imgloader
		// .ConvertBase64ToBitmap(GET_USERDETAILS.get(0)
		// .get(Constants.USER_AVATAR).toString()));
		imgloader
				.DisplayImage(
						GET_USERDETAILS.get(0).get(Constants.USER_IMAGENAME)
								.toString(), R.drawable.default_avatar_single,
						imgUserimage, 100);
		textUsername.setText(GET_USERDETAILS.get(0)
				.get(Constants.USER_FIRSTNAME).toString()
				+ " "
				+ GET_USERDETAILS.get(0).get(Constants.USER_LASTNAME)
						.toString());
		textUserCountry
				.setText(Constants.ARRAY_COUNTRY_COMPLETENAME[new JsonLocalFileFetch(
						context_).getPositionCountry(
						Constants.ARRAY_COUNTRY_NICNAME, GET_USERDETAILS.get(0)
								.get(Constants.USER_HOMECOUNT).toString())]);
		if (GET_USERDETAILS.get(0).get(Constants.USER_JOBPOSITION).toString()
				.equals("")
				&& GET_USERDETAILS.get(0).get(Constants.USER_COMPANY)
						.toString().equals(""))
			textUserBussiness.setVisibility(View.GONE);
		else {
			if (new ChangeLanguage().getlanguage().equals("hi")) {
				textUserBussiness.setText(GET_USERDETAILS.get(0)
						.get(Constants.USER_COMPANY).toString()
						+ " "
						+ context_.getResources().getString(R.string.EL_at)
						+ " "
						+ GET_USERDETAILS.get(0)
								.get(Constants.USER_JOBPOSITION).toString());
			} else
				textUserBussiness.setText(GET_USERDETAILS.get(0)
						.get(Constants.USER_JOBPOSITION).toString()
						+ " "
						+ context_.getResources().getString(R.string.EL_at)
						+ " "
						+ GET_USERDETAILS.get(0).get(Constants.USER_COMPANY)
								.toString());
		}

		textUserInterestedCountry
				.setText(""
						+ Constants.ARRAY_COUNTRY_COMPLETENAME[new JsonLocalFileFetch(
								context_).getPositionCountry(
								Constants.ARRAY_COUNTRY_NICNAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_INTERESTCOUNT)
										.toString())]);
		Uri otherPath = Uri
				.parse("android.resource://com.connexun.main/drawable/"
						+ GET_USERDETAILS.get(0)
								.get(Constants.USER_INTERESTCOUNT).toString());
		imgCountryIcon.setImageURI(otherPath);
		// textUserLanguage.setText("Language: "+GET_USERDETAILS.get(0)
		// .get(Constants.USER_LANGUAGE).toString());

		String lang = "";
		if (GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE).toString()
				.equalsIgnoreCase("en"))
			lang = Language_list[1];
		if (GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE).toString()
				.equalsIgnoreCase("it"))
			lang = Language_list[2];

		// spinnerInterestCountry.setse
		// TextView logout = (TextView) findViewById(R.id.logout);
		//
		//
		// logout.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// Intent i = new Intent(HomeActivity.this,
		// RegisterLoginActivity.class);
		// HomeActivity.this.startActivity(i);
		// }
		// });

		ImageButton editProfileButton = (ImageButton) row
				.findViewById(R.id.editProfileButton);

		editProfileButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Intent i = new Intent(context_, EditProfileActivity.class);
				// i.putExtra("GET_USERDETAILS", GET_USERDETAILS);
				// context_.startActivityForResult(i, 1);
				HomeActivity.sharedInstances
						.StartEditProfileActivit(GET_USERDETAILS);
				// session.logoutUser();
				// finish();
				// row.setVisibility(View.GONE);

			}
		});

		ImageButton closeButton = (ImageButton) row
				.findViewById(R.id.closeButton);

		closeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				row.setVisibility(View.GONE);
				// finish();
			}
		});
		textUserInterestedCountry
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						spinnerInterestCountry.performClick();
					}
				});
		textUserLanguage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerLanguage.performClick();
			}
		});
		imgbtnEditCompanyinterst.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerInterestCountry.performClick();
			}
		});
		imgbtnEditlanguage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinnerLanguage.performClick();
			}
		});
		spinnerInterestCountry
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						// Toast.makeText(EditIntLangActivity.this,
						// "Positions" + position, Toast.LENGTH_LONG)
						// .show();
						if (position != 0) {
							valueofLang = 1;
							textUserInterestedCountry
									.setText(GET_COUNTRYLIST_INTEREST
											.get(position).get(COUNTRYNAME)
											.toString());
							Uri otherPath = Uri
									.parse("android.resource://com.connexun.main/drawable/"
											+ GET_COUNTRYLIST_INTEREST.get(
													position).get(COUNTRYCODE));
							textUserInterestedCountry
									.setTag(GET_COUNTRYLIST_INTEREST.get(
											position).get(COUNTRYCODE));
							imgCountryIcon.setImageURI(otherPath);
							createLanguageSpinner(new JsonLocalFileFetch(
									context_).getArraylistposio(
									GET_COUNTRYLIST, GET_COUNTRYLIST_INTEREST
											.get(position).get(COUNTRYCODE)));

							spinnerLanguage.setAdapter(new spinnerInterest(
									context_, R.layout.spinner_country,
									interestCountry_image, Language_list));
//							Toast.makeText(context_, valueofLang + "--",
//									Toast.LENGTH_LONG).show();
							spinnerLanguage.setSelection(valueofLang);
							imgBtnLangSave.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		spinnerLanguage
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						// Toast.makeText(context_,
						// CountryCode_List[position] + "", Toast.LENGTH_LONG)
						// .show();
						if (position != 0) {
							langcountcode = CountryCode_List[position];
							textUserLanguage.setText(""
									+ Language_list[position].substring(0,
											Language_list[position]
													.indexOf("(")));
							textUserLanguage
									.setTag(Language_list_NICNAME[position]);
							imgBtnLangSave.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		try {

			CompleteURL = session.getUserDetails().get(
					SessionManager.KEY_USERID)
					+ "/"
					+ URLEncoder.encode(
							session.getUserDetails().get(
									SessionManager.KEY_USERTOKEN), "UTF-8");
		} catch (Exception e) {
			// TODO: handle exception
		}

		imgBtnLangSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!HomeActivity.sharedInstances.base64.equals("")) {
					imgloader.clearSingleCache(GET_USERDETAILS.get(0)
							.get(Constants.USER_IMAGENAME).toString());
				}

				if (new ConnectionDetector(context_).isConnectingToInternet()) {
					new EditLangOpration()
							.execute(Constants.LANGUAGE_INTERESTCHANGE
									+ CompleteURL);
					EasyTracker easyTracker = EasyTracker.getInstance(context_);

					// MapBuilder.createEvent().build() returns a Map of event
					// fields and values
					// that are set and sent with the hit.
					easyTracker.send(MapBuilder.createEvent("Change Interest", // Event
																				// category
																				// (required)
							"Change Interest", // Event action (required)
							"Change Interest", // Event label
							null) // Event value
							.build());
				} else {
					try {
						alert.showAlertDialog(
								context_,
								context_.getResources().getString(
										R.string.Alert_Internet_connection)
										+ "",
								context_.getResources().getString(
										R.string.Alert_Internet)
										+ "", false);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				Log.e("language", textUserLanguage.getTag() + "--");

			}
		});
		// spinnerInterestCountry.setSelection(new
		// JsonLocalFileFetch(context_).getArraylistposio(GET_COUNTRYLIST,
		// GET_USERDETAILS.get(0)
		// .get(Constants.USER_INTERESTCOUNT)
		// .toString()));
		textUserLanguage.setText(""
				+ Constants.ARRAY_COUNTRY_LANGUAGE[new JsonLocalFileFetch(
						context_).getPositionLanguage(
						Constants.ARRAY_COUNTRY_LANGUAGE_NICNAME,
						GET_USERDETAILS.get(0).get(Constants.USER_LANGUAGE)
								.toString())]);
		textUserLanguage.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_LANGUAGE).toString());
		textUserInterestedCountry.setTag(GET_USERDETAILS.get(0)
				.get(Constants.USER_INTERESTCOUNT).toString());
		Img_editimage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HomeActivity.sharedInstances.selectImage(imgUserimage);
				imgBtnLangSave.setVisibility(View.VISIBLE);
			}
		});

		// createSpinnerContent("");
		return row;
	}

	public void createSpinnerContent() {
		try {

			JSONObject obj = new JSONObject(loadJSONFromAsset());
			JSONArray m_jArry = obj.getJSONArray("countries");
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				Log.d("Details-->", jo_inside.getString("countrycode"));
				String countrycode = jo_inside.getString(COUNTRYCODE);
				String id = jo_inside.getString(ID);
				String related = jo_inside.getString(RELATED);
				String countryname = jo_inside.getString(COUNTRYNAME);
				String currencycode = jo_inside.getString(CURRENCYCODE);
				String currencyname = jo_inside.getString(CURRENCYNAME);
				String latitude = jo_inside.getString(LATITUDE);
				String longitude = jo_inside.getString(LONGITUDE);
				String woeid = jo_inside.getString(WOEID);
				String lang = jo_inside.getString(LANGUAGE);

				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li = new HashMap<String, String>();
				m_li.put(COUNTRYCODE, countrycode);
				m_li.put(ID, id);
				m_li.put(RELATED, related);
				m_li.put(COUNTRYNAME, countryname);
				m_li.put(CURRENCYCODE, currencycode);
				m_li.put(CURRENCYNAME, currencyname);
				m_li.put(LATITUDE, latitude);
				m_li.put(LONGITUDE, longitude);
				m_li.put(WOEID, woeid);
				m_li.put(LANGUAGE, lang);

				GET_COUNTRYLIST.add(m_li);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String loadJSONFromAsset() {
		String json = null;
		try {

			InputStream is = context_.getAssets().open("json/CountryList.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	public void createInterestSpinnerContent(int position) {

		GET_COUNTRYLIST_INTEREST = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_COUNTRYLIST.get(position);
		HashMap<String, String> m_li;
		m_li = new HashMap<String, String>();
		m_li.put(COUNTRYCODE, "interest_icon");
		m_li.put(ID, "0");
		m_li.put(RELATED, "1");
		m_li.put(
				COUNTRYNAME,
				context_.getResources().getString(
						R.string.Alert_registercontry_of_interest)
						+ "");
		m_li.put(CURRENCYCODE, "EUR");
		m_li.put(CURRENCYNAME, "");
		m_li.put(LATITUDE, "");
		m_li.put(LONGITUDE, "");
		m_li.put(WOEID, "");
		m_li.put(LANGUAGE, "");
		GET_COUNTRYLIST_INTEREST.add(m_li);
		// label.setText(map.get(RegisterActivity.COUNTRYNAME));
		ArrayList<HashMap<String, String>> arrylist_ = new ArrayList<HashMap<String, String>>();
		String itemsCount = map.get(RegisterActivity.RELATED);
		if (itemsCount.contains(",")) {
			String items[] = itemsCount.split(",");
			for (int i = 0; i < items.length; i++) {
				arrylist_.add(getIndexOFValue(items[i], GET_COUNTRYLIST));
			}
		} else {
			arrylist_.add(getIndexOFValue(itemsCount, GET_COUNTRYLIST));
		}
		Collections.sort(arrylist_, new Comparator<HashMap<String, String>>() {

			@Override
			public int compare(HashMap<String, String> lhs,
					HashMap<String, String> rhs) {
				// Do your comparison logic here and retrn accordingly.
				String firstValue = lhs.get(COUNTRYNAME);
				String secondValue = rhs.get(COUNTRYNAME);
				return firstValue.compareTo(secondValue);
			}
		});
		GET_COUNTRYLIST_INTEREST.addAll(arrylist_);

	}

	public void createLanguageSpinner(int position) {

		HashMap<String, String> map = new HashMap<String, String>();
		map = GET_COUNTRYLIST.get(position);
		Log.e("map", map + "-");
		HashMap<String, String> map2 = new HashMap<String, String>();
		map2 = GET_COUNTRYLIST.get(new JsonLocalFileFetch(context_)
				.getArraylistposio(GET_COUNTRYLIST,
						GET_USERDETAILS.get(0).get(Constants.USER_HOMECOUNT)
								.toString()));

		// label.setText(map.get(RegisterActivity.COUNTRYNAME));
		String itemsCount = map.get(LANGUAGE);
		String itemsCount2 = map2.get(LANGUAGE);

		if (itemsCount.contains(",") && itemsCount2.contains(",")) {
			String items[] = itemsCount.split(",");
			String items2[] = itemsCount2.split(",");
			Language_list = new String[items.length + items2.length + 1];
			CountryCode_List = new String[items.length + items2.length + 1];
			Language_list_NICNAME = new String[items.length + items2.length + 1];
			Language_list[0] = context_.getResources().getString(
					R.string.Alert_editlang_select_language)
					+ "";
			CountryCode_List[0] = context_.getResources().getString(
					R.string.Alert_editlang_select_language)
					+ "";
			Language_list_NICNAME[0] = context_.getResources().getString(
					R.string.Alert_editlang_select_language)
					+ "";
			for (int i = 0; i < items2.length; i++) {
				// GET_COUNTRYLIST_LANGUAGE.add(GET_COUNTRYLIST.get(Integer
				// .parseInt(items[i])));
				Language_list[i + 1] = Constants.ARRAY_COUNTRY_LANGUAGE[new JsonLocalFileFetch(
						context_).getPositionLanguage(
						Constants.ARRAY_COUNTRY_LANGUAGE_NICNAME, items2[i])]
						+ "("
						+ map2.get(COUNTRYCODE).toString().toUpperCase()
						+ ")";
				Language_list_NICNAME[i + 1] = items2[i];
				CountryCode_List[i + 1] = map2.get(COUNTRYCODE).toString();
				Log.e("Language_list", Language_list[i + 1] + "--"
						+ Language_list_NICNAME[i + 1]);

				if (textUserLanguage.getTag() != null
						&& textUserLanguage.getTag().toString()
								.equalsIgnoreCase(items2[i])) {
					valueofLang = i + 1;
				}
			}
			for (int i = 0; i < items.length; i++) {
				// GET_COUNTRYLIST_LANGUAGE.add(GET_COUNTRYLIST.get(Integer
				// .parseInt(items[i])));
				Language_list[i + items2.length + 1] = Constants.ARRAY_COUNTRY_LANGUAGE[new JsonLocalFileFetch(
						context_).getPositionLanguage(
						Constants.ARRAY_COUNTRY_LANGUAGE_NICNAME, items[i])]
						+ "("
						+ map.get(COUNTRYCODE).toString().toUpperCase()
						+ ")";
				Language_list_NICNAME[i + items2.length + 1] = items[i];
				CountryCode_List[i + items2.length + 1] = map.get(COUNTRYCODE)
						.toString();
				Log.e("Language_list", Language_list[i + items2.length + 1]
						+ "--" + Language_list_NICNAME[i + items2.length + 1]);
				if (textUserLanguage.getTag() != null
						&& textUserLanguage.getTag().toString()
								.equalsIgnoreCase(items[i])) {
					valueofLang = i + items2.length + 1;
				}
			}
		}
		// else {
		// GET_COUNTRYLIST_LANGUAGE.add(GET_COUNTRYLIST.get(Integer
		// .parseInt(itemsCount)));
		// }

	}

	public void getPosition() {
		createSpinnerContent();
		int position = 0;
		for (int i = 0; i < GET_COUNTRYLIST.size(); i++) {
			if (GET_COUNTRYLIST
					.get(i)
					.get(COUNTRYCODE)
					.equalsIgnoreCase(
							GET_USERDETAILS.get(0)
									.get(Constants.USER_HOMECOUNT).toString())) {
				position = i;
			}
		}
		createInterestSpinnerContent(position);
	}

	public class EditLangOpration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(context_);

		String country_interest = "";

		String language = "";
		String base64_ = "";
		String lagCountry = "";

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

			Dialog.setMessage(context_.getResources().getString(
					R.string.Alert_Pleasewait)
					+ "");
			Dialog.setCancelable(false);
			Dialog.show();

			try {
				// Set Request parameter
				country_interest += "&"
						+ URLEncoder.encode("interestcount", "UTF-8") + "="
						+ textUserInterestedCountry.getTag();

				language += "&" + URLEncoder.encode("language", "UTF-8") + "="
						+ textUserLanguage.getTag();
				base64_ += "&"
						+ URLEncoder.encode("avatar", "UTF-8")
						+ "="
						+ URLEncoder.encode(
								HomeActivity.sharedInstances.base64, "UTF-8");
				lagCountry += "&" + URLEncoder.encode("lagCountry", "UTF-8")
						+ "=" + langcountcode;
				Log.e("country_interest---language", country_interest + "----"
						+ language + "---lagCountry--" + lagCountry);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(urls[0]);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(Constants.RequestTimeOutLimit);
				conn.setReadTimeout(Constants.RequestTimeOutLimit);
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(country_interest);
				wr.write(language);
				wr.write(base64_);
				wr.write(lagCountry);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "\n");
				}

				// Append Server Response To Content String
				Content = sb.toString();

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				Log.wtf("ERROR : ", Error);

				Toast.makeText(
						context_,
						context_.getResources().getString(
								R.string.Alert_ServerError)
								+ "", Toast.LENGTH_LONG).show();
			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				Log.wtf("NO ERROR : ", Content);

				try {

					JSONObject jsonObj = new JSONObject(Content);

					Log.wtf("error1", Content);
					if (new ConnectivityServer().CheckExpirationTocken(
							context_, Content)) {
						return;
					}
					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("no auth")) {
						Toast.makeText(context_, "Token is not valid!",
								Toast.LENGTH_LONG).show();
					}

					if (jsonObj.get("response").toString()
							.equalsIgnoreCase("ok")) {
						Toast.makeText(
								context_,
								context_.getResources().getString(
										R.string.Alert_editlang_save)
										+ "", Toast.LENGTH_LONG).show();
						HashMap<String, String> m_li;
						m_li = new HashMap<String, String>();
						m_li.put(Constants.USER_ID,
								GET_USERDETAILS.get(0).get(Constants.USER_ID)
										.toString());
						m_li.put(
								Constants.USER_FIRSTNAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_FIRSTNAME)
										.toString());
						m_li.put(Constants.USER_LASTNAME, GET_USERDETAILS
								.get(0).get(Constants.USER_LASTNAME).toString());
						m_li.put(Constants.USER_GENDER, GET_USERDETAILS.get(0)
								.get(Constants.USER_GENDER).toString());
						m_li.put(Constants.USER_DATEOFBIRTH, GET_USERDETAILS
								.get(0).get(Constants.USER_DATEOFBIRTH)
								.toString());
						m_li.put(Constants.USER_EMAIL, GET_USERDETAILS.get(0)
								.get(Constants.USER_EMAIL).toString());
						m_li.put(Constants.USER_PHONENUMBER, GET_USERDETAILS
								.get(0).get(Constants.USER_PHONENUMBER)
								.toString());
						m_li.put(
								Constants.USER_IMAGENAME,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_IMAGENAME)
										.toString());
						m_li.put(
								Constants.USER_HOMECOUNT,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_HOMECOUNT)
										.toString());
						m_li.put(Constants.USER_INTERESTCOUNT,
								textUserInterestedCountry.getTag().toString());
						m_li.put(Constants.USER_LANGUAGE, textUserLanguage
								.getTag().toString());
						m_li.put(Constants.USER_DESCRIPTION, GET_USERDETAILS
								.get(0).get(Constants.USER_DESCRIPTION)
								.toString());
						m_li.put(Constants.USER_WEBSITE, GET_USERDETAILS.get(0)
								.get(Constants.USER_WEBSITE).toString());
						m_li.put(Constants.USER_STUDY, GET_USERDETAILS.get(0)
								.get(Constants.USER_STUDY).toString());
						m_li.put(Constants.USER_BUS, GET_USERDETAILS.get(0)
								.get(Constants.USER_BUS).toString());
						m_li.put(Constants.USER_TRAVAL, GET_USERDETAILS.get(0)
								.get(Constants.USER_TRAVAL).toString());
						m_li.put(Constants.USER_SOCCULT, GET_USERDETAILS.get(0)
								.get(Constants.USER_SOCCULT).toString());
						m_li.put(Constants.USER_COMPANY, GET_USERDETAILS.get(0)
								.get(Constants.USER_COMPANY).toString());
						m_li.put(Constants.USER_JOBPOSITION, GET_USERDETAILS
								.get(0).get(Constants.USER_JOBPOSITION)
								.toString());
						m_li.put(Constants.USER_COMPANYWEBSITE, GET_USERDETAILS
								.get(0).get(Constants.USER_COMPANYWEBSITE)
								.toString());
						m_li.put(Constants.USER_SCHOOL, GET_USERDETAILS.get(0)
								.get(Constants.USER_SCHOOL).toString());
						m_li.put(Constants.USER_DEGREE, GET_USERDETAILS.get(0)
								.get(Constants.USER_DEGREE).toString());
						m_li.put(
								Constants.USER_SCHOOLURL,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_SCHOOLURL)
										.toString());
						m_li.put(Constants.USER_LATITUDE, GET_USERDETAILS
								.get(0).get(Constants.USER_LATITUDE).toString());
						m_li.put(
								Constants.USER_LONGITUDE,
								GET_USERDETAILS.get(0)
										.get(Constants.USER_LONGITUDE)
										.toString());
						GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						// GET_USERDETAILS.set(0, m_li);
						GET_USERDETAILS.add(m_li);
						HomeActivity.sharedInstances.GET_USERDETAILS = new ArrayList<HashMap<String, String>>();
						HomeActivity.sharedInstances.GET_USERDETAILS = GET_USERDETAILS;
						// new ChangeLanguage(context_,
						// textUserLanguage.getTag()
						// .toString());

						session_email.createLoginSession(
								session_email.getUserDetails().get(
										SessionManager_email.KEY_EMAIL),
								textUserLanguage.getTag().toString());

						new JsonLocalFileFetch(context_).DeleteAllCache();
						Intent in = new Intent(context_, HomeActivity.class);
						in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context_.startActivity(in);
						// finish();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	public HashMap<String, String> getIndexOFValue(String value,
			ArrayList<HashMap<String, String>> listMap) {

		for (int i = 0; i < listMap.size(); i++) {
			if (listMap.get(i).get(ID).equalsIgnoreCase(value)) {
				return listMap.get(i);
			}

		}
		return null;
	}

}
