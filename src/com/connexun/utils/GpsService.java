package com.connexun.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

public class GpsService extends Service {

    String GPS_FILTER = "";
    Thread triggerService;
    LocationManager lm;
    GpsListener gpsLocationListener;
    boolean isRunning = true;
   
    @Override
    public void onCreate() {
         
          // TODO Auto-generated method stub
          super.onCreate();
          GPS_FILTER = "MyGPSLocation";
         
    }
   
    @Override
    public void onStart(Intent intent, int startId) {
          // TODO Auto-generated method stub
          super.onStart(intent, startId);     
          Log.e("LocationChange", "Speed --- "+Constants.GPSSpeed);
          triggerService = new Thread(new Runnable(){
                public void run(){
                      try{
                            Looper.prepare();//Initialize the current thread as a looper.
                            lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                            gpsLocationListener = new GpsListener();
                            long minTime = 30000; // 5 sec...
                            float minDistance = 0;
                            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
                                        minDistance, gpsLocationListener);
                            Log.e("LocationChange", "Lat & Long --- "+Constants.GPSLatitude+"--"+Constants.GPSLongitude);
                            Log.e("LocationChange", "Speed --- "+Constants.GPSSpeed);
                            Intent filterRes = new Intent(GPS_FILTER);
                            filterRes.putExtra("latitude", Constants.GPSLatitude+"--");
                            filterRes.putExtra("longitude", Constants.GPSLatitude+"--");
                            filterRes.putExtra("speed", Constants.GPSLatitude+"--");
                            sendBroadcast(filterRes);
                            Looper.loop();
                      }catch(Exception ex){
                            System.out.println("Exception in triggerService Thread -- "+ex);
                      }
                }
          }, "myLocationThread");
          triggerService.start();
    }
   
    @Override
    public void onDestroy() {
          // TODO Auto-generated method stub
          super.onDestroy();
          removeGpsListener();
    }
   
    @Override
    public IBinder onBind(Intent intent) {
          // TODO Auto-generated method stub
          return null;
    }
   
    private void removeGpsListener(){
          try{
                lm.removeUpdates(gpsLocationListener);
          }
          catch(Exception ex){
                System.out.println("Exception in GPSService --- "+ex);
          }
    }
   
    class GpsListener implements LocationListener{

          public void onLocationChanged(Location location) {
                // TODO Auto-generated method stub
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                float speed = location.getSpeed();
                Log.e("LocationChange", "Lat & Long --- "+Constants.GPSLatitude+"--"+Constants.GPSLongitude);
                Log.e("LocationChange", "Speed --- "+Constants.GPSSpeed);
                
                Intent filterRes = new Intent(GPS_FILTER);
                filterRes.putExtra("latitude", latitude);
                filterRes.putExtra("longitude", longitude);
                filterRes.putExtra("speed", speed);
                sendBroadcast(filterRes);
           
          }

          public void onProviderDisabled(String provider) {
                // TODO Auto-generated method stub
               
          }

          public void onProviderEnabled(String provider) {
                // TODO Auto-generated method stub
               
          }

          public void onStatusChanged(String provider, int status, Bundle extras) {
                // TODO Auto-generated method stub
               
          }

    }

}