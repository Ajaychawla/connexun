package com.connexun.utils;

import java.util.HashMap;

import com.connexun.main.EditProfileActivity;
import com.connexun.main.HomeActivity;
import com.connexun.main.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "Connexun_login";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	// User name (make variable public to access from outside)
	public static final String KEY_USERID = "userid";

	// Email address (make variable public to access from outside)
	public static final String KEY_USERTOKEN = "usertoken";
	//public static final String KEY_LANGUAGE = "language";
	
	public static final String KEY_EXPERT = "expert";

	public static final String FIRSTNAME = "firstname", LASTNAME = "lastname",
			USERNAME = "email", PASSWORD = "password",
			COUNTRYORIGIN = "countryorigin",
			CONTRYINTEREST = "countryinterest", BUSINESS = "business",
			EDUCAION = "education", TRAVAL = "traval",
			CULTURAL_AND_SOCIAL = "CulturalAndSocial";

	// Constructor
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(String userid, String usertoken,String expert) {
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);

		// Storing name in pref
		editor.putString(KEY_USERID, userid);
		Log.wtf("userid save", userid);
		// Storing email in pref
		editor.putString(KEY_USERTOKEN, usertoken);

		editor.putString(KEY_EXPERT, expert);

		// commit changes
		editor.commit();
	}

	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public void checkLogin() {
		// Check login status
		if (!this.isLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			_context.startActivity(i);
		}

	}

	/**
	 * Get stored session data
	 * 
	 * @return
	 * */
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		// user name

		user.put(KEY_USERID, pref.getString(KEY_USERID, null));
		Log.wtf("userid LOAD", pref.getString(KEY_USERID, null));
		Log.wtf("usertoken LOAD", pref.getString(KEY_USERTOKEN, null));

		// user email id
		user.put(KEY_USERTOKEN, pref.getString(KEY_USERTOKEN, null));
		user.put(KEY_EXPERT, pref.getString(KEY_EXPERT, "0"));
		//user.put(KEY_LANGUAGE, pref.getString(KEY_LANGUAGE, null));
		// return user
		return user;
	}

	// public String getUserDetails(){
	// // String user;
	// // user.put(KEY_USERID, pref.getString(KEY_USERID, null));
	// SharedPreferences mPrefs = getSharedPreferences("userauth",
	// Context.MODE_PRIVATE);
	// String usertoken = mPrefs.getString("usertoken", "");
	// String userid = mPrefs.getString("userid", "");
	//
	// return usertoken;
	// }
	//
	// private SharedPreferences getSharedPreferences(String string,
	// int modePrivate) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	/**
	 * Clear session details
	 * */
	public void logoutUser() {
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		new JsonLocalFileFetch(_context).DeleteAllCache();
		if (!isLoggedIn()) {
			Intent intent = new Intent(_context, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			_context.startActivity(intent);
		} else {
			logoutUser();
		}

		// After logout redirect user to Loing Activity
		// Intent i = new Intent(_context, LoginActivity.class);
		// // Closing all the Activities
		// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//
		// // Add new Flag to start new Activity
		// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//
		// // Staring Login Activity
		// _context.startActivity(i);
	}
	public void logoutUser_N() {
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		

		// After logout redirect user to Loing Activity
		// Intent i = new Intent(_context, LoginActivity.class);
		// // Closing all the Activities
		// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//
		// // Add new Flag to start new Activity
		// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//
		// // Staring Login Activity
		// _context.startActivity(i);
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
}
