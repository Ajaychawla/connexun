package com.connexun.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

public class ImageUploadServer {

	int serverResponseCode = 0;

	String upLoadServerUri = null;

	/********** File Path *************/
	final String uploadFilePath = "/mnt/sdcard/";
	final String uploadFileName = "service_lifecycle.png";
	InputStream is;

	public ImageUploadServer(String filepath) {
		// TODO Auto-generated constructor stub
		uploadFile(filepath);
	}
	public ImageUploadServer(Bitmap bitmap,String imageurl) {
		// TODO Auto-generated constructor stub
		uploadSlike(bitmap, imageurl);
	}

	public int uploadFile(String sourceFileUri) {

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);

		if (!sourceFile.isFile()) {

			Log.e("uploadFile", "Source File not exist :" + uploadFilePath + ""
					+ uploadFileName);

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("image", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; image=image;filename="
						+ fileName + "" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {

					String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
							+ " http://www.androidexample.com/media/uploads/"
							+ uploadFileName;
					Log.e("file uploaded", msg);

				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (Exception e) {

				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}

			return serverResponseCode;

		} // End else block
	}

	

	private void uploadSlike(Bitmap bitmapOrg, String imagurl) {

		try {

			ByteArrayOutputStream bao = new ByteArrayOutputStream();

			bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 90, bao);

			byte[] ba = bao.toByteArray();

			String ba1 = Base64.encodeToString(ba, Base64.NO_WRAP);

			ArrayList<NameValuePair> nameValuePairs = new

			ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("image", ba1));

			HttpClient httpclient = new DefaultHttpClient();

			HttpPost httppost = new

			HttpPost(imagurl);

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			is = entity.getContent();

			System.out.println(entity.getContentLength()+"----");
			Log.e("image upload res", entity.getContent()+"----");
			

		} catch (Exception e) {

			System.out.println("Error" + e);
		}

		System.out.println("uploaded");

	}

}
