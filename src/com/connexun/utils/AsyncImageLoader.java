package com.connexun.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

public class AsyncImageLoader {
	private HashMap<String, SoftReference<Drawable>> imageCache;
	Context context_;
	ImageLoader imgloader;
	public AsyncImageLoader(Context context){
		imageCache = new HashMap<String, SoftReference<Drawable>>();
		this.context_=context;
		imgloader=new ImageLoader(context_);
	}
	public void fetchDrawableOnThread(final String imageUrl, final ImageView imageView,final Drawable drawable_){
		if(imageCache.containsKey(imageUrl)){
			SoftReference<Drawable> softReference = imageCache.get(imageUrl);
			Drawable drawable = softReference.get();
			if(drawable != null){
				if(imageView != null){
				imageView.setImageDrawable(drawable);
				return;
				}
			}
		}
		final Handler handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				if(imageView != null){
					//imgloader.roundCorner(src, round)
					imageView.setImageDrawable((Drawable) msg.obj);
				}
			}
		};
		new Thread(){
			@Override
			public void run() {
				//Drawable drawable = loadImageFromUrl(imageUrl);
				Drawable drawable = drawable_;
				if(drawable != null){
					imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			} 
		}.start();

	}

	private Drawable loadImageFromUrl(String url){
		InputStream in = null;
		try {
			in = new URL(url).openStream();

		} catch (MalformedURLException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
		return Drawable.createFromStream(in, "src"); 
	}
	public void showImage(String path,ImageView im)   {
		Bitmap bm = null;
	    Log.i("showImage","loading:"+path);
	    BitmapFactory.Options bfOptions=new BitmapFactory.Options();
	    bfOptions.inDither=false;                     //Disable Dithering mode
	    bfOptions.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
	    bfOptions.inInputShareable=true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
	    bfOptions.inTempStorage=new byte[32 * 1024]; 


	    File file=new File(path);
	    FileInputStream fs=null;
	    try {
	        fs = new FileInputStream(file);
	    } catch (FileNotFoundException e) {
	        //TODO do something intelligent
	        e.printStackTrace();
	    }

	    try {
	        if(fs!=null) bm=BitmapFactory.decodeFileDescriptor(fs.getFD(), null, bfOptions);
	    } catch (IOException e) {
	        //TODO do something intelligent
	        e.printStackTrace();
	    } finally{ 
	        if(fs!=null) {
	            try {
	                fs.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	    }
	    //bm=BitmapFactory.decodeFile(path, bfOptions); This one causes error: java.lang.OutOfMemoryError: bitmap size exceeds VM budget
	    
	    im.setImageBitmap(imgloader.roundCorner(bm, bm.getWidth()));
	    //bm.recycle();
	    bm=null;



	}
	
	public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
            int reqHeight) {
 
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
 
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
 
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            }
            else if (orientation == 3) {
                matrix.postRotate(180);
            }
            else if (orientation == 8) {
                matrix.postRotate(270);
            }
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true); // rotating bitmap
            if (bmp.getWidth() >= bmp.getHeight()){

            	bmp = Bitmap.createBitmap(
            			bmp, 
            			bmp.getWidth()/2 - bmp.getHeight()/2,
    	    	     0,
    	    	     bmp.getHeight(), 
    	    	     bmp.getHeight()
    	    	     );

    	    	}else{

    	    		bmp = Bitmap.createBitmap(
    	    				bmp,
    	    	     0, 
    	    	     bmp.getHeight()/2 - bmp.getWidth()/2,
    	    	     bmp.getWidth(),
    	    	     bmp.getWidth() 
    	    	     );
    	    	}
            bmp = Bitmap.createScaledBitmap(bmp,reqWidth,
                    reqHeight, false);
        }
        catch (Exception e) {

        }
        return bmp;
        }
 
    public static int calculateInSampleSize(BitmapFactory.Options options,
            int reqWidth, int reqHeight) {
 
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
 
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
             }
         }
        
        Log.e("inSampleSize", inSampleSize+"--");
        Log.e("bitmAP width", width+"--");
        Log.e("bitmAP height", height+"--");
        
         return inSampleSize;
        }
}
