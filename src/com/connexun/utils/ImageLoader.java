package com.connexun.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.connexun.main.R;

public class ImageLoader {

	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	int Radius_ = 0;
	Context context_;

	public ImageLoader(Context context) {
		fileCache = new FileCache(context);
		context_ = context;
		executorService = Executors.newFixedThreadPool(5);

	}

	int stub_id = R.drawable.ic_launcher;

	public void DisplayImage(String url, int loader, ImageView imageView,
			int Radius) {
		Radius_ = Radius;
		stub_id = loader;
		imageViews.put(imageView, url);
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null)
			imageView.setImageBitmap(bitmap);
		else {
			queuePhoto(url, imageView);
			if(Radius_>0){
				
				Bitmap icon = BitmapFactory.decodeResource(context_.getResources(),
						stub_id);
				Radius_ = icon.getWidth();
				imageView.setImageBitmap(getCroppedBitmap(icon, Radius_));
			}else
			imageView.setImageResource(loader);
		}
	}

	private void queuePhoto(String url, ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// from web
		try {
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 300;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			if (Radius_ > 0) {
				Radius_ = o.outWidth;
				return getCroppedBitmap(BitmapFactory.decodeStream(
						new FileInputStream(f), null, o2), Radius_);
			} else {
				return BitmapFactory.decodeStream(new FileInputStream(f), null,
						o2);
			}

		} catch (FileNotFoundException e) {
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			Bitmap bmp = getBitmap(photoToLoad.url);
			memoryCache.put(photoToLoad.url, bmp);
			if (imageViewReused(photoToLoad))
				return;
			BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
			Activity a = (Activity) photoToLoad.imageView.getContext();
			a.runOnUiThread(bd);
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null)
				photoToLoad.imageView.setImageBitmap(bitmap);
			else{
				if(Radius_>0){
					
					Bitmap icon = BitmapFactory.decodeResource(context_.getResources(),
							stub_id);
					Radius_ = icon.getWidth();
					photoToLoad.imageView.setImageBitmap(getCroppedBitmap(icon, Radius_));
				}else
				photoToLoad.imageView.setImageResource(stub_id);
			}
				
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}
	public void clearSingleCache(String url) {
		memoryCache.clearSingleKey(url);
		fileCache.clearSingleFile(url);
	}

	public static Bitmap roundCorner(Bitmap src, float round) {
		// image size
		int width = src.getWidth();
		int height = src.getHeight();
		// create bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// config paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// config rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw rect to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}

	public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
		Bitmap sbmp;
		 Log.e("bmp.getWidth(), bmp.getHeight()", bmp.getWidth() +"---"+bmp.getHeight());
//		if (bmp.getWidth() != radius || bmp.getHeight() != radius)
//			sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
//		else
			sbmp = bmp;
		
		Log.e("sbmp.getWidth(), sbmp.getHeight()", sbmp.getWidth()+"---"+sbmp.getHeight());
		 if (sbmp.getWidth() >= sbmp.getHeight()){

			 sbmp = Bitmap.createBitmap(
					 sbmp, 
					 sbmp.getWidth()/2 - sbmp.getHeight()/2,
		    	     0,
		    	     sbmp.getHeight(), 
		    	     sbmp.getHeight()
		    	     );

		    	}else{

		    		sbmp = Bitmap.createBitmap(
		    				sbmp,
		    	     0, 
		    	     sbmp.getHeight()/2 - sbmp.getWidth()/2,
		    	     sbmp.getWidth(),
		    	     sbmp.getWidth() 
		    	     );
		    	}
		 
		 Log.e("sbmp.getWidth(), sbmp.getHeight()", sbmp.getWidth()+"---"+sbmp.getHeight());
		Bitmap output = Bitmap.createBitmap(sbmp.getWidth(), sbmp.getHeight(),
				Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xffa19774;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());

		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.parseColor("#BAB399"));
		canvas.drawCircle(sbmp.getWidth() / 2 + 0.7f,
				sbmp.getHeight() / 2 + 0.7f, sbmp.getWidth() / 2 + 0.1f, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(sbmp, rect, rect, paint);

		return output;
	}

	public Bitmap ConvertBase64ToBitmap(String base64String) {
		Bitmap decodedByte = null;
		try {

			if (!base64String.equals("")) {
				
                base64String= resizeBase64Image(base64String);
				byte[] decodedString = Base64.decode(base64String,
						Base64.DEFAULT);
				decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
						decodedString.length);
				if (decodedByte == null)
					decodedByte = BitmapFactory.decodeResource(
							context_.getResources(),
							R.drawable.default_avatar_single);

				decodedByte = getCroppedBitmap(decodedByte,
						decodedByte.getWidth());
                   
			} else {
				decodedByte = BitmapFactory.decodeResource(
						context_.getResources(),
						R.drawable.default_avatar_single);
				decodedByte = getCroppedBitmap(decodedByte,
						decodedByte.getWidth());
			}
		} catch (Exception e) {
			// TODO: handle exception
			decodedByte = BitmapFactory.decodeResource(context_.getResources(),
					R.drawable.default_avatar_single);
			decodedByte = getCroppedBitmap(decodedByte, decodedByte.getWidth());
		}
		catch(OutOfMemoryError e){
			
		}
//		finally {
//			decodedByte = BitmapFactory.decodeResource(context_.getResources(),
//					R.drawable.default_avatar_single);
//			decodedByte = getCroppedBitmap(decodedByte, decodedByte.getWidth());
//        }
			

		return decodedByte;
	}
	public String resizeBase64Image(String base64image){

	    byte [] encodeByte=Base64.decode(base64image,Base64.DEFAULT); //out of memory exception...

	    BitmapFactory.Options options=new BitmapFactory.Options();
	    options.inPurgeable = true;
	    Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
	    if (image.getWidth() >= image.getHeight()){

	    	image = Bitmap.createBitmap(
	    			image, 
	    			image.getWidth()/2 - image.getHeight()/2,
	    	     0,
	    	     image.getHeight(), 
	    	     image.getHeight()
	    	     );

	    	}else{

	    		image = Bitmap.createBitmap(
	    				image,
	    	     0, 
	    	     image.getHeight()/2 - image.getWidth()/2,
	    	     image.getWidth(),
	    	     image.getWidth() 
	    	     );
	    	}
	    image = Bitmap.createScaledBitmap(image, Constants.ImageSize, Constants.ImageSize, false);

	    ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	    image.compress(Bitmap.CompressFormat.PNG,100, baos);
	    byte [] newbytes=baos.toByteArray();

	    return Base64.encodeToString(newbytes, Base64.DEFAULT);

	}
}
