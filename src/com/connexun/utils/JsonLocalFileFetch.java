package com.connexun.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.connexun.widgets.EditLangCustom;

public class JsonLocalFileFetch {
	Context context_;

	public JsonLocalFileFetch(Context context) {
		// TODO Auto-generated constructor stub
		context_ = context;
	}

	public String loadJSONFromAsset(String jsonfile) {
		String json = null;
		try {

			InputStream is = context_.getAssets().open(jsonfile);

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	public int getPositionCountry(String countryArray[], String country) {
		int countPossion = 0;
		for (int i = 0; i < countryArray.length; i++) {
			if (countryArray[i].equalsIgnoreCase(country))
				countPossion = i;
		}
		return countPossion;
	}

	public int getPositionLanguage(String countryArray[], String country) {
		int countPossion = 0;
		for (int i = 0; i < countryArray.length; i++) {
			if (countryArray[i].equalsIgnoreCase(country))
				countPossion = i;
		}
		return countPossion;
	}

	public int getArraylistposio(
			ArrayList<HashMap<String, String>> countryArray, String country) {
		int countPossion = 0;
		for (int i = 0; i < countryArray.size(); i++) {
			if (countryArray.get(i).get(EditLangCustom.COUNTRYCODE).toString()
					.equalsIgnoreCase(country))
				countPossion = i;
		}
		return countPossion;
	}

	public void writeToFile(String jsonData, String jsonpath) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					context_.openFileOutput(jsonpath, Context.MODE_PRIVATE));
			outputStreamWriter.write(jsonData);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public String ReadFile(String FileName) {
		String dataRead = "";

		try {
			File myFile = new File(FileName);
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String aDataRow = "";
			String aBuffer = "";
			while ((aDataRow = myReader.readLine()) != null) {
				aBuffer += aDataRow + "\n";
			}
			dataRead = aBuffer;
			myReader.close();

		} catch (Exception e) {

		}
		return dataRead;
	}

	public void WriteFile(String Data, String FileName) {
		File cacheDir;
		try {
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED))
				cacheDir = new File(
						android.os.Environment.getExternalStorageDirectory(),
						"Connexun");
			else
				cacheDir = context_.getCacheDir();
			if (!cacheDir.exists())
				cacheDir.mkdirs();

			File myFile = new File(FileName);
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			myOutWriter.append(Data);
			myOutWriter.close();
			fOut.close();

		} catch (Exception e) {

		}
	}

	public void saveFile() {
		String FILENAME = "hello_file";
		String string = "hello world!";
		try {
			FileOutputStream fos = context_.openFileOutput(FILENAME,
					Context.MODE_PRIVATE);

			fos.write(string.getBytes());

			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void getFile() {
		String FILENAME = "hello_file";
		String string = "";
		StringBuilder sb = new StringBuilder();
		try {
			FileInputStream fos = context_.openFileInput(FILENAME);

			
		    InputStreamReader inputStreamReader = new InputStreamReader(fos);
		    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		   
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        sb.append(line);
		    }
		    string=sb.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("Readfile", string+"=--");

	}
	
	public void DeleteAllCache(){
		
		deletefile(Uri.parse(Constants.FolderPath+Constants.UserInfoFileName));
		deletefile(Uri.parse(Constants.FolderPath+Constants.WeatherFileName));
		deletefile(Uri.parse(Constants.FolderPath+Constants.NewsFileName));
		deletefile(Uri.parse(Constants.FolderPath+Constants.POIFileName));
		deletefile(Uri.parse(Constants.FolderPath+Constants.PeopleAroundFileName));
		
	}
	
	private void deletefile(Uri uri){
	File fdelete = new File(uri.getPath());
    if (fdelete.exists()) {
        if (fdelete.delete()) {
            System.out.println("file Deleted :" + uri.getPath());
        } else {
            System.out.println("file not Deleted :" + uri.getPath());
        }
    }
}

}
