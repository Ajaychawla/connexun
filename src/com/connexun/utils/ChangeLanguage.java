package com.connexun.utils;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class ChangeLanguage {
	
	Context context_;
	
	public ChangeLanguage(Context context,String lang) {
		this.context_=context;
		// TODO Auto-generated constructor stub
		
		changeLang(lang);
	}
	public ChangeLanguage() {
		// TODO Auto-generated constructor stub
	}

	public void saveLocale(String lang) {
		String langPref = "Language";
		SharedPreferences prefs = context_.getSharedPreferences("CommonPrefs",
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(langPref, lang);
		editor.commit();
	}

	public void changeLang(String lang) {
		if (lang.equalsIgnoreCase(""))
			return;
		Locale myLocale = new Locale(lang);
		saveLocale(lang);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		context_.getResources().updateConfiguration(config,
				context_.getResources().getDisplayMetrics());

	}
	
	public String getlanguage(){

		return Locale.getDefault().getLanguage()+"";
	}
	
}
