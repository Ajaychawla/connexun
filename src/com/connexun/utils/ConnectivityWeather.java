package com.connexun.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.connexun.interfaces.WeatherListner;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.util.Log;

@SuppressLint("NewApi")
public class ConnectivityWeather {

	WeatherListner wlistner_;

	public ConnectivityWeather(String Woid, WeatherListner wlistner) {

		// TODO Auto-generated constructor stub
		wlistner_ = wlistner;
		new wetherfetch().execute(Woid);
	}

	public ConnectivityWeather() {
		// TODO Auto-generated constructor stub
	}

	private class wetherfetch extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			Log.wtf("weee", "weee");

			// Start Progress Dialog (Message)

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			// Send data
			try {
				// Defined URL where to send data
				Content = QueryYahooWeather(urls[0]);

			} catch (Exception ex) {
				Error = ex.getMessage();
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog

			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);

			} else {
				String weatherString = Content;
				Document weatherDoc = convertStringToDocument(weatherString);
				wlistner_.WeathListner(weatherDoc);
				// MyWeather weatherResult = parseWeather(weatherDoc);
				// weather.setText(weatherResult.toString());
				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );

			}
		}
	}

	private String QueryYahooWeather(String Woid) {

		String qResult = "";
		String queryString = "http://weather.yahooapis.com/forecastrss?w="
				+ Woid;

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(queryString);

		try {
			HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();

			if (httpEntity != null) {
				// InputStream inputStream = getAssets().open("xml/wether.xml");
				InputStream inputStream = httpEntity.getContent();
				Reader in = new InputStreamReader(inputStream);
				BufferedReader bufferedreader = new BufferedReader(in);
				StringBuilder stringBuilder = new StringBuilder();

				String stringReadLine = null;

				while ((stringReadLine = bufferedreader.readLine()) != null) {
					stringBuilder.append(stringReadLine + "\n");
				}

				qResult = stringBuilder.toString();
			}

		} catch (NetworkOnMainThreadException e) {
			e.printStackTrace();

			Log.e("Network Error", e.toString() + "---");
		} catch (ClientProtocolException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return qResult;
	}

	private Document convertStringToDocument(String src) {
		Document dest = null;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder parser;

		try {
			parser = dbFactory.newDocumentBuilder();
			dest = parser.parse(new ByteArrayInputStream(src.getBytes()));
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();

		} catch (SAXException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return dest;
	}

	public HashMap<String, String> createSpinnerWeather(Document srcDoc) {
		// ArrayList<HashMap<String, String>> WEATHERLIST = new
		// ArrayList<HashMap<String, String>>();
		HashMap<String, String> m_li=null;
		
		try {
			// <yweather:location city="New York" region="NY"
			// country="United States"/>
			m_li = new HashMap<String, String>();
			Node locationNode = srcDoc
					.getElementsByTagName("yweather:location").item(0);
			m_li.put(Constants.COUNTRY_NAME, locationNode.getAttributes()
					.getNamedItem("country").getNodeValue().toString());
			m_li.put(Constants.COUNTRY_CITY, locationNode.getAttributes()
					.getNamedItem("city").getNodeValue().toString());
			m_li.put(Constants.COUNTRY_REGION, locationNode.getAttributes()
					.getNamedItem("region").getNodeValue().toString());

			// <yweather:condition text="Fair" code="33" temp="60"
			// date="Fri, 23 Mar 2012 8:49 pm EDT"/>
			Node conditionNode = srcDoc.getElementsByTagName(
					"yweather:condition").item(0);
			m_li.put(Constants.COUNTRY_CODE, conditionNode.getAttributes()
					.getNamedItem("code").getNodeValue().toString());
			m_li.put(Constants.COUNTRY_TEMP, conditionNode.getAttributes()
					.getNamedItem("temp").getNodeValue().toString());
			m_li.put(Constants.COUNTRY_DATE_TIME, conditionNode.getAttributes()
					.getNamedItem("date").getNodeValue().toString());
			m_li.put(Constants.COUNTRY_TEXTATMOS, conditionNode.getAttributes()
					.getNamedItem("text").getNodeValue().toString());

			// WEATHERLIST.add(m_li);
			return m_li;
		} catch (Exception e) {
			// TODO: handle exception
			return m_li=null;
		}
	}

}
