package com.connexun.utils;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;

public class ScreenSizeResolution {

	public int getScreenSizeResolution(Activity activity) {
		int screenSize = 0;
		// TODO Auto-generated constructor stub
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		float scaleFactor = metrics.density;
		float widthDp = widthPixels / scaleFactor;
		float heightDp = heightPixels / scaleFactor;
		float smallestWidth = Math.min(widthDp, heightDp);
		Log.e("smallestWidth", smallestWidth + "--");
		if (smallestWidth >= 720) {
			// Device is a 10" tablet
			screenSize = 720;
		} else if (smallestWidth >= 600) {
			// Device is a 7" tablet
			screenSize = 600;
		} else {
			screenSize = 320;
		}
		return screenSize;

	}

}
