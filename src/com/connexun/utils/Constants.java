package com.connexun.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class Constants {

	// Urls--
	public static String LoginUrl = "https://api.connexun.com/user/login";
	public static String ForgotPassword = " https://api.connexun.com/user/forgot/";
	public static String Register = "https://api.connexun.com/user";
	public static String NEWS = "https://api.connexun.com/news/";
	public static String PEOPLEAROUND = "https://api.connexun.com/peoplearound/";
	public static String POINTOFINTEREST = "https://api.connexun.com/poi/";
	public static String USERALLINFO = "https://api.connexun.com/user/";
	public static String USER_EDIT_PROFILE_SAVE = "https://api.connexun.com/user/";
	public static String LANGUAGE_INTERESTCHANGE = "https://api.connexun.com/user/il/";
	public static String CHANGE_PASSWORDURL = "https://api.connexun.com/user/changepassword/";
	public static String WEATHER_URL = "https://api.connexun.com/weather/";
	public static String TERMSANDCONDITION_URL = "http://connexun.com/en/Terms_and_Conditions";
	public static String IMAGEUPLOAD_URL = "https://api.connexun.com/user/avatar/";
	public static String PEOPLEARROUND_IN_URL = "https://api.connexun.com/peoplearound/in/";
	public static String PEOPLEARROUND_OUT_URL = "https://api.connexun.com/peoplearound/out/";
	public static String NEW_NEWSURL= "https://api.connexun.com/dnews/";
	public static String NEW_NEWSandPEOPLEARROUND_URL= "https://api.connexun.com/peoplearound/user/";
	
	// News Strings--
	public static String NEWS_ID = "id";
	public static String NEWS_ACQIRED_DATETIME = "acquiredDateTime";
	public static String NEWS_TYPE = "type";
	public static String NEWS_CATEGORY = "categories";
	public static String NEWS_PICTURE = "picture";
	public static String NEWS_TITLE = "title";
	public static String NEWS_DESCRIPTION = "description";
	public static String NEWS_URL = "url";
	public static String NEWS_SOURCE = "source";
	public static String NEWS_BOXTYPE = "boxtype";
	public static String NEWS_EVENTDATE = "eventdate";
	public static String NEWS_TAGS = "tags";
	public static String NEWS_POIS = "pois";
	public static String NEWS_EXPERT_USERID = "user_id";
	
	
	// People Around Strings--
	// {"response":[{"id":2,"latitude":37.57787,"longitude":-122.34809,"homecount":"in","interestcount":"it","image_name":"http://connexun.com/Symfony2/web/images/img/userimages/default-user-avatar.png","firstname":"Ritin","lastname":"Pali","emailyesno":null,"dateofbirthyesno":null,"phnumberyesno":null,"email":"Private","dateofbirth":"Private","phnumber":"Private","distance":29.030082779828337}]}
	public static String PEOPLEAROUND_ID = "id";
	public static String PEOPLEAROUND_LETITUDE = "latitude";
	public static String PEOPLEAROUND_LONGITUDE = "longitude";
	public static String PEOPLEAROUND_HOMECOUNT = "homecount";
	public static String PEOPLEAROUND_INTERESTCOUNT = "interestcount";
	public static String PEOPLEAROUND_IMAGE = "image_name";
	public static String PEOPLEAROUND_FIRSTNAME = "firstname";
	public static String PEOPLEAROUND_LASTNAME = "lastname";
	public static String PEOPLEAROUND_EMAILYESNO = "emailyesno";
	public static String PEOPLEAROUND_DOBYESNO = "dateofbirthyesno";
	public static String PEOPLEAROUND_PHONENUMBERYESNO = "phnumberyesno";
	public static String PEOPLEAROUND_EMAIL = "email";
	public static String PEOPLEAROUND_DOB = "dateofbirth";
	public static String PEOPLEAROUND_PHONENUMBER = "phnumber";
	public static String PEOPLEAROUND_DISTANCE = "distance";
	public static String PEOPLEAROUND_AVATAR = "avatar";

	// Weather Strings--
	public static String COUNTRY_LOCATIONTAG = "yweather:location";
	public static String COUNTRY_NAME = "country";
	public static String COUNTRY_CITY = "city";
	public static String COUNTRY_REGION = "region";

	public static String COUNTRY_CONDITIONTAG = "yweather:condition";
	public static String COUNTRY_TEMP = "temp";
	public static String COUNTRY_CODE = "code";
	public static String COUNTRY_DATE_TIME = "date";
	public static String COUNTRY_TEXTATMOS = "text";

	// public static String ARRAY_COUNTRY_NICNAME[] = { "in", "it", "br", "cn",
	// "ca" };
	// public static String ARRAY_COUNTRY_COMPLETENAME[] = { "India", "Italy",
	// "Brazil", "China", "Canada" };
	// public static String ARRAY_COUNTRY_CENTRE[] = { "New Delhi", "Rome",
	// "Bras�lia", "Beijing", "Ottawa" };
	// public static String ARRAY_COUNTRY_CURRENCYCODE[] = { "INR", "EUR",
	// "BRL",
	// "CNY", "CAD" };
	// public static String ARRAY_COUNTRY_LANGUAGE_NICNAME[] = { "en", "it",
	// "pt",
	// "cn", "ca" };
	// public static String ARRAY_COUNTRY_LANGUAGE[] = { "English", "Italian",
	// "Portuguese", "China", "Canada" };

	public static String ARRAY_COUNTRY_NICNAME[];
	public static String ARRAY_COUNTRY_COMPLETENAME[];
	public static String ARRAY_COUNTRY_CENTRE[];
	public static String ARRAY_COUNTRY_CURRENCYCODE[];
	public static String ARRAY_COUNTRY_LANGUAGE_NICNAME[];
	public static String ARRAY_COUNTRY_LANGUAGE[];

	// User All info
	// {"response":[{"id":536,"userinformation_id":null,"username":"test@test1.com",
	// "username_canonical":"test@test1.com","email":"test@test1.com","email_canonical":"test@test1.com",
	// "enabled":1,"salt":"not showing for security reasons","password":"vDYosYWCqop/qBaGRIXvWc1TydIGfrBvpFDy5s7CEweNudd98CM6eptMkbvnTnw9Gjvy762Q7A5qXYGhYi7Wow==",
	// "last_login":null,"locked":0,"expired":0,"expires_at":null,"confirmation_token":null,"password_requested_at":null,
	// "roles":"a:0:{}","credentials_expired":0,"credentials_expire_at":null,"firstname":"test","lastname":"test",
	// "homecount":"in","homecountcity":null,"interestcount":"it","interestcountcity":null,
	// "interests":"a:1:{i:0;s:3:\"bus\";}","emailyesno":null,"image_name":"http://connexun.com/Symfony2/web/images/img/userimages/default-user-avatar.png",
	// "updatedAt":null,"latitude":37.422005,"longitude":-122.084095,"language":"en",
	// "created_at":"2014-12-17T15:54:36.000Z","gender":null,"dateofbirth":null,"dateofbirthyesno":null,"languages":null,
	// "message":null,"website":null,"cntcode":null,"phnumber":null,"phnumberyesno":null,"occupation":null,
	// "companyname":null,"jobtitle":null,"industry":null,"compwebsite":null,"school":null,"degree":null,
	// "location":null,"schoolurl":null,"study":0,"bus":1,"travel":0,"soccult":0}]}

	public static String USER_ID = "id";
	public static String USER_FIRSTNAME = "firstname";
	public static String USER_LASTNAME = "lastname";
	public static String USER_GENDER = "gender";
	public static String USER_DATEOFBIRTH = "dateofbirth";
	public static String USER_EMAIL = "email";
	public static String USER_PHONENUMBER = "phnumber";
	public static String USER_IMAGENAME = "image_name";
	public static String USER_HOMECOUNT = "homecount";
	public static String USER_INTERESTCOUNT = "interestcount";
	public static String USER_LANGUAGE = "language";
	public static String USER_DESCRIPTION = "message";
	public static String USER_WEBSITE = "website";
	public static String USER_STUDY = "study";
	public static String USER_BUS = "bus";
	public static String USER_TRAVAL = "travel";
	public static String USER_SOCCULT = "soccult";
	public static String USER_COMPANY = "companyname";
	public static String USER_JOBPOSITION = "jobtitle";
	public static String USER_COMPANYWEBSITE = "compwebsite";
	public static String USER_SCHOOL = "school";
	public static String USER_DEGREE = "degree";
	public static String USER_SCHOOLURL = "schoolurl";
	public static String USER_LATITUDE = "latitude";
	public static String USER_LONGITUDE = "longitude";
	public static String USER_DATEOFBIRTH_YESNO = "dateofbirthyesno";
	public static String USER_PHONENUMBER_YESNO = "phnumberyesno";
	public static String USER_EMAIL_YESNO = "emailyesno";
	public static String USER_AVATAR = "avatar";
	public static String USER_EXPERT = "expert";
	
	// public static String USER_INFORMATIONID = "userinformation_id";
	// public static String USER_USERNAME = "username";
	// public static String USER_USERCANONICAL = "username_canonical";
	//
	// public static String USER_EMAILCANONICAL = "email_canonical";
	// public static String USER_ENABLED = "enabled";
	// public static String USER_SALT = "salt";
	// public static String USER_PASSWORD = "password";
	// public static String USER_LASTLOGIN = "last_login";
	// public static String USER_LOCKED = "locked";
	// public static String USER_EXPIRED = "expired";
	// public static String USER_EXPIREDAT = "expires_at";

	// People on interest (POI)
	public static String POI_ID = "id";
	public static String POI_TYPE = "type";
	public static String POI_NAME = "name";
	public static String POI_CATEGORY = "category";
	public static String POI_FORMATTEDADDRESS = "formatted_address";
	public static String POI_LATITUDE = "latitude";
	public static String POI_LONGITUDE = "longitude";
	public static String POI_PHONE = "phone";
	public static String POI_WEBSITE = "website";
	public static String POI_ICONNAME = "icon_name";
	public static String POI_DISTANCE = "distance";

	// Weather
	// {"response":[{"homecount":"it","hometemp":0,"homecode":"29","hometext":"Partly Cloudy",
	// "homecurrency":0.13261720045089848,"interestcount":"cn","interesttemp":-2,"interestcode":"32",
	// "interesttext":"Sunny","interestcurrency":7.5405}]}
	public static String WEATHER_HOMECOUNT = "homecount";
	public static String WEATHER_HOMETEMP = "hometemp";
	public static String WEATHER_HOMECODE = "homecode";
	public static String WEATHER_HOMETEXT = "hometext";
	public static String WEATHER_HOMECURRENCY = "homecurrency";
	public static String WEATHER_INTERESTCOUNT = "interestcount";
	public static String WEATHER_INTERESTTEMP = "interesttemp";
	public static String WEATHER_INTERESTCODE = "interestcode";
	public static String WEATHER_INTERESTTEXT = "interesttext";
	public static String WEATHER_INTERESTCURRENCY = "interestcurrency";
	public static String WEATHER_HOMECITY = "homecity";
	public static String WEATHER_INTERESTCITY = "interestcity";

	// Country List
	public static String COUNTRYLSIT_NAME = "name";
	public static String COUNTRYLSIT_DIALCODE = "dial_code";
	public static String COUNTRYLSIT_CODE = "code";

	// Country List File ArrayList
	public static ArrayList<HashMap<String, String>> COUNTRYLSITFILE_COUNTRYLIST = new ArrayList<HashMap<String, String>>();

	// Country List File
	public static String COUNTRYLSITFILE_COUNTRYCODE = "countrycode";
	public static String COUNTRYLSITFILE_ID = "id";
	public static String COUNTRYLSITFILE_RELATED = "related";
	public static String COUNTRYLSITFILE_COUNTRYNAME = "countryname";
	public static String COUNTRYLSITFILE_CURRENCYCODE = "currencycode";
	public static String COUNTRYLSITFILE_CURRENCYNAME = "currencyname";
	public static String COUNTRYLSITFILE_LATITUDE = "latitude";
	public static String COUNTRYLSITFILE_LONGITUDE = "longitude";
	public static String COUNTRYLSITFILE_WOEID = "woeid";
	public static String COUNTRYLSITFILE_CAPITAL = "capital";
	public static String COUNTRYLSITFILE_LANGUAGE = "Laguages";

	// Language File ArrayList
	public static ArrayList<HashMap<String, String>> LANGUAGEFILE_COUNTRYLIST = new ArrayList<HashMap<String, String>>();

	// Language File
	public static String LANGUAGEFILE_LANGNAME = "languageName";
	public static String LANGUAGEFILE_LANGCODE = "languageCode";

	// Folder Path
	public static String FolderPath = "/data/data/com.connexun.main/cache/";

	// UserInfo File Name
	public static String UserInfoFileName = "UserInfo.db";

	// Weather File Name
	public static String WeatherFileName = "Weather.db";

	// News File Name
	public static String NewsFileName = "News.db";

	// POI File Name
	public static String POIFileName = "POI.db";

	// PeopleAround File Name
	public static String PeopleAroundFileName = "PeopleAround.db";
	
	//Temp Images folder
	public static String TempImageFolder = "connexuntempimg";
	
	// Request Time Out Limit
	public static int RequestTimeOutLimit = 60000;
	
	// User Image Size
	public static int ImageSize=250;
	
	// GCM Registration id
	public static String GCM_REGISTRATION_ID="";
	
	// LAT LONG
	public static double GPSLatitude = 0;
	public static double GPSLongitude = 0;
	public static double GPSSpeed =0;
}
