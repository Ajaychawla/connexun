package com.connexun.utils;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.connexun.interfaces.DataListners;
import com.connexun.main.HomeActivity;
import com.connexun.main.R;

public class ConnectivityServer {

	Context context_;
	String filter_;
	View view_;

	public ConnectivityServer(Context context, String filter, String URL,
			DataListners dataListner, View view) {
		// TODO Auto-generated constructor stub
		this.context_ = context;
		this.filter_ = filter;
		this.view_ = view;
		Log.e(filter_ + " URL", URL);
		new ConnectivityAsync(dataListner).execute(URL);
	}

	public ConnectivityServer() {
		// TODO Auto-generated constructor stub
	}

	public ConnectivityServer(Activity context) {
		// TODO Auto-generated constructor stub
		this.context_ = context;
	}

	public class ConnectivityAsync extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;

		String user_id = "";
		String apitoken = "";
		DataListners dataListner;

		public ConnectivityAsync(DataListners dataListner) {
			// TODO Auto-generated constructor stub
			this.dataListner = dataListner;
		}

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			// Log.wtf("weee", "weee");
			// if (filter_.equalsIgnoreCase("News"));
			// HomeActivity.sharedInstances.newsProgress
			// .setVisibility(View.VISIBLE);

			try {
				// Set Request parameter
				// user_id += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERID);
				// apitoken += "/"
				// + session.getUserDetails().get(
				// SessionManager.KEY_USERTOKEN);

				// Log.e("URL parameters", user_id + "----" + apitoken);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data

				HttpClient Client = new DefaultHttpClient();

				// Create URL string

				// String URL =
				// "http://androidexample.com/media/webservice/httpget.php?user="+loginValue+"&name="+fnameValue+"&email="+emailValue+"&pass="+passValue;

				// Log.i("httpget", URL);

				String SetServerString = "";

				// Create Request to server and get response

				HttpGet httpget = new HttpGet(urls[0]);

				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is
				// established.
				// The default value is zero, that means the timeout is not
				// used.
				int timeoutConnection = Constants.RequestTimeOutLimit;
				HttpConnectionParams.setConnectionTimeout(httpParameters,
						timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = Constants.RequestTimeOutLimit;
				HttpConnectionParams
						.setSoTimeout(httpParameters, timeoutSocket);

				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				// SetServerString = Client.execute(httpget, responseHandler);

				DefaultHttpClient httpClient = new DefaultHttpClient(
						httpParameters);
				SetServerString = httpClient.execute(httpget, responseHandler);

				// Show response on activity
				Content = SetServerString;
				// content.setText(SetServerString);

			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			if (filter_.equalsIgnoreCase("News"))
				;
			// HomeActivity.sharedInstances.newsProgress
			// .setVisibility(View.GONE);
			if (Error != null) {

				// uiUpdate.setText("Output : "+Error);
				// dataListner.GetNewsListners(Error);
				if (filter_.equalsIgnoreCase("News"))
					dataListner.GetNewsListners(Error, view_);

				if (filter_.equalsIgnoreCase("Peoplearound"))
					dataListner.GetPeopleAroundListners(Error, view_);
				if (filter_.equalsIgnoreCase("Pointofinterst"))
					dataListner.GetPointOfIntrestListners(Error, view_);
				if (filter_.equalsIgnoreCase("Userallinfo"))
					dataListner.GetUserAllInfo(Error, view_);
				if (filter_.equalsIgnoreCase("Weather"))
					dataListner.GetWeatherDetail(Error, view_);
				// Log.wtf("ERROR : ", Error);

			} else {

				// Show Response Json On Screen (activity)
				// uiUpdate.setText( Content );
				// Log.wtf("NO ERROR : ", Content);

				try {
					if (filter_.equalsIgnoreCase("News"))
						dataListner.GetNewsListners(Content, view_);
					if (filter_.equalsIgnoreCase("Peoplearound"))
						dataListner.GetPeopleAroundListners(Content, view_);
					if (filter_.equalsIgnoreCase("Pointofinterst"))
						dataListner.GetPointOfIntrestListners(Content, view_);
					if (filter_.equalsIgnoreCase("Userallinfo"))
						dataListner.GetUserAllInfo(Content, view_);
					if (filter_.equalsIgnoreCase("Weather"))
						dataListner.GetWeatherDetail(Content, view_);
					// JSONObject jsonObj = new JSONObject(Content);
					// createSpinnerContent(Content);
					// Log.wtf("error1", Content);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}

	public ArrayList<HashMap<String, String>> createSpinnerNews(String content) {
		ArrayList<HashMap<String, String>> NEWSLIST = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			JSONArray m_jArry = obj.getJSONArray("response");
			// JSONArray json = new JSONArray(result);
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				Log.d("Details-->",
						jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String acq_time = jo_inside
						.getString(Constants.NEWS_ACQIRED_DATETIME);
				String type = jo_inside.getString(Constants.NEWS_TYPE);
				String category = jo_inside.getString(Constants.NEWS_CATEGORY);
				String pitcher = jo_inside.getString(Constants.NEWS_PICTURE);
				String title = jo_inside.getString(Constants.NEWS_TITLE);
				String description = jo_inside
						.getString(Constants.NEWS_DESCRIPTION);
				String url = jo_inside.getString(Constants.NEWS_URL);
				String NEWS_SOURCE = jo_inside.getString(Constants.NEWS_SOURCE);
				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.NEWS_ACQIRED_DATETIME,
						acq_time.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.NEWS_TYPE, type.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(Constants.NEWS_CATEGORY,
						category.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.NEWS_PICTURE, pitcher.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(Constants.NEWS_TITLE, title.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.NEWS_DESCRIPTION,
						description.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.NEWS_URL, url.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.NEWS_SOURCE,
						NEWS_SOURCE.replaceAll("null", "").replaceAll("NULL",
								""));

				NEWSLIST.add(m_li);

			}
			return NEWSLIST;
		} catch (Exception e) {
			// TODO: handle exception
			return NEWSLIST;
		}
	}

	public ArrayList<HashMap<String, String>> createSpinnerPeopleAround(
			String content) {

		ArrayList<HashMap<String, String>> PEOPLE_AROUND = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			JSONArray m_jArry = obj.getJSONArray("in");
			getContentPA(PEOPLE_AROUND, m_jArry);
			m_jArry = obj.getJSONArray("out");
			getContentPA(PEOPLE_AROUND, m_jArry);

			return PEOPLE_AROUND;
		} catch (Exception e) {
			// TODO: handle exception
			return PEOPLE_AROUND;
		}
	}

	public ArrayList<HashMap<String, String>> ExpertprofileSpinner(
			String content) {

		ArrayList<HashMap<String, String>> PEOPLE_AROUND = new ArrayList<HashMap<String, String>>();
		try {

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			JSONObject obj = new JSONObject(content);

			// JSONArray m_jArry1 = new JSONArray(content);
			// JSONObject jo_inside = m_jArry1.getJSONObject(0);
			String ary = obj.getString("response");

			JSONObject jo_inside = new JSONObject(ary);
			String ary1 = jo_inside.getString("username");
			// JSONArray m_jArry1 = new JSONArray(ary);
			// JSONArray m_jArry = new JSONArray(obj1);
			Log.e("m_jArry1", obj + "---" + ary1);

			// getContentPA(PEOPLE_AROUND, m_jArry);
			// JSONObject jo_inside = m_jArry.getJSONObject(i);
			// Log.d("Details-->",
			// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));

			String PEOPLEAROUND_ID = (jo_inside.has(Constants.PEOPLEAROUND_ID) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_ID);
			String PEOPLEAROUND_LETITUDE = (jo_inside
					.has(Constants.PEOPLEAROUND_LETITUDE) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_LETITUDE);
			String PEOPLEAROUND_LONGITUDE = (jo_inside
					.has(Constants.PEOPLEAROUND_LONGITUDE) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_LONGITUDE);
			String PEOPLEAROUND_HOMECOUNT = (jo_inside
					.has(Constants.PEOPLEAROUND_HOMECOUNT) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_HOMECOUNT);
			String PEOPLEAROUND_INTERESTCOUNT = (jo_inside
					.has(Constants.PEOPLEAROUND_INTERESTCOUNT) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_INTERESTCOUNT);
			String PEOPLEAROUND_IMAGE = (jo_inside
					.has(Constants.PEOPLEAROUND_IMAGE) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_IMAGE);
			String PEOPLEAROUND_FIRSTNAME = (jo_inside
					.has(Constants.PEOPLEAROUND_FIRSTNAME) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_FIRSTNAME);
			String PEOPLEAROUND_LASTNAME = (jo_inside
					.has(Constants.PEOPLEAROUND_LASTNAME) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_LASTNAME);
			String PEOPLEAROUND_EMAILYESNO = (jo_inside
					.has(Constants.PEOPLEAROUND_EMAILYESNO) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_EMAILYESNO);
			String PEOPLEAROUND_DOBYESNO = (jo_inside
					.has(Constants.PEOPLEAROUND_DOBYESNO) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_DOBYESNO);
			String PEOPLEAROUND_PHONENUMBERYESNO = (jo_inside
					.has(Constants.PEOPLEAROUND_PHONENUMBERYESNO) == false) ? ""
					: jo_inside
							.getString(Constants.PEOPLEAROUND_PHONENUMBERYESNO);
			String PEOPLEAROUND_EMAIL = (jo_inside
					.has(Constants.PEOPLEAROUND_EMAIL) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_EMAIL);
			String PEOPLEAROUND_DOB = (jo_inside
					.has(Constants.PEOPLEAROUND_DOB) == false) ? "" : jo_inside
					.getString(Constants.PEOPLEAROUND_DOB);
			String PEOPLEAROUND_PHONENUMBER = (jo_inside
					.has(Constants.PEOPLEAROUND_PHONENUMBER) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_PHONENUMBER);
			String PEOPLEAROUND_DISTANCE = (jo_inside
					.has(Constants.PEOPLEAROUND_DISTANCE) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_DISTANCE);
			String PEOPLEAROUND_AVATAR = (jo_inside
					.has(Constants.PEOPLEAROUND_AVATAR) == false) ? ""
					: jo_inside.getString(Constants.PEOPLEAROUND_AVATAR);
			// Add your values in your `ArrayList` as below:

			HashMap<String, String> m_li;
			m_li = new HashMap<String, String>();
			m_li.put(
					Constants.PEOPLEAROUND_ID,
					PEOPLEAROUND_ID.replaceAll("null", "").replaceAll("NULL",
							""));
			m_li.put(Constants.PEOPLEAROUND_LETITUDE, PEOPLEAROUND_LETITUDE
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_LONGITUDE, PEOPLEAROUND_LONGITUDE
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_HOMECOUNT, PEOPLEAROUND_HOMECOUNT
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_INTERESTCOUNT,
					PEOPLEAROUND_INTERESTCOUNT.replaceAll("null", "")
							.replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_IMAGE, PEOPLEAROUND_IMAGE
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_FIRSTNAME, PEOPLEAROUND_FIRSTNAME
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_LASTNAME, PEOPLEAROUND_LASTNAME
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_EMAILYESNO, PEOPLEAROUND_EMAILYESNO
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_DOBYESNO, PEOPLEAROUND_DOBYESNO
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_PHONENUMBERYESNO,
					PEOPLEAROUND_PHONENUMBERYESNO.replaceAll("null", "")
							.replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_EMAIL, PEOPLEAROUND_EMAIL
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(
					Constants.PEOPLEAROUND_DOB,
					PEOPLEAROUND_DOB.replaceAll("null", "").replaceAll("NULL",
							""));
			m_li.put(
					Constants.PEOPLEAROUND_PHONENUMBER,
					PEOPLEAROUND_PHONENUMBER.replaceAll("null", "").replaceAll(
							"NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_DISTANCE, PEOPLEAROUND_DISTANCE
					.replaceAll("null", "").replaceAll("NULL", ""));
			m_li.put(Constants.PEOPLEAROUND_AVATAR, PEOPLEAROUND_AVATAR
					.replaceAll("null", "").replaceAll("NULL", ""));

			PEOPLE_AROUND.add(m_li);
			return PEOPLE_AROUND;
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exception", e.toString() + "--");
			return PEOPLE_AROUND;
		}
	}

	public ArrayList<HashMap<String, String>> createPeopleAround_ListSpinner(
			String content, String filter) {

		ArrayList<HashMap<String, String>> PEOPLE_AROUND = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			JSONArray m_jArry = obj.getJSONArray(filter);
			Log.e("m_jArry", m_jArry + "--");
			getContentPA(PEOPLE_AROUND, m_jArry);

			return PEOPLE_AROUND;
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exception", e.toString() + "--");
			return PEOPLE_AROUND;
		}
	}

	public void getContentPA(ArrayList<HashMap<String, String>> PEOPLE_AROUND,
			JSONArray m_jArry) {
		try {

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));

				String PEOPLEAROUND_ID = (jo_inside
						.has(Constants.PEOPLEAROUND_ID) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_ID);
				String PEOPLEAROUND_LETITUDE = (jo_inside
						.has(Constants.PEOPLEAROUND_LETITUDE) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_LETITUDE);
				String PEOPLEAROUND_LONGITUDE = (jo_inside
						.has(Constants.PEOPLEAROUND_LONGITUDE) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_LONGITUDE);
				String PEOPLEAROUND_HOMECOUNT = (jo_inside
						.has(Constants.PEOPLEAROUND_HOMECOUNT) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_HOMECOUNT);
				String PEOPLEAROUND_INTERESTCOUNT = (jo_inside
						.has(Constants.PEOPLEAROUND_INTERESTCOUNT) == false) ? ""
						: jo_inside
								.getString(Constants.PEOPLEAROUND_INTERESTCOUNT);
				String PEOPLEAROUND_IMAGE = (jo_inside
						.has(Constants.PEOPLEAROUND_IMAGE) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_IMAGE);
				String PEOPLEAROUND_FIRSTNAME = (jo_inside
						.has(Constants.PEOPLEAROUND_FIRSTNAME) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_FIRSTNAME);
				String PEOPLEAROUND_LASTNAME = (jo_inside
						.has(Constants.PEOPLEAROUND_LASTNAME) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_LASTNAME);
				String PEOPLEAROUND_EMAILYESNO = (jo_inside
						.has(Constants.PEOPLEAROUND_EMAILYESNO) == false) ? ""
						: jo_inside
								.getString(Constants.PEOPLEAROUND_EMAILYESNO);
				String PEOPLEAROUND_DOBYESNO = (jo_inside
						.has(Constants.PEOPLEAROUND_DOBYESNO) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_DOBYESNO);
				String PEOPLEAROUND_PHONENUMBERYESNO = (jo_inside
						.has(Constants.PEOPLEAROUND_PHONENUMBERYESNO) == false) ? ""
						: jo_inside
								.getString(Constants.PEOPLEAROUND_PHONENUMBERYESNO);
				String PEOPLEAROUND_EMAIL = (jo_inside
						.has(Constants.PEOPLEAROUND_EMAIL) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_EMAIL);
				String PEOPLEAROUND_DOB = (jo_inside
						.has(Constants.PEOPLEAROUND_DOB) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_DOB);
				String PEOPLEAROUND_PHONENUMBER = (jo_inside
						.has(Constants.PEOPLEAROUND_PHONENUMBER) == false) ? ""
						: jo_inside
								.getString(Constants.PEOPLEAROUND_PHONENUMBER);
				String PEOPLEAROUND_DISTANCE = (jo_inside
						.has(Constants.PEOPLEAROUND_DISTANCE) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_DISTANCE);
				String PEOPLEAROUND_AVATAR = (jo_inside
						.has(Constants.PEOPLEAROUND_AVATAR) == false) ? ""
						: jo_inside.getString(Constants.PEOPLEAROUND_AVATAR);
				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(
						Constants.PEOPLEAROUND_ID,
						PEOPLEAROUND_ID.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_LETITUDE, PEOPLEAROUND_LETITUDE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_LONGITUDE,
						PEOPLEAROUND_LONGITUDE.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_HOMECOUNT,
						PEOPLEAROUND_HOMECOUNT.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_INTERESTCOUNT,
						PEOPLEAROUND_INTERESTCOUNT.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_IMAGE, PEOPLEAROUND_IMAGE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_FIRSTNAME,
						PEOPLEAROUND_FIRSTNAME.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_LASTNAME, PEOPLEAROUND_LASTNAME
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_EMAILYESNO,
						PEOPLEAROUND_EMAILYESNO.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_DOBYESNO, PEOPLEAROUND_DOBYESNO
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_PHONENUMBERYESNO,
						PEOPLEAROUND_PHONENUMBERYESNO.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_EMAIL, PEOPLEAROUND_EMAIL
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_DOB, PEOPLEAROUND_DOB
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_PHONENUMBER,
						PEOPLEAROUND_PHONENUMBER.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_DISTANCE, PEOPLEAROUND_DISTANCE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.PEOPLEAROUND_AVATAR, PEOPLEAROUND_AVATAR
						.replaceAll("null", "").replaceAll("NULL", ""));

				PEOPLE_AROUND.add(m_li);

			}
		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public ArrayList<HashMap<String, String>> createSpinnerPOI(String content) {
		ArrayList<HashMap<String, String>> POI = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			JSONArray m_jArry = obj.getJSONArray("response");
			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String POI_ID = jo_inside.getString(Constants.POI_ID);
				String POI_TYPE = jo_inside.getString(Constants.POI_TYPE);
				String POI_NAME = jo_inside.getString(Constants.POI_NAME);
				String POI_CATEGORY = jo_inside
						.getString(Constants.POI_CATEGORY);
				String POI_FORMATTEDADDRESS = jo_inside
						.getString(Constants.POI_FORMATTEDADDRESS);
				String POI_LATITUDE = jo_inside
						.getString(Constants.POI_LATITUDE);
				String POI_LONGITUDE = jo_inside
						.getString(Constants.POI_LONGITUDE);
				String POI_PHONE = jo_inside.getString(Constants.POI_PHONE);
				String POI_WEBSITE = jo_inside.getString(Constants.POI_WEBSITE);
				String POI_ICONNAME = jo_inside
						.getString(Constants.POI_ICONNAME);
				String POI_DISTANCE = jo_inside
						.getString(Constants.POI_DISTANCE);

				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.POI_ID, POI_ID.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(Constants.POI_TYPE, POI_TYPE.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(Constants.POI_NAME, POI_NAME.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.POI_CATEGORY,
						POI_CATEGORY.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.POI_FORMATTEDADDRESS, POI_FORMATTEDADDRESS
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.POI_LATITUDE,
						POI_LATITUDE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.POI_LONGITUDE,
						POI_LONGITUDE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.POI_PHONE, POI_PHONE.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.POI_WEBSITE,
						POI_WEBSITE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.POI_ICONNAME,
						POI_ICONNAME.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.POI_DISTANCE,
						POI_DISTANCE.replaceAll("null", "").replaceAll("NULL",
								""));

				POI.add(m_li);

			}
			return POI;
		} catch (Exception e) {
			// TODO: handle exception
			return POI;
		}
	}

	public ArrayList<HashMap<String, String>> createArryListforUserAllInfo(
			String content) {
		ArrayList<HashMap<String, String>> POI = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			JSONArray m_jArry = obj.getJSONArray("response");

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String USER_ID = jo_inside.getString(Constants.USER_ID);
				String USER_FIRSTNAME = jo_inside
						.getString(Constants.USER_FIRSTNAME);
				String USER_LASTNAME = jo_inside
						.getString(Constants.USER_LASTNAME);
				String USER_GENDER = jo_inside.getString(Constants.USER_GENDER);
				String USER_DATEOFBIRTH = jo_inside
						.getString(Constants.USER_DATEOFBIRTH);
				String USER_EMAIL = jo_inside.getString(Constants.USER_EMAIL);
				String USER_PHONENUMBER = jo_inside
						.getString(Constants.USER_PHONENUMBER);
				String USER_IMAGENAME = jo_inside
						.getString(Constants.USER_IMAGENAME);
				String USER_HOMECOUNT = jo_inside
						.getString(Constants.USER_HOMECOUNT);
				String USER_INTERESTCOUNT = jo_inside
						.getString(Constants.USER_INTERESTCOUNT);
				String USER_LANGUAGE = jo_inside
						.getString(Constants.USER_LANGUAGE);
				String USER_DESCRIPTION = jo_inside
						.getString(Constants.USER_DESCRIPTION);
				String USER_WEBSITE = jo_inside
						.getString(Constants.USER_WEBSITE);
				String USER_STUDY = jo_inside.getString(Constants.USER_STUDY);
				String USER_BUS = jo_inside.getString(Constants.USER_BUS);
				String USER_TRAVAL = jo_inside.getString(Constants.USER_TRAVAL);
				String USER_SOCCULT = jo_inside
						.getString(Constants.USER_SOCCULT);
				String USER_COMPANY = jo_inside
						.getString(Constants.USER_COMPANY);
				String USER_JOBPOSITION = jo_inside
						.getString(Constants.USER_JOBPOSITION);
				String USER_COMPANYWEBSITE = jo_inside
						.getString(Constants.USER_COMPANYWEBSITE);
				String USER_SCHOOL = jo_inside.getString(Constants.USER_SCHOOL);
				String USER_DEGREE = jo_inside.getString(Constants.USER_DEGREE);
				String USER_SCHOOLURL = jo_inside
						.getString(Constants.USER_SCHOOLURL);
				String USER_LATITUDE = jo_inside
						.getString(Constants.USER_LATITUDE);
				String USER_LONGITUDE = jo_inside
						.getString(Constants.USER_LONGITUDE);
				String USER_DATEOFBIRTH_YESNO = jo_inside
						.getString(Constants.USER_DATEOFBIRTH_YESNO);
				String USER_PHONENUMBER_YESNO = jo_inside
						.getString(Constants.USER_PHONENUMBER_YESNO);
				String USER_EMAIL_YESNO = jo_inside
						.getString(Constants.USER_EMAIL_YESNO);
				String USER_AVATAR = jo_inside.getString(Constants.USER_AVATAR);
				String USER_EXPERT = jo_inside.getString(Constants.USER_EXPERT);
				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.USER_ID, USER_ID.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_FIRSTNAME,
						USER_FIRSTNAME.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(
						Constants.USER_LASTNAME,
						USER_LASTNAME.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_GENDER,
						USER_GENDER.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.USER_DATEOFBIRTH, USER_DATEOFBIRTH
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.USER_EMAIL, USER_EMAIL
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.USER_PHONENUMBER, USER_PHONENUMBER
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_IMAGENAME,
						USER_IMAGENAME.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(
						Constants.USER_HOMECOUNT,
						USER_HOMECOUNT.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(Constants.USER_INTERESTCOUNT, USER_INTERESTCOUNT
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_LANGUAGE,
						USER_LANGUAGE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.USER_DESCRIPTION, USER_DESCRIPTION
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_WEBSITE,
						USER_WEBSITE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.USER_STUDY, USER_STUDY
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.USER_BUS, USER_BUS.replaceAll("null", "")
						.replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_TRAVAL,
						USER_TRAVAL.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_SOCCULT,
						USER_SOCCULT.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_COMPANY,
						USER_COMPANY.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(Constants.USER_JOBPOSITION, USER_JOBPOSITION
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.USER_COMPANYWEBSITE, USER_COMPANYWEBSITE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_SCHOOL,
						USER_SCHOOL.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_DEGREE,
						USER_DEGREE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_SCHOOLURL,
						USER_SCHOOLURL.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(
						Constants.USER_LATITUDE,
						USER_LATITUDE.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_LONGITUDE,
						USER_LONGITUDE.replaceAll("null", "").replaceAll(
								"NULL", ""));
				m_li.put(Constants.USER_DATEOFBIRTH_YESNO,
						USER_DATEOFBIRTH_YESNO.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.USER_PHONENUMBER_YESNO,
						USER_PHONENUMBER_YESNO.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.USER_EMAIL_YESNO, USER_EMAIL_YESNO
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(
						Constants.USER_AVATAR,
						USER_AVATAR.replaceAll("null", "").replaceAll("NULL",
								""));
				m_li.put(
						Constants.USER_EXPERT,
						USER_EXPERT.replaceAll("null", "").replaceAll("NULL",
								""));

				POI.add(m_li);

			}
			return POI;
		} catch (Exception e) {
			// TODO: handle exception
			return POI;
		}
	}

	public ArrayList<HashMap<String, String>> createWeatherSpinner(
			String content) {
		ArrayList<HashMap<String, String>> POI = new ArrayList<HashMap<String, String>>();
		try {

			JSONObject obj = new JSONObject(content);
			JSONArray m_jArry = obj.getJSONArray("response");
			// if(obj.get("response").toString()
			// .equalsIgnoreCase("no auth")){
			// new SessionManager(context_).logoutUser();
			// }

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));

				String WEATHER_HOMECOUNT = (jo_inside
						.has(Constants.WEATHER_HOMECOUNT) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMECOUNT);
				String WEATHER_HOMETEMP = (jo_inside
						.has(Constants.WEATHER_HOMETEMP) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMETEMP);
				String WEATHER_HOMECODE = (jo_inside
						.has(Constants.WEATHER_HOMECODE) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMECODE);
				String WEATHER_HOMETEXT = (jo_inside
						.has(Constants.WEATHER_HOMETEXT) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMETEXT);
				String WEATHER_HOMECURRENCY = (jo_inside
						.has(Constants.WEATHER_HOMECURRENCY) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMECURRENCY);
				String WEATHER_INTERESTCOUNT = (jo_inside
						.has(Constants.WEATHER_INTERESTCOUNT) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_INTERESTCOUNT);
				String WEATHER_INTERESTTEMP = (jo_inside
						.has(Constants.WEATHER_INTERESTTEMP) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_INTERESTTEMP);
				String WEATHER_INTERESTCODE = (jo_inside
						.has(Constants.WEATHER_INTERESTCODE) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_INTERESTCODE);
				String WEATHER_INTERESTTEXT = (jo_inside
						.has(Constants.WEATHER_INTERESTTEXT) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_INTERESTTEXT);
				String WEATHER_INTERESTCURRENCY = (jo_inside
						.has(Constants.WEATHER_INTERESTCURRENCY) == false) ? ""
						: jo_inside
								.getString(Constants.WEATHER_INTERESTCURRENCY);
				String WEATHER_HOMECITY = (jo_inside
						.has(Constants.WEATHER_HOMECITY) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_HOMECITY);
				String WEATHER_INTERESTCITY = (jo_inside
						.has(Constants.WEATHER_INTERESTCITY) == false) ? ""
						: jo_inside.getString(Constants.WEATHER_INTERESTCITY);

				// Add your values in your `ArrayList` as below:

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.WEATHER_HOMECOUNT, WEATHER_HOMECOUNT
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_HOMETEMP, WEATHER_HOMETEMP
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_HOMECODE, WEATHER_HOMECODE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_HOMETEXT, WEATHER_HOMETEXT
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_HOMECURRENCY, WEATHER_HOMECURRENCY
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTCOUNT, WEATHER_INTERESTCOUNT
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTTEMP, WEATHER_INTERESTTEMP
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTCODE, WEATHER_INTERESTCODE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTTEXT, WEATHER_INTERESTTEXT
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTCURRENCY,
						WEATHER_INTERESTCURRENCY.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_HOMECITY, WEATHER_HOMECITY
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.WEATHER_INTERESTCITY, WEATHER_INTERESTCITY
						.replaceAll("null", "").replaceAll("NULL", ""));
				POI.add(m_li);

			}
			return POI;
		} catch (Exception e) {
			// TODO: handle exception
			return POI;
		}
	}

	public ArrayList<HashMap<String, String>> CreateSpinnerCountires(
			String josnResult) {
		ArrayList<HashMap<String, String>> CONT_LIST = new ArrayList<HashMap<String, String>>();

		try {

			JSONArray json = new JSONArray(josnResult);

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < json.length(); i++) {
				JSONObject jo_inside = json.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String COUNTRYLSIT_NAME = jo_inside
						.getString(Constants.COUNTRYLSIT_NAME);
				String COUNTRYLSIT_DIALCODE = jo_inside
						.getString(Constants.COUNTRYLSIT_DIALCODE);
				String COUNTRYLSIT_CODE = jo_inside
						.getString(Constants.COUNTRYLSIT_CODE);

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.COUNTRYLSIT_NAME, COUNTRYLSIT_NAME
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSIT_DIALCODE, COUNTRYLSIT_DIALCODE
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSIT_CODE, COUNTRYLSIT_CODE
						.replaceAll("null", "").replaceAll("NULL", ""));
				CONT_LIST.add(m_li);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return CONT_LIST;
	}

	public ArrayList<HashMap<String, String>> CountryFile(String josnResult) {
		Constants.COUNTRYLSITFILE_COUNTRYLIST = new ArrayList<HashMap<String, String>>();

		try {

			// JSONArray json = new JSONArray(josnResult);

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();
			JSONObject obj = new JSONObject(josnResult);
			JSONArray m_jArry = obj.getJSONArray("countries");

			for (int i = 0; i < m_jArry.length(); i++) {
				JSONObject jo_inside = m_jArry.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String COUNTRYLSITFILE_COUNTRYCODE = jo_inside
						.getString(Constants.COUNTRYLSITFILE_COUNTRYCODE);
				String COUNTRYLSITFILE_ID = jo_inside
						.getString(Constants.COUNTRYLSITFILE_ID);
				String COUNTRYLSITFILE_RELATED = jo_inside
						.getString(Constants.COUNTRYLSITFILE_RELATED);
				String COUNTRYLSITFILE_COUNTRYNAME = jo_inside
						.getString(Constants.COUNTRYLSITFILE_COUNTRYNAME);
				String COUNTRYLSITFILE_CURRENCYCODE = jo_inside
						.getString(Constants.COUNTRYLSITFILE_CURRENCYCODE);
				String COUNTRYLSITFILE_CURRENCYNAME = jo_inside
						.getString(Constants.COUNTRYLSITFILE_CURRENCYNAME);
				String COUNTRYLSITFILE_LATITUDE = jo_inside
						.getString(Constants.COUNTRYLSITFILE_LATITUDE);
				String COUNTRYLSITFILE_LONGITUDE = jo_inside
						.getString(Constants.COUNTRYLSITFILE_LONGITUDE);
				String COUNTRYLSITFILE_WOEID = jo_inside
						.getString(Constants.COUNTRYLSITFILE_WOEID);
				String COUNTRYLSITFILE_CAPITAL = jo_inside
						.getString(Constants.COUNTRYLSITFILE_CAPITAL);
				String COUNTRYLSITFILE_LANGUAGE = jo_inside
						.getString(Constants.COUNTRYLSITFILE_LANGUAGE);

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.COUNTRYLSITFILE_COUNTRYCODE,
						COUNTRYLSITFILE_COUNTRYCODE.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_ID, COUNTRYLSITFILE_ID
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_RELATED,
						COUNTRYLSITFILE_RELATED.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_COUNTRYNAME,
						COUNTRYLSITFILE_COUNTRYNAME.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_CURRENCYCODE,
						COUNTRYLSITFILE_CURRENCYCODE.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_CURRENCYNAME,
						COUNTRYLSITFILE_CURRENCYNAME.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_LATITUDE,
						COUNTRYLSITFILE_LATITUDE.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_LONGITUDE,
						COUNTRYLSITFILE_LONGITUDE.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_WOEID, COUNTRYLSITFILE_WOEID
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_CAPITAL,
						COUNTRYLSITFILE_CAPITAL.replaceAll("null", "")
								.replaceAll("NULL", ""));
				m_li.put(Constants.COUNTRYLSITFILE_LANGUAGE,
						COUNTRYLSITFILE_LANGUAGE.replaceAll("null", "")
								.replaceAll("NULL", ""));

				Constants.COUNTRYLSITFILE_COUNTRYLIST.add(m_li);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Constants.COUNTRYLSITFILE_COUNTRYLIST;
	}

	public ArrayList<HashMap<String, String>> LanguageList(String josnResult) {
		Constants.LANGUAGEFILE_COUNTRYLIST = new ArrayList<HashMap<String, String>>();

		try {

			JSONArray json = new JSONArray(josnResult);

			// ArrayList<HashMap<String, String>> formList = new
			// ArrayList<HashMap<String, String>>();

			for (int i = 0; i < json.length(); i++) {
				JSONObject jo_inside = json.getJSONObject(i);
				// Log.d("Details-->",
				// jo_inside.getString(Constants.NEWS_ACQIRED_DATETIME));
				String LANGUAGEFILE_LANGNAME = jo_inside
						.getString(Constants.LANGUAGEFILE_LANGNAME);
				String LANGUAGEFILE_LANGCODE = jo_inside
						.getString(Constants.LANGUAGEFILE_LANGCODE);

				HashMap<String, String> m_li;
				m_li = new HashMap<String, String>();
				m_li.put(Constants.LANGUAGEFILE_LANGNAME, LANGUAGEFILE_LANGNAME
						.replaceAll("null", "").replaceAll("NULL", ""));
				m_li.put(Constants.LANGUAGEFILE_LANGCODE, LANGUAGEFILE_LANGCODE
						.replaceAll("null", "").replaceAll("NULL", ""));

				Constants.LANGUAGEFILE_COUNTRYLIST.add(m_li);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Constants.LANGUAGEFILE_COUNTRYLIST;
	}

	public String[] getContFileListContent(String object,
			ArrayList<HashMap<String, String>> ArraylistContent) {
		String FileListContent[] = new String[ArraylistContent.size()];
		for (int i = 0; i < ArraylistContent.size(); i++) {
			FileListContent[i] = ArraylistContent.get(i).get(object).toString();
		}
		return FileListContent;
	}

	public void filesLoad() {
		Constants.COUNTRYLSITFILE_COUNTRYLIST = CountryFile(new JsonLocalFileFetch(
				context_).loadJSONFromAsset("json/CountryList.json"));
		Constants.LANGUAGEFILE_COUNTRYLIST = LanguageList(new JsonLocalFileFetch(
				context_).loadJSONFromAsset("json/Language.json"));

		Constants.ARRAY_COUNTRY_NICNAME = getContFileListContent(
				Constants.COUNTRYLSITFILE_COUNTRYCODE,
				Constants.COUNTRYLSITFILE_COUNTRYLIST);
		Constants.ARRAY_COUNTRY_COMPLETENAME = getContFileListContent(
				Constants.COUNTRYLSITFILE_COUNTRYNAME,
				Constants.COUNTRYLSITFILE_COUNTRYLIST);
		Constants.ARRAY_COUNTRY_CENTRE = getContFileListContent(
				Constants.COUNTRYLSITFILE_CAPITAL,
				Constants.COUNTRYLSITFILE_COUNTRYLIST);
		Constants.ARRAY_COUNTRY_CURRENCYCODE = getContFileListContent(
				Constants.COUNTRYLSITFILE_CURRENCYCODE,
				Constants.COUNTRYLSITFILE_COUNTRYLIST);

		Constants.ARRAY_COUNTRY_LANGUAGE_NICNAME = getContFileListContent(
				Constants.LANGUAGEFILE_LANGCODE,
				Constants.LANGUAGEFILE_COUNTRYLIST);
		Constants.ARRAY_COUNTRY_LANGUAGE = getContFileListContent(
				Constants.LANGUAGEFILE_LANGNAME,
				Constants.LANGUAGEFILE_COUNTRYLIST);
	}

	public boolean CheckExpirationTocken(Context activity_, String content) {

		JSONObject obj;
		try {
			obj = new JSONObject(content);

			// Log.e("obj.get(response).toString()", obj.get("response")
			// .toString() + "---");
			if (obj.get("response").toString().equalsIgnoreCase("no auth")) {
				new SessionManager(activity_).logoutUser();
				Toast.makeText(
						activity_,
						context_.getResources().getString(
								R.string.Alert_AuthenticaionTocken)
								+ "", Toast.LENGTH_LONG).show();
				return true;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
