package com.connexun.interfaces;

import android.view.View;

public interface DataListners {
	public void GetNewsListners(String NEWS,View Loadingview);
	public void GetPeopleAroundListners(String PeopleAround,View Loadingview);
	public void GetPointOfIntrestListners(String PointOfInterest,View Loadingview);
	public void GetUserAllInfo(String GetAllInfo,View Loadingview);
	public void GetWeatherDetail(String GetWeatherDetail,View Loadingview);
}
